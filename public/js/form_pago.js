var base_url = $('#base_url').val();

function cambiaCP2(){
    var cp = $("#codigo_postal").val();  
    if(cp!=""){
        $.ajax({
            url: base_url+'Pago/getDatosCP',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                //$(".fis_estado_"+id).val(array[0].estado);
                //$(".fis_ciudad_"+id).val(array[0].ciudad);
                cambia_cp_colonia2();
            }
        });

    }
}

function cambia_cp_colonia2(){
    var cp = $("#codigo_postal").val();
    $('.colonia').empty();
    if(cp!=""){
        $.ajax({
            url: base_url+'Pago/getDatosCP_colonia',
            type: 'POST',
            data: { cp: cp, col: "0" },
            success: function(data){ 
                var array = $.parseJSON(data);
                if(array!=''){
                    $.ajax({
                        url: base_url+'Pago/getDatosCPSelect',
                        dataType: "json",
                        data: { cp: cp, col: "0" },
                        success: function(data){ 
                            data.forEach(function(element){
                                $('.colonia').prepend("<option value='"+element.colonia+"' >"+element.colonia+"</option>");
                            });
                        }
                     });
                }
            }
        });
    }else{
        //$(".fis_estado_"+id).attr("readonly",false);
    }
}

function registrar_pago(){
    var form_register = $('#form_data_pago');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            cliente:{
              required: true
            },
            calle:{
              required: true
            },
            cp:{
              required: true
            },
            colonia:{
              required: true
            },
            telefono:{
              required: true,
            },
            instrucciones:{
              required: true,
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_data_pago").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_registro').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Pago/add_pago_detalles',
            data: datos,
            statusCode:{
                404: function(data){
                    alert("No Se encuentra el archivo!");
                },
                500: function(){
                    alert("500");
                }
            },
            success:function(data){
                reg=parseFloat(data);
                $('#idpago').val(reg);
                swal({
                    title: "¡Éxito!",
                    text: "Información guardada correctamente.",
                    type: "success",
                    timer: 4000,
                    showConfirmButton: false
                });
                $('.paso_2_act').html('<span class="btn_pasos_act">&nbsp;<b>2</b>&nbsp;</span></span>');
                $('.btn_paso2_txt').html('<span style="width: 100%; position: absolute; padding: 2px;"><span class="paso_2_btn1"><a onclick="btn_paso_2_ocultar()" class="btn_flecha" ><i class="fa fa-chevron-up"></i></a></span><span class="paso_2_btn2" style="display: none;"><a onclick="btn_paso_2_ocultar2()" class="btn_flecha"><i class="fa fa-chevron-down"></i></a></span></span>');
                setTimeout(function(){ 
                    $('.btn_registro').attr('disabled',false);
                    btn_paso_1_ocultar();
                    btn_paso_2_ocultar();
                }, 1000);
            }
        });
    }   
}

function btn_paso_1_ocultar(){
    $('.paso_1_btn1').css('display','none');
    $('.paso_1_btn2').css('display','block');
}

function btn_paso_1_ocultar2(){
    $('.paso_1_btn1').css('display','block');
    $('.paso_1_btn2').css('display','none');
}

function btn_paso_2_ocultar(){
    $('.paso_2_btn1').css('display','none');
    $('.paso_2_btn2').css('display','block');
}

function btn_paso_2_ocultar2(){
    $('.paso_2_btn1').css('display','block');
    $('.paso_2_btn2').css('display','none');
}

function opcion_paso2(num){
    if(num==1){
        $('.img_paso_2_1').html('<img style="width: 45px;" src="'+base_url+'public/img/comprobado.png">');
        $('.img_paso_2_2').html('');
        html='<div id="paypal-button-container"></div>';
        $('.addformpago').html(html);
        activapaypay();
    }else{
        $('.img_paso_2_2').html('<img style="width: 45px;" src="'+base_url+'public/img/comprobado.png">');          
        $('.img_paso_2_1').html('');
    }
}
function activapaypayxxxxxxxxx(){
    paypal.Buttons({
        // Order is created on the server and the order id is returned
        createOrder() {
          return fetch('./Pago/paypalorder', {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            // use the "body" param to optionally pass additional order information
            // like product skus and quantities
            body: JSON.stringify({
              cart: [
                {
                  sku: "YOUR_PRODUCT_STOCK_KEEPING_UNIT",
                  quantity: "YOUR_PRODUCT_QUANTITY",
                },
              ],
            }),
          })
          .then((response) => response.json())
          .then((order) => order.id);
        },
        // Finalize the transaction on the server after payer approval
        onApprove(data) {
          return fetch(base_url+"Pago/capture_paypal_order", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              orderID: data.orderID
            })
          })
          .then((response) => response.json())
          .then((orderData) => {
            // Successful capture! For dev/demo purposes:
            console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));
            const transaction = orderData.purchase_units[0].payments.captures[0];
            alert(`Transaction ${transaction.status}: ${transaction.id}\n\nSee console for all available details`);
            // When ready to go live, remove the alert and show a success message within this page. For example:
            // const element = document.getElementById('paypal-button-container');
            // element.innerHTML = '<h3>Thank you for your payment!</h3>';
            // Or go to another URL:  window.location.href = 'thank_you.html';
          });
        }
      }).render('#paypal-button-container');
}
function activapaypay(){
    paypal.Buttons({
        // Order is created on the server and the order id is returned
        createOrder() {
          return fetch('./Pago/paypalorder', {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            // use the "body" param to optionally pass additional order information
            // like product skus and quantities
            body: JSON.stringify({
              cart: [
                {
                  sku: "YOUR_PRODUCT_STOCK_KEEPING_UNIT",
                  quantity: "YOUR_PRODUCT_QUANTITY",
                },
              ],
            }),
          })
          .then((response) => response.json())
          .then((order) => order.id);
        },
        // Finalize the transaction on the server after payer approval
        onApprove: function(data, actions) {
            // Captura la transacción una vez que el usuario aprueba el pago
            return actions.order.capture().then(function(details) {
                guardarcompra(details);
                // Puedes mostrar un mensaje de agradecimiento o redirigir a una página de confirmación
                //alert('Pago completado. ID de transacción: ' + details.id);
                swal({
                    title: "¡Éxito!",
                    text: "Pago completado. Fecha: "+details.create_time+" ID de transacción:"+details.id,
                    type: "success",
                    showConfirmButton: true
                });
                $('.text_total').html(0);
                enviar_correo(details.id);
                setTimeout(function(){ 
                    window.location = base_url+'Micuenta/mispedidos';        
                }, 4000);
            });
        },
        onError: function(err) {
            // Maneja los errores de pago
            console.error('Error en el pago:', err);
            alert('Error en el pago ');
        }
      }).render('#paypal-button-container');
}
function guardarcompra(details){
    $.ajax({
        type:'POST',
        url: base_url+'Pago/guardarcompra',
        data: details,
        statusCode:{
            404: function(data){
                alert("No Se encuentra el archivo!");
            },
            500: function(){
                alert("500");
            }
        },
        success:function(data){
            
        }
    });
}


function enviar_correo(id){
    $.ajax({
        type:'POST',
        url: base_url+'Pago/enviar_pago',
        data: {
            id:id
        },
        statusCode:{
            404: function(data){
                alert("No Se encuentra el archivo!");
            },
            500: function(){
                alert("500");
            }
        },
        success:function(data){
            
        }
    });
}