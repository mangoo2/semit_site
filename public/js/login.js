var base_url = $('#base_url').val();
var validar_correo=0;
var validar_user=0;
var reg=0;
$(document).ready(function() {
    $("#usuario").focus();
    $("#password").keypress(function(e) {
        if (e.which==13) {
            iniciar_sesion();
        }
    });

    $("#contrasena2").keypress(function(e) {
        if (e.which==13) {
            registrar_usuario();
        }
    });
});
function iniciar_sesion(){
    $.ajax({
	    type: "POST",
	    url: base_url+"index.php/Inicio/login_data",
	    data: $('#sign_in').serialize(), 
	    beforeSend: function(){

	    },
	    success: function (result) {
	        console.log(result);
	        setTimeout(function () { $('#loader').fadeOut(); }, 50);
	        if(result==1){
	            $('.alert1').css('display','block'); 
	            $('.alert2').css('display','none'); 
	            $('.btn_inicio').html('');
	            $('.text-uppercase').html('Iniciando sesión ...');
	            setTimeout(function(){window.location.href = base_url+"Inicio"},3500);
	        }else if(result==2){
                swal({
                    title: "¡Atención!",
                    text: "Estimado cliente, su usuario se encuentra bloqueado. Por favor, comuníquese con nosotros para resolver su situación.",
                    showConfirmButton: false,
                    timer: 7000,
                });
	        }else{
	            $('.btn_inicio').html('<br><div style="text-align: center; background: red; color: white; padding: 6px;    border-radius: 11px;">Usuario o contraseña incorrectos</div>');
	        }  
	    }
	});
}

function registrate_aqui(){
    setTimeout(function(){ 
        $(".nombre_cliente").focus();
    }, 1000);
    $('.login_1').css('display','none');
    $('.login_2').css('display','block');
    $('.login_3').css('display','none');
    $('.login_4').css('display','none');
}

function mostrarContrasena(){
      var tipo = document.getElementById("contrasena");
      if(tipo.type == "password"){
          tipo.type = "text";
      }else{
          tipo.type = "password";
      }
}

function verificar_correo(){
    $.ajax({
        type: 'POST',
        url: base_url+'Inicio/validar_correo',
        data: {
            correo:$('#correo').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                alert("Error!"+"No Se encuentra el archivo!"+"error");
            },
            500: function(){
                alet("Error!"+"500"+"error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_correo=1;
                $('.validar_correo').html("El correo ya existe");
                $('.registro_data').css('cursor','not-allowed');
            }else{
            	$('.validar_correo').html("");
                $('.registro_data').css('cursor','pointer');
                validar_correo=0;
            }
        }
    });
}


function verificar_usuario(){
    $.ajax({
        type: 'POST',
        url: base_url+'Inicio/validar',
        data: {
            usuario:$('#usuario_reg').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                alert("Error!"+"No Se encuentra el archivo!"+"error");
            },
            500: function(){
                alet("Error!"+"500"+"error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_user=1;
                $('.validar_user').html("El usuario ya existe");
                $('.registro_data').css('cursor','not-allowed');
            }else{
                $('.validar_user').html("");
                $('.registro_data').css('cursor','pointer');
                validar_user=0;
            }
        }
    });
}

function verificar_celular(){
    $.ajax({
        type: 'POST',
        url: base_url+'Inicio/validar_celular',
        data: {
            celular:$('#celular').val()
        },
        async: false,
        statusCode: {
            404: function(data){
                alert("Error!"+"No Se encuentra el archivo!"+"error");
            },
            500: function(){
                alet("Error!"+"500"+"error");
            }
        },
        success: function(data) {
            if (data == 1) {
                validar_user=1;
                $('.validar_celular').html("El número de celular ya existe");
            }else{
                $('.validar_celular').html("");
                validar_user=0;
            }
        }
    });
}

function registrar_usuario(){
    var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
        	correo:{
              required: true
            },
            usuario:{
              required: true
            },
            nombre:{
              required: true
            },
            celular:{
              required: true,
            },
            contrasena:{
              required: true,
              minlength: 5
            },
            contrasena2: {
                equalTo: contrasena,
                required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_data").valid();
    if($valid) {
        if(validar_user==0){
            if(validar_correo==0){
                var datos = form_register.serialize();
                $('.btn_registro').attr('disabled',true);
                $('.registro_data').html("Guardando datos");
                swal({
                    title: "¡Atención!",
                    text: "Se esta enviando un código a tu correo para validar",
                    showConfirmButton: false,
                    timer: 5000,
                });
                $.ajax({
                    type:'POST',
                    url: base_url+'Inicio/add_usuarios',
                    data: datos,
                    statusCode:{
                        404: function(data){
                            alert("No Se encuentra el archivo!");
                        },
                        500: function(){
                            alert("500");
                        }
                    },
                    success:function(data){
                    	reg=parseFloat(data);
                        swal({
                            title: "¡Éxito!",
                            text: "Código enviado correctamente.",
                            type: "success",
                            timer: 4000,
                            showConfirmButton: false
                        });
                        setTimeout(function(){ 
                            //window.location = base_url+'Inicio';
                            $('.login_1').css('display','none');
    					    $('.login_2').css('display','none');
    					    $('.login_3').css('display','block');
                            setTimeout(function(){ 
                                $("#codigo_t1").focus();
                            }, 1000);
                        }, 1000);
                    }
                });
            }else{
                $('.validar_correo').html("El correo ya existe");
            }
        }else{
            $('.validar_correo').html("El usuario ya existe");
        }
    }   
}


function validar_usuario(){
    var t1 = $('#codigo_t1').val();
    var t2 = $('#codigo_t2').val();
    var t3 = $('#codigo_t3').val();
    var t4 = $('#codigo_t4').val();
    var t5 = $('#codigo_t5').val();
    var n1=0;
    var n2=0; 
    var n3=0;
    var n4=0;
    var n5=0;
    if(t1==''){
        $('#codigo_t1').css('border-color','red');
        n1=0;
    }else{
        $('#codigo_t1').css('border-color','#012d6a');
        n1=1;
    }

    if(t2==''){
        $('#codigo_t2').css('border-color','red');
        n2=0;
    }else{
        $('#codigo_t2').css('border-color','#012d6a');
        n2=1;
    }

    if(t3==''){
        $('#codigo_t3').css('border-color','red');
        n3=0;
    }else{
        $('#codigo_t3').css('border-color','#012d6a');
        n3=1;
    }

    if(t4==''){
        $('#codigo_t4').css('border-color','red');
        n4=0;
    }else{
        $('#codigo_t4').css('border-color','#012d6a');
        n4=1;
    }

    if(t5==''){
        $('#codigo_t5').css('border-color','red');
        n5=0;
    }else{
        $('#codigo_t5').css('border-color','#012d6a');
        n5=1;
    }
    var suma = t1+t2+t3+t4+t5;

    if(n1==1 && n2==1 && n3==1 && n4==1 && n5==1){
        $('.btn_registro_valida').attr('disabled',true);
        $.ajax({
            type:'POST',
            url: base_url+'Inicio/validar_usuario',
            data: {reg:reg,codigo:suma},
            statusCode:{
                404: function(data){
                    alert("No Se encuentra el archivo!");
                },
                500: function(){
                    alert("500");
                }
            },
            success:function(data){
                var validarc = parseFloat(data);
                if(validarc==0){
                    //$('.validar_codigo').html('El código es incorrecto');
                    swal("¡Atención!","El código es incorrecto","error");
                    $('.btn_registro_valida').attr('disabled',false);
                }else{
                    //$('.validar_codigo').html('');
                    //$('.btn_registro_valida').html("Código correcto");
                    swal({
                        title: "¡Éxito!",
                        text: "Código correcto iniciando sesión",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false
                    });
                    iniciar_sesion_nuevo();
                    //setTimeout(function(){ 
                      //  window.location = base_url+'Inicio';
                    // }, 1000);
                }
                
            }
        });
    }
	
}

function iniciar_sesion_nuevo(){
    $.ajax({
        type: "POST",
        url: base_url+"index.php/Inicio/login_data",
        data: {usuario:$('#correo').val(),
            password:$('#contrasena').val()}, 
        beforeSend: function(){

        },
        success: function (result) {
            setTimeout(function(){window.location.href = base_url+"Inicio"},3500);
        }
    });
}


function olvidaste_contrasena(){
    $('.login_1').css('display','none');
    $('.login_2').css('display','none');
    $('.login_3').css('display','none');
    $('.login_4').css('display','block');
}

function editar_contrasena(){
    var form_register = $('#form_data_validar');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            correo:{
              required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_data_validar").valid();
    if($valid) {
        var datos = form_register.serialize();
        $('.btn_registro_valida').attr('disabled',true);
        swal({
            title: "¡Atención!",
            text: "Se esta enviando un código a tu correo para validar por favor espere",
            showConfirmButton: false
        });
        $.ajax({
            type:'POST',
            url: base_url+'Inicio/enviar_correo',
            data: datos,
            statusCode:{
                404: function(data){
                    alert("No Se encuentra el archivo!");
                },
                500: function(){
                    alert("500");
                }
            },
            success:function(data){
                reg=parseFloat(data);
                if(reg==0){
                    swal("¡Atención!","El correo es incorrecto","error");
                    $('.btn_registro_valida').attr('disabled',false);
                }else{
                    swal({
                        title: "¡Éxito!",
                        text: "Se te ha enviado un link tu correo",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });
                    setTimeout(function(){ 
                        //window.location = base_url+'Inicio';
                    }, 1000);
                }
            }
        });
    }   
}