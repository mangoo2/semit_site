var base_url = $('#base_url').val();
$(document).ready(function() {
	/*productos_all();*/
	get_total_compras();
});

/*function productos_all(){

	$.ajax({
	    type: "POST",
	    url: base_url+"/Importardatos/get_productos",
	    beforeSend: function(){

	    },
	    success: function (result) {
	    }
	});
}*/

function get_productos(){
    $('.fondo_trans').addClass('fondo_transparente');
	$.ajax({
	    type: "POST",
	    url: base_url+"Inicio/get_productos_compras", 
	    beforeSend: function(){

	    },
	    success: function (result){
	    	$('.text_productos').html(result);
            
	    }
	});
}

function quitar_producto(id){
	$.ajax({
	    type: "POST",
	    url: base_url+"Inicio/quitarproducto", 
	    data:{id:id},
	    beforeSend: function(){

	    },
	    success: function (result){
	    	get_productos();
	    	get_total_compras();
	    }
	});
}

function get_total_compras(){
	$.ajax({
	    type: "POST",
	    url: base_url+"Inicio/get_total_compras", 
	    beforeSend: function(){
	    },
	    success: function (result){
	    	$('.text_total').html(result);
	    }
	});
	
}


function sucursales(){
	cont=1;
	setInterval(function() {
		$('.sucu').hide();
		$('.suc'+cont).show();
		if(cont==4){
           cont=0;
		}
		cont++;
	}, 3000);
}


var base_url = $('#base_url').val();

function registrar_requerimientos(){
    var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            },
            correo:{
              required: true
            },
            productos:{
              required: true,
            },
            comentarios:{
              required: true,
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_data").valid();
    if($valid) {
            var datos = form_register.serialize();
            $('.registro_data').attr('disabled',true);
            /*swal({
            title: "¡Atención!",
            text: "Se esta guardando la información",
            showConfirmButton: false
            });*/
            $.ajax({
                type:'POST',
                url: base_url+'Servicios/registro_requerimiento',
                data: datos,
                statusCode:{
                    404: function(data){
                        alert("No Se encuentra el archivo!");
                    },
                    500: function(){
                        alert("500");
                    }
                },
                success:function(data){
                    swal({
                        title: "Formulario enviado con éxito.",
                        text: "Uno de nuestros ejecutivos te contactará a la brevedad para continuar con tu solicitud.",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });
                    setTimeout(function(){ 
                        $('#nombre').val('');
                        $('#correo').val('');
                        $('#comentarios').val('');
                        $('.vd_red').remove();
                        $('.registro_data').attr('disabled',false);
                        $('#formModal').modal('hide')
                    }, 1000);
                }
            });
    }   
}


function imagen_pro(id){
    $.ajax({
        type:'POST',
        url: base_url+'Productos/imagen_pro',
        data: {id:id},
        statusCode:{
            404: function(data){
                alert("No Se encuentra el archivo!");
            },
            500: function(){
                alert("500");
            }
        },
        success:function(data){
            $('.img_file').html(data);
        }
    });
}

function mostrar_text1(id){
    $('.btn_c1'+id).css('display','none');
    $('.btn_c2'+id).css('display','block');
    $('.mostrar_txt_'+id).css('display','block');
}

function mostrar_text2(id){
    $('.btn_c1'+id).css('display','block');
    $('.btn_c2'+id).css('display','none');
    $('.mostrar_txt_'+id).css('display','none');
}


function add_carrito(id){
    $.ajax({
        type: "POST",
        url: base_url+"Productos/add_carrito_compra_pro",
        data: {id:id,cantidad:$('#cantidad').val()}, 
        beforeSend: function(){

        },
        success: function (result){
            if(result==0){
                swal("¡Atención!","Para poder agregar al carrito tienes que iniciar sesión","error");
            }else{
                swal({
                    title: "¡Éxito!",
                    text: "Producto agregado con éxito al carrito de compras",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                get_total_compras();
            }    
        }
    });
}

function modificar_cant(id,precio){
    var cant=$('#cantidady'+id+' option:selected').val();
    var c=parseFloat(cant);
    var p=parseFloat(precio);
    var mult=parseFloat(c*p);
    var total=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mult);
    $.ajax({
        type: "POST",
        url: base_url+"Productos/edit_carrito_compra_pro",
        data: {id:id,cantidad:cant}, 
        beforeSend: function(){

        },
        success: function (result){
            /*swal({
                title: "¡Éxito!",
                text: "Se agrego al carrito",
                type: "success",
                timer: 1500,
                showConfirmButton: false
            });   */
            $('.cantida_pro_'+id).html(total);
            $('.precio_total_'+id).val(mult); 
            calculo_total();
        }
    });
}


function calculo_total(){
    var TABLA   = $(".modal_carrito ol > li div p span");
    var total=0;
    TABLA.each(function(){ 
        total_aux = $(this).find("input[class*='precio_total']").val();
        total+=parseFloat(total_aux);
    });  

    var precio=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total);
    $('.total_compras').html(precio);
}

conts=0;
function btn_sucursales(){
    if(conts==0){
        conts=1;
        $('.sucursale_text').css('display','block');
    }else{
        conts=0;
        $('.sucursale_text').css('display','none');
    }
}

function proceder_pago(){
    var num=$('.text_total').text();
    var n=parseFloat(num);
    if(num==0){
        $('.procesar_validar').css('display','block');
        setTimeout(function(){ 
            $('.procesar_validar').css('display','none');
        }, 4000);
    }else{
        window.location = base_url+'Pago';     
    }
    
}