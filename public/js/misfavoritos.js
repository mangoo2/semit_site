var base_url = $('#base_url').val();
$(document).ready(function() {
	categoria_data();
});

function categoria_data(){
	$.ajax({
	    type: "POST",
	    url: base_url+"Micuenta/get_favoritos",
	    beforeSend: function(){

	    },
	    success: function (result) {
	    	$('.demos2_text').html(result);
	    }
	});
}

function categoria_get(cat) {
	//var cat = $('#idcategoria option:selected').val();
	if(cat==0){
        window.location = base_url+'Inicio';		
	}else{
        window.location = base_url+'Inicio/categoria/'+cat;		
	}
}

function add_carrito(id){
	$.ajax({
	    type: "POST",
	    url: base_url+"Inicio/add_carrito_compra",
	    data: {id:id}, 
	    beforeSend: function(){

	    },
	    success: function (result){
	    	if(result==0){
                swal("¡Atención!","Para poder agregar al carrito tienes que iniciar sesión","error");
	    	}else{
	    		$('.car_'+id).html('<svg width="46" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-425 -305)"><path fill="#009bdb" d="M38.4375 25.7395 38.4375 9.84375 8.4375 9.84375 8.4375 7.92188C8.43516 5.87766 6.77859 4.22108 4.73438 4.21875L4.6875 4.21875C4.42861 4.21875 4.21875 4.42861 4.21875 4.6875 4.21875 4.94639 4.42861 5.15625 4.6875 5.15625L4.73438 5.15625C6.26114 5.1578 7.49845 6.39511 7.5 7.92188L7.5 31.4062C7.50216 33.1415 8.85502 34.575 10.5872 34.6777 9.15525 35.5615 8.71097 37.4388 9.5948 38.8708 10.4787 40.3027 12.356 40.747 13.7879 39.8632 15.2198 38.9793 15.6641 37.102 14.7803 35.67 14.5345 35.2719 14.2004 34.9357 13.8038 34.6875L31.1944 34.6875C29.7682 35.5807 29.3362 37.4608 30.2293 38.887 31.1224 40.3131 33.0026 40.7452 34.4287 39.852 35.8549 38.9589 36.287 37.0787 35.3939 35.6526 35.1493 35.262 34.8193 34.9321 34.4287 34.6875L37.9688 34.6875C38.2276 34.6875 38.4375 34.4776 38.4375 34.2188 38.4375 33.9599 38.2276 33.75 37.9688 33.75L10.7812 33.75C9.48745 33.7485 8.43905 32.7 8.4375 31.4062L8.4375 28.552ZM32.8125 39.375C31.6475 39.375 30.7031 38.4306 30.7031 37.2656 30.7031 36.1006 31.6475 35.1562 32.8125 35.1562 33.9775 35.1562 34.9219 36.1006 34.9219 37.2656 34.9206 38.43 33.9769 39.3737 32.8125 39.375ZM12.1875 39.375C11.0225 39.375 10.0781 38.4306 10.0781 37.2656 10.0781 36.1006 11.0225 35.1562 12.1875 35.1562 13.3525 35.1562 14.2969 36.1006 14.2969 37.2656 14.2956 38.43 13.3519 39.3737 12.1875 39.375ZM8.4375 16.4062 15 16.4062 15 21.0938 8.4375 21.0938ZM30 21.0938 23.4375 21.0938 23.4375 16.4062 30 16.4062ZM30.9375 16.4062 37.5 16.4062 37.5 21.0938 30.9375 21.0938ZM23.4375 22.0312 30 22.0312 30 25.5886 23.4375 26.2031ZM22.5 26.2917 15.9375 26.9062 15.9375 22.0312 22.5 22.0312ZM23.4375 15.4688 23.4375 10.7812 30 10.7812 30 15.4688ZM22.5 15.4688 15.9375 15.4688 15.9375 10.7812 22.5 10.7812ZM22.5 16.4062 22.5 21.0938 15.9375 21.0938 15.9375 16.4062ZM8.4375 22.0312 15 22.0312 15 26.9948 8.4375 27.6094ZM30.9375 25.5 30.9375 22.0312 37.5 22.0312 37.5 24.8855ZM37.5 15.4688 30.9375 15.4688 30.9375 10.7812 37.5 10.7812ZM15 10.7812 15 15.4688 8.4375 15.4688 8.4375 10.7812Z" transform="matrix(1.02222 0 0 1 425 305)" /></g></svg>');
		    	swal({
	                title: "¡Éxito!",
	                text: "Producto agregado con éxito al carrito de compras",
	                type: "success",
	                timer: 1500,
	                showConfirmButton: false
	            });
	            get_total_compras();
	        }    
	    }
	});
}

function eliminar_registro(id){
    $.confirm({
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fa fa-warning',
        title: 'Atención!',
        content: '¿Está seguro de eliminar este registro de tus favoritos?',
        type: 'red',
        typeAnimated: true,
        buttons:{
            confirmar: function (){
                $.ajax({
                    type:'POST',
                    url: base_url+"Micuenta/delete_producto",
                    data:{id:id},
                    statusCode:{
                        404: function(data){
                            Swal.fire("Error!", "No Se encuentra el archivo!", "error");
                        },
                        500: function(){
                            Swal.fire("Error!", "500", "error");
                        }
                    },
                    success:function(data){
                        swal("Éxito!", "Elimando Correctamente", "success");
                        $('.tr_pro_'+id).remove();
                    }
                }); 
            },
            cancelar: function () 
            {
                
            }
        }
    });
}