var base_url = $('#base_url').val();
var idventa = $('#idventa').val();
var idventad;
$(document).ready(function() {
	pedidos_data();
	if(idventa!=0){
        //$('#largeModal').modal('show');
	}
	$("#RegimenFiscalReceptor option[value="+ $('#RegimenFiscalReceptor_aux').val() +"]").attr("selected",true);
	$("#uso_cfdi option[value="+ $('#uso_cfdi_aux').val() +"]").attr("selected",true);
	$('#btn-timbrar').click(function(event) {
        $('body').loader('show');
        $( "#btn-timbrar" ).prop( "disabled", true );
        setTimeout(function(){ 
             $("#btn-timbrar" ).prop( "disabled", false );
        }, 10000);
        timbrado();
    });
});
function pedidos_data(){
	$.ajax({
	    type: "POST",
	    url: base_url+"Micuenta/get_facturacion", 
	    data:{
            idventa:$('#idventa').val()
	    },
	    beforeSend: function(){

	    },
	    success: function (result) {
	    	$('.demos2_text').html(result);
	    }
	});
}


function registrar_factura(){
    var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
        	rfc:{
              required: true
            },
            cp:{
              required: true
            },
            correo:{
              required: true
            },
            razon_social:{
              required: true,
            },
            RegimenFiscalReceptor:{
              required: true,
            },
            uso_cfdi: {
                required: true,
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_data").valid();
    if($valid) {
    	var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Micuenta/add_factura_cliente',
            data: datos,
            statusCode:{
                404: function(data){
                    alert("No Se encuentra el archivo!");
                },
                500: function(){
                    alert("500");
                }
            },
            success:function(data){
            	reg=parseFloat(data);
                swal({
                    title: "¡Éxito!",
                    text: "Datos guardados correctamente",
                    type: "success",
                    timer: 4000,
                    showConfirmButton: false
                });
                setTimeout(function(){ 
                	$('#largeModal').modal('hide');
                    //window.location = base_url+'Inicio';
                }, 1000);
            }
        });
    }   
}

function v_rf(){
    $('.pararf').prop( "disabled", true );
    var rf=$('#RegimenFiscalReceptor option:selected').val();
    console.log('regimen:'+rf);
    $( "#uso_cfdi ."+rf ).prop( "disabled", false )
}

function validarrfc(input) {
    var rfc = input.value.toUpperCase();

    if (rfcValido1(rfc)) { // ⬅️ Acá se comprueba
      //rfcexiste(rfc);
      $('.v_rfc').html('');
    } else {
      $('.v_rfc').html('No válido');
    }
        
}

function rfcValido1(rfcStr) {
    
    $('.help-block').remove();
    
    var strCorrecta;
    
    strCorrecta = rfcStr;
        
    if (rfcStr.length == 12){
       
       //var valid = '^(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
       var valid = '^(([A-Z,Ñ,&]|[a-z,ñ,&]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    
    } else {
       
       var valid = '^(([A-Z]|[a-z]|\s){1})(([A-Z]|[a-z]){3})([0-9]{6})((([A-Z]|[a-z]|[0-9]){3}))';
    }
    
    var validRfc=new RegExp(valid);
    
    var matchArray=strCorrecta.match(validRfc);
    
    if (matchArray==null) {        
        return false;        
    } else {
        return true;
    }
}

function modal_factura(idventa){
    //idventa=idventa;
    idventad=idventa;
    $('#largeModal').modal('show');
}
function timbrado(){

    $.ajax({
        type:'POST',
        url: base_url+'Timbrado/generafacturarabierta',
        data: {
            rfc:$('#rfc').val(),
            cp:$('#cp').val(),
            correo:$('#correo').val(),
            razon_social:$('#razon_social').val(),
            RegimenFiscalReceptor:$('#RegimenFiscalReceptor option:selected').val(),
            uso_cfdi:$('#uso_cfdi option:selected').val(),
            idventad:idventad

        },
        statusCode:{
            404: function(data){
                alert("No Se encuentra el archivo!");
                $('body').loader('hide');
            },
            500: function(){
                alert("500");
                $('body').loader('hide');
            }
        },
        success:function(response){
            console.log(response);
            var array = $.parseJSON(response);
            if (array.resultado=='error') {
                console.log(array.MensajeError);
                console.log(array.info.TimbrarCFDIResult.MensajeErrorDetallado);
                var texto=array.info.TimbrarCFDIResult.MensajeErrorDetallado;
                swal({    title: "Error "+array.CodigoRespuesta+"!",
                              text: texto,
                              type: "warning",
                              showCancelButton: false
                 });
                $('body').loader('hide');
                //retimbrar(array.facturaId,0);
            }else{
                $('body').loader('hide');
                var textofactura='Se ha creado la factura';
                
                swal({
                    title:"Éxito!", 
                    text:textofactura, 
                    type:"success"
                    },
                    function(){
                        window.location.reload();
                    });
            }
        }
    });
}