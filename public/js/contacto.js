var base_url = $('#base_url').val();

function registrar_correo(){
    var form_register = $('#form_data');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);
    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'vd_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            nombre:{
              required: true
            },
            correo:{
              required: true
            },
            mensaje:{
              required: true,
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    //////////Registro///////////
    var $valid = $("#form_data").valid();
    if($valid) {
            var datos = form_register.serialize();
            $('.registro_data').attr('disabled',true);
            swal({
            title: "¡Atención!",
            text: "Se esta enviando tu mensaje",
            showConfirmButton: false
            });
            $.ajax({
                type:'POST',
                url: base_url+'Inicio/contacto_correo',
                data: datos,
                statusCode:{
                    404: function(data){
                        alert("No Se encuentra el archivo!");
                    },
                    500: function(){
                        alert("500");
                    }
                },
                success:function(data){
                    swal({
                        title: "¡Éxito!",
                        text: "Se envio tu mensaje correctamente.",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });
                    setTimeout(function(){ 
                        $('#nombre').val('');
                        $('#correo').val('');
                        $('#mensaje').val('');
                        $('.vd_red').remove();
                        $('.registro_data').attr('disabled',false);
                    }, 1000);
                }
            });
    }   
}

function categoria_get(cat) {
    //var cat = $('#idcategoria option:selected').val();
    if(cat==0){
        window.location = base_url+'Inicio';        
    }else{
        window.location = base_url+'Inicio/categoria/'+cat;     
    }
}