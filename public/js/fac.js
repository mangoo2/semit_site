var base_url = $('#base_url').val();
var facturaid;
$(document).ready(function($) {
	$('#generarfactura').click(function(event) {
		$( "#generarfactura" ).prop( "disabled", true );
		
		setTimeout(function(){ 
			$( "#generarfactura" ).prop( "disabled", false );
		}, 1000);
		var form=$('#form-datos');
		$('.result_fac').html('');
		$('.result_fac2').html('');
		if(form.valid()){
			$('.bar-barraprogreso').removeClass('bg-danger').addClass('bg-success').css({"width": "20%"});
			setTimeout(function(){ 
				$('.bar-barraprogreso').removeClass('bg-danger').addClass('bg-success').css({"width": "30%"});
			}, 1000);





			$.ajax({
		        type:'POST',
		        url: base_url+'Timbrado/timbradodelcliente',
		        data:form.serialize(),
		        statusCode:{
		            404: function(data){
		                toastr.error('Se a detectado un error 404','Error!');
		            },
		            500: function(){
		                toastr.error('Se a detectado un error 500','Error!');
		            }
		        },
		        success:function(data){
		            var array = $.parseJSON(data);
		            if(array.estado=='error_00'){
		            	swal({
			                    title: "Facturacion fuera de rango",
			                    text: "El folio del ticket se encuentra fuera del mes de la fecha de realizacion de la venta",
			                    type: "error",
			                    timer: 6000
		                	},
						    function () {
						            location.reload(true);
						            
						    });
		            }
		            if(array.estado=='error_01'){
		            	$('.bar-barraprogreso').removeClass('bg-success').addClass('bg-danger').css({"width": "100%"});
		            	$('.result_fac2').html('Favor de verificar Folio y/o monto');
		            	$('#fac_folio').addClass('error');
						$('#fac_monto').addClass('error');
		            }
		            if(array.estado=='error_02'){
		            	$('.bar-barraprogreso').removeClass('bg-success').addClass('bg-danger').css({"width": "100%"});
		            	$('.result_fac2').html('La venta ya se le genero una factura previemente');
		            }
		            if(array.estado=='error'){
		            	$('.bar-barraprogreso').removeClass('bg-success').addClass('bg-danger').css({"width": "100%"});
		            	$('.result_fac2').html(array.html);
		            }
		            if(array.estado=='correcto'){
		            	$('.bar-barraprogreso').removeClass('bg-danger').addClass('bg-success').css({"width": "100%"});
		            	$('.result_fac2').html(array.html);
		            }
		        }
		    });
		}
	});
	$('#enviomail').click(function(event) {
		$.ajax({
		        type:'POST',
		        url: base_url+'Mailenvio/envio_fac',
		        data:{
		        	'idfac':facturaid,
		        	'email':$('#email').val()
		        },
		        statusCode:{
		            404: function(data){
		                toastr.error('Se a detectado un error 404','Error!');
		            },
		            500: function(){
		                toastr.error('Se a detectado un error 500','Error!');
		            }
		        },
		        success:function(data){
		            toastr.success('Se a enviado');
		        }
		    });
	});
});
function regimenfiscal(){
	$( "#fac_usocfdi option").prop( "disabled", true );
	var regimennumber = $('#fac_regimenfiscal option:selected').val();
	$( "#fac_usocfdi ."+regimennumber ).prop( "disabled", false );
}
function enviofactura(id){
	facturaid=id;
	$('#Modalfactura').modal('show');
	$('.iframediv').html('<iframe src="'+base_url+'Folios/facturapdf/'+id+'"></iframe>');
}
function obtenerdatos(){
	var rfc = $('#fac_rfc').val();
	$.ajax({
        type:'POST',
        url: base_url+'Facturacion/obtenerdatos',
        data:{
        	'rfc':rfc
        },
        statusCode:{
            404: function(data){
                toastr.error('Se a detectado un error 404','Error!');
            },
            500: function(){
                toastr.error('Se a detectado un error 500','Error!');
            }
        },
        success:function(data){
            var array = $.parseJSON(data);
            if(array.numdatos>0){
            	if(array.rfc!='XAXX010101000'){
            		$('#fac_razonsocial').val(array.razon_social);
					$('#fac_cp').val(array.cp);
					$('#fac_regimenfiscal').val(array.RegimenFiscalReceptor).change();
					$('#fac_usocfdi').val(array.uso_cfdi);
            	}
            	
            }
            
        }
    });
}