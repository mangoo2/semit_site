<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/sweealert/sweetalert.css">
<style type="text/css">
	:root {
        --color-green: #05a8e1;
        --color-secondary: #b9b9b9;
        --color-button: white;
        --color-black: white;
    }


    .switch-button .switch-button__checkbox {
        display: none;
    }
    .switch-button .switch-button__label {
        background-color: var(--color-secondary);
        width: 3rem;
        height: 1.5rem;
        border-radius: 1rem;
        display: inline-block;
        position: relative;
    }
    .switch-button .switch-button__label:before {
        transition: .2s;
        display: block;
        position: absolute;
        width: 1.5rem;
        height: 1.5rem;
        background-color: var(--color-button);
        content: '';
        border-radius: 50%;
        box-shadow: inset 0px 0px 0px 1px var(--color-black);
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label {
        background-color: #152342;
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label:before {
        transform: translateX(1.5rem);
    }

    .vd_red{
        color: red !important;
    }
    .vd_green{
        color: green !important; 
    } 

    .btn_pasos_act2{
        color: #152342;
        padding: 9px;
        border-radius: 25px;
        border: 2px solid #152342;
    }

    .btn_pasos_act{
        background-color: #152342; 
        color: white; 
        padding: 9px; 
        border-radius: 25px;
    }
    .btn_flecha{
        cursor: pointer;
        color: #152342;
        padding: 8px;
        border-radius: 25px;
        border: 2px solid #152342;
    }
</style>
<div class="container">
  <div class="row pt-3">
    <div class="col-md-10 mx-md-auto">
      <h4>Proceder al pago de tus productos <span style="color: #93ba1f;">(<?php echo $total_productos ?> Productos)</span></h4> 
        <div class="row">
            <div class="col-sm-6">
                <div>
                    <form id="form_data_pago" method="post">
                        <input type="hidden" name="id" id="idpago" value="<?php echo $idpago ?>">
                    	<span style="color: #93ba1f; font-size: 21px;"><b>Paso</b></span>&nbsp;&nbsp;<span class="btn_pasos_act">&nbsp;<b>1</b>&nbsp;</span>&nbsp;<span style="width: 100%; position: absolute; padding: 2px;"><span class="paso_1_btn1"><a onclick="btn_paso_1_ocultar()" class="btn_flecha"><i class="fa fa-chevron-down"></i></a></span><span class="paso_1_btn2" style="display: none;"><a onclick="btn_paso_1_ocultar2()" class="btn_flecha"><i class="fa fa-chevron-up"></i></a></span></span>
                        <h4><b>Dirección de envió</b></h4>
                        <div class="paso_1_btn1">
                        	<div class="row" style="margin-bottom: 11px;">
                                <div class="col-sm-2">
                                	<label style="color: #93ba1f;"><b>País</b></label>
                                </div>
                                <div class="col-sm-5">
                                	<input type="text" name="pais" style="border-radius: 20px; border: thick; background: #f1f1f1; text-align: center;" value="México" readonly="">
                                </div>
                            </div>    	
                        	<div class="row" style="margin-bottom: 11px;">
                                <div class="col-sm-4">
                                	<label style="color: #93ba1f;"><b>Nombre del cliente</b></label>
                                </div>
                                <div class="col-sm-8">
                                	<input type="text" name="cliente" style="border-radius: 20px; text-align: center; width: 100%; border: 2px solid #152342;" value="<?php echo $cliente ?>">
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 11px;">
                                <div class="col-sm-4">
                                	<label style="color: #93ba1f;"><b>Calle y número</b></label>
                                </div>
                                <div class="col-sm-8">
                                	<input type="text" name="calle" style="border-radius: 20px; text-align: center; width: 100%; border: 2px solid #152342;" value="<?php echo $calle ?>">
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 11px;">
                                <div class="col-sm-4">
                                	<label style="color: #93ba1f;"><b>Código postal</b></label>
                                </div>
                                <div class="col-sm-8">
                                	<input type="number" name="cp" id="codigo_postal" oninput="cambiaCP2()" style="border-radius: 20px; text-align: center; width: 100%; border: 2px solid #152342;" value="<?php echo $cp ?>">
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 11px;">
                                <div class="col-sm-4">
                                	<label style="color: #93ba1f;"><b>Colonia</b></label>
                                </div>
                                <div class="col-sm-8">
                                    <select class="form-control colonia" id="colonia" name="colonia"  style="border-radius: 20px; text-align: center; width: 100%; border: 2px solid #152342;"> 
                                       <?php if($colonia!=''){
                                        echo '<option value="'.$colonia.'">'.$colonia.'</option>';
                                       } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 11px;">
                                <div class="col-sm-4">
                                	<label style="color: #93ba1f;"><b>Teléfono</b></label>
                                </div>
                                <div class="col-sm-8">
                                	<input type="text" name="telefono" style="border-radius: 20px; text-align: center; width: 100%; border: 2px solid #152342;" value="<?php echo $telefono ?>">
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 11px;">
                                <div class="col-sm-4">
                                	<label style="color: #93ba1f;"><b>Instrucciones de entrega</b></label>
                                </div>
                                <div class="col-sm-8">
                                    <textarea name="instrucciones" style="border-radius: 35px; text-align: center; width: 100%; border: 2px solid #152342;"><?php echo $instrucciones ?></textarea>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 11px;">
                                <div class="col-sm-7">
                                    <label style="color: #152342;"><b>Usar dirección predeterminada</b></label>
                                </div>
                                <div class="col-sm-4">
                                    <div class="switch-button">
                                        <input type="checkbox" name="direccion_predeterminada" id="direccion_predeterminada" class="switch-button__checkbox">
                                        <label for="direccion_predeterminada" class="switch-button__label"> </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>  
                    <div class="row paso_1_btn1" style="margin-bottom: 11px;">
                        <div class="col-sm-12" align="center">
                        	<button class="btn_registro" style="background-color: #93ba1f; color: white; border-radius: 30px; padding: 7px; border: thin; font-size: 18px;" onclick="registrar_pago()">&nbsp;&nbsp;Guardar dirección&nbsp;&nbsp;</button>
                        </div>
                    </div>
                </div>  
                <br>
                <?php 
                    $etiqueta_pasos2='';
                if($pasos!=0){
                    $etiqueta_pasos2='btn_pasos_act';
                }else{
                    $etiqueta_pasos2='btn_pasos_act2';
                } ?>
                <div>
                    <span style="color: #93ba1f; font-size: 21px;"><b>Paso</b></span>&nbsp;&nbsp;<span class="paso_2_act"><span class="<?php echo $etiqueta_pasos2 ?>">&nbsp;<b>2</b>&nbsp;</span></span>&nbsp;<span class="btn_paso2_txt"><?php if($pasos!=0){ ?><span style="width: 100%; position: absolute; padding: 2px;"><span class="paso_2_btn1"><a onclick="btn_paso_2_ocultar()" class="btn_flecha" ><i class="fa fa-chevron-up"></i></a></span><span class="paso_2_btn2" style="display: none;"><a onclick="btn_paso_2_ocultar2()" class="btn_flecha"><i class="fa fa-chevron-down"></i></a></span></span><?php }else{ ?> 
                    <span style="width: 100%; position: absolute; padding: 2px;"><span class="paso_2_btn1"><a class="btn_flecha" ><i class="fa fa-chevron-up"></i></a></span><span class="paso_2_btn2" style="display: none;"><a class="btn_flecha"><i class="fa fa-chevron-down"></i></a></span></span>
                    <?php } ?></span>
                    <h4><b>Seleccionar método de pago</b></h4>
                    <div class="paso_2_btn2" style="display: none">
                        <div class="row">
                            <div class="col-sm-6">
                                <a onclick="opcion_paso2(1)" style="cursor: pointer"><img style="width: 130px;" src="<?php echo base_url() ?>public/img/footer/Image_07.png"></a>    
                                <div class="img_paso_2_1" style="text-align: center;"></div>
                            </div>
                            <div class="col-sm-6">
                                <a onclick="opcion_paso2(2)" style="cursor: pointer"><img style="width: 200px;" src="<?php echo base_url() ?>public/img/footer/Image_08.png"></a>    
                                <div class="img_paso_2_2" style="text-align: center;"></div>
                            </div>
                            <div class="col-md-12 addformpago">
                            
                            </div> 
                        </div>

                    </div>
                </div>
                <br>
                <?php 
                /*
                    $etiqueta_pasos3='';
                if($pasos==3){
                    $etiqueta_pasos3='btn_pasos_act';
                }else{
                    $etiqueta_pasos3='btn_pasos_act2';
                } ?>
                <div>
                    <span style="color: #93ba1f; font-size: 21px;"><b>Paso</b></span>&nbsp;&nbsp;<span class="paso_3_act"><span class="<?php echo $etiqueta_pasos3 ?>">&nbsp;<b>3</b>&nbsp;</span></span>&nbsp;<span style="width: 100%; position: absolute; padding: 2px;"><span class="paso_3_btn1"><a onclick="btn_paso_3_ocultar()" class="btn_flecha" ><i class="fa fa-chevron-down"></i></a></span><span class="paso_3_btn2" style="display: none;"><a onclick="btn_paso_3_ocultar2()" class="btn_flecha"><i class="fa fa-chevron-up"></i></a></span></span>
                    <h4><b>Resumen de pedido y Confirmación</b></h4>
                </div>  
                */ ?>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-5">
                <div style="border: 2px solid #152342; padding: 7px; border-radius: 26px;">          	<?php 
            	$html='<div class="modal_carrito" style="max-height: 400px;
    overflow-y: scroll;overflow-x: hidden;"><ol class="mini-products-list">';
            $resulcat=$this->ModelGeneral->get_productos_temporal($this->idcliente);
            $suma=0;
            foreach ($resulcat as $x){
                $multi=$x->cantidad*$x->precio_con_iva;
                if($x->file!=''){
                    $img_file='https://adminsys.semit.mx/uploads/productos/'.$x->file; 
                }else{
                    $img_file='https://adminsys.semit.mx/public/img/favICONSEMIT.png'; 
                } 

                $set1='';$set2='';$set3='';$set4='';$set5='';$set6='';
                $set7='';$set8='';$set9='';$set10='';$set11='';$set12='';
                $set13='';$set14='';$set15='';$set16='';$set17='';$set18='';
                $set19='';$set20='';
                if($x->cantidad==1){ $set1='selected';
                }else if($x->cantidad==2){ $set2='selected';
                }else if($x->cantidad==3){ $set3='selected';
                }else if($x->cantidad==4){ $set4='selected';
                }else if($x->cantidad==5){ $set5='selected';
                }else if($x->cantidad==6){ $set6='selected';
                }else if($x->cantidad==7){ $set7='selected';
                }else if($x->cantidad==8){ $set8='selected';
                }else if($x->cantidad==9){ $set9='selected';
                }else if($x->cantidad==10){ $set10='selected';
                }else if($x->cantidad==11){ $set11='selected';
                }else if($x->cantidad==12){ $set12='selected';
                }else if($x->cantidad==13){ $set13='selected';
                }else if($x->cantidad==14){ $set14='selected';
                }else if($x->cantidad==15){ $set15='selected';
                }else if($x->cantidad==16){ $set16='selected';
                }else if($x->cantidad==17){ $set17='selected';
                }else if($x->cantidad==18){ $set18='selected';
                }else if($x->cantidad==19){ $set19='selected';
                }else if($x->cantidad==20){ $set20='selected';
                } 
                
                $html.='<li class="item" style="content-visibility: auto;">
                    <div class="row" style="margin-bottom: 11px;">
	                    <div class="col-sm-8">
	                    	 <a><b style="color:black;">'.$x->nombre.'</b></a>
	                    </div>
	                    <div class="col-sm-4">
	                    	<a title="Camera X1000" class="product-image"><img style="width: 68px;" src="'.$img_file.'"></a>
	                    </div>
	                </div>                    
                    <div class="product-details">
                        <p class="qty-price" style="display: -webkit-box;
                            display: -ms-flexbox;
                            display: flex;
                            -ms-flex-wrap: wrap;
                            flex-wrap: wrap;
                            -webkit-box-align: stretch;
                            -ms-flex-align: stretch;
                            align-items: stretch;
                            width: 100%;">
                            <select class="form-select form-control h-auto" id="cantidady'.$x->id.'" disabled style="width: 72px;" onchange="modificar_cant('.$x->id.','.$x->precio_con_iva.')">
                                <option value="1" '.$set1.'>1</option>
                                <option value="2" '.$set2.'>2</option>
                                <option value="3" '.$set3.'>3</option>
                                <option value="4" '.$set4.'>4</option>
                                <option value="5" '.$set5.'>5</option>
                                <option value="6" '.$set6.'>6</option>
                                <option value="7" '.$set7.'>7</option>
                                <option value="8" '.$set8.'>8</option>
                                <option value="9" '.$set9.'>9</option>
                                <option value="10" '.$set10.'>10</option>
                                <option value="11" '.$set11.'>11</option>
                                <option value="12" '.$set12.'>12</option>
                                <option value="13" '.$set13.'>13</option>
                                <option value="14" '.$set14.'>14</option>
                                <option value="15" '.$set15.'>15</option>
                                <option value="16" '.$set16.'>16</option>
                                <option value="17" '.$set17.'>17</option>
                                <option value="18" '.$set18.'>18</option>
                                <option value="19" '.$set19.'>19</option>
                                <option value="20" '.$set20.'>20</option>
                              </select>&nbsp;&nbsp;&nbsp;<span style="display: flex;
    align-items: center;
    background-color: #93ba1f;
    /* padding: 7px; */
    width: 60%;
    justify-content: space-evenly;
    color: white;
    font-size: 13px;
    border-radius: 30px;"><b class="cantida_pro_'.$x->id.'">$'.$multi.'</b><input type="hidden" class="precio_total_'.$x->id.'" id="precio_total" value="'.$multi.'"></span>
                        </p>
                    </div>
                </li>'; 
                $suma+=$multi;
            }
          $html.='
        </ol></div>
        <div class="totals">
           <br>
          <span style="display: -webkit-box;
                            display: -ms-flexbox;
                            display: flex;
                            -ms-flex-wrap: wrap;
                            flex-wrap: wrap;
                            -webkit-box-align: stretch;
                            -ms-flex-align: stretch;
                            align-items: stretch;
                            width: 100%;"><span style="font-size: 25px; color: black;">Total:&nbsp; &nbsp; &nbsp; </span> <span style="display: flex;
    align-items: center;
    background-color: #93ba1f;
    /* padding: 7px; */
    width: 60%;
    justify-content: space-evenly;
    color: white;
    font-size: 13px;
    border-radius: 30px; padding: 5px;"><b>$'.$suma.'</b></span></span>
        </div>';
        $_SESSION['totalcarrito']=$suma;
          echo $html;
              ?>
               </div>
            </div>	
        </div>    
    </div>
  </div>
  <br><br>
</div>