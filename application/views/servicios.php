<style>
    #contenedor-principal {
      display: flex;
      justify-content: center;
    }
    .contenedor-secundario {
      background-color: #d0d0d0;
      margin: 17px;
      border-radius: 12px;
    }
    .contenedor {
        margin: 17px;
    }
</style>
<style type="text/css">
  .vd_red{
    color: red !important;
  }
  .vd_green{
          color: green !important; 
  } 
</style>
     <div role="main" class="main">
        <div class="container" style="background-image: url(<?php echo base_url()?>public/img/servicios.png) !important;  background-color: #2E3136; background-size: cover; background-position: center; max-width: 100% !important;">
          <div class="row pt-3">
            <div class="col-md-10 mx-md-auto">
              <br><br><br><br>
              <br><br>
              <h3 style="color:white; font-size: 90px;" align="center"><b>
                Servicios</b>
              </h3>
              <br><br>
              <div class="row">
                <div class="col-sm-2" align="center">
                  <p style="color:white">
                    <a href="#renta_equipo" style="color: white">Renta de equipo</a>
                  </p>    
                </div>
                <div class="col-sm-2" align="center">  
                  <p style="color:white">
                    <a href="#venta_equipo" style="color: white">Venta de equipo</a>
                  </p>  
                </div>  
                <div class="col-sm-5" align="center">  
                  <p style="color:white">
                    <a href="#mantenimiento" style="color: white">Mantenimiento de concentradores de oxígeno</a>
                  </p>  
                </div>
                <div class="col-sm-3" align="center">
                  <p style="color:white">
                    <a href="#recarga" style="color: white">Recarga de oxígeno medicina</a>
                  </p>
                </div>
              </div>  
              <br><br><br><br>
            </div>
          </div>

        </div>
        <div class="row pb-3" style="background: #f8f8f8;" id="recarga">
          <div class="col-lg-1" align="center"></div>
          <div class="col-lg-4" align="center"><br>
            <img style="width: 267px; margin: 26px;" src="<?php echo base_url() ?>public/img/icons/azul_claro.png">
          </div>  
          <div class="col-lg-6" style="line-height: 1.5em; font-size: 15px; text-align: justify;">
            <br><br><br>
            <h2>VENTA DE OXÍGENO MEDICINAL</h2><br>
            Servicio de recarga de oxígeno para equipos de carácter portátil, en sus diferentes tamaños. Sujeto a revisión por parte del personal encargado, quien tomará en cuenta las siguientes consideraciones de seguridad establecidas en la materia:
            <br><br>
            <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> Tiempo de vida útil del equipo. 20 años a partir de la fecha de elaboración.<br>
            <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> Prueba hidrostática. Prueba vigente que permite saber si el equipo es apto para recargar, se realiza cada 5 años.<br>
            <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> El equipo se recibe limpio y sin aditamentos (Vaso, cánula, regulador, etc.)<br>
            <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> Los equipos no podrán pasar a recarga si presentan:<br>
            <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> Temperatura elevada, deberá esperar a que el equipo se enfríe.<br>
            <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> Golpes o equipo en mal estado.
            <br><br>
            <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> <b>Matriz SEMIT Pino Suárez </b> <a href="https://www.google.com/maps/place/SEMIT+Equipo+Médico/@19.2803904,-99.6490494,15z/data=!4m6!3m5!1s0x85cd89911c240001:0xd94eef4aea0d2e93!8m2!3d19.2803917!4d-99.6490491!16s%2Fg%2F1tfqg3xg?entry=ttu" target="_block"><img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_09.png"></a> <a href="tel:7224542223"><img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_01.png"></a>
              <br>
              Lunes a viernes       09:00 a 19:00 horas. Horario corrido.
              <br>
              Sábados                09:00 a 16:00 horas. Horario corrido.
              <br>
              Domingo                10:00 a 15:00 horas. Horario corrido.
              <br><br>
              <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> <b>Sucursal SEMIT Alfredo del Mazo</b> <a href="https://www.google.com/maps/place/SEMIT+Equipo+Médico/@19.3068983,-99.6312463,17z/data=!3m1!4b1!4m11!1m5!3m4!1s0x0:0xc54f648fd3a9774d!2sSEMIT+Equipo+Médico!11m1!2e1!3m4!1s0x0:0xc54f648fd3a9774d!8m2!3d19.3068983!4d-99.6312463" target="_block"> <img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_09.png"></a> <a href="tel:7224540212"><img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_01.png"></a>
              <br>
              Lunes a viernes       09:00 a 14:30 y de 16:00 a 19:00 Horas.
              <br>
              Sábados                09:00 a 16:00 horas. Horario corrido.
              <br>
              <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> <b>Sucursal SEMIT Jesús Carranza</b> <a href="https://www.google.com/maps/place/SEMIT+Equipo+Médico/@19.2705288,-99.6586182,15z/data=!4m6!3m5!1s0x85d276224f73f085:0x299bc2b255ef5e48!8m2!3d19.2705288!4d-99.6586182!16s%2Fg%2F1txfphrl?entry=ttu" target="_block"> <img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_09.png"></a> <a href="tel:7224540216"><img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_01.png"></a>
              <br>
              Lunes a viernes       09:00 a 14:30 y de 16:00 a 19:00 Horas.
              <br>
              Sábados                09:00 a 16:00 horas. Horario corrido.
              <br>
              <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> <b>Sucursal SEMIT Metepec</b> <a href="https://www.google.com/maps/place/SEMIT+Equipo+Médico/@19.2581006,-99.6154728,15z/data=!4m5!3m4!1s0x0:0xea5faa8a3b7ca153!8m2!3d19.2581111!4d-99.6154628" target="_block"> <img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_09.png"></a> <a href="tel:7224540219"><img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_01.png"></a>
              <br>
              Lunes a viernes       09:00 a 14:30 y de 16:00 a 19:00 Horas.
              <br>
              Sábados                09:00 a 16:00 horas. Horario corrido.
            <br><br>
          </div>  
        </div>
        <div class="row pb-3" id="venta_equipo">
          <div class="col-lg-1" align="center"></div>  
          <div class="col-lg-6" style="line-height: 1.5em; font-size: 15px; text-align: justify;">
            <br><br><br>
            <h2>VENTA DE EQUIPO MÉDICO</h2>
            <br>
            Estamos comprometidos en ser un aliado que le ayude a elevar su calidad de vida mediante la comercialización de productos innovadores,  conozca las diferentes categorías de productos con la que contamos.
            <br><br>
            Ofrecemos la venta de equipo médico, contamos con más de 22 categorías de productos y superamos los 5000 productos en stock, todos de la mejor calidad y de las marcas más reconocidas en el mercado, tanto nacionales como importadas.
            <br><br>
            Adquiere tus productos en nuestras diferentes sucursales o llámanos para conocer más sobre nuestro servicio de entrega a domicilio mediante envío por paquetería
            <br><br>
            <button class="btn btn-rounded mb-2" style="background: #9ebf43; color: white;" data-bs-toggle="modal" data-bs-target="#largeModal" onclick="sucursales()">
              Sucursales
            </button>
            <br><br><br><br>
          </div>  
          <div class="col-lg-4" align="center"><br><br>
            <img style="width: 267px; margin: 26px;" src="<?php echo base_url() ?>public/img/icons/azul_fuerte.png">
          </div>
        </div>
        <div class="row pb-3" style="background: #f8f8f8;" id="mantenimiento">
          <div class="col-lg-1" align="center"></div>
          <div class="col-lg-4" align="center"><br><br>
            <img style="width: 267px; margin: 26px;" src="<?php echo base_url() ?>public/img/icons/menta.png">
          </div>  
          <div class="col-lg-6" style="line-height: 1.5em; font-size: 15px; text-align: justify;">
            <br><br><br>
            <h2>Mantenimiento de concentradores de oxígeno</h2>
            <br>
            En SEMIT, contamos con el personal técnico especializado, para la atención y servicio en el mantenimiento de equipos: concentradores de oxígeno: estacionarios y portátiles, así como CPAP, mediante 2 tipos de mantenimiento:
            <br><br>
            <b>Mantenimiento correctivo:</b>
            <br>
            Servicio de revisión, diagnóstico y compostura, para determinar la falla que presenta el equipo. (Sujeto a disponibilidad de refacciones y material necesario.)
            <br><br>
            <b>Mantenimiento preventivo</b>
            <br>
            Servicio de carácter regular, que permite a los equipos mantener su funcionamiento en óptimas condiciones. Implica cambio de filtros internos y externos, revisión de válvulas, limpieza interior y calibrado mediante scanner.
            <br><br>
            <b>Tiempo de entrega</b>
            <br>
            Se determina al momento de la recepción del equipo, está sujeto a la agenda de trabajo, conforme a la cantidad de equipos que van llegando.
            <br><br><br>
          </div>  
        </div>
        <div class="row pb-3" id="renta_equipo">
          <div class="col-lg-1" align="center"></div>  
          <div class="col-lg-6" style="line-height: 1.5em; font-size: 15px; text-align: justify;">
            <br><br><br><br>
            <h2>RENTA DE EQUIPO MÉDICO</h2>
            <br>
            En al región de Toluca, Estado de México, ofrecemos la renta de equipo médico para el cuidado de tu paciente en casa conoce las opciones en renta que tenemos en:
            <br><br>
            <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> Camas
            <br>
            <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> Barandales
            <br>
            <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> Gruas 
            <br>
            <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> Trapecios
            <br>
            <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> Marcas ortopédicas
            <br>
            <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> Concentradores de oxígeno
            <br>
            <img style="width:9px;" src="<?php echo base_url() ?>public/img/favicon.ico"> Sillas de ruedas
            <br>
            Conoce aquí los requerimientos para la renta de equipo médico:
            <br><br>
            <button class="btn btn-rounded mb-2" style="background: #9ebf43; color: white;" data-bs-toggle="modal" data-bs-target="#formModal">Requerimientos</button>
            <!-- Nombre completo, correo electronico, numero celular, select,option cama , barandales, textarea comentarios adicionales y boton de enviar validar todos los campos menos el text area - cuando le den enviar correo enviado con exito -->
            <br><br><br><br>
          </div>  
          <div class="col-lg-4" align="center"><br><br>
            <img style="width: 267px; margin: 26px;" src="<?php  echo base_url() ?>public/img/icons/verde.png">
          </div>
        </div>

        <br>

            
        <!-- 
          Semit_erp 
          menu operaciones -> solicitudes de rentas
          listado -> mostraer los campos que se llenen de los datos de requerimientos columna estatus por default pendiente ->rojo 

        -->

      </div>

      <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content" style="border: 5px solid #062e5b;">
            <div class="modal-header" style="border-bottom: var(--bs-modal-header-border-width) solid #ffffff;">
              <h4 class="modal-title" id="largeModalLabel">Sucursales</h4>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
              <div class="sucu suc1">
                <div class="row">
                  <div class="col-lg-5">
                    <img style="width: 100%; border-radius: 25px;" src="<?php echo base_url()?>public/img/pino-suarez-semit.jpg">
                  </div>
                  <div class="col-lg-7">
                    <span style="font-size: 29px; color: #062e5b;">Matriz Toluca</span><br>    
                    <span style="font-size: 17px; color: #062e5b;">(Pino Suarez)</span>
                    <ul style="list-style-type: none; padding-left: 0rem !important;">
                      <li class="">
                          <a href="https://www.google.com/maps/place/SEMIT+Equipo+M%C3%A9dico/@19.2803904,-99.6490494,15z/data=!4m2!3m1!1s0x0:0x58c9fafb033880b0?sa=X&amp;ved=2ahUKEwi7hcOVivb3AhV2K0QIHbMFDf8Q_BJ6BAhMEAU" target="_blank">            <span class="elementor-icon-list-icon">
                              <i aria-hidden="true" class="fas fa-map-marker-alt" style="color: #20c3bc"></i>            </span>
                                    <span class="elementor-icon-list-text" style="color: #062e5b;">José Maria Pino Suarez, No 722 (casi esquina Rafael Miguel Hidalgo) Toluca, México</span>
                                      </a>
                                  </li>
                                <li>
                          <a href="mailto:equiposmedicos@semit.mx">           <span class="elementor-icon-list-icon">
                              <i aria-hidden="true" class="fas fa-envelope" style="color: #20c3bc"></i>            </span>
                                    <span class="elementor-icon-list-text" style="color: #062e5b;">equiposmedicos@semit.mx</span>
                                      </a>
                                  </li>
                                <li>
                          <a href="mailto:asesorenventas@semit.mx">           <span class="elementor-icon-list-icon">
                              <i aria-hidden="true" class="fas fa-envelope" style="color: #20c3bc"></i>            </span>
                                    <span class="elementor-icon-list-text" style="color: #062e5b;"> asesorenventas@semit.mx</span>
                                      </a>
                                  </li>
                                <li>
                          <a href="tel:7224542223">           <span class="elementor-icon-list-icon">
                              <i aria-hidden="true" class="fas fa-phone-alt" style="color: #20c3bc"></i>           </span>
                                    <span class="elementor-icon-list-text" style="color: #062e5b;">(722) 454 22 23</span>
                                      </a>
                                  </li>
                                <li>
                                      <span class="elementor-icon-list-icon">
                              <i aria-hidden="true" class="far fa-clock" style="color: #20c3bc"></i>           </span>
                                    <span class="elementor-icon-list-text" style="color: #062e5b;">Horario de atención</span>
                                  </li>
                            </ul>

                    <p style="color: #062e5b;">Lunes a Viernes: 9:00 a.m. a 7:00 p.m.<br>Sábado: 9:00 a.m. a 4:00 p.m. -&nbsp;<span style="color: inherit;">Domingo: 10:00 a.m. a 3:00 p.m.</span></p>  
                  </div>
                </div>
              </div>  
              <div class="sucu suc2" style="display: none">
                <div class="row">
                  <div class="col-lg-5">
                    <img style="width: 100%; border-radius: 25px;" src="<?php echo base_url()?>public/img/semit-alfredodelmazo-2.jpg">
                  </div>
                  <div class="col-lg-7">
                    <span style="font-size: 29px; color: #062e5b;">Toluca</span><br>
                    <span style="font-size: 17px; color: #062e5b;">(Alfredo del Mazo)</span>

                    <ul style="list-style-type: none; padding-left: 0rem !important;">
                      <li class="">
                        <i aria-hidden="true" class="fas fa-map-marker-alt" style="color: #20c3bc"></i>            </span>
                        <span class="elementor-icon-list-text" style="color: #062e5b;">Boulevard Alfredo del Mazo No. 727 local 3 Plaza El Punto. Col. Científicos.</span>
                          </a>
                      </li>
                      <li class="">
                      <a href="mailto:alfredodelmazo@semit.com.mx">           <span class="elementor-icon-list-icon">
                      <i aria-hidden="true" class="fas fa-envelope" style="color: #20c3bc"></i>            </span>
                            <span class="elementor-icon-list-text" style="color: #062e5b;">alfredodelmazo@semit.mx</span>
                              </a>
                          </li>
                        <li class="">
                      <a href="tel:7224540212">           <span class="elementor-icon-list-icon">
                      <i aria-hidden="true" class="fas fa-phone-alt" style="color: #20c3bc"></i>           </span>
                            <span class="elementor-icon-list-text" style="color: #062e5b;">(722) 454 02 12</span>
                              </a>
                          </li>
                        <li class="">
                              <span class="elementor-icon-list-icon">
                      <i aria-hidden="true" class="far fa-clock" style="color: #20c3bc"></i>           </span>
                            <span class="elementor-icon-list-text" style="color: #062e5b;">Horario de atención</span>
                          </li>
                    </ul>
                    <p style="color: #062e5b;">Lunes a Viernes: 9:00 a.m. a 2:30 p.m. y 4:00 pm a 7:00 p.m.<br>Sábado: 9:00 a.m. a 4:00 p.m.</p>
                  </div>
                </div>
              </div>
              <div class="sucu suc3" style="display: none">
                <div class="row">
                  <div class="col-lg-5">
                    <img style="width: 100%; border-radius: 25px;" src="<?php echo base_url()?>public/img/semit-alfredodelmazo-2.jpg">
                  </div>
                  <div class="col-lg-7">
                    <span style="font-size: 29px; color: #062e5b;">Toluca</span><br>
                    <span style="font-size: 17px; color: #062e5b;">(Jesús Carranza)</span>

                    <ul style="list-style-type: none; padding-left: 0rem !important;">
                      <li>
                      <a href="https://www.google.com/maps/place/SEMIT+Equipo+M%C3%A9dico/@19.2705288,-99.6586182,15z/data=!4m5!3m4!1s0x0:0x299bc2b255ef5e48!8m2!3d19.270528!4d-99.6585843" target="_blank">            <span class="elementor-icon-list-icon">
                          <i aria-hidden="true" class="fas fa-map-marker-alt" style="color: #20c3bc"></i>            </span>
                                <span class="elementor-icon-list-text" style="color: #062e5b;">Jesús Carranza Sur, No. 323, (entre las Torres y Tollocan) Toluca México.</span>
                                  </a>
                              </li>
                            <li>
                      <a href="mailto:sucursaljcarranza@semit.com.mx">            <span class="elementor-icon-list-icon">
                          <i aria-hidden="true" class="fas fa-envelope" style="color: #20c3bc"></i>            </span>
                                <span class="elementor-icon-list-text" style="color: #062e5b;">sucursaljcarranza@semit.mx</span>
                                  </a>
                              </li>
                            <li>
                      <a href="tel:7224540216">           <span class="elementor-icon-list-icon">
                      <i aria-hidden="true" class="fas fa-phone-alt" style="color: #20c3bc"></i>           </span>
                            <span class="elementor-icon-list-text" style="color: #062e5b;">(722) 454 02 16</span>
                              </a>
                          </li>
                        <li>
                              <span class="elementor-icon-list-icon">
                      <i aria-hidden="true" class="far fa-clock" style="color: #20c3bc"></i>           </span>
                            <span class="elementor-icon-list-text" style="color: #062e5b;">Horario de atención</span>
                          </li>
                    </ul>
                    <p style="color: #062e5b;">Lunes a Viernes: 9:00 a.m. a 2:30 p.m. y 4:00 pm a 7:00 p.m.<br>Sábado: 9:00 a.m. a 4:00 p.m.</p>
                  </div>
                </div>
              </div>
              <div class="sucu suc4" style="display: none">
                <div class="row">
                  <div class="col-lg-5">
                    <img style="width: 100%; border-radius: 25px;" src="<?php echo base_url()?>public/img/metepec-semit-02.jpg">
                  </div>
                  <div class="col-lg-7">
                    <span style="font-size: 29px; color: #062e5b;">Metepec</span><br>
                    <span style="font-size: 17px; color: #062e5b;">(Plaza San Pedro)</span>

                    <ul style="list-style-type: none; padding-left: 0rem !important;">
                      <li>
                        <a href="https://www.google.com/maps/place/SEMIT+Equipo+M%C3%A9dico/@19.2581006,-99.6154728,15z/data=!4m5!3m4!1s0x0:0xea5faa8a3b7ca153!8m2!3d19.2581111!4d-99.6154628" target="_blank">           <span class="elementor-icon-list-icon">
                            <i aria-hidden="true" class="fas fa-map-marker-alt" style="color: #20c3bc"></i>            </span>
                                  <span class="elementor-icon-list-text" style="color: #062e5b;">Av. Benito Juárez, No. 528, Barrio San Mateo (a 300 m del Centro Médico Toluca)</span>
                                    </a>
                                </li>
                              <li>
                        <a href="mailto:metepec@semit.com.mx">            <span class="elementor-icon-list-icon">
                            <i aria-hidden="true" class="fas fa-envelope" style="color: #20c3bc"></i>            </span>
                                  <span class="elementor-icon-list-text" style="color: #062e5b;">metepec@semit.mx</span>
                                    </a>
                                </li>
                              <li>
                        <a href="tel:7224540219">           <span class="elementor-icon-list-icon">
                            <i aria-hidden="true" class="fas fa-phone-alt" style="color: #20c3bc"></i>           </span>
                                  <span class="elementor-icon-list-text" style="color: #062e5b;">(722) 454 02 19</span>
                                    </a>
                                </li>
                              <li>
                                    <span class="elementor-icon-list-icon">
                            <i aria-hidden="true" class="far fa-clock" style="color: #20c3bc"></i>           </span>
                                  <span class="elementor-icon-list-text" style="color: #062e5b;">Horario de atención</span>
                                </li>
                    </ul>
                    <p style="color: #062e5b;">Lunes a Viernes: 9:00 a.m. a 2:30 p.m. y 4:00 pm a 7:00 p.m.<br>Sábado: 9:00 a.m. a 4:00 p.m.</p>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="modal-footer">
              <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
            </div> -->
          </div>
        </div>
      </div>
<!-- Nombre completo, correo electronico, numero celular, select,option cama , barandales, textarea comentarios adicionales y boton de enviar validar todos los campos menos el text area - cuando le den enviar correo enviado con exito -->

      <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content" style="border: 5px solid #062e5b;">
            <div class="modal-header" style="border-bottom: var(--bs-modal-header-border-width) solid #ffffff;">
              <h4 class="modal-title" id="formModalLabel">Requerimientos</h4>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
              <div style="text-align: justify;">
              Mediante el siguiente <a download="semit_descargable" href="<?php echo base_url() ?>public/formato/semit_descargable.pdf" ><img style="width: 27px;" src="<?php echo base_url() ?>public/img/icons/pdf.png"></a> podrás encontrar los requerimientos y documentos solicitados para la renta de los productos que necesites. Por favor si estás interesado en adquirir productos en modalidad de renta, captura el siguiente formulario para que uno de nuestros ejecutivos se ponga en contacto directo contigo
              </div><br><br>
              <form id="form_data" class="mb-4">
                <div class="form-group row align-items-center">
                  <label class="col-sm-3 text-start text-sm-end mb-0">Nombre completo</label>
                  <div class="col-sm-9">
                    <input type="text" name="nombre" class="form-control" id="nombre">
                  </div>
                </div>
                <div class="form-group row align-items-center">
                  <label class="col-sm-3 text-start text-sm-end mb-0">Correo electrónico</label>
                  <div class="col-sm-9">
                    <input type="email" name="correo" class="form-control" id="correo">
                  </div>
                </div>
                <div class="form-group row align-items-center">
                  <label class="col-sm-3 text-start text-sm-end mb-0">Productos</label>
                  <div class="col-sm-9">
                    <select class="form-control" name="productos">
                      <option selected="" disabled="">Selecciona una opción</option>
                      <option value="1">Camas</option>
                      <option value="2">Barandales</option>
                      <option value="3">Gruas</option>
                      <option value="4">Trapecios</option>
                      <option value="5">Marcas ortopédicas</option>
                      <option value="6">Concentradores de oxígeno</option>
                      <option value="7">Sillas de ruedas</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 text-start text-sm-end mb-0">Comentarios adicionales</label>
                  <div class="col-sm-9">
                    <textarea rows="5" class="form-control" name="comentarios" id="comentarios"></textarea>
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer" style="border-top: var(--bs-modal-header-border-width) solid #ffffff; align-self: center;">
              <button type="button" class="btn registro_data" style="background: #9ebf43; color: white;" onclick="registrar_requerimientos()">Enviar formulario</button>
            </div>
          </div>
        </div>
      </div>