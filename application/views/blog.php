	   <?php 
        $resultext='DESCUBRE. APRENDE. INSPIRA';
        $resultext2='nuestro blog lo tiene todo';
        $imagen=base_url().'public/img/banners/instrumental.png';  
     ?>
     <div class="container" style="background-image: url(<?php echo $imagen ?>) !important;  background-color: #2E3136; background-size: cover; background-position: center; max-width: 100% !important;">
          <div class="row pt-3">
            <div class="col-md-10 mx-md-auto">
              <br><br><br><br><br><br>
              <h3 style="color:white; text-align: center; font-size: 50px;">
                <?php echo $resultext ?>
              </h3>
              <div style="color:#bccadb; text-align: center; font-size: 50px;">
                <?php echo $resultext2 ?>
              </div>
              <br><br><br><br><br><br><br><br>
            </div>
          </div>

        </div>
    
    <div class="container">
      <br><br>
          <div class="row pt-3">
            <div class="col-md-8">
              <?php 
                 $idpro=0;
                foreach ($get_glog as $x){ 
                  $idpro=$x->id;
                //http://localhost/
                //https://adminsys.semit.mx
                  ?>
                  <div style="height: 527px; width: 100%; background-size: 115% auto; box-shadow: 3px 19px 49px #999; background-image: url(https://adminsys.semit.mx/uploads/blogimg/<?php echo $x->file ?>);">
                  </div>
                  <div style="background: #012d6a; width: 193px;; position: relative; top: -45px; box-shadow: 2px 2px 5px #999; "><div style="padding: 10px; margin-left: 20px; margin-right: 20px;"><span style="color: white"><?php echo $x->titulo ?></span></div></div>
              <?php } ?>
            </div>
            <div class="col-md-4">
              <?php 
                $get_glog_2=$this->ModelGeneral->get_blog2($idpro);
                foreach ($get_glog_2 as $y){ ?>
                  <div style="height: 240px; width: 100%; background-size: 115% auto; box-shadow: 3px 19px 49px #999; background-image: url(https://adminsys.semit.mx/uploads/blogimg/<?php echo $y->file ?>);">
                  </div>
                  <div style="background: white; width: 150px; position: relative; left: 207px; top: -25px; box-shadow: 2px 2px 5px #999;"><div style="padding: 10px;"><span style="color: black"><?php echo $y->titulo ?></span></div></div>
                  
              <?php } ?>
            </div>
          </div>
          <br><br> 
        </div>
      <div class="container">
        <div class="row pt-3">
          <div class="col-md-12">
            <h1 align="center" style="font-size: 100px; font-weight: 800; color: #93ba1f;">LO NUEVO</h1>
          </div>
        </div>
      </div>
      <div class="container">
        <br><br>
        <div class="row pt-3">
          <div class="col-md-12" align="center">
            <div style="display: inline-flex;">
            <?php foreach ($get_glog3 as $z){ ?>
              <div style="width: 90%">
                <div style="border-top: 3px solid #c4c4c4; border-left: 3px solid #c4c4c4; border-right: 3px solid #c4c4c4; height: 218px; width: 90%; background-size: 115% auto; background-image: url(https://adminsys.semit.mx/uploads/blogimg/<?php echo $z->file ?>);">
                </div>
                <div style="width: 90%; text-align-last: start; border-bottom: 3px solid #c4c4c4; border-left: 3px solid #c4c4c4; border-right: 3px solid #c4c4c4; height: 164px; overflow: hidden;"><div style="padding: 3px;">
                  <b style="color: black"><?php echo $z->titulo ?></b>
                  <div style="text-align: justify; line-height: 0px;"><br><?php echo $z->articulo ?></div>
                 </div>
                </div>
              </div>
            <?php } ?>
            </div>
          </div>
        </div>
        <br><br>
      </div>      