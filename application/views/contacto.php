<style>
    #contenedor-principal {
      display: flex;
      justify-content: center;
    }
    .contenedor-secundario {
      background-color: #d0d0d0;
      margin: 17px;
      border-radius: 12px;
    }
    .contenedor {
        margin: 17px;
    }
</style>
<style type="text/css">
  .vd_red{
    color: red !important;
  }
  .vd_green{
          color: green !important; 
  } 
</style>
     <div role="main" class="main">

            
        <div class="container" style="background-image: url(<?php echo base_url()?>public/img/contacto.png) !important;  background-color: #2E3136; background-size: cover; background-position: center; max-width: 100% !important;">
          <div class="row pt-3">
            <div class="col-md-12 mx-md-auto">
              <br><br><br><br><br><br>
              <p style="color:white; font-size: 44px;" align="center"><b>Permítenos apoyarte</b></p>

              
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
              <br>
            </div>
          </div>
        </div>
        <br>
        <div class="row pb-3">
          <div class="col-lg-12">
            <div class="row">
              <div class="col-sm-1"></div>
              <div class="col-sm-4">
                <p style="color: #012d6a; font-size: 22px;"><b>Contáctanos</b></p>
                <ul class="list list-icons list-icons-lg">
                  <li class="mb-1"><i class="fa fa-phone" style="color: #012d6a;"></i><p class="m-0"><a href="tel:8001234567" style="color: #012d6a;">(722) 454 2223</a></p></li>
                  <li class="mb-1"><i class="fa fa-envelope" style="color: #012d6a;"></i><p class="m-0"><a href="mailto:mail@example.com" style="color: #012d6a;">contacto@semit.mx</a></p></li>
                  <li class="mb-1"><i class="fa fa-map-marker" style="color: #012d6a;"></i><p class="m-0" style="color: #012d6a;"><a href="https://www.google.com/maps/place/SEMIT+Equipo+Médico/@19.2803917,-99.6516294,17z/data=!3m2!4b1!5s0x85cd89911c3f6a09:0xbba3f20f2afb97ad!4m16!1m9!4m8!1m0!1m6!1m2!1s0x85cd89911c240001:0xd94eef4aea0d2e93!2sSEMIT+Equipo+Médico+Blvr.+José+María+Pino+Suárez+722%2FA+Cuauhtémoc+50130+Toluca+de+Lerdo,+Méx.!2m2!1d-99.6490491!2d19.2803917!3m5!1s0x85cd89911c240001:0xd94eef4aea0d2e93!8m2!3d19.2803917!4d-99.6490491!16s%2Fg%2F1tfqg3xg?entry=ttu" target="_block" style="color: #012d6a;">Blvr. José María Pino Suaréz 722, Cuauhtémoc 50130 Toluca de Lerdo Méx</a></p></li>
                </ul>
              </div>
              <div class="col-sm-2">
              </div>  
              <div class="col-sm-4">
                <div style="border: solid #f1f1f1 2px; position: absolute; width: 35%; background: white;">
                  <form id="form_data" method="POST" style="margin: 19px;">
                    <div class="row">
                      <div class="form-group col">
                        <input type="text" class="form-control text-3 h-auto py-2" placeholder="Nombre" name="nombre" id="nombre">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col">
                        <input type="email" class="form-control text-3 h-auto py-2" placeholder="Correo" name="correo" id="correo">
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col">
                        <textarea rows="3" class="form-control text-3 h-auto py-2" placeholder="Mensaje" name="mensaje" id="mensaje"></textarea>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col">
                        <button type="button" class="btn btn-modern w-100 text-uppercase rounded-0 font-weight-bold text-3 py-3 registro_data" onclick="registrar_correo()" style="border-radius: 14px !important; background-color: #012d6a; color: white">ENVÍAR</button>
                      </div>
                    </div>
                  </form>
                </div>  
              </div>
            </div>
          </div>  
        </div>
        <div>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3766.0452861460412!2d-99.65162939147602!3d19.280396745453256!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cd89911c240001%3A0xd94eef4aea0d2e93!2sSEMIT%20Equipo%20M%C3%A9dico!5e0!3m2!1ses!2smx!4v1687973287544!5m2!1ses!2smx" height="400" style="border:0;width: 100%;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
      </div>

      