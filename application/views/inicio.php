<style>
    #contenedor-principal {
      display: flex;
      justify-content: center;
    }
    .contenedor-secundario {
      background-color: #d0d0d0;
      margin: 17px;
      border-radius: 12px;
    }
    .contenedor {
        margin: 17px;
    }
</style>

     <div role="main" class="main">
        <div class="owl-carousel owl-carousel-light owl-carousel-light-init-fadeIn owl-theme manual dots-inside dots-horizontal-center show-dots-hover nav-inside nav-inside-plus nav-dark nav-md nav-font-size-md show-nav-hover mb-0" data-plugin-options="{'autoplayTimeout': 7000}" data-dynamic-height="['670px','670px','670px','550px','500px']" style="height: 670px;">
          <div class="owl-stage-outer">
            <div class="owl-stage">
            <?php 
              $contn=0;
              $contn2=0;
              foreach ($pestana as $x){ 
                $contn2++;
              }  
              foreach ($pestana as $x){ 
                  ?>
                  <div class="owl-item position-relative" style="background-image: url(https://adminsys.semit.mx/uploads/banners/<?php echo $x->file ?>); background-color: #2E3136; background-size: cover; background-position: center;">
                    <div class="container position-relative z-index-1 h-100">
                      <div class="d-flex flex-column align-items-center justify-content-center h-100">
                        <span class="pestana_txtt" style="font-size: 46px; color: #012d6a; position: absolute; left: -60px; top: 198px;"><b><?php echo $x->titulo ?></b></span>
                        <span class="pestana_txtt" style="color: #012d6a; position: absolute; left: -60px; top: 238px; font-size: 20px;"><?php echo $x->descripcion ?></span>
                        <?php if($x->mostrar_boton==1){ ?>
                          <a style="position: absolute; top: 276px; left: -89px; background: #9dbe43; color: white;" href="<?php echo $x->link ?>" data-hash="" data-hash-offset="0" data-hash-offset-lg="100" class="btn btn-effect-4 font-weight-semi-bold px-4 btn-py-2 text-3 pestana_txtt"><?php echo $x->nombre ?><!-- <i class="fas fa-shopping-basket ms-1"></i> --></a>
                        <?php } ?>
                      </div>
                    </div>
                  </div>
                <?php 
                if($contn2==1){ ?>
                  <div class="owl-item position-relative" style="background-image: url(https://adminsys.semit.mx/uploads/banners/<?php echo $x->file ?>); background-color: #2E3136; background-size: cover; background-position: center;">
                    <div class="container position-relative z-index-1 h-100">
                      <div class="d-flex flex-column align-items-center justify-content-center h-100">
                        <span class="pestana_txtt" style="font-size: 46px; color: #012d6a; position: absolute; left: -60px; top: 198px;"><b><?php echo $x->titulo ?></b></span>
                        <span class="pestana_txtt" style="color: #012d6a; position: absolute; left: -60px; top: 238px; font-size: 20px;"><?php echo $x->descripcion ?></span>
                        <?php if($x->mostrar_boton==1){ ?>
                          <a style="position: absolute; top: 276px; left: -89px; background: #9dbe43; color: white;" href="<?php echo $x->link ?>" data-hash="" data-hash-offset="0" data-hash-offset-lg="100" class="btn btn-effect-4 font-weight-semi-bold px-4 btn-py-2 text-3 pestana_txtt"><?php echo $x->nombre ?><!-- <i class="fas fa-shopping-basket ms-1"></i> --></a>
                        <?php } ?>
                      </div>
                    </div>
                  </div> 
                <?php } 
                $contn++; 
              } 
            ?>
            </div>
          </div>
                   
        </div>
        <br>
        <section id="demos_1" class="section section-no-border section-light position-relative z-index-3 pt-0 m-0">
          <h4 align="center" style="color: #012d6a;">LOS PRODUCTOS</h4>
          <h1 align="center" style="color: #012d6a;"><b>MÁS VENDIDOS</b></h1>
            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-4">  
                <div class="row">  
                  <?php foreach ($productos_get as $p){ 
                    $img_file=''; 
                    if($p->file!=''){
                        $img_file='https://adminsys.semit.mx/uploads/productos/'.$p->file; 
                    }else{
                        $img_file='https://adminsys.semit.mx/public/img/favICONSEMIT.png'; 
                    }  
                    ?>
                    <div class="col-lg-6">
                      <div class="appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200" data-appear-animation-duration="750" style="animation-delay: 200ms; margin-bottom: 26px;">
                        <div class="portfolio-item hover-effect-1 text-center" >
                          <span class="thumb-info thumb-info-no-zoom thumb-info-no-overlay thumb-info-no-bg" style="background: #f5f5f7;">
                            <span class="thumb-info-wrapper thumb-info-wrapper-demos thumb-info-wrapper-link m-0 lazyloaded" style="height: 295px; background-image: url(<?php echo $img_file ?>)" 
                              >
                              <a href="https://adminsys.semit.mx/Productos/descripcion/<?php echo $p->idproductos ?>"></a>
                            </span>
                            <?php //var_dump($idcliente);die;
                             if($idcliente!=0){  
                                  $resulpro=$this->ModelGeneral->get_select('carrito_temporal',array('idproducto'=>$p->idproductos,'idcliente'=>$idcliente));
                                  $num_cl='';
                                  foreach ($resulpro as $pr){
                                      $num_cl='fill="#009bdb"';
                                  }
                                  //var_dump($num_cl);die;
                                  $resulprof=$this->ModelGeneral->get_select('productos_favoritos',array('idproducto'=>$p->idproductos,'idcliente'=>$idcliente));
                                  $num_clf='';
                                  foreach ($resulprof as $pr){
                                      $num_clf='fill="red"';
                                  }
                                  $html.='<div class="row" style="position: absolute; top: 4px; display: inline-flex;">
                                  <div class="col-lg-12">
                                    <a style="cursor:pointer; width: 62px; height: 57px;" onclick="add_favoritos('.$p->idproductos.')"><span class="cora_'.$p->idproductos.'"><svg '.$num_clf.' width="45" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-380 -305)"><path d="M402.493 309.685C392.661 309.685 384.691 317.656 384.691 327.488 384.691 337.32 392.661 345.29 402.493 345.29 412.326 345.29 420.296 337.32 420.296 327.488 420.301 317.66 412.337 309.69 402.509 309.685 402.504 309.685 402.499 309.685 402.493 309.685ZM402.493 336.406C402.493 336.406 393.836 329.786 393.836 324.439 393.836 320.873 399.285 317.054 402.493 323.42 405.702 317.054 411.151 320.873 411.151 324.439 411.151 329.786 402.493 336.406 402.493 336.406Z" /></g></svg></span></a>

                                   <a style="cursor:pointer; width: 62px; height: 57px;" onclick="add_carrito('.$p->idproductos.')"><span class="car_'.$p->idproductos.'"><svg width="46" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-425 -305)"><path '.$num_cl.' d="M38.4375 25.7395 38.4375 9.84375 8.4375 9.84375 8.4375 7.92188C8.43516 5.87766 6.77859 4.22108 4.73438 4.21875L4.6875 4.21875C4.42861 4.21875 4.21875 4.42861 4.21875 4.6875 4.21875 4.94639 4.42861 5.15625 4.6875 5.15625L4.73438 5.15625C6.26114 5.1578 7.49845 6.39511 7.5 7.92188L7.5 31.4062C7.50216 33.1415 8.85502 34.575 10.5872 34.6777 9.15525 35.5615 8.71097 37.4388 9.5948 38.8708 10.4787 40.3027 12.356 40.747 13.7879 39.8632 15.2198 38.9793 15.6641 37.102 14.7803 35.67 14.5345 35.2719 14.2004 34.9357 13.8038 34.6875L31.1944 34.6875C29.7682 35.5807 29.3362 37.4608 30.2293 38.887 31.1224 40.3131 33.0026 40.7452 34.4287 39.852 35.8549 38.9589 36.287 37.0787 35.3939 35.6526 35.1493 35.262 34.8193 34.9321 34.4287 34.6875L37.9688 34.6875C38.2276 34.6875 38.4375 34.4776 38.4375 34.2188 38.4375 33.9599 38.2276 33.75 37.9688 33.75L10.7812 33.75C9.48745 33.7485 8.43905 32.7 8.4375 31.4062L8.4375 28.552ZM32.8125 39.375C31.6475 39.375 30.7031 38.4306 30.7031 37.2656 30.7031 36.1006 31.6475 35.1562 32.8125 35.1562 33.9775 35.1562 34.9219 36.1006 34.9219 37.2656 34.9206 38.43 33.9769 39.3737 32.8125 39.375ZM12.1875 39.375C11.0225 39.375 10.0781 38.4306 10.0781 37.2656 10.0781 36.1006 11.0225 35.1562 12.1875 35.1562 13.3525 35.1562 14.2969 36.1006 14.2969 37.2656 14.2956 38.43 13.3519 39.3737 12.1875 39.375ZM8.4375 16.4062 15 16.4062 15 21.0938 8.4375 21.0938ZM30 21.0938 23.4375 21.0938 23.4375 16.4062 30 16.4062ZM30.9375 16.4062 37.5 16.4062 37.5 21.0938 30.9375 21.0938ZM23.4375 22.0312 30 22.0312 30 25.5886 23.4375 26.2031ZM22.5 26.2917 15.9375 26.9062 15.9375 22.0312 22.5 22.0312ZM23.4375 15.4688 23.4375 10.7812 30 10.7812 30 15.4688ZM22.5 15.4688 15.9375 15.4688 15.9375 10.7812 22.5 10.7812ZM22.5 16.4062 22.5 21.0938 15.9375 21.0938 15.9375 16.4062ZM8.4375 22.0312 15 22.0312 15 26.9948 8.4375 27.6094ZM30.9375 25.5 30.9375 22.0312 37.5 22.0312 37.5 24.8855ZM37.5 15.4688 30.9375 15.4688 30.9375 10.7812 37.5 10.7812ZM15 10.7812 15 15.4688 8.4375 15.4688 8.4375 10.7812Z" transform="matrix(1.02222 0 0 1 425 305)" /></g></svg></span></a></div></div>';
                                }
                              
                              echo $html;  
                            ?>
                            <div class="row">
                              <div class="col-lg-2"></div>
                              <div class="col-lg-8">
                                <h5 style="background: #469ddc; color: white; border-radius: 7px; font-size: 19px;">$ <?php echo $p->precio_con_iva ?></h5>  
                              </div> 
                            </div>  
                            <div class="row" style="height: 103px;">
                              <div class="col-lg-12">
                                <h6 style="text-align: initial; margin-left: 6px; color: #012d6a; font-size: 14px;  text-align-last: center;"><b><?php echo $p->nombre; ?></b></h6>
                                <a class="btn btn-outline btn-primary btn-xs mb-2" onclick="add_carrito(<?php echo $p->idproductos ?>)">Agregar a carrito</a>
                              </div>
                            </div>  
                          </span>
                        </div>
                      </div>
                    </div>
                    <?php /*
                    <div class="col-lg-6">
                      <div class="appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200" data-appear-animation-duration="750" style="animation-delay: 200ms; margin-bottom: 26px;">
                        <div class="portfolio-item hover-effect-1 text-center" >
                          <span class="thumb-info thumb-info-no-zoom thumb-info-no-overlay thumb-info-no-bg" style="background: #f5f5f7;">
                            <span class="thumb-info-wrapper thumb-info-wrapper-demos thumb-info-wrapper-link m-0 lazyloaded" style="height: 295px; background-image: url(https://adminsys.semit.mx/uploads/productos/<?php echo $p->file ?>);" 
                              >
                              <a  target="_blank"></a>
                               
                            </span><!-- https://adminsys.semit.mx  <?php // echo base_url() ?> localhost/semit_erp/uploads/ -->
                            <div class="row">
                              <div class="col-lg-7">
                                <h6 style="text-align: initial; margin-left: 6px; color: #012d6a; font-size: 11px;"><b><?php echo $p->nombre ?></b></h6>
                              </div>
                              <div class="col-lg-4">
                                <h6 style="background: #469ddc; color: white; margin-left: 10px; border-radius: 7px; font-size: 11px;"><?php echo $p->precio_con_iva ?></h6>  
                              </div> 
                            </div>  
                          </span>
                        </div>
                      </div>
                    </div> */ ?>

                  <?php } ?>
                </div>
              </div>    
              <div class="col-lg-4">
                <?php foreach ($productos1_get as $p1){ 
                  $img_file1=''; 
                    if($p1->file!=''){
                        $img_file1='https://adminsys.semit.mx/uploads/productos/'.$p1->file; 
                    }else{
                        $img_file1='https://adminsys.semit.mx/public/img/favICONSEMIT.png'; 
                    }  
                    ?>
                    <div class="appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200" data-appear-animation-duration="750" style="animation-delay: 200ms; margin-bottom: 26px;">
                      <div class="portfolio-item hover-effect-1 text-center" >
                        <span class="thumb-info thumb-info-no-zoom thumb-info-no-overlay thumb-info-no-bg" style="background: #f5f5f7;height: 882px;;"><div class="row">
                                <div class="col-lg-7">
                                </div>
                                <!-- <div class="col-lg-4">
                                  <h6 style="background: #469ddc; color: white; margin-left: 10px; border-radius: 7px; margin-top: 17px; font-size: 11px;"><b><?php //echo $p->precio_con_iva ?></b></h6>  
                                </div>  -->
                              </div><br><br>
                          <span class="thumb-info-wrapper thumb-info-wrapper-demos thumb-info-wrapper-link m-0 lazyloaded" style="max-height: 600px !important; height: 546px; background-image: url(<?php echo $img_file1 ?>);" 
                            >
                            
                            <a href="https://adminsys.semit.mx/Productos/descripcion/<?php echo $p1->idproductos ?>"></a>
                            
                          </span><!-- https://adminsys.semit.mx  <?php // echo base_url() ?> localhost/semit_erp/uploads/ -->
                          <?php //var_dump($idcliente);die;
                             if($idcliente!=0){  
                                  $resulpro1=$this->ModelGeneral->get_select('carrito_temporal',array('idproducto'=>$p1->idproductos,'idcliente'=>$idcliente));
                                  $num_cl='';
                                  foreach ($resulpro1 as $pr1){
                                      $num_cl='fill="#009bdb"';
                                  }
                                  //var_dump($num_cl);die;
                                  $resulprof1=$this->ModelGeneral->get_select('productos_favoritos',array('idproducto'=>$p1->idproductos,'idcliente'=>$idcliente));
                                  $num_clf='';
                                  foreach ($resulprof1 as $pr1){
                                      $num_clf='fill="red"';
                                  }
                                  $html.='<div class="row" style="position: absolute; top: 4px; display: inline-flex;">
                                  <div class="col-lg-12">
                                    <a style="cursor:pointer; width: 62px; height: 57px;" onclick="add_favoritos('.$p1->idproductos.')"><span class="cora_'.$p1->idproductos.'"><svg '.$num_clf.' width="45" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-380 -305)"><path d="M402.493 309.685C392.661 309.685 384.691 317.656 384.691 327.488 384.691 337.32 392.661 345.29 402.493 345.29 412.326 345.29 420.296 337.32 420.296 327.488 420.301 317.66 412.337 309.69 402.509 309.685 402.504 309.685 402.499 309.685 402.493 309.685ZM402.493 336.406C402.493 336.406 393.836 329.786 393.836 324.439 393.836 320.873 399.285 317.054 402.493 323.42 405.702 317.054 411.151 320.873 411.151 324.439 411.151 329.786 402.493 336.406 402.493 336.406Z" /></g></svg></span></a>

                                   <a style="cursor:pointer; width: 62px; height: 57px;" onclick="add_carrito('.$p1->idproductos.')"><span class="car_'.$p1->idproductos.'"><svg width="46" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-425 -305)"><path '.$num_cl.' d="M38.4375 25.7395 38.4375 9.84375 8.4375 9.84375 8.4375 7.92188C8.43516 5.87766 6.77859 4.22108 4.73438 4.21875L4.6875 4.21875C4.42861 4.21875 4.21875 4.42861 4.21875 4.6875 4.21875 4.94639 4.42861 5.15625 4.6875 5.15625L4.73438 5.15625C6.26114 5.1578 7.49845 6.39511 7.5 7.92188L7.5 31.4062C7.50216 33.1415 8.85502 34.575 10.5872 34.6777 9.15525 35.5615 8.71097 37.4388 9.5948 38.8708 10.4787 40.3027 12.356 40.747 13.7879 39.8632 15.2198 38.9793 15.6641 37.102 14.7803 35.67 14.5345 35.2719 14.2004 34.9357 13.8038 34.6875L31.1944 34.6875C29.7682 35.5807 29.3362 37.4608 30.2293 38.887 31.1224 40.3131 33.0026 40.7452 34.4287 39.852 35.8549 38.9589 36.287 37.0787 35.3939 35.6526 35.1493 35.262 34.8193 34.9321 34.4287 34.6875L37.9688 34.6875C38.2276 34.6875 38.4375 34.4776 38.4375 34.2188 38.4375 33.9599 38.2276 33.75 37.9688 33.75L10.7812 33.75C9.48745 33.7485 8.43905 32.7 8.4375 31.4062L8.4375 28.552ZM32.8125 39.375C31.6475 39.375 30.7031 38.4306 30.7031 37.2656 30.7031 36.1006 31.6475 35.1562 32.8125 35.1562 33.9775 35.1562 34.9219 36.1006 34.9219 37.2656 34.9206 38.43 33.9769 39.3737 32.8125 39.375ZM12.1875 39.375C11.0225 39.375 10.0781 38.4306 10.0781 37.2656 10.0781 36.1006 11.0225 35.1562 12.1875 35.1562 13.3525 35.1562 14.2969 36.1006 14.2969 37.2656 14.2956 38.43 13.3519 39.3737 12.1875 39.375ZM8.4375 16.4062 15 16.4062 15 21.0938 8.4375 21.0938ZM30 21.0938 23.4375 21.0938 23.4375 16.4062 30 16.4062ZM30.9375 16.4062 37.5 16.4062 37.5 21.0938 30.9375 21.0938ZM23.4375 22.0312 30 22.0312 30 25.5886 23.4375 26.2031ZM22.5 26.2917 15.9375 26.9062 15.9375 22.0312 22.5 22.0312ZM23.4375 15.4688 23.4375 10.7812 30 10.7812 30 15.4688ZM22.5 15.4688 15.9375 15.4688 15.9375 10.7812 22.5 10.7812ZM22.5 16.4062 22.5 21.0938 15.9375 21.0938 15.9375 16.4062ZM8.4375 22.0312 15 22.0312 15 26.9948 8.4375 27.6094ZM30.9375 25.5 30.9375 22.0312 37.5 22.0312 37.5 24.8855ZM37.5 15.4688 30.9375 15.4688 30.9375 10.7812 37.5 10.7812ZM15 10.7812 15 15.4688 8.4375 15.4688 8.4375 10.7812Z" transform="matrix(1.02222 0 0 1 425 305)" /></g></svg></span></a></div></div>';
                                }
                              
                              echo $html;  
                            ?>
                          <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-8">
                              <h5 style="background: #469ddc; color: white; border-radius: 7px; font-size: 19px;">$ <?php echo $p1->precio_con_iva ?></h5>  
                            </div> 
                          </div>  
                          <div class="row" style="height: 103px;">
                            <div class="col-lg-12">
                              <h6 style="text-align: initial; margin-left: 6px; color: #012d6a; font-size: 14px;  text-align-last: center;"><b><?php echo $p1->nombre; ?></b></h6>
                              <a class="btn btn-outline btn-primary btn-xs mb-2" onclick="add_carrito(<?php echo $p1->idproductos ?>)">Agregar a carrito</a>
                            </div>
                          </div>   

                        </span>
                      </div>
                    </div>
                <?php } ?>
              </div>
              <div class="col-lg-2"></div>     
            </div>
          
        </section>
        
        <section id="demos_2" class="section section-no-border section-light position-relative z-index-3 pt-0 m-0" style="display: none">
          <div class="demos2_text"></div>
        </section>  
        <br>
        <div class="row pb-3">
          <div class="col-lg-12">
            <div class="row">
              <div class="col-sm-12" align="center">
                <a style="cursor: pointer;" href="<?php echo base_url() ?>Servicios#recarga"><img style="width: 210px; margin: 26px;" src="<?php echo base_url() ?>public/img/icons/azul_claro.png"></a>
                <a style="cursor: pointer;" href="<?php echo base_url() ?>Servicios#venta_equipo"><img style="width: 210px; margin: 26px;" src="<?php echo base_url() ?>public/img/icons/azul_fuerte.png"></a>
                <a style="cursor: pointer;" href="<?php echo base_url() ?>Servicios#mantenimiento"><img style="width: 210px; margin: 26px;" src="<?php echo base_url() ?>public/img/icons/menta.png"></a>
                <a style="cursor: pointer;" href="<?php echo base_url() ?>Servicios#renta_equipo"><img style="width: 210px; margin: 26px;" src="<?php echo base_url() ?>public/img/icons/verde.png"></a>
              </div>
            </div>
          </div>  
        </div>
        <br>
        <div class="row pb-3">
          <div class="col-lg-12">
            <div align="center" style="color: #062e5b;">DESCUBRE LAS OPCIONES QUE TE BRINDA<br>
              <b style="font-size: 24px;">SEMIT EQUIPO MÉDICO</b>
            </div>
            <br>
            <div class="row">
              <div class="col-sm-2" align="center"></div>
              <div class="col-sm-3" align="center">
                <img style="width: 177px;" src="<?php echo base_url() ?>public/img/icons/icono_pago.png"><br>
                <b style="color: #062e5b;">Elige cómo pagar</b><br>
                <span style="color: #062e5b;">Paga con tarjeta, débito o efectivo.</span>
              </div>
              <div class="col-sm-2" align="center">
                <img style="width: 177px;" src="<?php echo base_url() ?>public/img/icons/icono_envio.png"><br>
                <b style="color: #062e5b;">Envío a tu domicilio</b><br>
                <span style="color: #062e5b;">Contacta a un asesor para cotizar tu envío</span>
              </div>
              <div class="col-sm-3" align="center">
                <img style="width: 177px;" src="<?php echo base_url() ?>public/img/icons/iconoubicacion.png"><br>
                <b style="color: #062e5b;">Sucursales</b><br>
                <span style="color: #062e5b;">Paga con tarjeta, débito o efectivo</span>
              </div>
              <div class="col-sm-2" align="center"></div>
            </div>
          </div>  
        </div>
            
        <div class="container" style="background-image: url(<?php echo base_url()?>public/img/smt.png) !important;  background-color: #2E3136; background-size: cover; background-position: center; max-width: 100% !important;">
          <div class="row pt-3">
            <div class="col-md-10 mx-md-auto">
              <img style="width: 267px" src="<?php echo base_url() ?>public/img/logo_semit.png">
              <br><br>
              <h3 style="color:white"><b>
                Empresa #1 en ofrecer Equipos Médicos</b>
              </h3>
              <p style="color:white">
                Servicios y Equipos Médicos Internacionales de Toluca S.A de C.V es una empresa 100% mexicana, orientada a a Venta  y Renta de Equipos Médicos, Oxígeno Medicinal y Rehabilitación cuya misión  es ser una empresa confiable, humanista y comprometida.
              </p>

            </div>
          </div>

        </div>
        <div>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3766.0452861460412!2d-99.65162939147602!3d19.280396745453256!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cd89911c240001%3A0xd94eef4aea0d2e93!2sSEMIT%20Equipo%20M%C3%A9dico!5e0!3m2!1ses!2smx!4v1687973287544!5m2!1ses!2smx" height="400" style="border:0;width: 100%;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
        <div class="container" align="center"> 
          <button type="button" class="btn btn-modern w-30 text-uppercase rounded-0 font-weight-bold text-3 py-3" style="border-radius: 14px !important; background-color: #c6d420; color: white" onclick="btn_sucursales()">VER TODAS SUCURSALES</button>
          <br><br>
        </div> 

        <div class="container sucursale_text" style="display:none ">  
          <div class="row">
            <div class="col-lg-5">
              <h4 style="color:#c6d420; border-bottom: solid #012d6a;"><b>Matriz Toluca</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="tel:7224542223"><i class="fa fa-phone"></i> <span style="color:#012d6a;">&nbsp;&nbsp; (722) 454 22 23 </span></a>
              </h4>
              <b>Lunes a Viernes:</b> 9:00 a 19:00 hrs<br>
              <b>Sábado:</b> 9:00 a 16 hrs | <b>Domingo:</b> 9:00 a 15:horas hrs
              <br>
              <a href="https://www.google.com/maps/place/SEMIT+Equipo+Médico/@19.2803904,-99.6490494,15z/data=!4m6!3m5!1s0x85cd89911c240001:0xd94eef4aea0d2e93!8m2!3d19.2803917!4d-99.6490491!16s%2Fg%2F1tfqg3xg?entry=ttu" target="_block"><img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_09.png">Blvr. José María Pino Suárez 722/A,<br> Cuauhtémoc, 50130 Toluca de Lerdo, Méx.</a>
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15064.18172818902!2d-99.6490494!3d19.2803904!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cd89911c240001%3A0xd94eef4aea0d2e93!2sSEMIT%20Equipo%20M%C3%A9dico!5e0!3m2!1ses!2smx!4v1690241238348!5m2!1ses!2smx" height="200" style="border:0;width: 100%;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-5">
              <h4 style="color:#c6d420; border-bottom: solid #012d6a;"><b>Alfredo del Mazo</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="tel:7224540212"><i class="fa fa-phone"></i> <span style="color:#012d6a;">&nbsp;&nbsp; (722) 454 02 12 </span></a>
              </h4>
              <b>Lunes a Viernes:</b> 9:00 a 19:00 hrs<br>
              <b>Sábado:</b> 9:00 a 16 hrs | <b>Domingo:</b> 9:00 a 15:horas hrs
              <br>
              <a href="https://www.google.com/maps/place/SEMIT+Equipo+Médico/@19.3068983,-99.6312463,17z/data=!3m1!4b1!4m11!1m5!3m4!1s0x0:0xc54f648fd3a9774d!2sSEMIT+Equipo+Médico!11m1!2e1!3m4!1s0x0:0xc54f648fd3a9774d!8m2!3d19.3068983!4d-99.6312463" target="_block"><img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_09.png">Boulevard Alfredo del Mazo No. 727<br> local 3 Plaza El Punto. Col. Científicos Planta Baja, Las Torres, 50076 Toluca de Lerdo, Méx.</a>
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3765.4354159579216!2d-99.63382122567637!3d19.30690334468479!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cd8b52ccff74d9%3A0xc54f648fd3a9774d!2sSEMIT%20Equipo%20M%C3%A9dico!5e0!3m2!1ses!2smx!4v1690241298008!5m2!1ses!2smx" height="200" style="border:0;width: 100%;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
          </div>
          <br><br>
          <div class="row">
            <div class="col-lg-5">
              <h4 style="color:#c6d420; border-bottom: solid #012d6a;"><b>Jesús Carranza</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="tel:7224540216"><i class="fa fa-phone"></i> <span style="color:#012d6a;">&nbsp;&nbsp; (722) 454 0216 </span></a>
              </h4>
              <b>Lunes a Viernes:</b> 9:00 a 19:00 hrs<br>
              <b>Sábado:</b> 9:00 a 16 hrs | <b>Domingo:</b> 9:00 a 15:horas hrs
              <br>
              <a href="https://www.google.com/maps/place/SEMIT+Equipo+Médico/@19.2705288,-99.6586182,15z/data=!4m6!3m5!1s0x85d276224f73f085:0x299bc2b255ef5e48!8m2!3d19.2705288!4d-99.6586182!16s%2Fg%2F1txfphrl?entry=ttu" target="_block"><img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_09.png">C. Jesús Carranza 323, Moderna de la<br> Cruz, 50180 Toluca de Lerdo, Méx.</a>
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15065.088497269733!2d-99.6586182!3d19.2705288!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d276224f73f085%3A0x299bc2b255ef5e48!2sSEMIT%20Equipo%20M%C3%A9dico!5e0!3m2!1ses!2smx!4v1690241325285!5m2!1ses!2smx" height="200" style="border:0;width: 100%;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
            <div class="col-lg-2"></div>
            <div class="col-lg-5">
              <h4 style="color:#c6d420; border-bottom: solid #012d6a;"><b>Plaza San Pedro</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="tel:7224540219"><i class="fa fa-phone"></i> <span style="color:#012d6a;">&nbsp;&nbsp; (722) 454 0219 </span></a>
              </h4>
              <b>Lunes a Viernes:</b> 9:00 a 19:00 hrs<br>
              <b>Sábado:</b> 9:00 a 16 hrs | <b>Domingo:</b> 9:00 a 15:horas hrs
              <br>
              <a href="https://www.google.com/maps/place/SEMIT+Equipo+Médico/@19.2581006,-99.6154728,15z/data=!4m5!3m4!1s0x0:0xea5faa8a3b7ca153!8m2!3d19.2581111!4d-99.6154628" target="_block"><img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_09.png">Av. Lic. Benito Juárez García<br> #528-Local 1, San Mateo, 52140 Metepec, Méx.</a>
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15066.230628266163!2d-99.6154728!3d19.2581006!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cd8bc8e37810d1%3A0xea5faa8a3b7ca153!2sSEMIT%20Equipo%20M%C3%A9dico!5e0!3m2!1ses!2smx!4v1690241349383!5m2!1ses!2smx" height="200" style="border:0;width: 100%;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
          </div>
           <br><br>
        </div>  
      </div>

      