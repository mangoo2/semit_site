<section class="section section-no-border section-light position-relative z-index-3 pt-0 m-0">
  <div class="container">
    <div class="row">
      <div class="col-lg-12"><br>
        <h4 style="color: #012d6a;">Mi cuenta</h4>
      </div>  
    </div>  

    <div class="row">
      <div class="col-lg-4">
        <a style="cursor:pointer;" href="<?php echo base_url() ?>Micuenta/mispedidos">
          <div style="border: solid #012d6a; border-radius: 15px;">
            <div class="row" style="padding: 12px;">
              <div class="col-lg-5" align="center">
                <div >
                  <img style="width: 70px;" src="<?php echo base_url()?>public/img/micuenta/4.png">
                </div>
              </div>
              <div class="col-lg-7" align="center">
                <span style="color: #012d6a; font-size: 26px;"><b>Mis<br>Pedidos</b></span>
              </div>
            </div>  
          </div>
        </a>  
      </div>
      <div class="col-lg-4">
        <div style="border: solid #012d6a; border-radius: 15px;">
          <div class="row" style="padding: 12px;">
            <div class="col-lg-5" align="center">
              <div >
                <img style="width: 70px;" src="<?php echo base_url()?>public/img/micuenta/3.png">
              </div>
            </div>
            <div class="col-lg-7" align="center">
              <span style="color: #012d6a; font-size: 26px;"><b>Servicio<br>al cliente</b></span>
            </div>
          </div>  
        </div>
      </div>
      <div class="col-lg-4">
        <div style="border: solid #012d6a; border-radius: 15px;">
          <div class="row" style="padding: 12px;">
            <div class="col-lg-5" align="center">
              <div >
                <img style="width: 70px;" src="<?php echo base_url()?>public/img/micuenta/5.png">
              </div>
            </div>
            <div class="col-lg-7" align="center">
              <span style="color: #012d6a; font-size: 26px;"><b>Mi<br>cuenta</b></span>
            </div>
          </div>  
        </div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-lg-4">
        <a style="cursor:pointer;" href="<?php echo base_url() ?>Micuenta/facturacion">
          <div style="border: solid #012d6a; border-radius: 15px;">
            <div class="row" style="padding: 12px;">
              <div class="col-lg-5" align="center">
                <div >
                  <img style="width: 70px;" src="<?php echo base_url()?>public/img/micuenta/6.png">
                </div>
              </div>
              <div class="col-lg-7" align="center">
                <span style="color: #012d6a; font-size: 26px;"><b>Facturación<br>electrónica</b></span>
              </div>
            </div>  
          </div>
        </a>  
      </div>
      <div class="col-lg-4">
        <a style="cursor:pointer;" href="<?php echo base_url() ?>Micuenta/misfavoritos">
          <div style="border: solid #012d6a; border-radius: 15px;">
            <div class="row" style="padding: 12px;">
              <div class="col-lg-5" align="center">
                <div >
                  <img style="width: 70px;" src="<?php echo base_url()?>public/img/micuenta/8.png">
                </div>
              </div>
              <div class="col-lg-7" align="center">
                <span style="color: #012d6a; font-size: 26px;"><b>Mis<br>favoritos</b></span>
              </div>
            </div>  
          </div>
        </a>
      </div>
      <div class="col-lg-4">
        <div style="border: solid #012d6a; border-radius: 15px;">
          <div class="row" style="padding: 12px;">
            <div class="col-lg-5" align="center">
              <div >
                <img style="width: 70px;" src="<?php echo base_url()?>public/img/micuenta/7.png">
              </div>
            </div>
            <div class="col-lg-7" align="center" >
              <span style="color: #012d6a; font-size: 26px;"><b>Mis<br>cotizaciones</b></span>
            </div>
          </div>  
        </div>
      </div>
    </div>
  </div>
</section>