<style>
    #contenedor-principal {
      display: flex;
      justify-content: center;
    }
    .contenedor-secundario {
      background-color: #d0d0d0;
      margin: 17px;
      border-radius: 12px;
    }
    .contenedor {
        margin: 17px;
    }

</style>

     <div role="main" class="main">
        <div style="background-image: url(<?php echo base_url()?>public/img/nosotrosp.png) !important;  background-color: #2E3136; background-size: cover; background-position: center; max-width: 100% !important;">
          <div class="row pt-3">
            <div class="col-md-10 mx-md-auto">
              <br><br>
              <br><br>
              <br><br>
              <br><br>
              <br><br>
              <h1 style="color:white; text-align: center; font-size: 72px; line-height: 88px;"><b>
                Empresa #1<br> en ofrecer Equipos Médicos</b>
              </h1>
              <br><br>
              <br><br>
              <br><br>
              <br><br>
              <br><br>
            </div>
          </div>

        </div>
        <div style="background-color:#062e5b">
          <div class="row pb-3">
            <div class="col-lg-12">
              <br><br>
              <h3 align="center" style="color: white;line-height: 36px;">Servicios y Equipos Médicos<br>Internacionales de Toluca S.A. de C.V.</h3>
              <br>
            </div>
          </div>
          
        </div>
        
        <div  style="background-color:#f8f8f8; width: 100%">
          <div class="container">
            <div class="row">
              <div class="col-lg-4">
                <br><br><br><br>
                <img style="width: 100%" src="<?php echo base_url() ?>public/img/nosotros.png">
                <br>
                <br><br><br><br>
              </div>
              <div class="col-lg-1">
              </div>
              <div class="col-lg-7">
                <br><br><br><br><br><br>
                <h1>Con más de 27 años en el mercado</h1>
                <div style="text-align: justify; font-size: 17px;">Servicios y Equipos Médicos Internacionales de Toluca S.A. de C.V. es una empresa 100% mexicana. orientada a la Ventas y Renta de Equipo Médico, Oxigeno Medicinal y Rehabilitación cuya misión es ser una empresa confiable, humanista y comprometida.
                </div>
                <br>
                <div style="text-align: justify; font-size: 17px;">
                Para SEMIT es de gran importancia la calidad y calidez humana y esto se logra con ayuda de nuestro amplio equipo de especialistas que en cada labor y contacto con el cliente fomentan el respeto.
              </div>
              </div>
            </div>    
          </div>
        </div>  
        <div>
          <div>
            <div class="row">
              <div class="col-lg-1">
              </div>
              <div class="col-lg-4">
                <br><br>
                <div style="margin-top:20px;
                  width:100%;
                  border-top: 4px solid #062e5b;">
                <h3 style="width: 117px;
                  font-size: 30px;
                  margin-top: -12px;
                  margin-left: 120px;
                  background: white; color: #062e5b">&nbsp;&nbsp;Misión&nbsp;&nbsp;</h3>
                </div>
                <div class="row">
                  <div class="col-lg-4">
                    <img style="width: 100%" src="<?php echo base_url() ?>public/img/flecha.jpeg">
                  </div>
                  <div class="col-lg-8">
                    <div>Somos un equipo confiable con una visión humanista, comprometidos a elevar su calidad de vida mediante la comercialización de productos innovadores, lo que nos permite un desarrollo personal y empresarial con base en la calidad y liderazgo en el servicio.</div>
                  </div>
                </div>
              </div>    
              <div class="col-lg-2">
              </div>
              <div class="col-lg-4">
                <br><br>
                <div style="margin-top:20px;
                  width:100%;
                  border-top: 4px solid #062e5b;">
                  <h3 style="width: 116px;
                  font-size: 30px;
                  margin-top: -12px;
                  margin-left: 274px;
                  background: white;
                  color: #062e5b;">&nbsp;&nbsp;Visión&nbsp;&nbsp;</h3>
                  <div class="row">
                    <div class="col-lg-4">
                      <img style="width: 100%" src="<?php echo base_url() ?>public/img/ojo.jpeg">
                    </div>
                    <div class="col-lg-8">
                      <div>Ser el centro de negocios más importantes en el país, en la comercialización de equipos, aparatos e instrumentos médicos, nacionales e internacionales, que brindan respuesta completa a las necesidades de nuestros clientes.</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>  
            <br><br>  
          </div>
        </div> 
        <!--- -->
        <link rel="stylesheet" href="<?php echo base_url() ?>carrusel/maxcdn.bootstrapcdn.com_bootstrap_3.4.1_css_bootstrap.min.css">
        <script src="<?php echo base_url() ?>carrusel/ajax.googleapis.com_ajax_libs_jquery_3.6.4_jquery.min.js"></script>
        <script src="<?php echo base_url() ?>carrusel/maxcdn.bootstrapcdn.com_bootstrap_3.4.1_js_bootstrap.min.js"></script>

        
        <div>
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
            <!-- <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
              <li data-target="#myCarousel" data-slide-to="3"></li>
            </ol> -->

          <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                  <img src="<?php echo base_url() ?>public/img/nts_baner/Sucursal-img-02.jpg" style="width:100%;">
                  <div style="position: absolute; top: 76px; left: 157px;">
                    <div style="font-size: 38px; color: white; font-weight: 600;">Sucursales Semit</div>
                    <div style="color: #ffffff; font-size: 16px; font-weight: 600; line-height: 1;">Conozca nuestras tiendas físicas, tenemos una cerca de usted</div><br>
                    <span style="color: #ffffff; font-size: 16px; font-weight: 600;">Toluca</span><br>
                    <span style="color: #ffffff; font-size: 16px; font-weight: 600;">(Alfredo del Mazo)</span><br>
                    <a style="color: #ffffff; font-size: 16px; font-weight: 600;" href="https://www.google.com/maps/place/SEMIT+Equipo+M%C3%A9dico/@19.3068983,-99.6312463,17z/data=!3m1!4b1!4m11!1m5!3m4!1s0x0:0xc54f648fd3a9774d!2sSEMIT+Equipo+M%C3%A9dico!11m1!2e1!3m4!1s0x0:0xc54f648fd3a9774d!8m2!3d19.3068983!4d-99.6312463" target="_blank" rel="noopener" tabindex="0"> <span class="elementor-icon-list-text">Boulevard Alfredo del Mazo No. 727 local 3 Plaza El Punto. Col. Científicos.</span> </a><br>
                    <a style="color: #ffffff; font-size: 16px; font-weight: 600;" href="tel:7224540212" tabindex="0"> <span>(722) 454 02 12</span></a>
                  </div>
                </div>
              
                <div class="item">
                  <img src="<?php echo base_url() ?>public/img/nts_baner/Sucursal-img-03.jpg" style="width:100%;">
                  <div style="position: absolute; top: 76px; left: 157px;">
                    <div style="font-size: 38px; color: white; font-weight: 600;">Sucursales Semit</div>
                    <div style="color: #ffffff; font-size: 16px; font-weight: 600; line-height: 1;">Conozca nuestras tiendas físicas, tenemos una cerca de usted</div><br>
                    <span style="color: #ffffff; font-size: 16px; font-weight: 600;">Toluca</span><br>
                    <span style="color: #ffffff; font-size: 16px; font-weight: 600;">(Jesús Carranza)</span><br>
                    <span style="color: #ffffff; font-size: 16px; font-weight: 600;"><a style="color: #ffffff;" href="https://www.google.com/maps/place/SEMIT+Equipo+M%C3%A9dico/@19.2705288,-99.6586182,15z/data=!4m5!3m4!1s0x0:0x299bc2b255ef5e48!8m2!3d19.270528!4d-99.6585843" target="_blank" rel="noopener" tabindex="-1"> <span class="elementor-icon-list-text">Jesús Carranza Sur, No. 323, (entre las Torres y Tollocan) Toluca México.</span></a></span><br>
                    <a style="color: #ffffff; font-size:16px; font-weight: 600;" href="tel:7224540216" tabindex="0"> <span class="elementor-icon-list-text">(722)&nbsp;454 02 16</span></a>
                  </div>
                </div>
                <div class="item ">
                  <img src="<?php echo base_url() ?>public/img/nts_baner/Sucursal-img-04.jpg" style="width:100%;">  
                  <div style="position: absolute; top: 76px; left: 157px;">
                    <div style="font-size: 38px; color: white; font-weight: 600;">Sucursales Semit</div>
                    <div style="color: #ffffff; font-size: 16px; font-weight: 600; line-height: 1;">Conozca nuestras tiendas físicas, tenemos una cerca de usted</div><br>
                    <span style="color: #ffffff; font-size: 16px; font-weight: 600;">Metepec</span><br>
                    <span style="color: #ffffff; font-size: 16px; font-weight: 600;">(Plaza San Pedro)</span><br>
                    <span style="color: #ffffff; font-size: 16px; font-weight: 600;"><a style="color: #ffffff;" href="https://www.google.com/maps/place/SEMIT+Equipo+M%C3%A9dico/@19.2581006,-99.6154728,15z/data=!4m5!3m4!1s0x0:0xea5faa8a3b7ca153!8m2!3d19.2581111!4d-99.6154628" target="_blank" rel="noopener" tabindex="0"> <span class="elementor-icon-list-text">Av. Benito Juárez, No. 528, Barrio San Mateo (a 300 m del Centro Médico Toluca)</span> </a></span><br>
                    <a style="color: #ffffff; font-size: 16px; font-weight: 600;" href="tel:7224540219" tabindex="0"> <span class="elementor-icon-list-text">(722) 454 02 19</span></a>
                  </div>
                </div>
                <div class="item">
                  <img src="<?php echo base_url() ?>public/img/nts_baner/fondo-img.jpg" style="width:100%;">
                  <div style="position: absolute; top: 76px; left: 157px;">
                    <div style="font-size: 38px; color: white; font-weight: 600;">Sucursales Semit</div>
                    <div style="color: #ffffff; font-size: 16px; font-weight: 600; line-height: 1;">Conozca nuestras tiendas físicas, tenemos una cerca de usted</div><br>
                    <span style="color: #ffffff; font-size: 16px; font-weight: 600;">Matriz Toluca</span><br>
                    <span style="color: #ffffff; font-size: 16px; font-weight: 600;">(Pino Suarez)</span><br>
                    <span style="color: #ffffff; font-size: 16px; font-weight: 600;"><a style="color: #ffffff;" href="https://www.google.com/maps/place/SEMIT+Equipo+M%C3%A9dico/@19.2803904,-99.6490494,15z/data=!4m2!3m1!1s0x0:0x58c9fafb033880b0?sa=X&amp;ved=2ahUKEwi7hcOVivb3AhV2K0QIHbMFDf8Q_BJ6BAhMEAU" target="_blank" rel="noopener" tabindex="0"> <span class="elementor-icon-list-text">José Maria Pino Suarez, No 722 (casi esquina Rafael Miguel) Toluca, México</span> </a></span><br>
                    <a style="color: #ffffff; font-size: 16px; font-weight: 600;" href="tel:7224542223" tabindex="0"> <span class="elementor-icon-list-text">(722) 454 22 23</span></a>
                  </div>
                </div>

              </div>

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span style="width: 30px;
    height: 30px;
    margin-top: -10px;
    font-size: 30px;
        position: absolute;
    top: 50%;
    z-index: 5;
    display: inline-block;" class="fa fa-angle-left"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span style="width: 30px;
    height: 30px;
    margin-top: -10px;
    font-size: 30px;
        position: absolute;
    top: 50%;
    z-index: 5;
    display: inline-block;" class="fa fa-angle-right"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>

        <!-- -->
        <br>
        <br>  
      </div>