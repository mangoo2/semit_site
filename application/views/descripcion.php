<br>
<style type="text/css">
	.circulo{
	    width: 12px;
	    height: 12px;
	    -moz-border-radius: 50%;
	    -webkit-border-radius: 50%;
	    border-radius: 50%;
	    background: #9ebf43;
	    color: #9ebf43;
	}
	.thumb-info .thumb-info-wrapper.thumb-info-wrapper-demos {
	    max-height: 360px;
	}
</style>
<?php 
    $idpro=0;
    $nombre='';
    $resumen='';
    $precio_con_iva=0.00;
    $img_file='https://adminsys.semit.mx/public/img/favICONSEMIT.png';
    $categoria=0;
    foreach ($resulpro as $x){ 
    	$idpro=$x->id;
        $nombre=$x->nombre;
        $resumen=$x->resumen;
        $precio_con_iva=$x->precio_con_iva;
        //$img_file='https://adminsys.semit.mx/uploads/productos/'.$x->file; 
        $categoria=$x->categoria;
    } 
    $cattxt='';
    $resulcate=$this->ModelGeneral->get_select('categoria',array('categoriaId'=>$categoria));
    foreach ($resulcate as $ca) {
      $cattxt=$ca->categoria;
    }

    ?>
<div class="container">

	<div class="row">
		<div class="col-lg-12">
			<h6 style="color:#012d6a"><b><?php echo 'CATEGORÍA - '.$cattxt.' - '.$nombre ?></b></h6>
			<div class="row">
				<div class="col-lg-5">
				   <table style="width: 100%">
				   	    <thead>
				   	    	<tr>
				   	    		<th style="width: 25%">
				   	    			<?php foreach ($resulprodll as $dl){ 
				   	    				$img_file='https://adminsys.semit.mx/uploads/productos/'.$dl->file; 
				   	    				//$img_file='http://localhost/semit_erp/uploads/productos/'.$dl->file; 
				   	    				?>
					   	    			<a style="cursor:pointer;" onclick="imagen_pro(<?php echo $dl->id ?>)">
					   	    				<img style="width: 90px; height: 90px;" src="<?php echo $img_file ?>">
					   	    			</a>
				   	    			<?php } ?>
				   	    		</th>
				   	    		<th style="width: 75%;">
				   	    			<div class="img_file">
					   	                <div class="appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200" data-appear-animation-duration="750" style="animation-delay: 200ms; margin-bottom: 26px;">
					                        <div class="portfolio-item hover-effect-1 text-center" >
					                          <span class="thumb-info thumb-info-no-zoom thumb-info-no-overlay thumb-info-no-bg" style="background: #f5f5f7;">
					                            <span class="thumb-info-wrapper thumb-info-wrapper-demos thumb-info-wrapper-link m-0 lazyloaded" style="height: 421px;; background: url(<?php echo $img_file ?>) white;background-size: contain;background-position: center;background-repeat: no-repeat;" 
					                              >
					                            </span>
					                          </span>
					                        </div>
					                    </div>			
				                    </div>
				   	    		</th>
				   	    	</tr>
				   	    </thead>
                        
                    </table>	
				</div>
				<div class="col-lg-4">
				    <h3 style="color:#012d6a"><?php echo $nombre ?></h3> 
				    <span style="line-height: 1.3em !important;"><?php echo $resumen ?></span>
				    <br>
				    <?php foreach ($resulpro_cart as $c){ ?>
				    	<div style="border-bottom: 2px solid #012d6a">
		                    <div class="row">
		                    	<div class="col-lg-10">
								   	 <div><b><?php echo $c->titulo ?></b></div>  
							    </div>
							    <div class="col-lg-2" align="right">
							    	<a class="btn_c1<?php echo $c->id ?>" style="cursor: pointer;" onclick="mostrar_text1(<?php echo $c->id ?>)"><i class="fa fa-plus" style="font-size: 22px; color: #9ebf43;"></i></a>
							    	<a class="btn_c2<?php echo $c->id ?>" style="cursor: pointer; display: none" onclick="mostrar_text2(<?php echo $c->id ?>)"><i class="fa fa-minus" style="font-size: 22px; color: #9ebf43;"></i></a>
							    </div>	
							</div>
							<div class="mostrar_txt_<?php echo $c->id ?>" style="display: none">
								<div class="row">
			                    	<div class="col-lg-12">
	                                    <?php echo $c->descripcion ?>
			                    	</div>
			                    </div>		
		                    </div>
						</div>
					<?php } ?>

				</div>
				<div class="col-lg-3">
				    <div style="border: solid #012d6a; border-radius: 15px; padding: 20px;">
		              <div class="row">
		                <div class="col-lg-12">
		                    <h5 align="center" style="background: #9ebf43; color: white; border-radius: 11px; font-size: 19px; padding: 3px;">$<?php echo $precio_con_iva; ?></h5> 
		                    <p style="color: black"><b>Producto Disponible</b> <span class="circulo">----</span></p>
		                </div>
		              </div>  
		              <div class="row">
		                <div class="col-lg-8">
		                	<span style="color: black"><b>Costo de envío</b></span>
		                </div>
		                <div class="col-lg-4">
		                	<h5 align="center" style="background: #9ebf43; color: white; border-radius: 11px; font-size: 19px; padding: 3px;">$0.00</h5> 
		                </div>	
		              </div> 
		              <div class="row">
		                <div class="col-lg-12">
		                	<div class="form-group row">
		                		<div class="col-lg-2"></div>
						        <label class="col-lg-3 col-form-label form-control-label line-height-9 pt-2 text-2" style="color: black"><b>Cantidad</b></label>
						        <div class="col-lg-5 has-success">
						            <select class="form-select form-control h-auto py-2" id="cantidad">
						        		<option value="1">1</option>
						        		<option value="2">2</option>
						        		<option value="3">3</option>
						        		<option value="4">4</option>
						        		<option value="5">5</option>
						        		<option value="6">6</option>
						        		<option value="7">7</option>
						        		<option value="8">8</option>
						        		<option value="9">9</option>
						        		<option value="10">10</option>
						        		<option value="11">11</option>
						        		<option value="12">12</option>
						        		<option value="13">13</option>
						        		<option value="14">14</option>
						        		<option value="15">15</option>
						        		<option value="16">16</option>
						        		<option value="17">17</option>
						        		<option value="18">18</option>
						        		<option value="19">19</option>
						        		<option value="20">20</option>
						      		</select>
						        </div>
					        </div>
		                </div>	
		              </div>  
		              <div class="row">
		                <div class="col-lg-12">
		                	<?php if($idcliente!=0){ ?>
		                	<a class="btn w-100 mb-2 btn-rounded" style="background: #9ebf43; color: white;" onclick="add_carrito(<?php echo $idpro ?>)">Agregar al carrito</a>
		                    <?php }else{
		                    	echo '<br>';
		                    } ?>
		                	<a class="btn w-100 mb-2 btn-rounded" style="background: #4dc2b3; color: white;">Realizar cotización</a>
		                	<?php if($idcliente!=0){ ?>
		                	<a class="btn w-100 mb-2 btn-rounded" style="background: #9ebf43; color: white;" onclick="add_favoritos_d(<?php echo $idpro ?>)">Agregar a favoritos</a>
		                    <?php }else{
		                    	echo '<br><br>';
		                    } ?>
		                </div>	
		              </div> 	
		            </div>
				</div>
			</div>

		</div>
	</div>
</div>
<br><br>