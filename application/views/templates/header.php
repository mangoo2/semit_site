<!DOCTYPE html>
<html lang="en">
  <head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Venta y renta de equipo médico | Oxígeno medicinal | Toluca</title>  

    <meta name="keywords" content="WebSite Template" />
    <meta name="description" content="Venta y renta de equipo médico">
    <meta name="author" content="mangoo">
    <meta name="theme-color" content="#012d6a" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url(); ?>public/img/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>public/img/favicon.ico">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts  -->
    <link id="googleFonts" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7CShadows+Into+Light&display=swap" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/animate/animate.compat.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/magnific-popup/magnific-popup.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/theme-elements.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/theme-blog.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/theme-shop.css">
   
    <!-- Current Page CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/circle-flip-slideshow/css/component.css">

    <!-- Skin CSS -->
    <link id="skinCSS" rel="stylesheet" href="<?php echo base_url(); ?>css/skins/default.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/sweealert/sweetalert.css">
    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/custom.css">
    <link href="<?php echo base_url(); ?>js/confirm/jquery-confirm.min.css" type="text/css" rel="stylesheet">
    
    <!-- Head Libs -->
    <script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.min.js"></script>
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
    <!--<script type="text/javascript" src="https://openpay.s3.amazonaws.com/openpay.v1.min.js"></script>-->
    <!--<script type='text/javascript' src="https://openpay.s3.amazonaws.com/openpay-data.v1.min.js"></script>-->
    
    
    <script src="https://www.paypal.com/sdk/js?client-id=<?php if(isset($client_id_paypay)){ echo $client_id_paypay;}else{ echo 'xxxx';}?>&currency=MXN"></script>
  </head>

  <style type="text/css">
        body {
            font-family: Galano, Helvetica, Arial, sans-serif !important;
        }
        
        @media (max-width: 575px){
            .img_logo1{
                display: none !important;
            }
            
            .img_logo2{
                display: block !important;
            }     
            /*.input-group {
                position: relative;
                display: flex;
                flex-wrap: inherit;
                align-items: stretch;
                width: 100%;
            }*/
            /*.search-with-select .search-form-wrapper {
                display: flex !important;
                visibility: visible !important;
                opacity: 1 !important;
                position: absolute;
                top: 67px !important;
                left: 50%;
                transform: translate3d(-41%, 0, 0) !important;
                width: 300px;
                background-color: #012d6a;
                padding: 0.2rem;
                border-radius: 28px;
                transition: ease opacity 300ms;
            }*/

            #headerSearch{
                width: 258px !important;
            }

           .search-with-select .search-form-wrapper {
                display: flex !important;
                visibility: visible !important;
                opacity: 1 !important;
                position: absolute;
                top: 65px;
                left: 50%;
                transform: translate3d(-33%, 0, 0);
                width: 300px;
                background-color: #012d6a;
                padding: 0.2rem;
                border-radius: 28px;
                transition: ease opacity 300ms;
            }
            .input-group {
                position: relative;
                display: flex;
                flex-wrap: inherit !important;
                align-items: stretch !important;
                width: 100%;
            }

            .search-with-select .search-form-wrapper:before {
                content: '';
                position: absolute;
                bottom: 100%;
                left: 50%;
                border-bottom: 6px solid #012d6a !important;
                border-left: 6px solid transparent;
                border-right: 6px solid transparent;
                transform: translate3d(-50%, 0, 0);
            }

            #header .header-nav-features {
                position: relative;
                padding-left: 0px !important;
                margin-left: 10px;
            }

            .search-form-wrapper{
                width: 270px !important;
                position: absolute !important;
                top: 46px !important;
                left: 63px !important;
            }
        }

        #header .header-body {
            display: flex;
            flex-direction: column;
            background: #012d6a;
            transition: min-height 0.3s ease;
            width: 100%;
            border-top: 0px solid #EDEDED !important;
            /* border-bottom: 1px solid transparent; */
            z-index: 1001;
        }

        .pestana_txtt{
            left: 8px !important;
        }

        #header .header-row {
            display: flex;
            flex-grow: 1;
            align-items: flex-start !important;
            align-self: stretch;
            max-height: 100%;
        }

        #footer a:not(.btn):not(.no-footer-css):hover {
            text-decoration: none;
            color: #929292 !important;
        }
  </style>
  <body data-plugin-page-transition> 
    <div class="body">
  