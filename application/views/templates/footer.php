<footer id="footer"  style="margin-top: -10px; background: #ffffff !important; border-top: 4px solid #ffffff!important;">
        <div class="container">
          <div class="row py-5 my-4" style="margin-bottom: 0rem!important">
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
              <img src="<?php echo base_url() ?>public/img/SEMIT.jpg"><br><br>
              <p class="pe-1" style="text-align: justify;">Para SEMIT es de gran importancia la calidad y calidez humana y esto se logra con ayuda de nuestro amplio equipo de especialistas que en cada labor y contacto con el cliente fomentan el respeto</p>
              
            </div> 

            <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
              <h5 class="text-3" style="color:black">Categorías</h5>
              <ul class="list-unstyled mb-0">
                <?php foreach ($categorias as $x){ ?>
                  <?php echo '<li class="">
                        <p class="text-2 mb-0" style="margin-bottom: -10px !important;"><a style="cursor:pointer" onclick="categoria_footer_get('.$x->categoriaId.')"><img style="width:9px;" src="'.base_url().'public/img/favicon.ico">&nbsp;&nbsp;'.$x->categoria.' </a></p>
                      </li>'?>
                <?php } ?>
              </ul>
            </div>
            <div class="col-md-6 col-lg-3 mb-4 mb-md-0">
              <div class="contact-details">
                <h5 class="text-3 mb-3" style="color:black">Mapa de sitio</h5>
              </div>
            </div>
            <div class="col-md-6 col-lg-2">
              <h5 class="text-3 mb-3" style="color:black">Contáctos</h5>
              <ul class="list list-icons list-icons-lg">
                <li class="mb-1" style="padding-left: 0px !important;">Matriz Toluca<a href="https://www.google.com/maps/place/SEMIT+Equipo+Médico/@19.2803904,-99.6490494,15z/data=!4m6!3m5!1s0x85cd89911c240001:0xd94eef4aea0d2e93!8m2!3d19.2803917!4d-99.6490491!16s%2Fg%2F1tfqg3xg?entry=ttu" target="_block"><img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_09.png"></a> <a href="tel:7224542223"><img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_01.png"></a></li>
                <li class="mb-1" style="padding-left: 0px !important;">Alfredo del Mazo<a href="https://www.google.com/maps/place/SEMIT+Equipo+Médico/@19.3068983,-99.6312463,17z/data=!3m1!4b1!4m11!1m5!3m4!1s0x0:0xc54f648fd3a9774d!2sSEMIT+Equipo+Médico!11m1!2e1!3m4!1s0x0:0xc54f648fd3a9774d!8m2!3d19.3068983!4d-99.6312463" target="_block"> <img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_09.png"></a> <a href="tel:7224540212"><img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_01.png"></a></li>
                <li class="mb-1" style="padding-left: 0px !important;">Jesús Carranza<a href="https://www.google.com/maps/place/SEMIT+Equipo+Médico/@19.2705288,-99.6586182,15z/data=!4m6!3m5!1s0x85d276224f73f085:0x299bc2b255ef5e48!8m2!3d19.2705288!4d-99.6586182!16s%2Fg%2F1txfphrl?entry=ttu" target="_block"> <img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_09.png"></a> <a href="tel:7224540216"><img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_01.png"></a></li>
                <li class="mb-1" style="padding-left: 0px !important;">Plaza San Pedro<a href="https://www.google.com/maps/place/SEMIT+Equipo+Médico/@19.2581006,-99.6154728,15z/data=!4m5!3m4!1s0x0:0xea5faa8a3b7ca153!8m2!3d19.2581111!4d-99.6154628" target="_block"> <img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_09.png"></a> <a href="tel:7224540219"><img style="width: 16px;" src="<?php echo base_url() ?>public/img/footer/Image_01.png"></a></li>
                <li class="mb-1" style="padding-left: 0px !important;">
                  <a href="https://www.facebook.com/SemitOficial" target="_block"><img style="width: 27px;" src="<?php echo base_url() ?>public/img/footer/redes/3.svg"></a>
                  <a href="https://www.instagram.com/equipomedicosemit/" target="_block"><img style="width: 27px;" src="<?php echo base_url() ?>public/img/footer/redes/1.svg"></a>
                  <a href="https://www.youtube.com/@semitequipomedico4235" target="_block"><img style="width: 27px;" src="<?php echo base_url() ?>public/img/footer/redes/2.svg"></a>
                </i>
                <!-- href="https://goo.gl/maps/ZXozp3CzbHP4bU7HA" -->
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-2">
              <img style="width: 183px" src="<?php echo base_url() ?>public/img/footer/Image_06.png">
            </div>
            <div class="col-lg-2">
              <img style="width: 183px; margin-left: 13px;" src="<?php echo base_url() ?>public/img/footer/Image_07.png">
            </div>
            <div class="col-lg-3">
              <img style="width: 183px; margin-left: 31px; margin-top: 13px;" src="<?php echo base_url() ?>public/img/footer/Image_08.png">
            </div>
            <div class="col-lg-1">
            </div>  
            <div class="col-lg-4">
              <h6 style="color: black; margin-top: 18px;"><a style="color: black;" href="<?php echo base_url() ?>Inicio/aviso_de_privacidad">Aviso de privacidad</a> | Todos los derechos reservados | <?php echo date('Y') ?></h6>
            </div>  
          </div>
        </div>      
        <!-- <div class="footer-copyright">
          <div class="container py-2">
            <div class="row py-4">
              <div class="col-lg-1 d-flex align-items-center justify-content-center justify-content-lg-start mb-2 mb-lg-0">
                <a href="index.html" class="logo pe-0 pe-lg-3">
                  <img alt="Porto Website Template" src="img/logo-footer.png" class="opacity-5" width="49" height="22" data-plugin-options="{'appearEffect': 'fadeIn'}">
                </a>
              </div>
              <div class="col-lg-7 d-flex align-items-center justify-content-center justify-content-lg-start mb-4 mb-lg-0">
                <p>© Copyright 2023. All Rights Reserved.</p>
              </div>
              <div class="col-lg-4 d-flex align-items-center justify-content-center justify-content-lg-end">
                <nav id="sub-menu">
                  <ul>
                    <li><i class="fas fa-angle-right"></i><a href="page-faq.html" class="ms-1 text-decoration-none"> FAQ's</a></li>
                    <li><i class="fas fa-angle-right"></i><a href="sitemap.html" class="ms-1 text-decoration-none"> Sitemap</a></li>
                    <li><i class="fas fa-angle-right"></i><a href="contact-us.html" class="ms-1 text-decoration-none"> Contact Us</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div> -->
      </footer>
    </div>

    <!-- Vendor -->
    <script src="<?php echo base_url(); ?>vendor/plugins/js/plugins.min.js"></script>

    <!-- Theme Base, Components and Settings -->
    <script src="<?php echo base_url(); ?>js/theme.js"></script>

    <!-- Circle Flip Slideshow Script -->
    <script src="<?php echo base_url(); ?>vendor/circle-flip-slideshow/js/jquery.flipshow.min.js"></script>
    <!-- Current Page Views -->
    <script src="<?php echo base_url(); ?>js/views/view.home.js"></script>

    <!-- Theme Custom -->
    <script src="<?php echo base_url(); ?>js/custom.js"></script>
    <script src="<?php echo base_url(); ?>js/sweealert/sweetalert.min.js"></script>
    <!-- Theme Initialization Files -->
    <script src="<?php echo base_url(); ?>js/theme.init.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url(); ?>js/confirm/jquery-confirm.min.js"></script>
    <script src="<?php echo base_url(); ?>public/js/catalogo.js"></script>
  </body>
</html>