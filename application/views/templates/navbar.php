<style type="text/css">
  #header .header-nav-main nav > ul > li.dropdown .dropdown-menu li:hover > a, #header .header-nav-main nav > ul > li.dropdown .dropdown-menu li:focus > a, #header .header-nav-main nav > ul > li.dropdown .dropdown-menu li.active > a, #header .header-nav-main nav > ul > li.dropdown .dropdown-menu li:active > a, #header .header-nav-main nav > ul > li.dropdown .dropdown-menu li.open > a, #header .header-nav-main nav > ul > li.dropdown .dropdown-menu li.accessibility-open > a {
    color: #777 !important;
  }

  .fondo_transparente{
    background-color: #152342;
    opacity: .4;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    -webkit-transition: opacity .4s;
  }
</style>
<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyStartAt': 45, 'stickySetTop': '-45px', 'stickyChangeLogo': true}">
        <div class="header-body">
          <div class="header-container container">
            <div class="header-row">
              <!--<div class="header-column">
                <div class="header-row">
                  <div class="header-logo">
                    <a href="index.html">
                      <img alt="Porto" width="100" height="48" data-sticky-width="82" data-sticky-height="40" data-sticky-top="25" src="<?php // echo base_url(); ?>public/img/SEMIT.jpg">
                    </a>
                  </div>
                </div>
              </div> -->
              <div class="header-column justify-content-end">
                <div class="header-row pt-3" style="justify-content: center !important;">
                  <nav class="header-nav-top">
                    <ul class="nav nav-pills">
                      <!-- <li class="nav-item nav-item-anim-icon d-none d-md-block">
                        <a class="nav-link ps-0" href="about-us.html"><i class="fas fa-angle-right"></i> Sobre nosotros</a>
                      </li>
                      <li class="nav-item nav-item-anim-icon d-none d-md-block">
                        <a class="nav-link" href="contact-us.html"><i class="fas fa-angle-right"></i> Contact Us</a>
                      </li> -->
                      <li style="line-height: 2px !important;">
                        <a href="<?php echo base_url() ?>Inicio">
                          <img class="img_logo1" style="" alt="Porto" width="135" height="55" data-sticky-width="82" data-sticky-height="40" data-sticky-top="25" src="<?php echo base_url(); ?>public/img/logo_semit.png">
                          <img class="img_logo2" style="display: none" width="22" height="25" src="<?php echo base_url(); ?>public/img/fav.png"><br><br>
                        </a>
                      </li>
                      <li style="line-height: 2px !important;">
                        <div class="header-nav-feature ps-lg-5 pe-lg-4">
                          <form role="search" action="page-search-results.html" method="get" wtx-context="355C6C8D-B588-473A-82CA-F757A66AD706">
                            <div class="search-with-select">
                              <div class="search-form-wrapper input-group" style="width: 100%;">
                                <input class="form-control text-1" id="headerSearch" name="q" type="search" value="" placeholder="Buscar tus productos aquí" wtx-context="0934EA35-3AAA-4763-84C6-2CE5BFD162E2" style="border-radius: 0.5rem 0 0 0.5rem !important;" oninput="categoria_data()">
                                <div class="search-form-select-wrapper" style="border-radius: 0 0.5rem 0.5rem 0; ">
                                  <button class="btn" type="submit" aria-label="Search">
                                    <i class="icons icon-magnifier header-nav-top-icon text-color-dark"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </form>
                        </div>
                      </li>  
                      </li style="line-height: 2px !important;">
                      <li class="nav-item nav-item-left-border nav-item-left-border-remove nav-item-left-border-md-show" style="line-height: 2px !important;">
                        <span class="ws-nowrap" style="color: #fff !important;"><a href="https://wa.me/7224542223"><img style="width:19px;" src="<?php echo base_url() ?>public/img/whatsapp.svg"></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="tel:7224542223"><img style="width:19px;" src="<?php echo base_url() ?>public/img/llamada-telefonica.svg"></a> Toluca:(722) 454 2223</span>
                          
                    
                        <?php if($nombre!=''){ ?>
    
                        <?php }else{ ?>
                          <button class="btn" style="padding: 0.533rem 0.2rem !important;">
                            <!-- <i class="fas fa-shopping-cart header-nav-top-icon" style="color: #fff;"></i> -->
                            <img style="width: 17px;" src="<?php echo base_url(); ?>public/img/carrito.png">
                          </button>
                          <a class="btn" href="<?php echo base_url() ?>Inicio/login" style="padding: 0.533rem 0.2rem !important;">
                           <!-- <i class="fas fa-user header-nav-top-icon" style="color: #fff;"></i>  -->
                           <img style="width: 16px;" src="<?php echo base_url() ?>public/img/user_white.png">
                          </a>
                        <?php } ?>
                      </li>
                    </ul>
                  </nav>
                  
                  <?php if($nombre!=''){ ?>
                  <div class="header-nav-features" style="padding-left: 0px !important;">
                    <div class="header-nav-feature header-nav-features-cart d-inline-flex ms-2">
                      <div style="background: red;
                      padding: 2px;
                      border-radius: 12px;
                      position: absolute;
                      top: -13px;
                      left: 15px;
                      color:white;
                      font: -webkit-small-control;
                      width: 115%;
                      text-align: center;
                      " class="text_total">0</div>
                      <a class="header-nav-features-toggle" aria-label="" style="cursor: pointer;"><span style="color:white; font-size: 17px;" onclick="get_productos()">
                        <img style="width: 17px;" src="<?php echo base_url(); ?>public/img/carrito.png">
                      </a>
                      
                      <div class="header-nav-features-dropdown" id="headerTopCartDropdown" style="padding-right: 3px;">
                          <div class="text_productos">
                            <h4>Cargando datos...</h4>
                          </div>

                      </div>
                    </div>
                  </div>
                      <div class="fondo_trans">  
                      </div>
                  <div class="header-nav-features" style="padding-left: 0px !important;">
                    <div class="header-nav-feature header-nav-features-cart d-inline-flex ms-2">
                      <a href="#" class="header-nav-features-toggle" aria-label=""><span style="color:white; font-size: 17px;">
                        <?php echo $nombre ?></span> <i class="fas fa-chevron-down" style="color:white"></i>
                      </a>
                      <div class="header-nav-features-dropdown" id="headerTopCartDropdown">
                        <ol class="mini-products-list">
                          <li class="item">
                            <a href="#" title="Camera X1000" class="product-image"><img src="<?php echo base_url() ?>public/img/product-1.jpg" alt="Camera X1000"></a>
                            <div class="product-details">
                              <p class="product-name" style="font-size: 23px; color:#012d6a;">
                                Bienvenido
                              </p>
                              <p class="qty-price" style="font-size: 14px;">
                                <b> <?php echo $nombre ?></b>
                              </p>
                            </div>
                          </li>
                        </ol>
                        <div class="actions">
                          <a style="width: 100% !important; background-color: #012d6a; color: white" class="btn" href="<?php echo base_url() ?>Micuenta">Mi cuenta</a><br>
                          <a style="width: 100% !important; background-color: #469ddc; color: white;" class="btn" href="<?php echo base_url();?>Inicio/logout">
                          Cerrar sesión</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                  <!-- <div class="header-nav-features">
                    <div class="header-nav-feature header-nav-features-cart d-inline-flex ms-2">
                      <a href="#" class="header-nav-features-toggle" aria-label="">
                        <i class="fas fa-search header-nav-top-icon" style="color:white"></i>
                      </a>
                      <div class="header-nav-features-dropdown" id="headerTopCartDropdown">
                        <ol class="mini-products-list">
                          <li class="item">
                            <a href="#" title="Camera X1000" class="product-image"><img src="img/products/product-1.jpg" alt="Camera X1000"></a>
                            <div class="product-details">
                              <p class="product-name">
                                <a href="#">Camera X1000 </a>
                              </p>
                              <p class="qty-price">
                                 1X <span class="price">$890</span>
                              </p>
                              <a href="#" title="Remove This Item" class="btn-remove"><i class="fas fa-times"></i></a>
                            </div>
                          </li>
                        </ol>
                        <div class="totals">
                          <span class="label">Total:</span>
                          <span class="price-total"><span class="price">$890</span></span>
                        </div>
                        <div class="actions">
                          <a class="btn btn-dark" href="#">View Cart</a>
                          <a class="btn btn-primary" href="#">Checkout</a>
                        </div>
                      </div>
                    </div>
                  </div> -->
                </div>
                <div class="header-row">
                  <div class="header-nav pt-1" style="justify-content: center !important;">
                    <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
                      <nav class="collapse">
                        <ul class="nav nav-pills" id="mainNav">
                          <?php /*
                          <li>
                            <div class="row">
                            <div class="form-group col">
                              <div class="position-relative">
                                <i class="fas fa-bars text-color-black text-3 position-absolute left-15 top-50pct transform3dy-n50 z-index-1"></i>
                                <div class="custom-select-1">
                                      <select class="form-select form-control h-auto" id="idcategoria" style="width: 190px; padding-left: 41px;" onchange="categoria_get()">
                                        <option value="0" <?php if($categorianm==0) echo 'selected'?>>Categorías</option>
                                        <?php foreach ($categorias as $x){ ?>
                                          <option value="<?php echo $x->categoriaId ?>" <?php if($x->categoriaId==$categorianm) echo 'selected'?>><?php echo $x->categoria ?></option>
                                        <?php } ?>
                                      </select>
                                    </div>
                                </div>
                            </div>
                          </div>
                          </li> */ ?>
                          <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle" style="background: white;color: black;width: 135px;">
                              <i class="fas fa-bars text-color-black text-3 left-15 top-50pct"></i>&nbsp;&nbsp;&nbsp;&nbsp;Categorías
                            </a>
                            <ul class="dropdown-menu" style="border-radius: 23px;background: #012d6a; border-top-color: #012d6a;">
                              <?php foreach ($categorias as $x){ ?>
                              <li class="dropdown-submenu" style="border-top-color: #012d6a; border-radius: 15px;">
                                <a class="dropdown-item" onclick="categoria_get(<?php echo $x->categoriaId ?>)" style="color: #fff; border-bottom: 1px solid #012d6a00; cursor: pointer;"><?php echo $x->categoria ?></a>
                              </li>
                              <?php } ?>
                              <input type="hidden" id="idcategoria" value="<?php echo $categorianm ?>">
                            </ul>
                          </li>    
                          <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle active" href="<?php echo base_url() ?>Inicio">
                              Home
                            </a>
                          </li>
                          <li class="dropdown dropdown-mega">
                            <a class="dropdown-item dropdown-toggle" href="<?php echo base_url() ?>Nosotros">
                              Nosotros
                            </a>
                          </li>
                          <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle" href="<?php echo base_url() ?>Productos">
                              Productos
                            </a>
                          </li>
                          <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle" href="<?php echo base_url() ?>Losmasvendidos">
                              Los más vendidos 
                            </a>
                          </li>
                          <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle" href="<?php echo base_url() ?>Servicios">
                              Servicios
                            </a>
                            <!-- <ul class="dropdown-menu">
                              <li class="dropdown-submenu">
                                <a class="dropdown-item" href="#">Large Image</a>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="blog-large-image-full-width.html">Full Width</a></li>
                                  <li><a class="dropdown-item" href="blog-large-image-sidebar-left.html">Left Sidebar</a></li>
                                  <li><a class="dropdown-item" href="blog-large-image-sidebar-right.html">Right Sidebar </a></li>
                                  <li><a class="dropdown-item" href="blog-large-image-sidebar-left-and-right.html">Left and Right Sidebar</a></li>
                                </ul>
                              </li>
                              <li class="dropdown-submenu">
                                <a class="dropdown-item" href="#">Medium Image</a>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="blog-medium-image-sidebar-left.html">Left Sidebar</a></li>
                                  <li><a class="dropdown-item" href="blog-medium-image-sidebar-right.html">Right Sidebar </a></li>
                                </ul>
                              </li>
                              <li class="dropdown-submenu">
                                <a class="dropdown-item" href="#">Grid</a>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="blog-grid-4-columns.html">4 Columns</a></li>
                                  <li><a class="dropdown-item" href="blog-grid-3-columns.html">3 Columns</a></li>
                                  <li><a class="dropdown-item" href="blog-grid-full-width.html">Full Width</a></li>
                                  <li><a class="dropdown-item" href="blog-grid-no-margins.html">No Margins</a></li>
                                  <li><a class="dropdown-item" href="blog-grid-no-margins-full-width.html">No Margins Full Width</a></li>
                                  <li><a class="dropdown-item" href="blog-grid-sidebar-left.html">Left Sidebar</a></li>
                                  <li><a class="dropdown-item" href="blog-grid-sidebar-right.html">Right Sidebar </a></li>
                                  <li><a class="dropdown-item" href="blog-grid-sidebar-left-and-right.html">Left and Right Sidebar</a></li>
                                </ul>
                              </li>
                              <li class="dropdown-submenu">
                                <a class="dropdown-item" href="#">Masonry</a>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="blog-masonry-4-columns.html">4 Columns</a></li>
                                  <li><a class="dropdown-item" href="blog-masonry-3-columns.html">3 Columns</a></li>
                                  <li><a class="dropdown-item" href="blog-masonry-full-width.html">Full Width</a></li>
                                  <li><a class="dropdown-item" href="blog-masonry-no-margins.html">No Margins</a></li>
                                  <li><a class="dropdown-item" href="blog-masonry-no-margins-full-width.html">No Margins Full Width</a></li>
                                  <li><a class="dropdown-item" href="blog-masonry-sidebar-left.html">Left Sidebar</a></li>
                                  <li><a class="dropdown-item" href="blog-masonry-sidebar-right.html">Right Sidebar </a></li>
                                </ul>
                              </li>
                              <li class="dropdown-submenu">
                                <a class="dropdown-item" href="#">Timeline</a>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="blog-timeline.html">Full Width</a></li>
                                  <li><a class="dropdown-item" href="blog-timeline-sidebar-left.html">Left Sidebar</a></li>
                                  <li><a class="dropdown-item" href="blog-timeline-sidebar-right.html">Right Sidebar </a></li>
                                </ul>
                              </li>
                              <li class="dropdown-submenu">
                                <a class="dropdown-item" href="#">Single Post</a>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="blog-post.html">Full Width</a></li>
                                  <li><a class="dropdown-item" href="blog-post-slider-gallery.html">Slider Gallery</a></li>
                                  <li><a class="dropdown-item" href="blog-post-image-gallery.html">Image Gallery</a></li>
                                  <li><a class="dropdown-item" href="blog-post-embedded-video.html">Embedded Video</a></li>
                                  <li><a class="dropdown-item" href="blog-post-html5-video.html">HTML5 Video</a></li>
                                  <li><a class="dropdown-item" href="blog-post-blockquote.html">Blockquote</a></li>
                                  <li><a class="dropdown-item" href="blog-post-link.html">Link</a></li>
                                  <li><a class="dropdown-item" href="blog-post-embedded-audio.html">Embedded Audio</a></li>
                                  <li><a class="dropdown-item" href="blog-post-small-image.html">Small Image</a></li>
                                  <li><a class="dropdown-item" href="blog-post-sidebar-left.html">Left Sidebar</a></li>
                                  <li><a class="dropdown-item" href="blog-post-sidebar-right.html">Right Sidebar </a></li>
                                  <li><a class="dropdown-item" href="blog-post-sidebar-left-and-right.html">Left and Right Sidebar</a></li>
                                </ul>
                              </li>
                              <li class="dropdown-submenu">
                                <a class="dropdown-item" href="#">Post Comments</a>
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" href="blog-post.html#comments">Default</a></li>
                                  <li><a class="dropdown-item" href="blog-post-comments-facebook.html#comments">Facebook Comments</a></li>
                                  <li><a class="dropdown-item" href="blog-post-comments-disqus.html#comments">Disqus Comments</a></li>
                                </ul>
                              </li>
                            </ul> -->
                          </li>
                          <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle" href="<?php echo base_url() ?>Blog">
                              Blog
                            </a>
                          </li>
                          <li class="dropdown">
                            <a class="dropdown-item dropdown-toggle" href="<?php echo base_url() ?>Inicio/contacto">
                              Contacto
                            </a>
                          </li>
                        </ul>
                      </nav>
                    </div>
<!--                     <ul class="header-social-icons social-icons d-none d-sm-block">
                      <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                      <li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                      <li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul> -->
                    <button class="btn header-btn-collapse-nav" data-bs-toggle="collapse" data-bs-target=".header-nav-main nav">
                      <i class="fas fa-bars"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>