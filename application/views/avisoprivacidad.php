<!DOCTYPE html>
<html lang="en">
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Venta y renta de equipo médico | Oxígeno medicinal | Toluca</title>  

		<meta name="keywords" content="WebSite Template" />
		<meta name="description" content="Porto - Multipurpose Website Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>public/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?php echo base_url(); ?>public/img/favicon.ico">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link id="googleFonts" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400&display=swap" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/animate/animate.compat.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/theme-elements.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/theme-blog.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/theme-shop.css">

		<!-- Skin CSS -->
		<link id="skinCSS" rel="stylesheet" href="<?php echo base_url(); ?>css/skins/default.css">
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/sweealert/sweetalert.css">
		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.min.js"></script>
        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
	</head>
	<style type="text/css">
		@media (max-width: 575px){
			.img_logo{
				width: 180px !important;
			    height: 91% !important;
			    position: absolute !important;
			    top: 4px !important;
			    left: 106px !important;
			}
			.input_codigo{
				width: 13% !important;
			}
			.col-lg-2 {
			    flex: 0 0 auto !important;
			    width: 16.66666667% !important;
			}
			.input_c{
                width: 100% !important;
			}
		}
        
        @media (min-width: 1200px){
		    #footer {
			    background: #212529;
			    border-top: 4px solid #212529;
			    font-size: 0.9em;
			    margin-top: 177px !important;
			    padding: 0;
			    position: relative;
			    clear: both;
			}
		}

		body {
            font-family: Galano, Helvetica, Arial, sans-serif !important;
        }
	</style>
	<style type="text/css">
		.vd_red{
			color: red !important;
		}
		.vd_green{
            color: green !important; 
		} 
	</style>
	<body data-plugin-page-transition>

		<div class="body">
			<div role="main" class="main">
				<section class="page-header page-header-modern bg-color-light-scale-1 page-header-lg" style="padding: 12px 0; background: #012d6a !important; ">
					<div class="container">
						<div class="row">
							<div class="col-md-12 align-self-center p-static order-2">
								<a href="<?php echo base_url() ?>Inicio"><img class="img_logo" style="width: 157px; height: 83%; position: absolute; top: 4px; left: 62px;" alt="Porto" width="100" height="48" data-sticky-width="82" data-sticky-height="40" data-sticky-top="25" src="<?php echo base_url(); ?>public/img/logo_semit.png"></a><br><br>
							</div>
						</div>
					</div>
				</section>
				<div class="container py-4">
					<div>
						<?php echo $avisoprivacidad ?> 
					</div>
				</div>
			</div>
			<footer id="footer"  style="background: #e6e6e6 !important; border-top: 4px solid #ffffff !important;">
		        <div class="container">

		          <div class="row">
		            <div class="col-lg-2">
		              <img style="width: 183px" src="<?php echo base_url() ?>public/img/footer/Image_06.png">
		            </div>
		            <div class="col-lg-2">
		              <img style="width: 164px; margin-left: 13px; margin-top: 7px;" src="<?php echo base_url() ?>public/img/footer/Image_07.png">
		            </div>
		            <div class="col-lg-3">
		              <img style="width: 183px; margin-left: 31px; margin-top: 13px;" src="<?php echo base_url() ?>public/img/footer/Image_08.png">
		            </div>
		            <div class="col-lg-1">
		            </div>  
		            <div class="col-lg-4">
		              <h6 style="color: black; margin-top: 18px;">Aviso de privacidad | Todos los derechos reservados | <?php echo date('Y') ?></h6>
		            </div>  
		          </div>
		        </div>      
            </footer>
		</div>
        
        <script src="<?php echo base_url();?>js/jquery-3.5.1.min.js"></script>
		<!-- Vendor -->
 		<script src="<?php echo base_url(); ?>vendor/plugins/js/plugins.min.js"></script>

		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>js/theme.js"></script>

		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>js/custom.js"></script>

		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>js/theme.init.js"></script>
		<script src="<?php echo base_url(); ?>js/sweealert/sweetalert.min.js"></script>
		<script src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>public/js/login.js"></script>
	</body>
</html>
