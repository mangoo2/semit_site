<div class="container" style="background-image: url(<?php echo base_url()?>public/img/contacto.png) !important;  background-color: #2E3136; background-size: cover; background-position: center; max-width: 100% !important;">
          <div class="row pt-3">
            <div class="col-md-12 mx-md-auto">
              <br><br><br><br><br>
              <p style="color:white; font-size: 44px;" align="center"><b>LOS PRODUCTOS</b></p>
              <p style="color:white; font-size: 44px;" align="center"><b>MÁS VENDIDOS</b></p>

              <br>
              <br>
              <br>
              <br>
              <br>
            </div>
          </div>
        </div>
        <section id="demos_1" class="section section-no-border section-light position-relative z-index-3 pt-0 m-0">
          <h4 align="center" style="color: #012d6a;">LOS PRODUCTOS</h4>
          <h1 align="center" style="color: #012d6a;"><b>MÁS VENDIDOS</b></h1>
            <div class="row">
              <div class="col-lg-2"></div>
              <div class="col-lg-4">  
                <div class="row">  
                  <?php foreach ($productos_get as $p){ 
                    $img_file=''; 
                    if($p->file!=''){
                        $img_file='https://adminsys.semit.mx/uploads/productos/'.$p->file; 
                    }else{
                        $img_file='https://adminsys.semit.mx/public/img/favICONSEMIT.png'; 
                    }  
                    ?>
                    <div class="col-lg-6">
                      <div class="appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200" data-appear-animation-duration="750" style="animation-delay: 200ms; margin-bottom: 26px;">
                        <div class="portfolio-item hover-effect-1 text-center" >
                          <span class="thumb-info thumb-info-no-zoom thumb-info-no-overlay thumb-info-no-bg" style="background: #f5f5f7;">
                            <span class="thumb-info-wrapper thumb-info-wrapper-demos thumb-info-wrapper-link m-0 lazyloaded" style="height: 295px; background-image: url(<?php echo $img_file ?>)" 
                              >
                              <a href="https://adminsys.semit.mx/Productos/descripcion/<?php echo $p->idproductos ?>"></a>
                            </span>
                            <?php //var_dump($idcliente);die;
                             if($idcliente!=0){  
                                  $resulpro=$this->ModelGeneral->get_select('carrito_temporal',array('idproducto'=>$p->idproductos,'idcliente'=>$idcliente));
                                  $num_cl='';
                                  foreach ($resulpro as $pr){
                                      $num_cl='fill="#009bdb"';
                                  }
                                  //var_dump($num_cl);die;
                                  $resulprof=$this->ModelGeneral->get_select('productos_favoritos',array('idproducto'=>$p->idproductos,'idcliente'=>$idcliente));
                                  $num_clf='';
                                  foreach ($resulprof as $pr){
                                      $num_clf='fill="red"';
                                  }
                                  $html.='<div class="row" style="position: absolute; top: 4px; display: inline-flex;">
                                  <div class="col-lg-12">
                                    <a style="cursor:pointer; width: 62px; height: 57px;" onclick="add_favoritos('.$p->idproductos.')"><span class="cora_'.$p->idproductos.'"><svg '.$num_clf.' width="45" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-380 -305)"><path d="M402.493 309.685C392.661 309.685 384.691 317.656 384.691 327.488 384.691 337.32 392.661 345.29 402.493 345.29 412.326 345.29 420.296 337.32 420.296 327.488 420.301 317.66 412.337 309.69 402.509 309.685 402.504 309.685 402.499 309.685 402.493 309.685ZM402.493 336.406C402.493 336.406 393.836 329.786 393.836 324.439 393.836 320.873 399.285 317.054 402.493 323.42 405.702 317.054 411.151 320.873 411.151 324.439 411.151 329.786 402.493 336.406 402.493 336.406Z" /></g></svg></span></a>

                                   <a style="cursor:pointer; width: 62px; height: 57px;" onclick="add_carrito('.$p->idproductos.')"><span class="car_'.$p->idproductos.'"><svg width="46" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-425 -305)"><path '.$num_cl.' d="M38.4375 25.7395 38.4375 9.84375 8.4375 9.84375 8.4375 7.92188C8.43516 5.87766 6.77859 4.22108 4.73438 4.21875L4.6875 4.21875C4.42861 4.21875 4.21875 4.42861 4.21875 4.6875 4.21875 4.94639 4.42861 5.15625 4.6875 5.15625L4.73438 5.15625C6.26114 5.1578 7.49845 6.39511 7.5 7.92188L7.5 31.4062C7.50216 33.1415 8.85502 34.575 10.5872 34.6777 9.15525 35.5615 8.71097 37.4388 9.5948 38.8708 10.4787 40.3027 12.356 40.747 13.7879 39.8632 15.2198 38.9793 15.6641 37.102 14.7803 35.67 14.5345 35.2719 14.2004 34.9357 13.8038 34.6875L31.1944 34.6875C29.7682 35.5807 29.3362 37.4608 30.2293 38.887 31.1224 40.3131 33.0026 40.7452 34.4287 39.852 35.8549 38.9589 36.287 37.0787 35.3939 35.6526 35.1493 35.262 34.8193 34.9321 34.4287 34.6875L37.9688 34.6875C38.2276 34.6875 38.4375 34.4776 38.4375 34.2188 38.4375 33.9599 38.2276 33.75 37.9688 33.75L10.7812 33.75C9.48745 33.7485 8.43905 32.7 8.4375 31.4062L8.4375 28.552ZM32.8125 39.375C31.6475 39.375 30.7031 38.4306 30.7031 37.2656 30.7031 36.1006 31.6475 35.1562 32.8125 35.1562 33.9775 35.1562 34.9219 36.1006 34.9219 37.2656 34.9206 38.43 33.9769 39.3737 32.8125 39.375ZM12.1875 39.375C11.0225 39.375 10.0781 38.4306 10.0781 37.2656 10.0781 36.1006 11.0225 35.1562 12.1875 35.1562 13.3525 35.1562 14.2969 36.1006 14.2969 37.2656 14.2956 38.43 13.3519 39.3737 12.1875 39.375ZM8.4375 16.4062 15 16.4062 15 21.0938 8.4375 21.0938ZM30 21.0938 23.4375 21.0938 23.4375 16.4062 30 16.4062ZM30.9375 16.4062 37.5 16.4062 37.5 21.0938 30.9375 21.0938ZM23.4375 22.0312 30 22.0312 30 25.5886 23.4375 26.2031ZM22.5 26.2917 15.9375 26.9062 15.9375 22.0312 22.5 22.0312ZM23.4375 15.4688 23.4375 10.7812 30 10.7812 30 15.4688ZM22.5 15.4688 15.9375 15.4688 15.9375 10.7812 22.5 10.7812ZM22.5 16.4062 22.5 21.0938 15.9375 21.0938 15.9375 16.4062ZM8.4375 22.0312 15 22.0312 15 26.9948 8.4375 27.6094ZM30.9375 25.5 30.9375 22.0312 37.5 22.0312 37.5 24.8855ZM37.5 15.4688 30.9375 15.4688 30.9375 10.7812 37.5 10.7812ZM15 10.7812 15 15.4688 8.4375 15.4688 8.4375 10.7812Z" transform="matrix(1.02222 0 0 1 425 305)" /></g></svg></span></a></div></div>';
                                }
                              
                              echo $html;  
                            ?>
                            <div class="row">
                              <div class="col-lg-2"></div>
                              <div class="col-lg-8">
                                <h5 style="background: #469ddc; color: white; border-radius: 7px; font-size: 19px;">$ <?php echo $p->precio_con_iva ?></h5>  
                              </div> 
                            </div>  
                            <div class="row" style="height: 103px;">
                              <div class="col-lg-12">
                                <h6 style="text-align: initial; margin-left: 6px; color: #012d6a; font-size: 14px;  text-align-last: center;"><b><?php echo $p->nombre; ?></b></h6>
                                <a class="btn btn-outline btn-primary btn-xs mb-2" onclick="add_carrito(<?php echo $p->idproductos ?>)">Agregar a carrito</a>
                              </div>
                            </div>  
                          </span>
                        </div>
                      </div>
                    </div>
                    <?php /*
                    <div class="col-lg-6">
                      <div class="appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200" data-appear-animation-duration="750" style="animation-delay: 200ms; margin-bottom: 26px;">
                        <div class="portfolio-item hover-effect-1 text-center" >
                          <span class="thumb-info thumb-info-no-zoom thumb-info-no-overlay thumb-info-no-bg" style="background: #f5f5f7;">
                            <span class="thumb-info-wrapper thumb-info-wrapper-demos thumb-info-wrapper-link m-0 lazyloaded" style="height: 295px; background-image: url(https://adminsys.semit.mx/uploads/productos/<?php echo $p->file ?>);" 
                              >
                              <a  target="_blank"></a>
                               
                            </span><!-- https://adminsys.semit.mx  <?php // echo base_url() ?> localhost/semit_erp/uploads/ -->
                            <div class="row">
                              <div class="col-lg-7">
                                <h6 style="text-align: initial; margin-left: 6px; color: #012d6a; font-size: 11px;"><b><?php echo $p->nombre ?></b></h6>
                              </div>
                              <div class="col-lg-4">
                                <h6 style="background: #469ddc; color: white; margin-left: 10px; border-radius: 7px; font-size: 11px;"><?php echo $p->precio_con_iva ?></h6>  
                              </div> 
                            </div>  
                          </span>
                        </div>
                      </div>
                    </div> */ ?>

                  <?php } ?>
                </div>
              </div>    
              <div class="col-lg-4">
                <?php foreach ($productos1_get as $p1){ 
                  $img_file1=''; 
                    if($p1->file!=''){
                        $img_file1='https://adminsys.semit.mx/uploads/productos/'.$p1->file; 
                    }else{
                        $img_file1='https://adminsys.semit.mx/public/img/favICONSEMIT.png'; 
                    }  
                    ?>
                    <div class="appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200" data-appear-animation-duration="750" style="animation-delay: 200ms; margin-bottom: 26px;">
                      <div class="portfolio-item hover-effect-1 text-center" >
                        <span class="thumb-info thumb-info-no-zoom thumb-info-no-overlay thumb-info-no-bg" style="background: #f5f5f7;height: 882px;;"><div class="row">
                                <div class="col-lg-7">
                                </div>
                                <!-- <div class="col-lg-4">
                                  <h6 style="background: #469ddc; color: white; margin-left: 10px; border-radius: 7px; margin-top: 17px; font-size: 11px;"><b><?php //echo $p->precio_con_iva ?></b></h6>  
                                </div>  -->
                              </div><br><br>
                          <span class="thumb-info-wrapper thumb-info-wrapper-demos thumb-info-wrapper-link m-0 lazyloaded" style="max-height: 600px !important; height: 546px; background-image: url(<?php echo $img_file1 ?>);" 
                            >
                            
                            <a href="https://adminsys.semit.mx/Productos/descripcion/<?php echo $p1->idproductos ?>"></a>
                            
                          </span><!-- https://adminsys.semit.mx  <?php // echo base_url() ?> localhost/semit_erp/uploads/ -->
                          <?php //var_dump($idcliente);die;
                             if($idcliente!=0){  
                                  $resulpro1=$this->ModelGeneral->get_select('carrito_temporal',array('idproducto'=>$p1->idproductos,'idcliente'=>$idcliente));
                                  $num_cl='';
                                  foreach ($resulpro1 as $pr1){
                                      $num_cl='fill="#009bdb"';
                                  }
                                  //var_dump($num_cl);die;
                                  $resulprof1=$this->ModelGeneral->get_select('productos_favoritos',array('idproducto'=>$p1->idproductos,'idcliente'=>$idcliente));
                                  $num_clf='';
                                  foreach ($resulprof1 as $pr1){
                                      $num_clf='fill="red"';
                                  }
                                  $html.='<div class="row" style="position: absolute; top: 4px; display: inline-flex;">
                                  <div class="col-lg-12">
                                    <a style="cursor:pointer; width: 62px; height: 57px;" onclick="add_favoritos('.$p1->idproductos.')"><span class="cora_'.$p1->idproductos.'"><svg '.$num_clf.' width="45" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-380 -305)"><path d="M402.493 309.685C392.661 309.685 384.691 317.656 384.691 327.488 384.691 337.32 392.661 345.29 402.493 345.29 412.326 345.29 420.296 337.32 420.296 327.488 420.301 317.66 412.337 309.69 402.509 309.685 402.504 309.685 402.499 309.685 402.493 309.685ZM402.493 336.406C402.493 336.406 393.836 329.786 393.836 324.439 393.836 320.873 399.285 317.054 402.493 323.42 405.702 317.054 411.151 320.873 411.151 324.439 411.151 329.786 402.493 336.406 402.493 336.406Z" /></g></svg></span></a>

                                   <a style="cursor:pointer; width: 62px; height: 57px;" onclick="add_carrito('.$p1->idproductos.')"><span class="car_'.$p1->idproductos.'"><svg width="46" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-425 -305)"><path '.$num_cl.' d="M38.4375 25.7395 38.4375 9.84375 8.4375 9.84375 8.4375 7.92188C8.43516 5.87766 6.77859 4.22108 4.73438 4.21875L4.6875 4.21875C4.42861 4.21875 4.21875 4.42861 4.21875 4.6875 4.21875 4.94639 4.42861 5.15625 4.6875 5.15625L4.73438 5.15625C6.26114 5.1578 7.49845 6.39511 7.5 7.92188L7.5 31.4062C7.50216 33.1415 8.85502 34.575 10.5872 34.6777 9.15525 35.5615 8.71097 37.4388 9.5948 38.8708 10.4787 40.3027 12.356 40.747 13.7879 39.8632 15.2198 38.9793 15.6641 37.102 14.7803 35.67 14.5345 35.2719 14.2004 34.9357 13.8038 34.6875L31.1944 34.6875C29.7682 35.5807 29.3362 37.4608 30.2293 38.887 31.1224 40.3131 33.0026 40.7452 34.4287 39.852 35.8549 38.9589 36.287 37.0787 35.3939 35.6526 35.1493 35.262 34.8193 34.9321 34.4287 34.6875L37.9688 34.6875C38.2276 34.6875 38.4375 34.4776 38.4375 34.2188 38.4375 33.9599 38.2276 33.75 37.9688 33.75L10.7812 33.75C9.48745 33.7485 8.43905 32.7 8.4375 31.4062L8.4375 28.552ZM32.8125 39.375C31.6475 39.375 30.7031 38.4306 30.7031 37.2656 30.7031 36.1006 31.6475 35.1562 32.8125 35.1562 33.9775 35.1562 34.9219 36.1006 34.9219 37.2656 34.9206 38.43 33.9769 39.3737 32.8125 39.375ZM12.1875 39.375C11.0225 39.375 10.0781 38.4306 10.0781 37.2656 10.0781 36.1006 11.0225 35.1562 12.1875 35.1562 13.3525 35.1562 14.2969 36.1006 14.2969 37.2656 14.2956 38.43 13.3519 39.3737 12.1875 39.375ZM8.4375 16.4062 15 16.4062 15 21.0938 8.4375 21.0938ZM30 21.0938 23.4375 21.0938 23.4375 16.4062 30 16.4062ZM30.9375 16.4062 37.5 16.4062 37.5 21.0938 30.9375 21.0938ZM23.4375 22.0312 30 22.0312 30 25.5886 23.4375 26.2031ZM22.5 26.2917 15.9375 26.9062 15.9375 22.0312 22.5 22.0312ZM23.4375 15.4688 23.4375 10.7812 30 10.7812 30 15.4688ZM22.5 15.4688 15.9375 15.4688 15.9375 10.7812 22.5 10.7812ZM22.5 16.4062 22.5 21.0938 15.9375 21.0938 15.9375 16.4062ZM8.4375 22.0312 15 22.0312 15 26.9948 8.4375 27.6094ZM30.9375 25.5 30.9375 22.0312 37.5 22.0312 37.5 24.8855ZM37.5 15.4688 30.9375 15.4688 30.9375 10.7812 37.5 10.7812ZM15 10.7812 15 15.4688 8.4375 15.4688 8.4375 10.7812Z" transform="matrix(1.02222 0 0 1 425 305)" /></g></svg></span></a></div></div>';
                                }
                              
                              echo $html;  
                            ?>
                          <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-8">
                              <h5 style="background: #469ddc; color: white; border-radius: 7px; font-size: 19px;">$ <?php echo $p1->precio_con_iva ?></h5>  
                            </div> 
                          </div>  
                          <div class="row" style="height: 103px;">
                            <div class="col-lg-12">
                              <h6 style="text-align: initial; margin-left: 6px; color: #012d6a; font-size: 14px;  text-align-last: center;"><b><?php echo $p1->nombre; ?></b></h6>
                              <a class="btn btn-outline btn-primary btn-xs mb-2" onclick="add_carrito(<?php echo $p1->idproductos ?>)">Agregar a carrito</a>
                            </div>
                          </div>   

                        </span>
                      </div>
                    </div>
                <?php } ?>
              </div>
              <div class="col-lg-2"></div>     
            </div>
          
        </section>