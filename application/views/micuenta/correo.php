<?php 
    
    $result=$this->ModelPago->get_productos_venta('9B7064402U284711S');
    $trans='';
    $nombre='';
    $total='';
    $calle='';
    $colonia='';
    $fechatxt='';
    foreach ($result as $x){
        $trans=$x->idtransacción;
        $nombre=$x->nombre;
        $total=$x->total;
        $calle=$x->calle;
        $colonia=$x->colonia;
        $fecha=$x->reg;
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fechatxt=date('d',strtotime($x->reg))." de ".$meses[date('n',strtotime($x->reg))-1]. " del ".date('Y',strtotime($x->reg));
    }

   echo $html="
<div style='background: url(http://estudiosunne.sicoi.net/public/img/anahuac-fondo-footer.jpg);width: 97%;height: 94%;padding: 25px;margin: 0px; font-family: Galano, Helvetica, Arial, sans-serif !important;'>
    <img class='img_logo1' style=' alt='Porto' width='135' height='55' data-sticky-width='82' data-sticky-height='40' data-sticky-top='25' src='http://pagedemo.semit.mx/public/img/SEMIT.jpg'>
    <br>
    <div style='text-align-last: end; width: 69%;'>
        <b style='width:17px;'>Confirmación de pedido<br>
            Pedido:".$trans."
        </b> 
    </div>
    <br><br>
    <b style='width:25px; color: #009bdb;'>Hola ".$nombre."</b>
    <br>
    <b>Gracias por tu pedido</b>
    <br>
    <b>Los detalles de tu pedido se indica a continuación</b>
    <br></br>
    <table style='width:70%; background: #eaeaea;border-top: 3px solid #009bdb;'>
        <thead>
            <tr>
                <th>
                   Tu fecha de solicitud es: <br><b>".$fechatxt."</b>
                   <br><br> 
                   <a style='background:#009bdb;color:white;padding: 2px;border-radius: 8px;text-decoration: wavy; font-size: 18px;' href='http://pagedemo.semit.mx/Micuenta/mispedidos'>&nbsp;&nbsp;&nbsp;Detalles del pedido&nbsp;&nbsp;&nbsp;</a>
                </th>
                <th>
                    Tu pedido será  enviado a:<br>".$nombre."<br>".$calle." ".$colonia."
                    <br><br>
                    Total del pedido (impuestos aplicables incluídos):<br> $".number_format($total,2,'.',',')."
                </th>
            </tr>
        </thead>
    </table>
</div>";