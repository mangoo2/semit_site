<style type="text/css">
  .btn_blue{
    background-color: #152342;
    padding: 11px;
    border-radius: 21px;
    border: 3px solid #93ba1f;
    color: white;
    font-size: 14px; width: 100%;
  }
  .hr_t{
    background: rgb(21 35 66); border: 0;
    height: 1px;
    margin: 12px 0;
    opacity: 14;
  }
  .btn_green{
    background-color: #93ba1f;
    padding: 11px;
    border-radius: 21px;
    border: 3px solid #152342;
    color: white;
    font-size: 14px; width: 100%;
  }

  .vd_red{
    color: red !important;
  }
  .vd_green{
          color: green !important; 
  } 

  :root {
        --color-green: #05a8e1;
        --color-secondary: #b9b9b9;
        --color-button: white;
        --color-black: white;
    }


    .switch-button .switch-button__checkbox {
        display: none;
    }
    .switch-button .switch-button__label {
        background-color: var(--color-secondary);
        width: 3rem;
        height: 1.5rem;
        border-radius: 1rem;
        display: inline-block;
        position: relative;
    }
    .switch-button .switch-button__label:before {
        transition: .2s;
        display: block;
        position: absolute;
        width: 1.5rem;
        height: 1.5rem;
        background-color: var(--color-button);
        content: '';
        border-radius: 50%;
        box-shadow: inset 0px 0px 0px 1px var(--color-black);
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label {
        background-color: #152342;
    }
    .switch-button .switch-button__checkbox:checked + .switch-button__label:before {
        transform: translateX(1.5rem);
    }
</style>
<input type="hidden" id="idventa" value="<?php echo $idventa ?>">
<input type="hidden" id="RegimenFiscalReceptor_aux" value="<?php echo $RegimenFiscalReceptor ?>">
<input type="hidden" id="uso_cfdi_aux" value="<?php echo $uso_cfdi ?>">
<section class="section section-no-border section-light position-relative z-index-3 pt-0 m-0">
  <div class="container">
    <div class="row">
      <div class="col-lg-7"><br>
        <h3 style="color: #012d6a;"><img style="width: 38px;" src="<?php echo base_url()?>public/img/micuenta/6.png"> Facturación electrónica</h3>
      </div>  
      <div class="col-lg-5" align="right">
      	<br>
      	<a style="background-color: #93ba1f;
    padding: 11px;
    border-radius: 21px;
    border: 3px solid #152342;
    color: white;
    font-size: 14px; width: 100%" href="<?php echo base_url() ?>Micuenta">&nbsp;&nbsp;&nbsp;&nbsp;Regresar&nbsp;&nbsp;&nbsp;&nbsp;</a>
      </div>	
    </div>  
  </div>
</section>    

<div class="container">

  <div class="demos2_text"><br><br>
    <h2 align="center">Cargando productos <br> porfavor espere...</h2>
  </div>
</div>  

<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="largeModalLabel">Facturar compra</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <form id="form_data" method="post">

          <div class="row">
           <div class="col-lg-12">
              <label class="form-label text-color-dark text-3">RFC: <span class="text-color-danger">*</span></label>
              <input type="text" name="rfc" id="rfc" class="form-control form-control-lg text-4"  oninput="validarrfc(this)" style="border-color: #012d6a !important;" value="<?php echo $rfc ?>"><span style="color: red" class="v_rfc"></span>
           </div>
          </div>
          <div class="row">
           <div class="col-lg-4">
              <label class="form-label text-color-dark text-3">Código portal: <span class="text-color-danger">*</span></label>
              <input type="text" name="cp" id="cp" maxlength="5" minlength="5" class="form-control form-control-lg text-4" style="border-color: #012d6a !important;" value="<?php echo $cp ?>">
           </div>
           <div class="col-lg-8">
              <label class="form-label text-color-dark text-3">Correo electrónico: <span class="text-color-danger">*</span></label>
              <input type="email" name="correo" id="correo" class="form-control form-control-lg text-4" style="border-color: #012d6a !important;" value="<?php echo $correo ?>">
           </div>
          </div>
          <div class="row">
           <div class="col-lg-12">
              <label class="form-label text-color-dark text-3">Razon social: <span class="text-color-danger">*</span></label>
              <input type="text" name="razon_social" id="razon_social" class="form-control form-control-lg text-4" style="border-color: #012d6a !important;" value="<?php echo $razon_social ?>">
           </div>
          </div>
          <div class="row">
           <div class="col-lg-12">
              <label class="form-label text-color-dark text-3">Régimen Fiscal: <span class="text-color-danger">*</span></label>
              <select class="form-control" name="RegimenFiscalReceptor" id="RegimenFiscalReceptor" style="border-color: #012d6a !important;" onchange="v_rf()">
                  <option>Seleccionar una opción</option> 
                  <option value="601" >601 General de Ley Personas Morales</option>
                  <option value="603" >603 Personas Morales con Fines no Lucrativos</option>
                  <option value="605" >605 Sueldos y Salarios e Ingresos Asimilados a Salarios</option>
                  <option value="606" >606 Arrendamiento</option>
                  <option value="607" >607 Régimen de Enajenación o Adquisición de Bienes</option>
                  <option value="608" >608 Demás ingresos</option>
                  <option value="609" >609 Consolidación</option>
                  <option value="610" >610 Residentes en el Extranjero sin Establecimiento Pe</option>
                  <option value="611" >611 Ingresos por Dividendos (socios y accionistas)</option>
                  <option value="612" >612 Personas Físicas con Actividades Empresariales</option>
                  <option value="614" >614 Ingresos por intereses</option>
                  <option value="615" >615 Régimen de los ingresos por obtención de premios</option>
                  <option value="616" >616 Sin obligaciones fiscales</option>
                  <option value="620" >620 Sociedades Cooperativas de Producción que optaingresos</option>
                  <option value="621" >621 Incorporación Fiscal</option>
                  <option value="622" >622 Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras</option>
                  <option value="623" >623 Opcional para Grupos de Sociedades</option>
                  <option value="624" >624 Coordinados</option>
                  <option value="625" >625 Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas</option>
                  <option value="626" >626 Régimen Simplificado de Confianza</option>
                  <option value="628" >628 Hidrocarburos</option>
                  <option value="629" >629 De los Regímenes Fiscales Preferentes Multinacionales</option>
                  <option value="630" >630 Enajenación de acciones en bolsa de valores</option>      
              </select>
           </div>
          </div>
          <div class="row">
           <div class="col-lg-12">
              <label class="form-label text-color-dark text-3">Uso de CFDI: <span class="text-color-danger">*</span></label>
              <select class="form-control" id="uso_cfdi" name="uso_cfdi" style="border-color: #012d6a !important;"> 
                  <option>Seleccionar una opción</option> 
                  <?php  foreach ($get_uso_cfdi as $item) { ?> 
                      <option value="<?php echo $item->uso_cfdi;?>" class="pararf <?php echo str_replace(',',' ',$item->pararf) ?>" disabled ><?php echo $item->uso_cfdi_text;?></option> 
                  <?php } ?> 
              </select> 
           </div>
          </div>
        </form>  
      </div>
      <div class="modal-footer">
        <div class="row" style="width: -webkit-fill-available;">
          <div class="col-lg-1">
            <div class="switch-button">
                <input type="checkbox" name="direccion_predeterminada" id="direccion_predeterminada" class="switch-button__checkbox" onclick="registrar_factura()">
                <label for="direccion_predeterminada" class="switch-button__label"> </label>
            </div>
          </div>
          <div class="col-lg-7">
            ¿Desea guardar sus datos fiscales para facturar compras futuras?
          </div>  
          <div class="col-lg-4" style="text-align-last: end;">
            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cancelar</button>
            <button type="button" class="btn btn-timbrar" id="btn-timbrar" style="background-color: #93ba1f; color: white; border: 3px solid #152342;">Timbrar</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
