<section class="section section-no-border section-light position-relative z-index-3 pt-0 m-0">
  <div class="container">
    <div class="row">
      <div class="col-lg-7"><br>
        <h3 style="color: #012d6a;"><svg fill="red" width="45" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-380 -305)"><path d="M402.493 309.685C392.661 309.685 384.691 317.656 384.691 327.488 384.691 337.32 392.661 345.29 402.493 345.29 412.326 345.29 420.296 337.32 420.296 327.488 420.301 317.66 412.337 309.69 402.509 309.685 402.504 309.685 402.499 309.685 402.493 309.685ZM402.493 336.406C402.493 336.406 393.836 329.786 393.836 324.439 393.836 320.873 399.285 317.054 402.493 323.42 405.702 317.054 411.151 320.873 411.151 324.439 411.151 329.786 402.493 336.406 402.493 336.406Z" /></g></svg> Mis favoritos</h3>
      </div>  
      <div class="col-lg-5" align="right">
      	<br>
      	<a style="background-color: #93ba1f;
    padding: 11px;
    border-radius: 21px;
    border: 3px solid #152342;
    color: white;
    font-size: 14px; width: 100%" href="<?php echo base_url() ?>Micuenta">&nbsp;&nbsp;&nbsp;&nbsp;Regresar&nbsp;&nbsp;&nbsp;&nbsp;</a>
      </div>	
    </div>  
  </div>
</section>    

<div class="container">
  <div class="demos2_text"><br><br>
    <h2 align="center">Cargando productos <br> porfavor espere...</h2>
  </div>
</div>  

