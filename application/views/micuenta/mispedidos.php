<style type="text/css">
  .btn_blue{
    background-color: #152342;
    padding: 5px;
    border-radius: 21px;
    border: 3px solid #93ba1f;
    color: white;
    font-size: 14px;
    display: inline-block;
    font-weight: bold;
    margin: 2px;
    width: -webkit-fill-available;
    text-align-last: center;
  }
  .hr_t{
    background: rgb(21 35 66); border: 0;
    height: 1px;
    margin: 12px 0;
    opacity: 14;
  }
  .vd_red{
    color: red !important;
  }
  .vd_green{
          color: green !important; 
  } 
</style>
<section class="section section-no-border section-light position-relative z-index-3 pt-0 m-0">
  <div class="container">
    <div class="row">
      <div class="col-lg-7"><br>
        <h3 style="color: #012d6a;"><img style="width: 38px;" src="<?php echo base_url()?>public/img/micuenta/4.png"> Mis pedidos</h3>
      </div>  
      <div class="col-lg-5" align="right">
      	<br>
      	<a style="background-color: #93ba1f;
    padding: 11px;
    border-radius: 21px;
    border: 3px solid #152342;
    color: white;
    font-size: 14px; width: 100%" href="<?php echo base_url() ?>Micuenta">&nbsp;&nbsp;&nbsp;&nbsp;Regresar&nbsp;&nbsp;&nbsp;&nbsp;</a>
      </div>	
    </div>  
  </div>
</section>    

<div class="container">
  <div class="demos2_text"><br><br>
    <h2 align="center">Cargando productos <br> porfavor espere...</h2>
  </div>
</div>  

<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="largeModalLabel">Reseña de producto</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-hidden="true">&times;</button>
      </div>
      <div class="modal-body">
        <form id="form_data" method="post">
          <div class="row">
           <div class="col-lg-12">
              <textarea name="resena" id="resena" rows="5" class="form-control form-control-lg text-4" style="border-color: #012d6a !important;"></textarea>
           </div>
          </div>
        </form>  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-light" data-bs-dismiss="modal">Cerrar</button>
        <button type="button" class="btn" style="background-color: #93ba1f; color: white; border: 3px solid #152342;" onclick="registrar_resena()">Aceptar</button>
      </div>
    </div>
  </div>
</div>
