<!DOCTYPE html>
<html lang="en">
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Venta y renta de equipo médico | Oxígeno medicinal | Toluca</title>  

		<meta name="keywords" content="WebSite Template" />
		<meta name="description" content="Porto - Multipurpose Website Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>public/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?php echo base_url(); ?>public/img/favicon.ico">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link id="googleFonts" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400&display=swap" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/animate/animate.compat.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/theme-elements.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/theme-blog.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/theme-shop.css">

		<!-- Skin CSS -->
		<link id="skinCSS" rel="stylesheet" href="<?php echo base_url(); ?>css/skins/default.css">
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/sweealert/sweetalert.css">
		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.min.js"></script>
        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
	</head>
	<style type="text/css">
		@media (max-width: 575px){
			.img_logo{
				width: 180px !important;
			    height: 91% !important;
			    position: absolute !important;
			    top: 4px !important;
			    left: 106px !important;
			}
			.input_codigo{
				width: 13% !important;
			}
			.col-lg-2 {
			    flex: 0 0 auto !important;
			    width: 16.66666667% !important;
			}
			.input_c{
                width: 100% !important;
			}
		}
        
        @media (min-width: 1200px){
		    #footer {
			    background: #212529;
			    border-top: 4px solid #212529;
			    font-size: 0.9em;
			    /*margin-top: 177px !important;*/
			    padding: 0;
			    /*position: relative;*/
			    clear: both;
			}
		}
		footer{
			position: fixed !important;
    		width: 100%;
    		bottom: 0;
		}
		.main{
			margin-bottom: 64px;
		}
	</style>
	<style type="text/css">
		.vd_red{
			color: red !important;
		}
		.vd_green{
            color: green !important; 
		} 
	</style>
	<body data-plugin-page-transition>

		<div class="body">
			<div role="main" class="main">
				<section class="page-header page-header-modern bg-color-light-scale-1 page-header-lg" style="padding: 12px 0; background: #012d6a !important; ">
					<div class="container">
						<div class="row">
							<div class="col-md-12 align-self-center p-static order-2">
								<a href="<?php echo base_url() ?>Inicio"><img class="img_logo" style="width: 157px; height: 83%; position: absolute; top: 4px; left: 62px;" alt="Porto" width="100" height="48" data-sticky-width="82" data-sticky-height="40" data-sticky-top="25" src="<?php echo base_url(); ?>public/img/logo_semit.png"></a><br><br>
							</div>
						</div>
					</div>
				</section>
				<div class="container py-4 login_1">
					<div class="row justify-content-center">
						<div class="col-md-6 col-lg-5 mb-5 mb-lg-0" >
							<span style="font-size: 40px; color: black; line-height: 48px;">Ingresa tu usuario y contraseña de Semit </span>
						</div>	
						<div class="col-md-6 col-lg-5 mb-lg-0">
							<div style="border-radius: 15px; border: solid;">
								<div style="">
									<h2 class="font-weight-bold text-5 mb-0" style="margin: 14px;">Login</h2>
									<form id="sign_in" method="post" class="needs-validation" style="margin: 14px;">
										<div class="row">
											<div class="form-group col">
												<label class="form-label text-color-dark text-3">Usuario / Correo electrónico: <span class="text-color-danger">*</span></label>
												<input type="text" name="usuario" id="usuario" class="form-control form-control-lg text-4" required style="border-color: #012d6a !important;">
											</div>
										</div>
										<div class="row">
											<div class="form-group col">
												<label class="form-label text-color-dark text-3">Contraseña <span class="text-color-danger">*</span></label>
												<input type="password" name="password" id="password" class="form-control form-control-lg text-4" required>
											</div>
										</div>
									</form>
									<div class="row" style="margin: 2px;">
										<div class="form-group col">
											<button type="button" class="btn btn-modern w-100 text-uppercase rounded-0 font-weight-bold text-3 py-3" onclick="iniciar_sesion()" style="border-radius: 14px !important; background-color: #012d6a; color: white" id="ingresar_login">Iniciar sesión</button>
										    <div class="btn_inicio"> </div>	
										</div>
									</div>
								</div>
							</div>
							<br><div style="text-align: center;">
							<a style="color: #012d6a; cursor: pointer;" onclick="olvidaste_contrasena()"><b>¿Olvidaste tu correo o contraseña de ingreso?</b></a><br>
							<a style="color: #012d6a; cursor: pointer;" onclick="registrate_aqui()"><b>¿No tienes una cuenta de SEMIT?</b></a><br><br>
							<button type="button" class="btn btn-sm mb-2" onclick="registrate_aqui()" style="border-radius: 14px !important; background-color: #012d6a; color: white; width: 50%;">Regístrate aquí</button>
							</div>
						</div>
					</div>
				</div>
				<div class="login_1">
                	<br><br><br>
                </div>
				<div class="container py-4 login_2" style="display: none">
					<div class="row justify-content-center">
						<div class="col-md-6 col-lg-4 mb-5 mb-lg-0" >
							<span style="font-size: 40px; color: black; line-height: 48px;">Regístrate con nosotros</span>
						</div>	
						<div class="col-md-6 col-lg-8 mb-lg-0">
							<div style="border-radius: 15px; border: solid;">
								<div style="">
									<h2 class="font-weight-bold text-5 mb-0" style="margin: 14px;">Datos generales</h2>
									<form id="form_data" method="post" class="needs-validation" style="margin: 14px;">
										<div class="row">
											<div class="form-group col">
												<label class="form-label text-color-dark text-3">Nombre completo: <span class="text-color-danger">*</span></label>
												<input type="text" name="nombre" id="nombre" class="form-control form-control-lg text-4 nombre_cliente" style="border-color: #012d6a !important;">
											</div>
										</div>
										<div class="row">
											<div class="form-group col">
												<label class="form-label text-color-dark text-3">Correo electrónico: <span class="text-color-danger">*</span></label>
												<input type="email" name="correo" id="correo" class="form-control form-control-lg text-4" style="border-color: #012d6a !important;" oninput="verificar_correo()"><span class="validar_correo" style="color: red"></span>
											</div>
										</div>
										<div class="row">
											<div class="form-group col">
												<label class="form-label text-color-dark text-3">Usuario: <span class="text-color-danger">*</span></label>
												<input autocomplete="nope" type="text" name="usuario" id="usuario_reg" class="form-control form-control-lg text-4" style="border-color: #012d6a !important;" oninput="verificar_usuario()"><span class="validar_user" style="color: red"></span>
											</div>
										</div>
										<div class="row">
											<div class="form-group col">
												<label class="form-label text-color-dark text-3">Celular: <span class="text-color-danger">*</span></label>
												<input type="text" name="celular" id="celular" class="form-control form-control-lg text-4" style="border-color: #012d6a !important;" oninput="verificar_celular()"><span class="validar_celular" style="color: red"></span> 
											</div>
										</div>
										<div class="row">
											<div class="col-lg-11">
												<div class="form-group col">
													<label class="form-label text-color-dark text-3">Contraseña <span class="text-color-danger">*</span></label>
													<input type="password" name="contrasena" id="contrasena" class="form-control form-control-lg text-4">
												</div>
											</div>
											<div class="col-lg-1">
												<label class="form-label text-3" style="color:#ffffff00;">__</label><br>
												<a onclick="mostrarContrasena()"><i class="fa fa-eye" style="font-size: 33px;"></i></a>
											</div>	
										</div>
										<div class="row">
											<div class="form-group col">
												<label class="form-label text-color-dark text-3">Validar contraseña <span class="text-color-danger">*</span></label>
												<input type="password" name="contrasena2" id="contrasena2" class="form-control form-control-lg text-4">
											</div>
										</div>
									</form>
									<div class="row" style="margin: 2px;">
										<div class="form-group col">
											<button type="button" class="btn btn-modern w-100 text-uppercase rounded-0 font-weight-bold text-3 py-3 registro_data" onclick="registrar_usuario()" style="border-radius: 14px !important; background-color: #012d6a; color: white">Registrarme</button>
										</div>
									</div>
								</div>
							</div>
							<br><div style="text-align: center;">
							<a type="button" class="btn btn-sm mb-2" href="<?php echo base_url() ?>Inicio/login" style="border-radius: 14px !important; background-color: #012d6a; color: white; width: 50%;">Regresar</a>
							</div>
						</div>
					</div>
				</div>
				<div class="container py-4 login_3" style="display: none">
					<div class="row justify-content-center">
						<div class="col-md-6 col-lg-4 mb-5 mb-lg-0" >
							<span style="font-size: 40px; color: black; line-height: 48px;">Validación de código</span>
						</div>	
						<div class="col-md-6 col-lg-4 mb-lg-0">
							<div style="border-radius: 15px; border: solid;">
								<div style="">
									<h2 class="font-weight-bold text-5 mb-0" style="margin: 14px;"></h2>
									<form id="form_data_codigo" method="post" class="needs-validation" style="margin: 14px;">
										<div class="row">
											<label class="form-label text-color-dark text-3">Ingresa código:<span class="text-color-danger">*</span></label>
										</div>
										<div class="row">
											<div class="col-lg-2">
												<input type="text" name="codigo" id="codigo_t1" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none;    border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t2').focus(); } }">
											</div>
											<div class="col-lg-2">	
												<input type="text" name="codigo" id="codigo_t2" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none;    border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t3').focus(); } }">
											</div>
											<div class="col-lg-2">
												<input type="text" name="codigo" id="codigo_t3" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none;    border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t4').focus(); } }">
											</div>
											<div class="col-lg-2">
												<input type="text" name="codigo" id="codigo_t4" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none;    border-inline: none; text-align: center; padding: 0rem 0rem !important;" onkeyup="if (this.value.length == this.getAttribute('maxlength')) { if (event.keyCode!=1) { getElementById('codigo_t5').focus(); } }">
											</div>
											<div class="col-lg-2">
												<input type="text" name="codigo" id="codigo_t5" class="form-control form-control-lg text-4 input_codigo input_c" maxlength="1" style="border-color: #012d6a !important; border-bottom-width: medium; border-block-start: none;    border-inline: none; text-align: center; padding: 0rem 0rem !important;">
											</div>
											<div class="col-lg-12">
											    <span class="validar_codigo" style="color: red"></span>
											</div>    
										</div>
									</form>
									<div class="row" style="margin: 2px;">
										<div class="form-group col">
											<button type="button" class="btn btn-modern w-100 text-uppercase rounded-0 font-weight-bold text-3 py-3 btn_registro_valida" onclick="validar_usuario()" style="border-radius: 14px !important; background-color: #012d6a; color: white">Por favor introduce el código que fue enviado a tu correo electrónico</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                <div class="login_3" style="display: none">
                	<br><br><br><br><br><br><br><br><br><br><br><br><br>
                </div>
                <div class="container py-4 login_4" style="display: none">
					<div class="row justify-content-center">
						<div class="col-md-6 col-lg-4 mb-5 mb-lg-0" >
							<span style="font-size: 40px; color: black; line-height: 48px;">Ingresa tu correo o teléfono</span>
						</div>	
						<div class="col-md-6 col-lg-5 mb-lg-0">
							<div style="border-radius: 15px; border: solid;">
								<div style="">
									<h2 class="font-weight-bold text-5 mb-0" style="margin: 14px;"></h2>
									<form id="form_data_validar" method="post" class="needs-validation" style="margin: 14px;">
										<div class="row">
											<div class="form-group col">
												<label class="form-label text-color-dark text-3">Correo o teléfono: <span class="text-color-danger">*</span></label>
												<input type="text" name="correo" class="form-control form-control-lg text-4" required style="border-color: #012d6a !important;">
											</div>
										</div>
									</form>
									<div class="row" style="margin: 2px;">
										<div class="form-group col">
											<button type="button" class="btn btn-modern w-100 text-uppercase rounded-0 font-weight-bold text-3 py-3 btn_registro_valida" onclick="editar_contrasena()" style="border-radius: 14px !important; background-color: #012d6a; color: white">Aceptar</button>
										</div>
									</div>
								</div>
							</div>
							<br><div style="text-align: center;">
							<a type="button" class="btn btn-sm mb-2" href="<?php echo base_url() ?>Inicio/login" style="border-radius: 14px !important; background-color: #012d6a; color: white; width: 50%;">Regresar</a>
							</div>
						</div>
					</div>
				</div>
				<div class="login_4" style="display: none">
                	<br><br><br><br><br><br><br>
                </div>
			</div>
			<footer id="footer"  style="background: #e6e6e6 !important; border-top: 4px solid #ffffff !important;">
		        <div class="container">

		          <div class="row">
		            <div class="col-lg-2">
		              <img style="width: 183px" src="<?php echo base_url() ?>public/img/footer/Image_06.png">
		            </div>
		            <div class="col-lg-2">
		              <img style="width: 164px; margin-left: 13px; margin-top: 7px;" src="<?php echo base_url() ?>public/img/footer/Image_07.png">
		            </div>
		            <div class="col-lg-3">
		              <img style="width: 183px; margin-left: 31px; margin-top: 13px;" src="<?php echo base_url() ?>public/img/footer/Image_08.png">
		            </div>
		            <div class="col-lg-1">
		            </div>  
		            <div class="col-lg-4">
		              <h6 style="color: black; margin-top: 18px;"><a style="color: black;" href="<?php echo base_url() ?>Inicio/aviso_de_privacidad">Aviso de privacidad</a> | Todos los derechos reservados | <?php echo date('Y') ?></h6>
		            </div>  
		          </div>
		        </div>      
            </footer>
		</div>
        
        <script src="<?php echo base_url();?>js/jquery-3.5.1.min.js"></script>
		<!-- Vendor -->
 		<script src="<?php echo base_url(); ?>vendor/plugins/js/plugins.min.js"></script>

		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>js/theme.js"></script>

		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>js/custom.js"></script>

		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>js/theme.init.js"></script>
		<script src="<?php echo base_url(); ?>js/sweealert/sweetalert.min.js"></script>
		<script src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>public/js/login.js"></script>
	</body>
</html>
