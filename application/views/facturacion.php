<!DOCTYPE html>
<html lang="en">
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Venta y renta de equipo médico | Oxígeno medicinal | Toluca</title>  

		<meta name="keywords" content="facturacion semit" />
		<meta name="description" content="facturacion web semit">
		<meta name="author" content="mangoo">
		<meta name="theme-color" content="#012d6a" />

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo base_url(); ?>public/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?php echo base_url(); ?>public/img/favicon.ico">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link id="googleFonts" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400&display=swap" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/fontawesome-free/css/all.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/animate/animate.compat.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/theme-elements.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/theme-blog.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/theme-shop.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/plugins/toastr/toastr.min.css">

		<!-- Skin CSS -->
		<link id="skinCSS" rel="stylesheet" href="<?php echo base_url(); ?>css/skins/default.css">
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/sweealert/sweetalert.css">
		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.min.js"></script>
        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
	</head>
	<style type="text/css">
		@media (max-width: 575px){
			.img_logo{width: 180px !important;height: 91% !important;position: absolute !important;top: 4px !important;left: 106px !important;}
			.input_codigo{width: 13% !important;}
			.col-lg-2 {flex: 0 0 auto !important;width: 16.66666667% !important;}
			.input_c{width: 100% !important;}
		}
        
        @media (min-width: 1200px){
		    #footer {background: #212529;border-top: 4px solid #212529;font-size: 0.9em;/*margin-top: 177px !important;*/padding: 0;/*position: relative;*/clear: both;}
		}
		footer{position: fixed !important;width: 100%;bottom: 0;}
		.main{margin-bottom: 64px;}
		.vd_red{color: red !important;}
		.vd_green{color: green !important;} 
		.bordercontenedor{border-radius: 15px; border: solid;margin-bottom: 10px;}
		.title-fac{color: #cbd244;font-size: 20px;}
		.subtitle-fac{text-align: center;color: #012d6a;font-size: 15px;font-weight: bold;}
		ul {list-style-image: url("<?php echo base_url(); ?>public/img/favicon.ico");}
		ul li{margin-bottom: 15px;}
		.btn-semit{min-width: 172px;margin-bottom: 10px;}
		.card_pro p{margin-bottom: 0px;}
		option:disabled {background: #e1e1e1;}
		#form-datos label{color: #cbd244;font-weight: bold;}
		label.error{color: red !important;font-size: 11px;}
		.progress {height: 12px;background: #ededed;}
	   	.result_fac, .result_fac2{font-weight: bold;}
	   	.btn-fac-ok{width: 50px;display: block;height: 47px;float: left;}
	   	

	   	.btn-fac-ok.pdf{
	   		background: url('<?php echo base_url()?>public/img/pdf1.png');
	   		background-size: contain;
	   		background-repeat: no-repeat;

	   	}
	   	.btn-fac-ok.xml{
	   		background: url('<?php echo base_url()?>public/img/xml2.png');
	   		background-size: 89%;
	   		background-repeat: no-repeat;

	   	}
	   	.btn-fac-ok.mail{
	   		background: url('<?php echo base_url()?>public/img/ennvio3.svg');
	   		background-size: 100%;
	   		background-repeat: no-repeat;

	   	}
	   	.iframediv iframe{width: 100%;border:0;}
	</style>
	<body data-plugin-page-transition>

		<div class="body">
			<div role="main" class="main">
				<section class="page-header page-header-modern bg-color-light-scale-1 page-header-lg" style="padding: 12px 0; background: #012d6a !important; ">
					<div class="container">
						<div class="row">
							<div class="col-md-12 align-self-center p-static order-2">
								<a href="<?php echo base_url() ?>Inicio"><img class="img_logo" style="width: 157px; height: 83%; position: absolute; top: 4px; left: 62px;" alt="Porto" width="100" height="48" data-sticky-width="82" data-sticky-height="40" data-sticky-top="25" src="<?php echo base_url(); ?>public/img/logo_semit.png"></a><br><br>
							</div>
						</div>
					</div>
				</section>
				<div class="container py-4 login_1">
					<div class="row">
						<div class="col-md-12 title-fac">
							Facturación Electrónica versión 4.0
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-md-8">
							<div class="bordercontenedor" style="">
								<div class="row">
									<div class="col-md-12 subtitle-fac">
										Normas de facturación electrónica
									</div>
									<div class="col-md-12">
										<ul>
											<li><b>Plazo:</b> Usted podrá solicitar su factura en el momento de la de la compra en el tienda o desde este portal.</li>
											<li><b>Fecha de la Factura:</b> corresponde al día en que se genera.</li>
											<li><b>Disponibilidad:</b> si no solicito factura en Tienda, su ticket estará disponible para ser facturado al instante de ser generado el ticket de venta.</li>
											<li><b>Archivos XML y PDF:</b> Disponible para extraer o enviar vía correo electrónico.</li>
											<li><b>La factura solo podrá ser expedida el mismo mes de haber efectuado su compra.</b></li>
										</ul>
									</div>	
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bordercontenedor" style="padding: 10px; text-align:center;">
								<button type="button" class="btn btn-success btn-semit">Guía rápida</button>
								<button type="button" class="btn btn-success btn-semit">Preguntas frecuentes</button>
							</div>
							<div class="bordercontenedor card_pro" style="padding: 10px; text-align:justify;">
								<p>Si presenta algún inconveniente para generar su factura.</p>
								<p>Envié un correo electrónico a la cuenta:</p>
								<p><a href="mailto:equposmedicos@semit.mx"><i class="far fa-envelope"></i> equposmedicos@semit.mx</a></p>
								<p><a href="tel:7224542223"><i class="fas fa-phone"></i> (722) 454 22 23</a></p>
								
							</div>
						</div>
						
					</div>
					
						<form class="form" id="form-datos">
							<div class="row">
								<div class="col-md-8">
									<div class="bordercontenedor" style="padding: 10px;">
										<div class="form-group">
											<div class="row">
												<div class="col-md-3"><label for="fac_razonsocial">Razón social</label></div>
												<div class="col-md-9"><input type="text" class="form-control" id="fac_razonsocial" name="fac_razonsocial" required></div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-3"><label for="fac_rfc">RFC</label></div>
												<div class="col-md-3"><input type="text" class="form-control" id="fac_rfc" name="fac_rfc" required onchange="obtenerdatos()"></div>
												<div class="col-md-3"><label for="fac_cp">Código postal</label></div>
												<div class="col-md-3"><input type="number" class="form-control" id="fac_cp" name="fac_cp" minlength="5" maxlength="5" required></div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-3"><label for="fac_regimenfiscal">Régimen fiscal</label></div>
												<div class="col-md-9"><select class="form-control" id="fac_regimenfiscal" name="fac_regimenfiscal" required onchange="regimenfiscal()"><option value="">Seleccione</option><?php 
													foreach ($fregimen->result() as $item) {
														echo '<option value="'.$item->clave.'">'.$item->clave.' '.$item->descripcion.'</option>';
													}
												?></select></div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-3"><label for="fac_usocfdi">Uso de CFDI</label></div>
												<div class="col-md-9"><select class="form-control" id="fac_usocfdi" name="fac_usocfdi" required><option value="">Seleccione</option><?php 
													foreach ($fuso->result() as $key) {
														echo '<option value="'.$key->uso_cfdi.'" class="option_'.$key->uso_cfdi.' pararf '.str_replace(',',' ',$key->pararf).'" disabled>'.$key->uso_cfdi_text.'</option>';
													}
												?></select></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="bordercontenedor" style="padding: 10px;">
										<div class="form-group">
											<label for="fac_folio">Folio de ticket</label>
											<input type="text" name="fac_folio" id="fac_folio" class="form-control" value="<?php if(isset($_GET['folio'])){echo $_GET['folio'];} ?>" required>
										</div>
									
										<div class="form-group">
											<label for="fac_monto">Monto de ticket</label>
											<input type="number" name="fac_monto" id="fac_monto" class="form-control" value="<?php if(isset($_GET['monto'])){echo $_GET['monto'];} ?>" required>
										</div>
									</div>
								</div>
							</div>
						</form>
					
					<div class="row">
						<div class="col-md-12">
							<div class="bordercontenedor" style="padding: 10px;">
								<div class="progress">
								  <div class="bar-barraprogreso progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
								</div>
								<div class="result_fac">
									
								</div>
								<div class="result_fac2">
									
								</div>
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-12" style="text-align:center;">
							<button type="button" class="btn btn-success btn-semit" id="generarfactura">Generar factura</button>
						</div>
						
					</div>
				</div>
				
			</div>
			<div id="Modalfactura" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalPopoversLabel" style="display: none;" aria-hidden="true">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalPopoversLabel">Envió de factura</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">×</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <div class="row">
			        	<div class="col-md-12 iframediv">
			        		
			        	</div>
			        	<div class="col-md-12">
			        		<label>Email</label>
			        		<input type="email"  class="form-control" id="email">
			        	</div>
			        </div>
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
			        <button type="button" class="btn btn-primary" id="enviomail">Enviar</button>
			      </div>
			    </div>
			  </div>
			</div>
			<footer id="footer"  style="background: #e6e6e6 !important; border-top: 4px solid #ffffff !important;">
		        <div class="container">

		          <div class="row">
		            <div class="col-lg-2">
		              <img style="width: 183px" src="<?php echo base_url() ?>public/img/footer/Image_06.png">
		            </div>
		            <div class="col-lg-2">
		              <img style="width: 164px; margin-left: 13px; margin-top: 7px;" src="<?php echo base_url() ?>public/img/footer/Image_07.png">
		            </div>
		            <div class="col-lg-3">
		              <img style="width: 183px; margin-left: 31px; margin-top: 13px;" src="<?php echo base_url() ?>public/img/footer/Image_08.png">
		            </div>
		            <div class="col-lg-1">
		            </div>  
		            <div class="col-lg-4">
		              <h6 style="color: black; margin-top: 18px;"><a style="color: black;" href="<?php echo base_url() ?>Inicio/aviso_de_privacidad">Aviso de privacidad</a> | Todos los derechos reservados | <?php echo date('Y') ?></h6>
		            </div>  
		          </div>
		        </div>      
            </footer>
		</div>
        
        <script src="<?php echo base_url();?>js/jquery-3.5.1.min.js"></script>
		<!-- Vendor -->
 		<script src="<?php echo base_url(); ?>vendor/plugins/js/plugins.min.js"></script>

		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>js/theme.js"></script>

		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>js/custom.js"></script>

		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>js/theme.init.js"></script>
		<script src="<?php echo base_url(); ?>js/sweealert/sweetalert.min.js"></script>
		<script src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
		<script src="<?php echo base_url();?>public/plugins/toastr/toastr.min.js"></script>
        <script src="<?php echo base_url(); ?>public/js/fac.js?v=<?php echo date('YmdGis');?>"></script>
	</body>
</html>
