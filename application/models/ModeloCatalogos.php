<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function updateCatalogo($Tabla,$data,$where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($Tabla);
        //return $id;
    }
    function getselectwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function getselectwheren_2($select,$table,$where){
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query->result();
    }
    function getdeletewheren($table,$where){
        $this->db->where($where);
        $this->db->delete($table);
    }
    function getdeletewheren3($table,$where){
        $this->db->delete($table,$where);
    }
    function getdeletewheren2($table,$where){
        
        $strq = "DELETE from $table WHERE idpago=$where";
        $query = $this->db->query($strq);
    }
    function getselectwherenall($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query->result();
    }
    public function getseleclike($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE estatus=1 and $cols like '%".$values."%'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    public function getseleclike2($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE status=1 and $cols like '%".$values."%'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    public function getseleclike3($tables,$cols,$values){
        $strq = "SELECT * from $tables WHERE activo=1 and $cols like '%".$values."%'";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }
    function updatestock($Tabla,$value,$masmeno,$value2,$idname,$id){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname=$id ";
        $query = $this->db->query($strq);
        return $id;
    }
    function updatestock3($Tabla,$value,$masmeno,$value2,$idname1,$id1,$idname2,$id2,$idname3,$id3){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname1=$id1 and $idname2=$id2 and $idname3=$id3 ";
        $query = $this->db->query($strq);
        //return $id;
    }



    function descripcionfactura($factura){
        $strq = "SELECT * FROM f_facturas_servicios where FacturasId=$factura";
        $query = $this->db->query($strq);
        $descripcion='';
        foreach ($query->result() as $item) {
            $descripcion.='<div>'.$item->Descripcion2.'</div>';
        }
        return $descripcion;
    }








    /*
    function getlistfoliostatus($cliente){

    */
    //==============================================
   
    //==============================================

    function nombreunidadfacturacion($id){
        $nombre=$id;
        /*
        $sql="SELECT nombre
                from unidades
                WHERE UnidadId='$id'";
        $query=$this->db->query($sql);
        $nombre='';
        foreach ($query->result() as $item) {
            $nombre=$item->nombre;
        }
        */
        return $nombre;
    }
    function traeProductosFactura($FacturasId){
        $sql="SELECT 
                dt.Unidad AS ClaveUnidad, 
                ser.Clave AS ClaveProdServ,
                dt.Descripcion2 as Descripcion, 
                dt.Cu, 
                dt.descuento, 
                dt.Cantidad, 
                dt.Importe, 
                u.nombre,
                u.Clave AS cunidad,
                dt.iva
                FROM f_facturas_servicios AS dt 
                left JOIN f_unidades AS u ON dt.Unidad = u.Clave 
                LEFT JOIN f_servicios AS ser ON ser.Clave = dt.ServicioId 
                WHERE dt.FacturasId =$FacturasId";
        $query=$this->db->query($sql);
        return $query;
    }

    function complementofacturas($facturaId){
        $sql="SELECT comp.complementoId,comp.FechaPago,comp.rutaXml,compd.NumParcialidad,compd.ImpPagado
                FROM f_complementopago AS comp
                inner join f_complementopago_documento as compd on compd.complementoId=comp.complementoId
                where comp.Estado=1 and compd.facturasId=$facturaId
            ";
        $query=$this->db->query($sql);
        return $query;
    }
  

    function total_facturas($tipo,$finicio, $ffin){
        if($tipo==1){
            $estado =' Estado = 1 ';
        }else{
            $estado =' Estado = 0 ';
        }
        $sql="SELECT count(*) as total FROM f_facturas 
                WHERE $estado and activo =1 and fechatimbre between '$finicio 00:00:00' and '$ffin 23:59:59'";

        $query=$this->db->query($sql);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        return $total;
    }
    function totalcomplementos($tipo,$finicio, $ffin){
        if($tipo==1){
            $estado =' Estado = 1 ';
        }else{
            $estado =' Estado = 0 ';
        }
        $sql="SELECT count(*) as total FROM f_complementopago 
                WHERE activo=1 and $estado and fechatimbre between '$finicio 00:00:00' and '$ffin 23:59:59'";

        $query=$this->db->query($sql);
        $total=0;
        foreach ($query->result() as $item) {
            $total=$item->total;
        }
        return $total;
    }
    function ultimoFolio() {
        //$strq = "SELECT Folio FROM f_facturas WHERE activo=1 and Folio!=0 ORDER BY FacturasId DESC limit 1";
        $strq = "SELECT max(Folio) as Folio FROM f_facturas WHERE activo=1";
        $Folio = 0;
        $query = $this->db->query($strq);
        foreach ($query->result() as $row) {
            $Folio =$row->Folio;
        } 
        return $Folio;
    }

    function topclientesfacturados(){
        $strq = "SELECT clienteId,Nombre,Rfc,sum(total) as total FROM `f_facturas` WHERE Estado=1 and activo=1 GROUP BY clienteId ORDER BY `total` DESC LIMIT 5";
        $query = $this->db->query($strq);
        return $query; 
    }
    function sumadelmesfacturados($fini,$ffin){
        $strq = "SELECT sum(total) as total FROM f_facturas WHERE Estado=1 and activo=1 AND fechatimbre BETWEEN '$fini 00:00:00' AND '$ffin 23:59:59'";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            if($row->total==null){
                $total =0;
            }else{
                $total =$row->total;    
            }
            
        }

        return $total;
    }
    function sumapagadocomplemento($factura){
        $strq = "SELECT sum(comd.ImpPagado) as total FROM `f_complementopago_documento` as comd INNER JOIN f_complementopago as com on com.complementoId=comd.complementoId WHERE comd.facturasId=$factura and com.Estado=1";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            if($row->total==null){
                $total =0;
            }else{
                $total =$row->total;    
            }
            
        }

        return $total;
    }

    public function get_cliente($buscar){
        $strq="SELECT clienteId,razon_social
                FROM clientes 
                WHERE activo = 1 AND razon_social like '%$buscar%'
                UNION
                SELECT clienteId,Nombre AS Nombre
                FROM f_facturas 
                WHERE activo = 1 AND Folio like '%$buscar%'"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function total_facturas_mes_num($anio,$mes){

        $strq = "SELECT COUNT(*) AS total 
                 FROM f_facturas
                 WHERE Estado=1 AND YEAR(fechatimbre)=$anio AND MONTH(fechatimbre)=$mes";
        $query = $this->db->query($strq);
        
        foreach ($query->result() as $row) {
            $total=$row->total;
        }

        return $total; 
    }

    function total_facturas_mes($anio,$mes){

        $strq = "SELECT SUM(total) AS total 
                 FROM f_facturas
                 WHERE Estado=1 AND YEAR(fechatimbre)=$anio AND MONTH(fechatimbre)=$mes";
        $query = $this->db->query($strq);
        
        foreach ($query->result() as $row) {
            $total=$row->total;
        }

        return $total; 
    }

    function total_facturas_ventas_mes($anio,$mes){

        $strq = "SELECT SUM(total) AS total 
                 FROM f_facturas
                 WHERE Estado=1 AND YEAR(fechatimbre)=$anio AND MONTH(fechatimbre)=$mes";
        $query = $this->db->query($strq);
        
        foreach ($query->result() as $row) {
            $total=$row->total;
        }

        return $total; 
    }

    function obtenerformapagoventa($idventa){

        $strq = "SELECT * FROM `venta_erp_formaspagos` WHERE idventa=$idventa ORDER BY monto DESC LIMIT 1";
        $query = $this->db->query($strq);
        $formapago='';
        foreach ($query->result() as $row) {
            $formapago=$row->formapago;
        }

        return $formapago; 
    }

}