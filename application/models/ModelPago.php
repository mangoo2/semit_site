<?php
class ModelPago extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_productos_total($id){
        $strq = "SELECT COUNT(*) AS total
                FROM carrito_temporal as c
                WHERE c.idcliente=$id"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function getDatosCPEstado_gruop($cp,$col){
        $this->db->select("ep.*, es.estado, es.id AS idestado,ep.mnpio");
        $this->db->from("estados_cp ep");
        $this->db->join("estado es","es.id=ep.id_estado","left");
        $this->db->where("codigo",$cp);
        $this->db->group_by("ep.codigo");
        $query=$this->db->get();
        return $query->result();
    }

    public function getDatosCPEstado($cp,$col){
        $this->db->select("ep.*, es.estado, es.id AS idestado");
        $this->db->from("estados_cp ep");
        $this->db->join("estado es","es.id=ep.id_estado","left");
        $this->db->where("codigo",$cp);
        $query=$this->db->get();
        return $query->result();
    }
    
    function get_detallespago($id){
        $strq = "SELECT *
                FROM pago_productos as c
                WHERE c.estatus=0 AND c.idcliente=$id"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_productos_venta($idtrans){
        $strq = "SELECT v.id AS idventa,v.total,v.idtransacción,DATE_FORMAT(v.reg,  '%Y-%m-%d' ) AS reg,c.nombre,c.calle,c.cp,c.colonia,v.reg,c.correo
                FROM venta as v
                inner JOIN clientes as c on c.id=v.idcliente
                WHERE v.idtransacción='$idtrans' GROUP BY v.id"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

}