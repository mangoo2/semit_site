<?php
class ModelGeneral extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function add_record($table,$data){
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
    
    public function edit_record($cos,$id,$data,$table){
        $this->db->set($data);
        $this->db->where($cos, $id);
        return $this->db->update($table);
    }
    
    public function edit_record_tabla($data,$table){
        $this->db->set($data);
        return $this->db->update($table);
    }

    public function delete_registro($data,$table){
        $this->db->where($data);
        return $this->db->delete($table);
    }
    
    public function getselectwhere($tables,$cols,$values){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($cols,$values);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    
    public function get_table($table){
    	$sql = "SELECT * FROM $table";
        $query = $this->db->query($sql);
        return $query->result();
    }
    public function get_select($tables,$where){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    
    public function get_select_order($tables,$where,$title,$order){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);/// Se puede ocupar un array para n condiciones
        $this->db->order_by($title,$order);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    

    public function getlike($tables,$match1,$matchv1,$match2,$matchv2){
        $this->db->select("*");
        $this->db->from($tables);
        $array1 = array($match1 => $matchv1);
        $array2 = array($match2 => $matchv2);
        $this->db->like($array1);
        $this->db->or_like($array2);
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }

    function get_productos(){
        $strq = "SELECT p.id AS idproductos,p.nombre,p.precio_con_iva,(select file from productos_files where idproductos = p.id AND activo=1 GROUP BY idproductos) as file
                FROM productos as p
                WHERE p.estatus=1 AND p.mostrar_pagina=1 AND p.mas_vendidos GROUP by rand() limit 4"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_productosone(){
        $strq = "SELECT p.id AS idproductos,p.nombre,p.precio_con_iva,(select file from productos_files where idproductos = p.id AND activo=1 GROUP BY idproductos) as file
                FROM productos as p
                WHERE p.estatus=1  AND p.mostrar_pagina=1 AND p.mas_vendidos GROUP by rand() limit 1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function  get_productos_cat($categoria,$nam,$precio){
        $like="";
        $whprecio="";
        if($nam!=''){
            $like=" AND p.nombre LIKE '%$nam%'"; 
        }
        
        if($precio!=0){
            $like=" AND p.nombre LIKE '%$nam%'"; 
        }
        
        $orderby="";
        if($precio==0){
            $whprecio="";
            $orderby="";
        }else if($precio==1){
            //$whprecio=" AND p.precio_con_iva<=299";
            $orderby="ORDER BY p.precio_con_iva ASC";
        }else if($precio==2){
            //$whprecio=" AND p.precio_con_iva>=300";
            $orderby="ORDER BY p.precio_con_iva DESC";
        }
        $strq = "SELECT p.nombre,p.precio_con_iva,p.id,(select file from productos_files where idproductos = p.id AND activo=1 GROUP BY idproductos) as file
                FROM productos as p
                WHERE p.estatus=1 AND p.categoria=$categoria $whprecio $like $orderby lIMIT 20"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_productos_temporal($idcliente){
        $strq = "SELECT p.nombre,p.precio_con_iva,c.id,c.cantidad,c.idproducto,(select file from productos_files where idproductos = c.idproducto AND activo=1 GROUP BY idproductos) as file
                FROM carrito_temporal as c
                inner JOIN productos as p on p.id=c.idproducto
                WHERE p.mostrar_pagina=1 AND c.idcliente=$idcliente"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_productos_compra($idcliente){
        $strq = "SELECT count(*) AS total
                FROM carrito_temporal as c
                WHERE c.idcliente=$idcliente"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_blog(){
        $strq = "SELECT * 
                FROM blog as b
                WHERE b.activo=1 limit 1"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_blog2($id){
        $strq = "SELECT * 
                FROM blog as b
                WHERE b.activo=1 AND b.id!=$id limit 2"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_blog3(){
        $strq = "SELECT * 
                FROM blog as b
                WHERE b.activo=1 limit 3"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

}