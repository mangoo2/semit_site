<?php
class ModelMicuenta extends CI_Model {

    public function __construct() {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function get_productos_favoritos($idcliente){
        $strq = "SELECT p.nombre,f.idproducto,(select file from productos_files where idproductos = f.idproducto AND activo=1 GROUP BY idproductos) as file
                FROM productos_favoritos as f
                inner JOIN productos as p on p.id=f.idproducto
                WHERE f.idcliente=$idcliente"; 
        $query = $this->db->query($strq);
        return $query->result();
    }


    function get_productos_pedidos($idcliente){
        $strq = "SELECT v.id AS idventa,v.total,v.idtransacción,p.id,DATE_FORMAT(v.reg,  '%Y-%m-%d' ) AS reg,vd.costo,c.nombre,p.nombre AS producto,(select file from productos_files where idproductos = vd.producto AND activo=1 GROUP BY idproductos) as file
                FROM venta as v
                inner JOIN venta_detalles as vd on vd.idventa=v.id
                inner JOIN productos as p on p.id=vd.producto
                inner JOIN clientes as c on c.id=v.idcliente
                WHERE v.idcliente=$idcliente"; 
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_productos_venta($idventa,$idcliente){
        $where='';
        if($idventa!=0){
            $where=' AND v.id='.$idventa; 
        }else{
            $where='';
        }
        $strq = "SELECT vd.id as idventad, v.id AS idventa,v.total,v.idtransacción,p.id,DATE_FORMAT(v.reg,  '%Y-%m-%d' ) AS reg,vd.costo,c.nombre,p.nombre AS producto,(select file from productos_files where idproductos = vd.producto AND activo=1 GROUP BY idproductos) as file,fac.FacturasId,fac.rutaXml
                FROM venta as v
                inner JOIN venta_detalles as vd on vd.idventa=v.id
                inner JOIN productos as p on p.id=vd.producto
                inner JOIN clientes as c on c.id=v.idcliente
                left JOIN f_facturas as fac on fac.relacion_venta_pro=vd.id and fac.Estado=1
                WHERE v.idcliente=$idcliente $where"; 
        $query = $this->db->query($strq);
        return $query->result();
    }
    
}