<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelofacturas extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function getfacturas($params){
        $cliente=$params['cliente'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $star=$params['star'];
        $columns = array( 
            0=>'fac.FacturasId',
            1=>'fac.Folio',
            2=>'fac.Nombre',
            3=>'fac.Rfc',
            4=>'fac.total',
            5=>'fac.fechatimbre',
            6=>'per.nombre',
            7=>'fac.favorito',
            8=>'fac.favorito',
            9=>'fac.Estado',
            
            10=>'fac.rutaXml',
            11=>'fac.rutaAcuseCancelacion',
            12=>'fac.cadenaoriginal',
            13=>'fac.FormaPago',
            14=>'fac.clienteId',
            15=>'fac.correoenviado',
            16=>'fac.estatus_correo',
            17=>'fac.uuid'
            
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('f_facturas fac');
        $this->db->join('personal per', 'per.personalId = fac.usuario_id','left');
        
        if($cliente>0){
            $this->db->where(array('fac.clienteId'=>$cliente));
        }
        if($finicio!=''){
            $this->db->where(array('fac.fechatimbre >='=>$finicio.' 00:00:00'));
        }
        if($ffin!=''){
            $this->db->where(array('fac.fechatimbre <='=>$ffin.' 23:59:59'));
        }
        if ($star==1) {
            $this->db->where(array('fac.favorito'=>1));
        }
        if($params['estatus_v']!=0) {
            $this->db->where(array('fac.Estado'=>$params['estatus_v']));    
        }
        $this->db->where(array('fac.activo'=>1));
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    public function total_facturas($params){
        $cliente=$params['cliente'];
        $finicio=$params['finicio'];
        $ffin=$params['ffin'];
        $star=$params['star'];
        $columns = array( 
            0=>'FacturasId',
            1=>'Folio',
            2=>'Nombre',
            3=>'Rfc',
            4=>'total',
            5=>'Estado',
            6=>'fechatimbre',
            7=>'rutaXml',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('f_facturas');
       
        if($cliente>0){
            $this->db->where(array('clienteId'=>$cliente));
        }
        if($finicio!=''){
            $this->db->where(array('fechatimbre >='=>$finicio.' 00:00:00'));
        }
        if($ffin!=''){
            $this->db->where(array('fechatimbre <='=>$ffin.' 23:59:59'));
        }
        if ($star==1) {
            $this->db->where(array('favorito'=>1));
        }
        if($params['estatus_v']!=0) {
            $this->db->where(array('Estado'=>$params['estatus_v']));    
        }
        $this->db->where(array('activo'=>1));
        //$where = ;
        //$this->db->where(array('facturaabierta'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        //$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }
    function documentorelacionado($complemento){
        $strq = "SELECT 
                    compd.facturasId,
                    compd.IdDocumento,
                    compd.NumParcialidad,
                    compd.ImpSaldoAnt,
                    compd.ImpPagado,
                    compd.ImpSaldoInsoluto,
                    compd.MetodoDePagoDR,
                    fac.moneda,
                    fac.Folio,
                    fac.serie,
                    fac.FormaPago,
                    fac.iva,
                    fac.ivaretenido,
                    fac.subtotal,
                    fac.serie
                FROM f_complementopago_documento as compd
                inner join f_facturas as fac on fac.FacturasId=compd.facturasId
                where compd.complementoId=$complemento";
        $query = $this->db->query($strq);
        return $query;
    }
    function facturadetalle($facturaId){
        $strq = "SELECT * FROM `f_facturas_servicios` as fas 
                inner join f_unidades as uni on uni.Clave=fas.Unidad
                WHERE fas.FacturasId=".$facturaId;
        $query = $this->db->query($strq);
        return $query; 
    }
    

    



}