<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timbrado extends CI_Controller {
	function __construct()    {
        parent::__construct();
        //$this->load->model('Login_model');
        //$this->load->model('General_model');
        //$this->load->model('ModelCatalogos');
        $this->load->model('ModeloCatalogos');
        $this->load->model('Modelofacturas');
        //$this->idpersonal=$this->session->userdata('idpersonal');
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fecha_reciente = date('Y-m-d');
        $this->fechahoy = date('Y-m-d');
        $this->fechahoyc = date('Y-m-d H:i:s');
        $this->fechahoyL = date('Y-m-d_H_i_s');
        $this->trunquearredondear=0;//0 redondeo 1 trunquear (dos dijitos)
        $this->demoproductivo=0;//0 demo 1 productivo
        if ($this->session->userdata('logeado')){
            $this->idcliente=$this->session->userdata('id');
            //$this->perfilid=$this->session->userdata('perfilid');
            //$this->idsucursal=1;// esto sera por seccion para que cambien entre cuentas     xxx
            
        }else{
            //redirect('Inicio');
        }
    }

	public function index(){
        redirect('Inicio');
    }
    function generafacturarabierta(){
      $datas = $this->input->post();
      if(isset($datas['conceptos'])){
            $conceptos = $datas['conceptos'];
            unset($datas['conceptos']);
      }
      //$save=$datas['save'];
      //$saveante=$datas['saveante'];
      //log_message('error', 'save: '.$save);
      //log_message('error', 'saveante: '.$saveante);

      $idcliente=$this->idcliente;
      $rfc=str_replace(' ', '', $datas['rfc']);
      $razon_social=$datas['razon_social'];
      $cp=str_replace(' ', '', $datas['cp']);
      $direccion='';
      $idventad = $datas['idventad'];
      $uso_cfdi = $datas['uso_cfdi'];
      $RegimenFiscalReceptor = $datas['RegimenFiscalReceptor'];
      
      $pais = 'MEXICO';
      $TipoComprobante='I';
      
      $ultimoFolioval=$this->ModeloCatalogos->ultimoFolio() + 1;
      
      //$conceptos_result=$this->ModeloCatalogos->getselectwheren('venta_detalles',array('idventa'=>$idventa));
      $conceptos_result=$this->ModeloCatalogos->getselectwheren('venta_detalles',array('id'=>$idventad));
      $importetotal=0;
      foreach ($conceptos_result->result() as $itemc) {
        $importetotal+=($itemc->cantidad*$itemc->costo);
      }
      $subtotal=round($importetotal/1.16,2);
      $iva=round($subtotal*0.16,2);
      $data = array(
                
                "nombre"        => $razon_social,
                "direccion"     => $direccion, 
                "cp"            => $cp,
                "rfc"           => $rfc,
                "folio"         => $ultimoFolioval,
                "PaisReceptor"  =>$pais,
                "clienteId"       => $idcliente,
                "serie" =>'H',
                //"status"        => 1,
                "TipoComprobante"=>$TipoComprobante,
                "usuario_id"       => $this->idcliente,
                "creada_sesionId"=> $this->idcliente,
                "rutaXml"           => '',
                "FormaPago"         => 'PUE',
                "tarjeta"       => '',
                "MetodoPago"        => '04',//04 tarjeta
                "ordenCompra"   => '',//no se ocupa
                "moneda"        => 'MXN',
                "observaciones" => '',
                "numproveedor"  => '',
                "numordencompra"=> '',
                "Lote"          => '',//no se ocupa
                "Paciente"      => '',//no se ocupa
                "Caducidad"     => '',//no se ocupa
                "uso_cfdi"      => $uso_cfdi,
                "subtotal"      => $subtotal,
                "iva"           => $iva,
                "total"         => $importetotal,
                "honorario"     => $subtotal,
                "ivaretenido"   => 0,
                "isr"           => 0,
                "cincoalmillarval"  => 0,
                "CondicionesDePago" => '',
                "outsourcing"   => 0,
                "facturaabierta"=>1,
                "f_relacion"    => 0,
                "f_r_tipo"      => 0,
                "f_r_uuid"      => '',
                "RegimenFiscalReceptor"=>$RegimenFiscalReceptor,
                "relacion_venta_pro"=>$idventad
            );
        if(isset($datas['pg_global'])){
          $data['pg_global']=$datas['pg_global'];
        }
        if(isset($datas['pg_periodicidad'])){
          $data['pg_periodicidad']=$datas['pg_periodicidad'];
        }
        if(isset($datas['pg_meses'])){
              $data['pg_meses']=$datas['pg_meses'];
        }
        if(isset($datas['pg_anio'])){
              $data['pg_anio']=$datas['pg_anio'];
        }    
      $FacturasId=$this->ModeloCatalogos->Insert('f_facturas',$data);

      foreach ($conceptos_result->result() as $itemc) {
            $conceptos_prot=$this->ModeloCatalogos->getselectwheren('productos',array('id'=>$itemc->producto));
            $nombre='';
            foreach ($conceptos_prot->result() as $itemp) {
                $nombre=$itemp->nombre;
            }
            $cu=round($itemc->costo/1.16,2);
            $ivap=round($cu*0.16,2);

            $dataco['FacturasId']=$FacturasId;
            $dataco['Cantidad'] =$itemc->cantidad;
            $dataco['Unidad'] ='H87';
            $dataco['servicioId'] ='10101501';
            $dataco['Descripcion'] ='Gatos vivos';
            $dataco['Descripcion2'] =$nombre;
            $dataco['Cu'] =$cu;
            $dataco['descuento'] =0;
            $dataco['Importe'] =($itemc->cantidad*$cu);
            $dataco['iva'] =$ivap;
            $this->ModeloCatalogos->Insert('f_facturas_servicios',$dataco);
      }

      /*
      $DATAc = json_decode($conceptos);
      for ($i=0;$i<count($DATAc);$i++) {

            $dataco['FacturasId']=$FacturasId;
            $dataco['Cantidad'] =$DATAc[$i]->Cantidad;
            $dataco['Unidad'] =$DATAc[$i]->Unidad;
            $dataco['servicioId'] =$DATAc[$i]->servicioId;
            $dataco['Descripcion'] =rtrim($DATAc[$i]->Descripcion);
            $dataco['Descripcion2'] =$DATAc[$i]->Descripcion2;
            $dataco['Cu'] =$DATAc[$i]->Cu;
            $dataco['descuento'] =$DATAc[$i]->descuento;
            $dataco['Importe'] =($DATAc[$i]->Cantidad*$DATAc[$i]->Cu);
            $dataco['iva'] =$DATAc[$i]->iva;
            $this->ModeloCatalogos->Insert('f_facturas_servicios',$dataco);
      }
      */
      
      
      
        $respuesta=$this->emitirfacturas($FacturasId);
      
      
      echo json_encode($respuesta);
    }
    function emitirfacturas($facturaId){
        $productivo=$this->demoproductivo;//0 demo 1 producccion
        //$this->load->library('Nusoap');
        //require_once APPPATH."/third_party/nusoap/nusoap.php"; 
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        //========================
        $datosFacturaa=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
        $datosFacturaa=$datosFacturaa->result();
        $datosFacturaa=$datosFacturaa[0];
        //============
        $datosFactura = array(
          'carpeta'=>'hulesyg',
          'pwskey'=>'',
          'archivo_key'=>'',
          'archivo_cer'=>'',
          'factura_id'=>$facturaId
        );
        //================================================
          $respuesta=array();
          $passwordLlavePrivada   = $datosFactura['pwskey'];
          $nombreArchivoKey       = base_url() . $datosFactura['archivo_key'];
          $nombreArchivoCer       = base_url() . $datosFactura['archivo_cer'];
          // ---------------- INICIAN COMANDOS OPENSSL --------------------
          //Generar archivo que contendra cadena de certificado (Convertir Certificado .cer a .pem)
          $comando='openssl x509 -inform der -in '.$nombreArchivoCer.' -out ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem';    
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -startdate > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/IniciaVigencia.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -enddate > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/FinVigencia.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -serial > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl x509 -inform DER -in '.$nombreArchivoCer.' -noout -text > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/datos.txt';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl pkcs8 -inform DER -in '.$nombreArchivoKey.' -passin pass:'.$passwordLlavePrivada.' > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada_pem.txt';
          //exec($comando,$salida_script,$valor_exit);
          $comando='openssl pkcs8 -inform DER -in '.$nombreArchivoKey.' -passin pass:'.$passwordLlavePrivada.' > ' . base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada.pem';
          //echo '<br><br>' . $comando .'<br><br>';
          //exec($comando,$salida_script,$valor_exit);
        // ---------------- TERMINAN COMANDOS OPENSSL --------------------      
          //GUARDAR NUMERO DE SERIE EN ARCHIVO     
          //$archivo=fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt','r');
          //$numeroCertificado= fread($archivo, filesize(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt'));   
          $numeroCertificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt');
          //fclose($archivo);
          $numeroCertificado=  str_replace("serial=", "", $numeroCertificado);//quitar letras
        
          $temporal=  str_split($numeroCertificado);//Pasar todos los digitos a un array
        
          $numeroCertificado="";
          $i=0;  
          foreach ($temporal as $value) {
              
              if(($i%2))
              
                  $numeroCertificado .= $value;
          
              $i++;
          }
        
        $numeroCertificado = str_replace('\n','',$numeroCertificado);
        
        $numeroCertificado = trim($numeroCertificado);
        
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('certificado'=>$numeroCertificado),array('FacturasId'=>$facturaId));
        //$this->model_mango_invoice->actualizaCampoFactura('certificado',$numeroCertificado,$datosFactura['factura_id']);
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('nocertificado'=>$numeroCertificado),array('FacturasId'=>$facturaId));
        //$this->model_mango_invoice->actualizaCampoFactura('nocertificado',$numeroCertificado,$datosFactura['factura_id']);
        
        $respuesta['numeroCertificado']=$numeroCertificado;

        //$archivo = fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem', "r");
        
        //$certificado = fread($archivo, filesize(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem'));
        $certificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem');
        
        //fclose($archivo);
        $certificado=  str_replace("-----BEGIN CERTIFICATE-----", "", $certificado);
        
        $certificado=  str_replace("-----END CERTIFICATE-----", "", $certificado);
        
        $certificado=  str_replace("\n", "", $certificado);
        $certificado=  str_replace(" ", "", $certificado);
        $certificado=  str_replace("\n", "", $certificado);
        $certificado= preg_replace("[\n|\r|\n\r]", '', $certificado);
        $certificado= trim($certificado);
        
        $respuesta['certificado']=$certificado;
            
        $arrayDatosFactura=  $this->getArrayParaCadenaOriginal($facturaId);  
        //=================================================================
          $horaactualm1=date('Y-m-d H:i:s');
          //$horaactualm1 = strtotime ( '-1 minute' , strtotime ( $horaactualm1 ) ) ;
          $horaactualm1 = strtotime ( '-1 hour -1 minute' , strtotime ( $horaactualm1 ) ) ;
          //$horaactualm1=date ( 'H:i:s' , $horaactualm1 );
          $datosfacturaadd['fechageneracion']=date ( 'Y-m-d' , $horaactualm1 ).'T'.date ( 'H:i:s' , $horaactualm1 );
        //==================================================================
        $xml=  $this->getXML($facturaId,'',$certificado,$numeroCertificado,$datosfacturaadd);
        //echo "contruccion de xml ===========================";  //borrar despues 
        //echo $xml;
        //echo "=====";
        
        log_message('error', 'generaCadenaOriginalReal xml1: '.$xml);
        //log_message('error', '-');log_message('error', '-');log_message('error', '-');log_message('error', '-');log_message('error', '-');
        $str = trim($this->generaCadenaOriginalReal($xml,$datosFactura['carpeta']));    
        log_message('error', 'generaCadenaOriginalReal xml2: '.$str);
        $cadena=$str;
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('cadenaoriginal'=>$str),array('FacturasId'=>$datosFactura['factura_id']));
        //$clienteSOAP = new nusoap_client('https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL',array('soap_version' => SOAP_1_2)); //productivo
        //$clienteSOAP = new nusoap_client('https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL');//productivo
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
          //$URL_WS='https://app.appfacturainteligente.com/WSTimbrado33Test/WSCFDI33.svc?WSDL';//solo para pruebas ya que no tengo los accesos al demo
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        
        //$clienteSOAP = new nusoap_client('https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL');//pruebas
        $clienteSOAP = new SoapClient($URL_WS);

        //$clienteSOAP->soap_defencoding='UTF-8';
        
        $referencia=$datosFactura['factura_id']."-C-". $this->idpersonal;
        
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
          //solo para pruebas ya que no tengo los accesos al demo
          //$password= "contRa$3na";
          //$usuario= "COE960119D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
        
        
        $fp = fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada.pem', "r"); 
        
        $priv_key = fread($fp, 8192); 
        
        fclose($fp); 
        
        $pkeyid = openssl_get_privatekey($priv_key);
        
        //openssl_sign($cadena, $sig, $pkeyid);
        //openssl_sign($cadena, $sig, $pkeyid,'sha256');
        openssl_sign(utf8_encode($cadena), $sig, $pkeyid,'sha256');
        
        openssl_free_key($pkeyid);
        
        $sello = base64_encode($sig);
        //echo $sello;
        $respuesta['sello']=$sello;
        
        //$this->model_mango_invoice->actualizaCampoFactura('sellocadena',$sello,$datosFactura['factura_id']);
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('sellocadena'=>$sello),array('FacturasId'=>$datosFactura['factura_id']));
        //Gererar XML 2013-12-05T12:05:56
        $xml=  $this->getXML($facturaId,$sello,$certificado,$numeroCertificado,$datosfacturaadd);                
        //$fp = fopen(DIR_FILES . $datosFactura['carpeta'] . '/facturas/preview_'. $datosFactura['rfcEmisor'].'_'. $datosFactura['folio'].'.xml', 'w');//Cadena original 
        file_put_contents($datosFactura['carpeta'].'/facturas/preview_'. $datosFacturaa->Rfc.'_'. $datosFacturaa->Folio.'.xml',$xml);
                
        //Preparar parametros para web service    
        //cadenaXML

        $result = $clienteSOAP->TimbrarCFDI(array(
                    'usuario' => $usuario,
                    'password' => $password,
                    'cadenaXML' => $xml,
                    'referencia' => $referencia));
        file_put_contents($datosFactura['carpeta'] . '/facturaslog/'.'log_facturas_result_'.$this->fechahoyL.'_.txt', json_encode($result));
        $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Referencia'=>$referencia),array('FacturasId'=>$datosFactura['factura_id']));
    
        if( $result->TimbrarCFDIResult->CodigoRespuesta != '0' ) {                       
            
            $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'facturaId'=>$facturaId,
                          'info'=>$result,
                          'info2'=>$result->TimbrarCFDIResult->MensajeErrorDetallado
                          );
            //$this->session->data['error'] = $resultado['CodigoRespuesta'] . ' ' . $resultado['MensajeError'] . ' ' . $resultado['MensajeErrorDetallado']. ' No se puede timbrar la factura verifique la informacion por favor <BR/> PORFAVOR RETIMBRAR MAS TARDE';
                        
            //$this->load->model("setting/general");  
            
            //$this->model_setting_general->saveLog($this->session->data['id_usuario'],$this->session->data['activo'],$resultado,2);
            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>2),array('FacturasId'=>$datosFactura['factura_id']));
            //$this->model_mango_invoice->actualizaCampoFactura('Estado','2',$datosFactura['factura_id']);
            
            //quirar restriccion de salida de sesion por error 12/02/2014 david garduño
            //$this->redirect($this->url->link('common/logout', 'token=' . $this->session->data['token'], 'SSL'));
            
            return $resultado;
        
        } else {
          
            try {
           
                $xmlCompleto=utf8_decode($result->TimbrarCFDIResult->XMLResultado);//Contiene XML
            
                //Guardar en archivo     y la ruta en la bd
                $ruta = $datosFactura['carpeta'] . '/facturas/'.$datosFacturaa->Rfc.'_'.$datosFacturaa->Folio.'.xml';
            
                file_put_contents($ruta,"\xEF\xBB\xBF".$xmlCompleto);
                $this->ModeloCatalogos->updateCatalogo('f_facturas',array('rutaXml'=>$ruta),array('FacturasId'=>$datosFactura['factura_id']));            
            
                //$sxe = new SimpleXMLElement($xmlCompleto);
                         
                //$ns = $sxe->getNamespaces(true);
            
                //$sxe->registerXPathNamespace('c', $ns['cfdi']);
            
                //$sxe->registerXPathNamespace('t', $ns['tfd']);
 
                //$uuid = '';
            
                //foreach ($sxe->xpath('//t:TimbreFiscalDigital') as $tfd) {          
                 
                    //Actualizamos el Folio Fiscal UUID
                    //$uuid = $tfd['UUID'];
                    $updatedatossat=array(
                      'uuid'=>$result->TimbrarCFDIResult->Timbre->UUID,
                      'sellosat'=>$result->TimbrarCFDIResult->Timbre->SelloSAT,
                      'nocertificadosat'=>$result->TimbrarCFDIResult->Timbre->NumeroCertificadoSAT,
                      'nocertificado'=>$numeroCertificado,
                      'certificado'=>$numeroCertificado,
                      'fechatimbre'=>date('Y-m-d G:i:s'),
                      'Estado'=>1
                    );
                    $this->ModeloCatalogos->updateCatalogo('f_facturas',$updatedatossat,array('FacturasId'=>$datosFactura['factura_id'])); 
                //} 
            
                //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>1),array('FacturasId'=>$datosFactura['factura_id']));

                $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Generada',
                          'facturaId'=>$facturaId
                          );
            return $resultado;
           }  catch (Exception $e) {
              $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'facturaId'=>$facturaId,
                          'info'=>$result,
                          'info2'=>$result->TimbrarCFDIResult->MensajeErrorDetallado
                          );
             
            $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>2),array('FacturasId'=>$datosFactura['factura_id']));
            
            return $resultado;
                
           }
        
        }
        
        return $resultado;

    }
    function getArrayParaCadenaOriginal($facturaId){
      $datosFactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
      $datosFactura=$datosFactura->result();
      $datosFactura=$datosFactura[0];

      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      //=======================================================================
            $conceptos = array();
            $datosconceptos=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$facturaId));
            foreach ($datosconceptos->result() as $item) {
              $unidad=$this->ModeloCatalogos->nombreunidadfacturacion($item->Unidad);
              $conceptos[]    =   array(
                                       'cantidad' => $item->Cantidad,
                                       'unidad' =>  $unidad,
                                       'descripcion' =>  $item->Descripcion,
                                       'valorUnnitario' =>  $item->Cu,
                                       'importe' =>  $item->Importe
                                    );
            }
      //=======================================================================
      $datos=array(
                  'comprobante' => array(
                                            'version' => '3.2',  //Requerido                     
                                            'fecha' => $this->fechahoyc,       //Requerido                
                                            'tipoDeComprobante' => $datosFactura->TipoComprobante,//Requedido                       
                                            'formaDePago' => $datosFactura->FormaPago,                       
                                            'subtotal' => $datosFactura->subtotal,                       
                                            'total' => $datosFactura->total,                       
                                            'metodoDePago' => $datosFactura->MetodoPago,                       
                                            'lugarExpedicion' => $datosconfiguracion->LugarExpedicion                       
                                        ),
                  'emisor'    => array(
                                            'rfc' => $datosconfiguracion->Rfc,//Requedido
                                            'domocilioFiscal' => array(
                                                                        'calle' => $datosconfiguracion->Calle,//Requerido
                                                                        'municipio' => $datosconfiguracion->Municipio,//Requerido 
                                                                        'estado' => $datosconfiguracion->Estado,//Requerido 
                                                                        'pais' => $datosconfiguracion->PaisExpedicion,//Requedido
                                                                        'codigoPostal' => $datosconfiguracion->CodigoPostal//Requedido
                                                                        ),
                                            'expedidoEn' => array(
                                                                        'pais' => $datosconfiguracion->PaisExpedicion//Requedido
                                                                 ),
                                            'regimenFiscal' => array(
                                                                        'regimen' => $datosconfiguracion->CodigoPostal//Requedido
                                                                    )
                                      ),
                  'receptor'    => array(
                                            'rfc' => $datosFactura->Rfc,//Requedido
                                            'nombreCliente' => $datosFactura->Nombre,//Opcional
                                            'domicilioFiscal' => array( 
        
           
                                                                        'calle' => '',//Opcional
                                                                        'noExterior' => '',//Opcional
                                                                        'colonia' => '',//Opcional                   todos son datos del receptor
                                                                        'municipio' => '',//Opcional
                                                                        'estado' => '',//Opcional
                                                                        'pais' => $datosFactura->PaisReceptor //Requedido
                                                                       )
                                      ),
                  'conceptos' => $conceptos,
             
                  'traslados' => array(
                                        'impuesto' => '002',//iva
                                        'tasa' => 16.00,
                                        'importe' => $datosFactura->subtotal,
                                        'totalImpuestosTrasladados' => $datosFactura->iva
                                      )
                                      
                );
         return $datos;
    }
    public function generaCadenaOriginalReal($xml,$carpeta) {   
        error_reporting(0);
        $paso = new DOMDocument();
        //log_message('error', 'generaCadenaOriginalReal xml: '.$xml);
        $paso->loadXML($xml);
        
        $xsl = new DOMDocument();               
        
        //$file= base_url().$carpeta."/xslt33/cadenaoriginal_3_3.xslt";
        $file= base_url().$carpeta."/xslt40/cadenaoriginal_4_0.xslt";
        //$file="http://www.sat.gob.mx/sitio_internet/cfd/4/cadenaoriginal_4_0/cadenaoriginal_4_0.xslt";
        
        $xsl->load($file);
        
        $proc = new XSLTProcessor();
        // activar php_xsl.dll
        
        $proc->importStyleSheet($xsl); 
        
        $cadena_original = $proc->transformToXML($paso);
        
        //echo '<br><br>' . $cadena_original . '<br><br>';
        
        $str = mb_convert_encoding($cadena_original, 'UTF-8');                      
   
        //GUARDAR CADENA ORIGINAL EN ARCHIVO  
         
        $sello = utf8_decode($str);
                        
        $sello = str_replace('#13','',$sello);
        
        $sello = str_replace('#10','',$sello);
        
        //$fp = fopen(DIR_FILES . $carpeta . '/temporalsat/CadenaOriginal.txt', 'w');//Cadena original 
        /*
        $fp = fopen(base_url() . $carpeta . '/temporalsat/CadenaOriginal.txt', 'w');//Cadena original 
        
        fwrite($fp, $sello);
        
        fclose($fp);
        */
        file_put_contents( $carpeta . '/temporalsat/CadenaOriginal.txt',$sello);
        return $sello;    
    }
    private function getXML($facturaId,$sello,$certificado,$numeroCertificado,$datosfacturaadd) {
      log_message('error', '$sello c: '.$sello);
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        //==============================================================================================================

        $datosFactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$facturaId));
        $datosFactura=$datosFactura->result();
        $datosFactura=$datosFactura[0];
        //====================================
        $clienteId=$datosFactura->clienteId;
        //$datoscliente=$this->ModeloCatalogos->getselectwheren('clientes',array('clienteId'=>$clienteId));
        //$datoscliente=$datoscliente->result();
        //$datoscliente=$datoscliente[0];
        //===============================================================================================================
            $xmlConceptos = '';
            $conceptos = array();
            $datosconceptos=$this->ModeloCatalogos->getselectwheren('f_facturas_servicios',array('FacturasId'=>$facturaId));
            foreach ($datosconceptos->result() as $item) {
                $unidad=$this->ModeloCatalogos->nombreunidadfacturacion($item->Unidad);
                $xmlConceptos.= '<cfdi:Concepto cantidad="' . chop(ltrim(trim($item->Cantidad))) . '" ';
                $xmlConceptos.= 'unidad="' . chop(ltrim($unidad)) . '" descripcion="' . chop(ltrim($item->servicioId)) . '"';
                $xmlConceptos.= ' valorUnitario="' . chop(ltrim(trim(round($item->Cu, 2)))) . '" importe="' . round(($item->Importe), 2) . '"/>';

              $conceptos[]    =   array(
                                       'cantidad' => $item->Cantidad,
                                       'unidad' =>  $item->Unidad,
                                       'descripcion' =>  $item->Descripcion,
                                       'valorUnnitario' =>  $item->Cu,
                                       'importe' =>  $item->Importe
                                    );
            }
        //===============================================================================================================
        
        /******************************************************/
        /*          INICIA ASIGNACION DE VARIABLES           */
        /****************************************************/
        //$certificado=str_replace("\n", "", $certificado);
        $caract   = array('&','ñ','Ñ','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú','"');
        $caract2  = array('&amp;','&ntilde;','&Ntilde;','','a','e','i','o','u','A','E','I','O','U','&quot;');
        $caractc   = array('&','ñ','Ñ','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú');
        $caract2c  = array('','n','N','','a','e','i','o','u','A','E','I','O','U');
        $fecha=$this->fechahoyc;
        $calleEmisor=$datosconfiguracion->Calle;
        $NoexteriorEmisor=$datosconfiguracion->Noexterior;
        $coloniaEmisor=$datosconfiguracion->Colonia;
        $localidadEmisor=$datosconfiguracion->localidad;
        $estadoEmisor=$datosconfiguracion->Estado;
        $municipioEmisor=$datosconfiguracion->Municipio;
        $codigoPostalEmisor=$datosconfiguracion->CodigoPostal;
        $formaPago=$datosFactura->FormaPago;
        $subtotal=$datosFactura->subtotal;
        $total=$datosFactura->total;
        $uso_cfdi=$datosFactura->uso_cfdi;
        $FacturasId=$datosFactura->FacturasId;
        $serie=$datosFactura->serie;
        $f_relacion=$datosFactura->f_relacion;
        $f_r_tipo=$datosFactura->f_r_tipo;
        $f_r_uuid=$datosFactura->f_r_uuid;
        $impuesto='002';
        $tasa=16.00;
        $impuestosTrasladados=$datosFactura->iva;
        if($this->trunquearredondear==0){
          $impuestosTrasladados=round($impuestosTrasladados,2);
        }else{
          $impuestosTrasladados=floor(($impuestosTrasladados*100))/100;
        }
        $tipoComprobante='Ingreso';
        $metodoPago=$datosFactura->MetodoPago;
        $lugarExpedicion=$datosconfiguracion->LugarExpedicion;
        $rfcEmisor=$datosconfiguracion->Rfc;
        $nombreEmisor=$datosconfiguracion->Nombre;
        $paisEmisor=$datosconfiguracion->Pais;
        $paisExpedicion=$datosconfiguracion->PaisExpedicion;
        $regimenFiscal=$datosconfiguracion->Regimen;
        $rfcReceptor=str_replace(' ', '', $datosFactura->Rfc);
        $paisReceptor=$datosFactura->PaisReceptor;
        
        $localidadReceptor='';
        
        $xmlConceptos=$xmlConceptos;
        $nombreCliente=strval(str_replace($caract, $caract2, $datosFactura->Nombre));
        $CondicionesDePago = strval(str_replace($caractc, $caract2c, $datosFactura->CondicionesDePago));
        //$calleReceptor=$datosFactura['calleReceptor'];
        //$noExteriorReceptor=$datosFactura['noExteriorReceptor'];
        //$municipioReceptor=$datosFactura['municipioReceptor'];
        //$estadoReceptor=$datosFactura['estadoReceptor'];
        //$coloniaReceptor=$datosFactura['coloniaReceptor'];
        $folio=$datosFactura->Folio;

        $isr=$datosFactura->isr;
        $ivaretenido=$datosFactura->ivaretenido;
        $cedular=$datosFactura->cedular;
        $cincoalmillarval=$datosFactura->cincoalmillarval;
        $outsourcing=$datosFactura->outsourcing;
        $totalimpuestosretenido=$isr+$ivaretenido+$outsourcing;

        if ($totalimpuestosretenido==0) {
            $TotalImpuestosRetenidoss='';
        }else{
            $TotalImpuestosRetenidoss='TotalImpuestosRetenidos="'.round($totalimpuestosretenido,2).'"';
        }
        if ($cedular!=0) {
            $impuestoslocal='xmlns:implocal="http://www.sat.gob.mx/implocal"';
            $impuestoslocal2=' http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd';
        }else{
            $impuestoslocal='';
            $impuestoslocal2='';
        }
        if ($cincoalmillarval>0) {
            $impuestoslocal='xmlns:implocal="http://www.sat.gob.mx/implocal"';
            $impuestoslocal2=' http://www.sat.gob.mx/implocal http://www.sat.gob.mx/sitio_internet/cfd/implocal/implocal.xsd';
        }else{
            $impuestoslocal='';
            $impuestoslocal2='';
        }

        
        //cambios para poner el acr?nimo de la moneda en el XML
        if ($datosFactura->moneda=="USD") {
            $moneda="USD";
            $TipoCambio='TipoCambio="19.35"';
        }elseif ($datosFactura->moneda=="EUR") {
            $moneda="EUR";
            $TipoCambio='';
        }else{
            $moneda="MXN";
            $TipoCambio='';
        }
        //DATOS DE TIMBRE
        if(isset($datosFactura->uuid)) { 
        
            $uuid = $datosFactura->uuid;
            
        } else {
            
            $uuid= '';   
        }
        if(isset($datosFactura->sellosat)) {
            
            $selloSAT = $datosFactura->sellosat;
        } else {
            
            $selloSAT = '';
        }
        if(isset($datosFactura->certificado)) {   
            $certificadoSAT=$datosFactura->certificado;
        } else {
            $certificadoSAT='';
        }
        /*
        if(!empty($datosFactura['noInteriorReceptor'])) {   
            $noInt = '" noInterior="' . $datosFactura['noInteriorReceptor'] . '';
        } else {
            
            $noInt = '';
        }
        */
        /*  $minuto=date('i');
        $m=$minuto-1;
        $contar=strlen($m);
        if($contar==1)
        {
        $min='0'.$m;
        }else{$min=$m;} */  
        
        $importabasetotal=0;
        
        //.date('Y-m-d').'T'.date('H:i').':'.$segundos.
        
        /******************************************************/
        /*          TERMINA ASIGNACION DE VARIABLES          */
        /****************************************************/   
        /*
        $horaactualm1=date('H:i:s');
        $horaactualm1 = strtotime ( '-1 minute' , strtotime ( $horaactualm1 ) ) ;
        $horaactualm1=date ( 'H:i:s' , $horaactualm1 );
        */
        $factura_detalle = $this->ModeloCatalogos->traeProductosFactura($FacturasId); 
        $descuentogeneral=0;
        foreach ($factura_detalle->result() as $facturadell) {
          $descuentogeneral=$descuentogeneral+$facturadell->descuento;
        }

        if ($descuentogeneral>0) {
          $xmldescuentogene=' Descuento="'.$descuentogeneral.'" ';
        }else{
          $xmldescuentogene=' ';
        }

        $fechageneracion=$datosfacturaadd['fechageneracion'];
        $razon_social_receptor=str_replace(array('&','ñ','"'),array('&amp;','&ntilde;','&quot;'), $datosFactura->Nombre);
        $xml= '<?xml version="1.0" encoding="utf-8"?>';
        $xml.='<cfdi:Comprobante xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '.$impuestoslocal.' xsi:schemaLocation="http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd'.$impuestoslocal2.'" Exportacion="01" ';
        $xml .= 'Version="4.0" Serie="'.$serie.'" Folio="'.$folio.'" Fecha="'.$fechageneracion.'" FormaPago="'.$metodoPago.'" ';
        if($CondicionesDePago!=''){
            $xml .= 'CondicionesDePago="'.$CondicionesDePago.'" ';
        }
        //$xml .= 'SubTotal="'.$subtotal.'" Descuento="0.00" Moneda="'.$moneda.'" TipoCambio="1" Total="'.$total.'" TipoDeComprobante="I" MetodoPago="'.$formaPago.'" LugarExpedicion="' .$codigoPostalEmisor . '" ';
        $xml .= 'SubTotal="'.$subtotal.'" Moneda="'.$moneda.'" '.$xmldescuentogene.' '.$TipoCambio.'  Total="'.round($total,2).'" TipoDeComprobante="I" MetodoPago="'.$formaPago.'" LugarExpedicion="' .$codigoPostalEmisor . '" ';
        $xml .='xmlns:cfdi="http://www.sat.gob.mx/cfd/4" NoCertificado="'.$numeroCertificado.'" Certificado="'.$certificado.'" Sello="'.$sello.'">';
                if($f_relacion==1){
                    $xml .='<cfdi:CfdiRelacionados TipoRelacion="'.$f_r_tipo.'">
                                <cfdi:CfdiRelacionado UUID="'.$f_r_uuid.'"/>
                            </cfdi:CfdiRelacionados>';
                }
                if($rfcReceptor=='XAXX010101000' and $datosFactura->pg_global==1){
                    $xml .= '<cfdi:InformacionGlobal Año="'.$datosFactura->pg_anio.'" Meses="'.$datosFactura->pg_meses.'" Periodicidad="'.$datosFactura->pg_periodicidad.'"/>';
                }
        $xml .= '<cfdi:Emisor Nombre="'.$nombreEmisor.'" RegimenFiscal="'.$regimenFiscal.'" Rfc="'.$rfcEmisor.'"></cfdi:Emisor>';
        $xml .= '<cfdi:Receptor Rfc="'.$rfcReceptor.'" Nombre="'.$razon_social_receptor.'" DomicilioFiscalReceptor="'.$datosFactura->Cp.'" RegimenFiscalReceptor="'.$datosFactura->RegimenFiscalReceptor.'" UsoCFDI="'.$uso_cfdi.'" ></cfdi:Receptor>';
        $xml .='<cfdi:Conceptos>';
        
        //$isr=$datosFactura['isr'];
        //$ivaretenido=$datosFactura['ivaretenido'];
        //$cedular=$datosFactura['cedular'];

        
        $partidaproducto=1;
        $niva=$tasa/100;
        $comparariva = $datosFactura->iva;//colocar
        foreach ($factura_detalle->result() as $facturadell) {
            $valorunitario=$facturadell->Cu;
            if($facturadell->descuento>0){
              $xmldescuento=' Descuento="'.$facturadell->descuento.'" ';
            }else{
              $xmldescuento='';
            }
            if($facturadell->Importe==$facturadell->descuento){
                $ObjetoImp='01';//no objeto de impuesto
            }else{
                $ObjetoImp='02';//si, objeto de impuestos
            }
            if($rfcReceptor=='XAXX010101000'){
                $xml_unidad='';
            }else{
                $xml_unidad=' Unidad="'.$facturadell->nombre.'" ';
            }
            
            //$xml .='<cfdi:Concepto ClaveUnidad="'.$facturadell['cunidad'].'" Unidad="'.$facturadell['nombre'].'" ClaveProdServ="'.$facturadell['ClaveProdServ'].'" NoIdentificacion="'.str_pad((int) $partidaproducto,10,"0",STR_PAD_LEFT).'" Cantidad="'.$facturadell['Cantidad'].'" Descripcion="'.$facturadell['Descripcion'].'" ValorUnitario="'.$facturadell['Cu'].'" Importe="'.$facturadell['Importe'].'" Descuento="0.00">';
            $xml .='<cfdi:Concepto ClaveUnidad="'.$facturadell->cunidad.'" ClaveProdServ="'.$facturadell->ClaveProdServ.'"';
             //$xml.='NoIdentificacion="'.str_pad((int) $partidaproducto,10,"0",STR_PAD_LEFT).'" ';
             $xml.=' Cantidad="'.$facturadell->Cantidad.'" '.$xml_unidad.' Descripcion="'.utf8_encode(str_replace($caract, $caract2, $facturadell->Descripcion)).'" ValorUnitario="'.$valorunitario.'" Importe="'.round($facturadell->Importe,2).'" '.$xmldescuento.' ObjetoImp="'.$ObjetoImp.'">';
                if($comparariva>0 or $facturadell->iva==0){
                //if($facturadell->iva>0){
                  $importabase=$facturadell->Importe-$facturadell->descuento;
                  if($this->trunquearredondear==0){
                    $trasladadoimporte=round($importabase*$niva,4);
                  }else{
                    $trasladadoimporte=$importabase*$niva;
                    $trasladadoimporte=floor(($trasladadoimporte*100))/100;
                  }
                  if($importabase>0 or $facturadell->iva==0){
                    $xml .='<cfdi:Impuestos>';
                        $xml .='<cfdi:Traslados>';
                            $importabasetotal=$importabasetotal+$importabase;
                            if($facturadell->iva>0){
                                $xml .='<cfdi:Traslado Base="'.$importabase.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="'.$niva.'0000" Importe="'.$trasladadoimporte.'"></cfdi:Traslado>';
                            }else{
                                $importabasetotal=$importabasetotal-$importabase;
                                $xml .='<cfdi:Traslado Base="'.$importabase.'" Impuesto="002" TipoFactor="Exento" ></cfdi:Traslado>';
                            }
                        $xml .='</cfdi:Traslados>';
                        //-----------------------------------------------------------------------------------
                        if($isr!=0 ||$ivaretenido!=0||$outsourcing>0){
                            $xml .='<cfdi:Retenciones>';
                            if ($isr!=0) {
                                $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="001" TipoFactor="Tasa" TasaOCuota="0.100000" Importe="'.round($facturadell->Importe*0.100000,2).'"></cfdi:Retencion>';
                            }
                            if ($ivaretenido!=0) {
                                $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.106666" Importe="'.round($facturadell->Importe*0.106666,2).'"></cfdi:Retencion>'; 
                            }   
                            if ($outsourcing!=0) {
                                $xml .='<cfdi:Retencion Base="'.$facturadell->Importe.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.060000" Importe="'.round($facturadell->Importe*0.06,2).'"></cfdi:Retencion>'; 
                            }      
                            $xml .='</cfdi:Retenciones>';
                        }
                        //------------------------------------------------------------------------------------- 
                        //$xml .='<cfdi:Retenciones>';
                        //    $xml .='<cfdi:Retencion Base="1000" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="0.00" />';      
                        //$xml .='</cfdi:Retenciones>';
                    $xml .='</cfdi:Impuestos>';
                }
            }
            $xml .='</cfdi:Concepto>';
            $partidaproducto++;
        }
        $xml .='</cfdi:Conceptos>';
        if($comparariva>0){
            //$xml .='<cfdi:Impuestos TotalImpuestosRetenidos="0.00" TotalImpuestosTrasladados="'.$impuestosTrasladados.'">';
            $xml .='<cfdi:Impuestos '.$TotalImpuestosRetenidoss.' TotalImpuestosTrasladados="'.$impuestosTrasladados.'">';
                if($isr!=0 ||$ivaretenido!=0||$outsourcing>0){
                    $xml .='<cfdi:Retenciones>';
                    if ($isr!=0) {
                        $xml .='<cfdi:Retencion Impuesto="001" Importe="'.number_format(round($isr,2), 2, ".", "").'"></cfdi:Retencion>';
                    }
                    if ($ivaretenido!=0) {
                        $xml .='<cfdi:Retencion Impuesto="002" Importe="'.number_format(round($ivaretenido,2), 2, ".", "").'"></cfdi:Retencion>'; 
                    } 
                    if ($outsourcing>0) {
                        $xml .='<cfdi:Retencion Impuesto="002" Importe="'.number_format(round($outsourcing,2), 2, ".", "").'"></cfdi:Retencion>'; 
                    }      
                    $xml .='</cfdi:Retenciones>';
                }
               
                
                
                $xml .='<cfdi:Traslados>';
                    
                        $xml .='<cfdi:Traslado Base="'.$importabasetotal.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="'.$impuestosTrasladados.'"></cfdi:Traslado>';    
                $xml .='</cfdi:Traslados>';
            $xml .='</cfdi:Impuestos>';
        }
        /*
        if($cedular!=0){
            
        }
        */
        if($cedular!=0||$uuid||$cincoalmillarval>0) {            
            $xml .= '<cfdi:Complemento>';
            if ($cedular!=0) {
                //$xml .= '<cfdi:Complemento xmlns:cfdi="http://www.sat.gob.mx/cfd/3" >';
                $xml .= '<implocal:ImpuestosLocales version="1.0" TotaldeTraslados="0.00" TotaldeRetenciones="'.$cedular.'" >';
                $xml .= '<implocal:RetencionesLocales  TasadeRetencion="01.00" ImpLocRetenido="CEDULAR" Importe="'.$cedular.'"/>';
                $xml .= '</implocal:ImpuestosLocales>';
            }
            if ($cincoalmillarval>0) {
                $xml .= '<implocal:ImpuestosLocales version="1.0" TotaldeTraslados="0.00" TotaldeRetenciones="'.$cincoalmillarval.'" >';
                  $xml .= '<implocal:RetencionesLocales Importe="'.$cincoalmillarval.'" TasadeRetencion="0.50" ImpLocRetenido=".005 Insp y Vig" />';
                $xml .= '</implocal:ImpuestosLocales>';
            }
            if ($uuid) {
                $xml .='<tfd:TimbreFiscalDigital Version="1.1" RfcProvCertif="FLI081010EK2"';
                $xml .='UUID="'.$uuid.'" FechaTimbrado="'.$fecha.'" ';
                $xml .='SelloCFD="'.$sello.'" NoCertificadoSAT="'.$certificadoSAT.'" SelloSAT="'.$selloSAT.'"';
                $xml .='xmlns:tfd="http://www.sat.gob.mx/TimbreFiscalDigital"';
                $xml .= 'xsi:schemaLocation="http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/cfd/TimbreFiscalDigital/TimbreFiscalDigitalv11.xsd" />';
            }
            







                
            $xml .= '</cfdi:Complemento>';
        }
        
        $xml .= '</cfdi:Comprobante>';
        
       
        return $xml;
    }
    /*
    function cancelarCfdi(){
      $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/hulesygrapas';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/hulesyg/temporalsat/';
      $rutaf=$rutainterna.'/hulesyg/facturas/';
      $rutalog=$rutainterna.'/hulesyg/facturaslog/';

      $productivo=1;//0 demo 1 producccion
      $datas = $this->input->post();
      $facturas=$datas['facturas'];
      unset($datas['facturas']);

      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      $rFCEmisor=$datosconfiguracion->Rfc;
      $passwordClavePrivada=$datosconfiguracion->paswordkey;

      $datosCancelacion=array();
      $DATAf = json_decode($facturas);
      for ($i=0;$i<count($DATAf);$i++) {
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$DATAf[$i]->FacturasIds));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];
        $FacturasId=$datosfactura->FacturasId;
        $datosCancelacion[] = array(
                                  'Propiedad'=>'cancelacion',
                                  'RFCReceptor'=>$datosfactura->Rfc,
                                  'Total'=>round($datosfactura->total, 2),
                                  'UUID'=>$datosfactura->uuid

                                );
      }
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
      //=======================================================
        
        $fp = fopen($ruta.'pfx.pem', "r");
        $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
        fclose($fp);
        
        
        $rutaarchivos=$rutainterna.'/hulesyg/elementos/';
        $rutapass=$rutaarchivos.'passs.txt';
        $passwordClavePrivada = file_get_contents($rutapass);
        
        //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
        
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'rFCEmisor' => $rFCEmisor,
            'listaCFDI' => $datosCancelacion,
            'clavePrivada_Base64'=>$clavePrivada,
            'passwordClavePrivada'=> $passwordClavePrivada  );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'xxxxx_.txt', json_encode($parametros));
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->CancelarCFDIConValidacion($parametros);

        if($result->CancelarCFDIConValidacionResult->OperacionExitosa) {
          $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Cancelada',
                          'info'=>$result
                          );
          //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
                $fileacuse='acuseCancelacion_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
                $ruta = $rutaf .$fileacuse;
                //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
                file_put_contents($ruta,$result->CancelarCFDIConValidacionResult->XMLAcuse);

                $datosdecancelacion = array(
                                            'Estado'=>0,
                                            'rutaAcuseCancelacion '=>$fileacuse,
                                            'fechacancelacion'=>$this->fechahoyc
                                          );
                $this->ModeloCatalogos->updateCatalogo('f_facturas',$datosdecancelacion,array('FacturasId'=>$FacturasId));
          
        }else{
          $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->CancelarCFDIConValidacionResult->MensajeError,
                          'MensajeError'=>$result->CancelarCFDIConValidacionResult->MensajeErrorDetallado,
                          'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                          'info'=>$result
                          );
          file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
        }
        
        echo json_encode($resultado);
    }
    */
    function cancelarCfdi(){
      $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/hulesygrapas';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/hulesyg/temporalsat/';
      $rutaf=$rutainterna.'/hulesyg/facturas/';
      $rutalog=$rutainterna.'/hulesyg/facturaslog/';

      $productivo=1;//0 demo 1 producccion
      $datas = $this->input->post();
      $facturas=$datas['facturas'];
      $motivo=$datas['motivo'];
      $uuidrelacionado=$datas['uuidrelacionado'];

      unset($datas['facturas']);
      unset($datas['motivo']);
      unset($datas['uuidrelacionado']);

      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      $rFCEmisor=$datosconfiguracion->Rfc;
      $passwordClavePrivada=$datosconfiguracion->paswordkey;

      $datosCancelacion=array();
      $DATAf = json_decode($facturas);
      for ($i=0;$i<count($DATAf);$i++) {
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$DATAf[$i]->FacturasIds));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];
        $FacturasId=$datosfactura->FacturasId;
        $datosCancelacion[] = array(
                                  'RFCReceptor'=>$datosfactura->Rfc,
                                  'Total'=>round($datosfactura->total, 2),
                                  'UUID'=>$datosfactura->uuid,
                                  'Motivo'=>$motivo,
                                  'FolioSustitucion'=>$uuidrelacionado

                                );
      }
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
      //=======================================================
        
        $fp = fopen($ruta.'pfx.pem', "r");
        $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
        fclose($fp);
        
        
        $rutaarchivos=$rutainterna.'/hulesyg/elementos/';
        $rutapass=$rutaarchivos.'passs.txt';
        $passwordClavePrivada = file_get_contents($rutapass);
        
        //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
        
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'rFCEmisor' => $rFCEmisor,
            'listaCFDI' => $datosCancelacion,
            'clavePrivada_Base64'=>$clavePrivada,
            'passwordClavePrivada'=> $passwordClavePrivada  );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'xxxxx_.txt', json_encode($parametros));
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->CancelarCFDI($parametros);

        $fileacuse1='log_cancelcion2_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
        $ruta1 = $rutalog .$fileacuse1;

        file_put_contents($ruta1,json_encode($result));

        if($result->CancelarCFDIResult->OperacionExitosa) {
          $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Cancelada',
                          'info'=>$result
                          );
          //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
                $fileacuse='acuseCancelacion_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
                $ruta = $rutaf .$fileacuse;
                //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
                file_put_contents($ruta,$result->CancelarCFDIResult->XMLAcuse);

                $datosdecancelacion = array(
                                            'Estado'=>0,
                                            'rutaAcuseCancelacion '=>$fileacuse,
                                            'fechacancelacion'=>$this->fechahoyc
                                          );
                $this->ModeloCatalogos->updateCatalogo('f_facturas',$datosdecancelacion,array('FacturasId'=>$FacturasId));
          
        }else{
          $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->CancelarCFDIResult->MensajeError,
                          'MensajeError'=>$result->CancelarCFDIResult->MensajeErrorDetallado,
                          'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                          'info'=>$result
                          );
          file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
        }
        
        echo json_encode($resultado);
    }
    /*
    function cancelarCfdicomplemento(){
      $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/hulesygrapas';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/hulesyg/temporalsat/';
      $rutaf=$rutainterna.'/hulesyg/facturas/';
      $rutalog=$rutainterna.'/hulesyg/facturaslog/';

      $productivo=1;//0 demo 1 producccion
      $datas = $this->input->post();
      $facturas=$datas['facturas'];
      unset($datas['facturas']);

      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      $rFCEmisor=$datosconfiguracion->Rfc;
      $passwordClavePrivada=$datosconfiguracion->paswordkey;

      $datosCancelacion=array();
      //$DATAf = json_decode($facturas);
      //for ($i=0;$i<count($DATAf);$i++) {
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$facturas));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];
        $FacturasId=$datosfactura->FacturasId;
        $datosCancelacion[] = array(
                                  'Propiedad'=>'cancelacion',
                                  'RFCReceptor'=>$datosfactura->R_rfc,
                                  'Total'=>0,
                                  'UUID'=>$datosfactura->uuid

                                );
        //$this->liberarfacturas($DATAf[$i]->FacturasIds);
      //}
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
      //=======================================================
        
        $fp = fopen($ruta.'pfx.pem', "r");
        $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
        fclose($fp);
        
        
        $rutaarchivos=$rutainterna.'/hulesyg/elementos/';
        $rutapass=$rutaarchivos.'passs.txt';
        $passwordClavePrivada = file_get_contents($rutapass);
        
        //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
        
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'rFCEmisor' => $rFCEmisor,
            'listaCFDI' => $datosCancelacion,
            'clavePrivada_Base64'=>$clavePrivada,
            'passwordClavePrivada'=> $passwordClavePrivada  );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'xxxxx_.txt', json_encode($parametros));
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->CancelarCFDIConValidacion($parametros);

        if($result->CancelarCFDIConValidacionResult->OperacionExitosa) {
          $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Cancelada',
                          'info'=>$result
                          );
          //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
                $fileacuse='acuseCancelacion_'.$facturas.'_'.$datosfactura->uuid.'.xml';
                $ruta = $rutaf .$fileacuse;
                //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
                file_put_contents($ruta,$result->CancelarCFDIConValidacionResult->XMLAcuse);

                $datosdecancelacion = array(
                                            'Estado'=>0,
                                            'rutaAcuseCancelacion '=>$fileacuse,
                                            'fechacancelacion'=>$this->fechahoyc
                                          );
                $this->ModeloCatalogos->updateCatalogo('f_complementopago',$datosdecancelacion,array('complementoId'=>$facturas));
          
        }else{
          $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->CancelarCFDIConValidacionResult->MensajeError,
                          'MensajeError'=>$result->CancelarCFDIConValidacionResult->MensajeErrorDetallado,
                          'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                          'info'=>$result
                          );
          file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
        }
        
        echo json_encode($resultado);
    }
    */
    function cancelarCfdicomplemento(){
      $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/hulesygrapas';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/hulesyg/temporalsat/';
      $rutaf=$rutainterna.'/hulesyg/facturas/';
      $rutalog=$rutainterna.'/hulesyg/facturaslog/';

      $productivo=1;//0 demo 1 producccion
      $datas = $this->input->post();
      $facturas=$datas['facturas'];
      $motivo=$datas['motivo'];
      $uuidrelacionado=$datas['uuidrelacionado'];
      
      unset($datas['facturas']);
      unset($datas['motivo']);
      unset($datas['uuidrelacionado']);

      $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>1));
      $datosconfiguracion=$datosconfiguracion->result();
      $datosconfiguracion=$datosconfiguracion[0];
      $rFCEmisor=$datosconfiguracion->Rfc;
      $passwordClavePrivada=$datosconfiguracion->paswordkey;

      $datosCancelacion=array();
      //$DATAf = json_decode($facturas);
      //for ($i=0;$i<count($DATAf);$i++) {
        $datosfactura=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$facturas));
        $datosfactura=$datosfactura->result();
        $datosfactura=$datosfactura[0];
        $FacturasId=$datosfactura->FacturasId;
        $datosCancelacion[] = array(
                                  'RFCReceptor'=>$datosfactura->R_rfc,
                                  'Total'=>0,
                                  'UUID'=>$datosfactura->uuid,
                                  'Motivo'=>$motivo,
                                  'FolioSustitucion'=>$uuidrelacionado

                                );
        //$this->liberarfacturas($DATAf[$i]->FacturasIds);
      //}
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
      //=======================================================
        
        $fp = fopen($ruta.'pfx.pem', "r");
        $clavePrivada = fread($fp, filesize($ruta.'pfx.pem'));
        fclose($fp);
        
        
        $rutaarchivos=$rutainterna.'/hulesyg/elementos/';
        $rutapass=$rutaarchivos.'passs.txt';
        $passwordClavePrivada = file_get_contents($rutapass);
        
        //$passwordClavePrivada='Daniel182 ';// solo para las pruebas
        
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'rFCEmisor' => $rFCEmisor,
            'listaCFDI' => $datosCancelacion,
            'clavePrivada_Base64'=>$clavePrivada,
            'passwordClavePrivada'=> $passwordClavePrivada  );
        file_put_contents($rutalog.'log_'.$this->fechahoyL.'xxxxx_.txt', json_encode($parametros));
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->CancelarCFDI($parametros);

        if($result->CancelarCFDIResult->OperacionExitosa) {
          $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Cancelada',
                          'info'=>$result
                          );
          //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
                $fileacuse='acuseCancelacion_'.$facturas.'_'.$datosfactura->uuid.'.xml';
                $ruta = $rutaf .$fileacuse;
                //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
                file_put_contents($ruta,$result->CancelarCFDIResult->XMLAcuse);

                $datosdecancelacion = array(
                                            'Estado'=>0,
                                            'rutaAcuseCancelacion '=>$fileacuse,
                                            'fechacancelacion'=>$this->fechahoyc
                                          );
                $this->ModeloCatalogos->updateCatalogo('f_complementopago',$datosdecancelacion,array('complementoId'=>$facturas));
          
        }else{
          $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->CancelarCFDIResult->MensajeError,
                          'MensajeError'=>$result->CancelarCFDIResult->MensajeErrorDetallado,
                          'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                          'info'=>$result
                          );
          file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
        }
        
        echo json_encode($resultado);
    }
    function retimbrar(){
      $factura = $this->input->post('factura');
      $facturaresult=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$factura));
      foreach ($facturaresult->result() as $item) {
        $Folio=$item->Folio;
        if($Folio==0){
          $newfolio=$this->ModeloCatalogos->ultimoFolio() + 1;
          $this->ModeloCatalogos->updateCatalogo('f_facturas',array('Folio'=>$newfolio),array('FacturasId'=>$factura));
        }
      }
      $respuesta=$this->emitirfacturas($factura);
      echo json_encode($respuesta);
    }
    function addcomplemento(){
      $data = $this->input->post();
        $arraydoc=$data['arraydocumento'];
        unset($data['arraydocumento']);
        //$idfactura=$data['idfactura'];
        //==================================================================
          //$datosfactura=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$idfactura));
          //$datosfactura=$datosfactura->result();
          //$datosfactura=$datosfactura[0];
        //==================================================================
          $idsucursal=$this->idsucursal;
          $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$idsucursal));
          $datosconfiguracion=$datosconfiguracion->result();
          $datosconfiguracion=$datosconfiguracion[0];
        //==================================================================

        //$Sello=$datosfactura->sellosat;
        //$Sello=$datosfactura->sellocadena;
        
        $datacp['Folio']=$data['Folio'];
        $datacp['Fecha']=$data['Fecha'];
        $datacp['Sello']='';
        //$datacp['NoCertificado']=$datosfactura->nocertificado;
        //$datacp['Certificado']=$datosfactura->certificado;
        $datacp['Certificado']='';
        $datacp['LugarExpedicion']=$data['LugarExpedicion'];
        $datacp['E_rfc']=$datosconfiguracion->Rfc;
        $datacp['E_nombre']=$datosconfiguracion->Nombre;
        $datacp['E_regimenfiscal']=$datosconfiguracion->Regimen;
        $datacp['R_rfc']=$data['rfcreceptor'];
        $datacp['R_nombre']=$data['razonsocialreceptor'];
        $datacp['R_regimenfiscal']='';
        $datacp['FechaPago']=$data['Fechatimbre'];
        $datacp['FormaDePagoP']=$data['FormaDePagoP'];
        $datacp['MonedaP']=$data['ModedaP'];
        $datacp['Monto']=$data['Monto'];
        $datacp['NumOperacion']=str_replace(' ', '', $data['NumOperacion']);
        $datacp['CtaBeneficiario']=$data['CtaBeneficiario'];
        
        $datacp['Serie']=$data['Serie'];//xxx
        $datacp['Foliod']=$data['Folio'];
        $datacp['MonedaDR']=$data['ModedaP'];

        $datacp['f_relacion']    = $data['f_r'];
        $datacp['f_r_tipo']      = $data['f_r_t'];
        $datacp['f_r_uuid']      = $data['f_r_uuid'];
        
    
        $datacp['UsoCFDI']='';
        $idcomplemento=$this->ModeloCatalogos->Insert('f_complementopago',$datacp);
        $DATAdoc = json_decode($arraydoc);
        for ($j=0;$j<count($DATAdoc);$j++){
          $datacdoc['complementoId']=$idcomplemento;
          $datacdoc['facturasId']=$DATAdoc[$j]->idfactura;
          $datacdoc['IdDocumento']=$DATAdoc[$j]->IdDocumento;
          $datacdoc['serie']=$DATAdoc[$j]->serie;
          $datacdoc['folio']=$DATAdoc[$j]->folio;
          $datacdoc['NumParcialidad']=$DATAdoc[$j]->NumParcialidad;
          $datacdoc['ImpSaldoAnt']=$DATAdoc[$j]->ImpSaldoAnt;
          $datacdoc['ImpPagado']=$DATAdoc[$j]->ImpPagado;
          $datacdoc['ImpSaldoInsoluto']=$DATAdoc[$j]->ImpSaldoInsoluto;
          $datacdoc['MetodoDePagoDR']=$DATAdoc[$j]->MetodoDePagoDR;
          $this->ModeloCatalogos->Insert('f_complementopago_documento',$datacdoc);
        }





        $respuesta=$this->procesarcomplemento($idcomplemento);

        echo json_encode($respuesta);
    }
    function retimbrarcomplemento(){
      $complemento = $this->input->post('complemento');
      $respuesta=$this->procesarcomplemento($complemento);
        echo json_encode($respuesta);
    }
    function procesarcomplemento($idcomplemento){
      //========================================================================
      $rutainterna= $_SERVER['DOCUMENT_ROOT'].'/hulesygrapas';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      $ruta=$rutainterna.'/hulesyg/temporalsat/';
      $rutaf=$rutainterna.'/hulesyg/facturas/';
      $rutalog=$rutainterna.'/hulesyg/facturaslog/';
        $datosFactura = array(
          'carpeta'=>'hulesyg',
          'pwskey'=>'',
          'archivo_key'=>'',
          'archivo_cer'=>'',
          'factura_id'=>0
        );
        $numeroCertificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat/Serial.txt');
          //fclose($archivo);
          $numeroCertificado=  str_replace("serial=", "", $numeroCertificado);//quitar letras
        
          $temporal=  str_split($numeroCertificado);//Pasar todos los digitos a un array
        
          $numeroCertificado="";
          $i=0;  
          foreach ($temporal as $value) {
              
              if(($i%2))
              
                  $numeroCertificado .= $value;
          
              $i++;
          }
        
        $numeroCertificado = str_replace('\n','',$numeroCertificado);
        
        $numeroCertificado = trim($numeroCertificado);

        $certificado=file_get_contents(base_url() . $datosFactura['carpeta'] . '/temporalsat/certificate.pem');
        
        //fclose($archivo);
        $certificado=  str_replace("-----BEGIN CERTIFICATE-----", "", $certificado);
        
        $certificado=  str_replace("-----END CERTIFICATE-----", "", $certificado);
        
        $certificado=  str_replace("\n", "", $certificado);
        $certificado=  str_replace(" ", "", $certificado);
        $certificado=  str_replace("\n", "", $certificado);
        $certificado= preg_replace("[\n|\r|\n\r]", '', $certificado);
        $certificado= trim($certificado);
      //========================================================================================
        $xml=$this->generaxmlcomplemento('',$certificado,$numeroCertificado,$idcomplemento);
        log_message('error', 'se genera xml2: '.$xml);
        $str = trim($this->generaCadenaOriginalReal($xml,$datosFactura['carpeta']));    
        
        $cadena=$str;
      //===========================================================================================
        $fp = fopen(base_url() . $datosFactura['carpeta'] . '/temporalsat/llaveprivada.pem', "r"); 
        
        $priv_key = fread($fp, 8192); 
        
        fclose($fp); 
        
        $pkeyid = openssl_get_privatekey($priv_key);
        
        //openssl_sign($cadena, $sig, $pkeyid);
        openssl_sign($cadena, $sig, $pkeyid,'sha256');
        
        openssl_free_key($pkeyid);
        
        $sello = base64_encode($sig);
      //===============accessos al webservice===================================================

        $productivo=0;//0 demo 1 productivo
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
        $idsucursal=$this->idsucursal;
        $datosconfiguracion=$this->ModeloCatalogos->getselectwheren('f_configuraciones',array('ConfiguracionesId'=>$idsucursal));
        $datosconfiguracion=$datosconfiguracion->result();
        $datosconfiguracion=$datosconfiguracion[0];
        $rFCEmisor=$datosconfiguracion->Rfc;
        $passwordClavePrivada=$datosconfiguracion->paswordkey;

        $clienteSOAP = new SoapClient($URL_WS);

      //==================================================================
          $datoscomplemento=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$idcomplemento));
          $datoscomplemento=$datoscomplemento->result();
          $datoscomplemento=$datoscomplemento[0];
        //==================================================================
          
          $xmlcomplemento=$this->generaxmlcomplemento($sello,$certificado,$numeroCertificado,$idcomplemento);
          //log_message('error', 'se genera xml3: '.$xmlcomplemento);
            file_put_contents('hulesyg/facturas/preview_complemento_'.$idcomplemento.'.xml',$xmlcomplemento);
            
            $referencia=$datoscomplemento->E_rfc.'_'.$idcomplemento;
        //======================================================================================
            
          $result = $clienteSOAP->TimbrarCFDI(array(
                    'usuario' => $usuario,
                    'password' => $password,
                    'cadenaXML' => $xmlcomplemento,
                    'referencia' => $referencia));
          file_put_contents($rutalog.'log_complemento'.$this->fechahoyL.'_.txt', json_encode($result));
          if($result->TimbrarCFDIResult->CodigoRespuesta > 0 ) {                       
            
            $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'idcomplemento'=>$idcomplemento,
                          'info'=>$result,
                          'info2'=>$result->TimbrarCFDIResult->MensajeErrorDetallado
                          );
            
            $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('Estado'=>2),array('complementoId'=>$idcomplemento));
                        
            return $resultado;
        
        } else {
          
            try {
           
                $xmlCompleto=utf8_decode($result->TimbrarCFDIResult->XMLResultado);//Contiene XML
            
                //Guardar en archivo     y la ruta en la bd
                $ruta = $datosFactura['carpeta'] . '/facturas/complemento_'.$idcomplemento.'.xml';
            
                file_put_contents($ruta,"\xEF\xBB\xBF".$xmlCompleto);
                $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('rutaXml'=>$ruta),array('complementoId'=>$idcomplemento));            
            
                $sxe = new SimpleXMLElement($xmlCompleto);
                         
                $ns = $sxe->getNamespaces(true);
            
                $sxe->registerXPathNamespace('c', $ns['cfdi']);
            
                $sxe->registerXPathNamespace('t', $ns['tfd']);
 
                $uuid = '';
            
                foreach ($sxe->xpath('//t:TimbreFiscalDigital') as $tfd) {          
                 
                    //Actualizamos el Folio Fiscal UUID
                    //$uuid = $tfd['UUID'];
                    $updatedatossat=array(
                      'uuid'=>$tfd['UUID'],
                      'Sello'=>$tfd['SelloCFD'],
                      'sellosat'=>$tfd['SelloSAT'],
                      'NoCertificado'=>$tfd['NoCertificado'],
                      'nocertificadosat'=>$tfd['NoCertificadoSAT'],
                      'fechatimbre'=>date('Y-m-d G:i:s'),
                      'cadenaoriginal'=>$cadena,
                      'Estado'=>1
                    );
                    $this->ModeloCatalogos->updateCatalogo('f_complementopago',$updatedatossat,array('complementoId'=>$idcomplemento)); 
                } 
            
                //$this->ModeloCatalogos->updateCatalogo('f_facturas',array('Estado'=>1),array('FacturasId'=>$datosFactura['factura_id']));

                $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Complemento Generado',
                          'idcomplemento'=>$idcomplemento
                          );
            return $resultado;
           }  catch (Exception $e) {
              $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->TimbrarCFDIResult->CodigoRespuesta,
                          'MensajeError'=>$result->TimbrarCFDIResult->MensajeError,
                          'Mensajeadicional'=>'PORFAVOR RETIMBRAR MAS TARDE',
                          'idcomplemento'=>$idcomplemento,
                          'info'=>$result,
                          'info2'=>$result->TimbrarCFDIResult->MensajeErrorDetallado
                          );
             
            $this->ModeloCatalogos->updateCatalogo('f_complementopago',array('Estado'=>2),array('complementoId'=>$idcomplemento));
            
            return $resultado;
                
           }
        
        }
          //echo json_encode($result);
          
          return $resultado;

    }
    function generaxmlcomplemento($sello,$certificado,$numeroCertificado,$idcomplemento){
        
          $datoscomplemento=$this->ModeloCatalogos->getselectwheren('f_complementopago',array('complementoId'=>$idcomplemento));
          $datoscomplemento=$datoscomplemento->result();
          $datoscomplemento=$datoscomplemento[0];
          $caract   = array('&','ñ','Ñ','°','á','é','í','ó','ú','Á','É','Í','Ó','Ú');
          $caract2  = array('&amp','&ntilde','&ntilde','','a','e','i','o','u','A','E','I','O','U');
          $R_nombre=strval(str_replace($caract, $caract2, $datoscomplemento->R_nombre));
            $f_relacion=$datoscomplemento->f_relacion;
            $f_r_tipo=$datoscomplemento->f_r_tipo;
            $f_r_uuid=$datoscomplemento->f_r_uuid;
        //==================================================================
            $datoscliente=$this->ModeloCatalogos->getselectwheren('clientes',array('rfc'=>$datoscomplemento->R_rfc,'activo'=>1));
            $datoscliente=$datoscliente->result();
            $datoscliente=$datoscliente[0];
        //==================================================================
          $NumOperacion=str_replace(' ', '', $datoscomplemento->NumOperacion);

          $xmlcomplemento='<?xml version="1.0" encoding="utf-8"?>';
          $xmlcomplemento.='
            <cfdi:Comprobante xmlns:pago20="http://www.sat.gob.mx/Pagos20" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:cfdi="http://www.sat.gob.mx/cfd/4" xsi:schemaLocation="http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd http://www.sat.gob.mx/Pagos20 http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos20.xsd" Version="4.0" Exportacion="01"
              Folio="'.$datoscomplemento->Folio.'" Fecha="'.date('Y-m-d',strtotime($datoscomplemento->Fecha)).'T'.date('H:i:s',strtotime($datoscomplemento->Fecha)).'" Sello="'.$sello.'" NoCertificado="'.$numeroCertificado.'" Certificado="'.$certificado.'" SubTotal="'.$datoscomplemento->SubTotal.'" Moneda="'.$datoscomplemento->Moneda.'" Total="'.$datoscomplemento->Total.'" TipoDeComprobante="'.$datoscomplemento->TipoDeComprobante.'" LugarExpedicion="'.$datoscomplemento->LugarExpedicion.'">';
              if($f_relacion==1){
                    $xmlcomplemento .='<cfdi:CfdiRelacionados TipoRelacion="'.$f_r_tipo.'">
                                <cfdi:CfdiRelacionado UUID="'.$f_r_uuid.'"/>
                            </cfdi:CfdiRelacionados>';
                }$xmlcomplemento .='
            <cfdi:Emisor Rfc="'.$datoscomplemento->E_rfc.'" Nombre="'.$datoscomplemento->E_nombre.'" RegimenFiscal="'.$datoscomplemento->E_regimenfiscal.'"/>
            <cfdi:Receptor Rfc="'.$datoscomplemento->R_rfc.'" Nombre="'.$R_nombre.'"  DomicilioFiscalReceptor="'.$datoscliente->cp.'" RegimenFiscalReceptor="'.$datoscliente->RegimenFiscalReceptor.'" UsoCFDI="CP01"/>
            <cfdi:Conceptos>
              <cfdi:Concepto ClaveUnidad="'.$datoscomplemento->ClaveUnidad.'" ClaveProdServ="'.$datoscomplemento->ClaveProdServ.'" Cantidad="'.$datoscomplemento->Cantidad.'" Descripcion="'.$datoscomplemento->Descripcion.'" ValorUnitario="'.$datoscomplemento->ValorUnitario.'" Importe="'.$datoscomplemento->Importe.'" ObjetoImp="01"/>
            </cfdi:Conceptos>
            <cfdi:Complemento>';
            $dcompdoc=$this->Modelofacturas->documentorelacionado($idcomplemento);
            
            $xmlcomplemento.='<pago20:Pagos Version="2.0">
                <pago20:Totales MontoTotalPagos="'.$datoscomplemento->Monto.'" />
                <pago20:Pago FechaPago="'.date('Y-m-d',strtotime($datoscomplemento->FechaPago)).'T'.date('H:i:s',strtotime($datoscomplemento->FechaPago)).'" FormaDePagoP="'.$datoscomplemento->FormaDePagoP.'" MonedaP="MXN" Monto="'.$datoscomplemento->Monto.'" TipoCambioP="1"  ';
            $xmlcomplemento.=' NumOperacion="'.$NumOperacion.'" '; 
            //$xmlcomplemento.=' CtaBeneficiario="9680096800" ';
            $xmlcomplemento.=' >';
            //$dcompdoc=$this->ModeloCatalogos->getselectwheren('f_complementopago_documento',array('complementoId'=>$idcomplemento));
            
            foreach ($dcompdoc->result() as $itemdoc) {
             $xmlcomplemento.='<pago20:DoctoRelacionado IdDocumento="'.$itemdoc->IdDocumento.'" Serie="'.$itemdoc->serie.'" Folio="'.$itemdoc->Folio.'" MonedaDR="'.$datoscomplemento->MonedaDR.'" NumParcialidad="'.$itemdoc->NumParcialidad.'" ImpSaldoAnt="'.$itemdoc->ImpSaldoAnt.'" ImpPagado="'.$itemdoc->ImpPagado.'" ImpSaldoInsoluto="'.$itemdoc->ImpSaldoInsoluto.'" ObjetoImpDR="02" EquivalenciaDR="1">';
                                
                $xmlcomplemento.='<pago20:ImpuestosDR>
                                    
                                  </pago20:ImpuestosDR>';
                                  
             $xmlcomplemento.='</pago20:DoctoRelacionado>';
            }
            
            $xmlcomplemento.='</pago20:Pago>
            </pago20:Pagos>
            </cfdi:Complemento>
            </cfdi:Comprobante>';
            //log_message('error', 'se genera xml: '.$xmlcomplemento);
      return $xmlcomplemento;
    }
    function estatuscancelacionCfdi($uuid){
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'].'/kyocera2';//rutalocal
      //$rutainterna= $_SERVER['DOCUMENT_ROOT'];//rutaserver
      //$ruta=$rutainterna.'/kyocera/temporalsat/';
      //$rutaf=$rutainterna.'/kyocera/facturas/';
      //$rutalog=$rutainterna.'/kyocera/facturaslog/';

      $productivo=1;//0 demo 1 producccion
      
        $res_uuid=$uuid;
        $res_facturaid=0;
      
      

      //var_dump($datosfactura->uuid);
      //===========================================================
        if($productivo==0){
          $URL_WS='https://app.fel.mx/WSTimbrado33Test/WSCFDI33.svc?WSDL';
        }elseif ($productivo==1) {
          //Produccion
          $URL_WS='https://www.fel.mx/WSTimbrado33/WSCFDI33.svc?WSDL';
        }else{
          $URL_WS='';
        }
        if($productivo==0){
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }elseif ($productivo==1) {
          //Produccion
          $password= "@fuJXsS@";
          $usuario= "HERD890308UAA";
        }else{
          //usuario demo
          $password= "contRa$3na";
          $usuario= "HERD890308D33";
        }
      //=======================================================
        /*
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'UUID' => $datosfactura->uuid);
        */
        $parametros = array(
            'usuario' => $usuario,
            'password' => $password,
            'uUID' => $res_uuid);
        //echo json_encode($parametros);
        $clienteSOAP = new SoapClient($URL_WS);

        $result = $clienteSOAP->ObtenerAcuseCancelacion($parametros);

        //$fileacuse1='log_cancelcion_estatus_'.$res_facturaid.'_'.$res_uuid.'.xml';
        //$ruta1 = $rutalog .$fileacuse1;
        $resultado=$result;
        
        //file_put_contents($ruta1,json_encode($result));
        /*if($result->CancelarCFDIResult->OperacionExitosa) {
          $resultado = array(
                          'resultado'=>'correcto',
                          'Mensajeadicional'=>'Factura Cancelada',
                          'info'=>$result
                          );
          //Guardar en archivo  y la ruta en la bd del acuse de la cancelacion
                $fileacuse='acuseCancelacion_'.$FacturasId.'_'.$datosfactura->uuid.'.xml';
                $ruta = $rutaf .$fileacuse;
                //file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
                file_put_contents($ruta,$result->CancelarCFDIResult->XMLAcuse);

                $datosdecancelacion = array(
                                            'Estado'=>0,
                                            'rutaAcuseCancelacion '=>$fileacuse,
                                            'fechacancelacion'=>$this->fechahoyc
                                          );
                $this->ModeloCatalogos->updateCatalogo('f_facturas',$datosdecancelacion,array('FacturasId'=>$FacturasId));
          
        }else{
          $resultado = array(
                          'resultado'=>'error',
                          'CodigoRespuesta'=>$result->CancelarCFDIResult->MensajeError,
                          'MensajeError'=>$result->CancelarCFDIResult->MensajeErrorDetallado,
                          'Mensajeadicional'=>'PORFAVOR CANCELAR MAS TARDE',
                          'info'=>$result
                          );
          file_put_contents($rutalog.'log_'.$this->fechahoyL.'_.txt', json_encode($result));
        }
        */
        /*
          CodigoRespuesta":"825" //El UUID aún no ha sido cancelado
          {"ObtenerAcuseCancelacionResult":{"CodigoConfirmacion":null,"CodigoRespuesta":"825","CreditosRestantes":0,"MensajeError":"El UUID aun no ha sido cancelado.","MensajeErrorDetallado":null,"OperacionExitosa":false,"PDFResultado":null,"Timbre":null,"XMLResultado":null}}


          CodigoRespuesta":"800" //Operación exitosa
          {"ObtenerAcuseCancelacionResult":{"CodigoConfirmacion":null,"CodigoRespuesta":"800","CreditosRestantes":0,"MensajeError":"","MensajeErrorDetallado":null,"OperacionExitosa":true,"PDFResultado":null,"Timbre":{"Estado":"Cancelado","FechaTimbrado":"2023-03-23T14:26:13","NumeroCertificadoSAT":"","SelloCFD":"mnWkSb9U8xhqvhB6cQOZr5hgJHRZOcZDoNk4vR7XHMJVZX78p\/jZLeYKH6I8WuD\/lrxEtl0dPLaaDzy6PcsFQibzA1mGLWCOi3Z4vBFvtmgcNGrZdWn5hZG\/EMUvE8m667HGaCZSRLxoIjT73ClvQJ640XG+awGTzd8xhcQvMfjNwxQMorA4yVFdivzbYSHChBDMsDmSnGZoCbCaL\/HsTJVM5vHW+sEV3qDZ1IHcp9TqKOU7+YFqxY\/Xrzu2mOHTGPXnEp2tT2a1oa3zf2xExngkVg72O+w\/E6jk7HuyE2iri3b15GwDiZGU\/tbYm\/kyf+wnR3tiw\/VVollajVnCtQ==","SelloSAT":"","UUID":"F615D95D-5F1D-490A-98AB-903397D60B4F"},"XMLResultado":"\r\n\r\n \r\n F615D95D-5F1D-490A-98AB-903397D60B4F<\/UUID>\r\n 201<\/EstatusUUID>\r\n <\/Folios>\r\n \r\n \r\n \r\n \r\n \r\n \r\n \r\n not(ancestor-or-self::*[local-name()='Signature'])<\/XPath>\r\n <\/Transform>\r\n <\/Transforms>\r\n \r\n jxzMoRslxiV3czAkeXpq5qwIC6xb9a4RYzpgPKBkXWJomCcng5lu8xoTUR\/y2RU9LYWmWZv0iKZTO9HPjTlZvA==<\/DigestValue>\r\n <\/Reference>\r\n <\/SignedInfo>\r\n LoEndrN4K7U4FmhFJS\/O\/DpNNDxJWEowmBlg2lrgkAZbwKqYnzgSdEJppkBm5av36q2F\/3c6H\/\/86j3XFy\/vjQ==<\/SignatureValue>\r\n \r\n 00001088888800000038<\/KeyName>\r\n \r\n \r\n qRWzHPVtRHYyDQTnnpPYtOBmb5Raaddb4XZH1DNlIhuhUrh7RKwfvcwh05wEu1lgUnej9BwsLd4u1SeYyawmaF2zIhzssP19yhOQUN9dRBeSg51m8XqCiodxoGilgNm7eDdZQ7j0ZYV0UOkMgGfomw\/3L0z\/O1gTbRDSg8gwM4BMSSu+iSMTcIKqn0oh4C+u77vKRui9NX6WZW2uRnmvEnDybxxtxTQVR3VlM4tdXeYTfkr5WUUrtaFyFW\/B7zs9Mb+FO5Cr\/TbWmu+xJP6LlN4UbZdPVe7+7rrv\/hwzeYe2LeFT0Jtmvuy1NfR\/uUZa35+UMou+RbsDOTm4vfJyuw==<\/Modulus>\r\n AQAB<\/Exponent>\r\n <\/RSAKeyValue>\r\n <\/KeyValue>\r\n <\/KeyInfo>\r\n <\/Signature>\r\n<\/Acuse>"}}

        */
          $MensajeError='';
          if($resultado->ObtenerAcuseCancelacionResult->CodigoRespuesta=='825'){
            $MensajeError=$resultado->ObtenerAcuseCancelacionResult->MensajeError;
          }else{

          }
        
        //echo '['.json_encode($resultado).']';
          return $MensajeError;
    }
    function estatuscancelacionCfdi_all(){
        $datos = $this->input->post('datos');
        //log_message('error', $datos);
        $DATA = json_decode($datos);
        $facturas=array();
        for ($i=0;$i<count($DATA);$i++) {
            $complemento=0;
            $uuid=$DATA[$i]->uuid;
            $resultado=$this->estatuscancelacionCfdi($uuid);

            $facturas[]=array(
                    'uuid'=>$uuid,
                    'resultado'=>$resultado
                );
        }
        echo json_encode($facturas);
    }
    function timbradodelcliente(){
      $params = $this->input->post();
      $fac_razonsocial = $params['fac_razonsocial'];
      $fac_rfc = $params['fac_rfc'];
      $fac_cp = $params['fac_cp'];
      $fac_regimenfiscal = $params['fac_regimenfiscal'];
      $fac_usocfdi = $params['fac_usocfdi'];
      $fac_folio = $params['fac_folio'];
      $fac_monto = $params['fac_monto'];
      $mes_actual=date('n');//Representación numérica de un mes, sin ceros iniciales

      $estado='0';
      $html='';
      $str_p1="SELECT * FROM `venta_erp` WHERE folio='$fac_folio' AND total='$fac_monto' ";
      $query_p1 = $this->db->query($str_p1);
      if($query_p1->num_rows()>0){
        $query_p1_r=$query_p1->row();
        $idventa=$query_p1_r->id;
        $reg=$query_p1_r->reg;
        $reg_m=date("n", strtotime($reg));
        if($reg_m==$mes_actual){
          if($idventa>0){
              $str_p2="SELECT * FROM venta_fk_factura as vf
                      INNER JOIN f_facturas as fac on fac.FacturasId=vf.idfactura
                      WHERE fac.Estado=1 AND vf.idventa='$idventa'";
              $query_p2 = $this->db->query($str_p2);
              if($query_p2->num_rows()==0){
                    $MetodoPago=$this->ModeloCatalogos->obtenerformapagoventa($idventa);
                    $conceptos_result=$this->ModeloCatalogos->getselectwheren('venta_erp_detalle',array('idventa'=>$idventa));
                //==================Generacion de factura================
                    $pais = 'MEXICO';
                    $TipoComprobante='I';
                    
                    $ultimoFolioval=$this->ModeloCatalogos->ultimoFolio() + 1;

                    //==========================================================

                    $data = array(
                  
                          "nombre"        => $fac_razonsocial,
                          "direccion"     => '', 
                          "cp"            => $fac_cp,
                          "rfc"           => str_replace(' ','',$fac_rfc),
                          "folio"         => $ultimoFolioval,
                          "PaisReceptor"  =>$pais,
                          "clienteId"       => $query_p1_r->id_razonsocial,
                          "serie" =>'H',
                          //"status"        => 1,
                          "TipoComprobante"=>$TipoComprobante,
                          "usuario_id"       => 0,
                          "creada_sesionId"=> 0,
                          "rutaXml"           => '',
                          "FormaPago"         => 'PUE',
                          "tarjeta"       => '',
                          "MetodoPago"        => $MetodoPago,//04 tarjeta
                          "ordenCompra"   => '',//no se ocupa
                          "moneda"        => 'MXN',
                          "observaciones" => '',
                          "numproveedor"  => '',
                          "numordencompra"=> '',
                          "Lote"          => '',//no se ocupa
                          "Paciente"      => '',//no se ocupa
                          "Caducidad"     => '',//no se ocupa
                          "uso_cfdi"      => $fac_usocfdi,
                          "subtotal"      => $query_p1_r->subtotal,
                          "iva"           => $query_p1_r->iva,
                          "total"         => $query_p1_r->total,
                          "honorario"     => 0,
                          "ivaretenido"   => 0,
                          "isr"           => 0,
                          "cincoalmillarval"  => 0,
                          "CondicionesDePago" => '',
                          "outsourcing"   => 0,
                          "facturaabierta"=>1,
                          "f_relacion"    => 0,
                          "f_r_tipo"      => 0,
                          "f_r_uuid"      => '',
                          "RegimenFiscalReceptor"=>$fac_regimenfiscal,
                          "relacion_venta_pro"=>0,
                          "faccliente"=>1
                      );
                    $FacturasId=$this->ModeloCatalogos->Insert('f_facturas',$data);
                    foreach ($conceptos_result->result() as $itemc) {
                          $conceptos_prot=$this->ModeloCatalogos->getselectwheren('productos',array('id'=>$itemc->idproducto));
                          $nombre='';
                          $Unidad ='H87';
                          $servicioId ='01010101';
                          $Descripcion ='No existe en el catalogo';
                          foreach ($conceptos_prot->result() as $itemp) {
                              $nombre=$itemp->nombre;
                              if($itemp->unidad_sat!=''){
                                $Unidad =$itemp->unidad_sat;
                              }
                              if($itemp->servicioId_sat!=''){
                                $servicioId =$itemp->servicioId_sat;
                                $Descripcion ='x';
                              }
                          }
                          //$cu=round($itemc->costo/1.16,2);
                          //========================================================================================================
                          //VERIFICAR ESTA INPLEMENTACION DEL IVA CON LOS ULTIMOS CAMBIOS EN EL CATALOGO DE PRODUCTOS
                          //VERIFICAR ESTA INPLEMENTACION DEL IVA CON LOS ULTIMOS CAMBIOS EN EL CATALOGO DE PRODUCTOS
                          //========================================================================================================
                          $ivap=round($itemc->precio_unitario*0.16,2);
                          if($itemc->incluye_iva==1){
                            $ivap=round($itemc->precio_unitario*0.16,2);  
                          }else{
                            $ivap=0;
                          }

                          $dataco['FacturasId']=$FacturasId;
                          $dataco['Cantidad'] =$itemc->cantidad;
                          $dataco['Unidad'] =$Unidad;
                          $dataco['servicioId'] =$servicioId;
                          $dataco['Descripcion'] =$Descripcion;
                          $dataco['Descripcion2'] =$nombre;
                          $dataco['Cu'] =$itemc->precio_unitario;
                          $dataco['descuento'] =$itemc->descuento;;
                          $dataco['Importe'] =($itemc->cantidad*$itemc->precio_unitario);
                          $dataco['iva'] =$ivap;
                          $this->ModeloCatalogos->Insert('f_facturas_servicios',$dataco);
                    }
                    $this->ModeloCatalogos->Insert('venta_fk_factura',array('idventa'=>$idventa,'idfactura'=>$FacturasId));
                //=======================================================
                    $respuesta=$this->emitirfacturas($FacturasId);
                    
                    if($respuesta['resultado']=='correcto'){

                      $res=$this->ModeloCatalogos->getselectwheren('f_facturas',array('FacturasId'=>$FacturasId));
                      $res=$res->row();
                      $estado='correcto';
                      $html='<div class="row">
                              <div class="col-md-4 ">
                                Factura generada con éxito <i class="fa-solid fa-check" style="font-size: 28px;color: #cbd244;"></i>
                              </div>
                              <div class="col-md-4">
                                
                              </div>
                              <div class="col-md-4">
                                <a href="'.base_url().'Folios/facturapdf/'.$FacturasId.'" class="btn-fac-ok pdf" target="_blank"></a>
                                <a href="'.base_url().$res->rutaXml.'" class="btn-fac-ok xml" download></a>
                                <a onclick="enviofactura('.$FacturasId.')" class="btn-fac-ok mail"></a>
                              </div>
                              
                            </div>';
                            $this->envio_fac($FacturasId);
                    }else{
                      $estado='error';
                      if($respuesta['info2']!=''){
                        $html=$respuesta['info2'];  
                      }else{
                        $html=$respuesta['MensajeError'];
                      }
                      $this->deleteregfac($FacturasId);
                      
                    }
              }else{
                $estado='error_02';
              }
          }
        }else{
          $estado='error_00';
        }
      }else{
        $estado='error_01';
      }


      $json_data =array('estado'=>$estado,'html'=>$html);
      echo json_encode($json_data);
    }
    function deleteregfac($FacturasId){
      $this->ModeloCatalogos->getdeletewheren('venta_fk_factura',array('idfactura'=>$FacturasId));
      $this->ModeloCatalogos->getdeletewheren('f_facturas_servicios',array('FacturasId'=>$FacturasId));
      $this->ModeloCatalogos->getdeletewheren('f_facturas',array('FacturasId'=>$FacturasId));
    }
    function envio_fac($idfac){

        $strqfac="SELECT * FROM f_facturas  WHERE FacturasId='$idfac'";
                $query_fac = $this->db->query($strqfac);
                $query_fac=$query_fac->row();

        $result_rf=$this->ModeloCatalogos->getselectwheren('f_regimenfiscal',array('clave'=>$query_fac->RegimenFiscalReceptor));
        $result_rf=$result_rf->row();

        
        $result_uc=$this->ModeloCatalogos->getselectwheren('f_uso_cfdi',array('uso_cfdi'=>$query_fac->uso_cfdi));
        $result_uc=$result_uc->row();

        //================================
            //cargamos la libreria email
            $this->load->library('email');

            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='mail.semit.mx'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'info@semit.mx';

            //Nuestra contraseña
            $config["smtp_pass"] = 'yUs&J=T4*V$J';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
            $asunto='Facturación WEB';
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('info@semit.mx',$asunto);
             
            //======================
            //$this->email->bcc('andres@mangoo.mx');
              $arraybbc[]='info@semit.mx';
              $arraybbc[]='gerardo@mangoo.com.mx';
              $arraybbc[]='ag-gerardo@hotmail.com';
              //$arraybbc[]='andres@mangoo.mx';
            $this->email->bcc($arraybbc);
            
            
            //Definimos el asunto del mensaje
            $this->email->subject($asunto);
            
            //$this->email->to($arraybbc);
        //==============================================
          $message  = '<div style="background: url('.base_url().'public/img/fondo.jpg) #eaeaea;width: 100%;height: 94%;min-height: 752px;padding-top: 25px;margin: 0px;">
                      <div style="width: 10%; height: 10px; float: left;">
                          
                      </div>
                      <div style="background: white; border-top: 3px solid #ff0101; padding: 25px 25px 0px 25px; width: 80%; max-width: 825px;margin-left: auto; margin-right: auto; float: left; -webkit-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);-moz-box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);box-shadow: 10px 10px 42px -6px rgba(0,0,0,0.75);">
                          <div style="width: 100%; float: left;">
                              <div style="width: 30%;float: left;">
                                  <img src="'.base_url().'public/img/SEMIT.jpg" style="max-height: 100px;">
                              </div>
                              <div style="width: 40%;float: left; min-height:10px; font-style: italic; font-size: 14px; color:#2e4064;">
                                  
                              </div>
                              <div style="width: 30%;float: left;">
                                  
                              </div>
                          </div>
                          <div style="width: 100%; float: left;">
                              <div style="width: 50%;float: left;height: 10px;"></div>
                              <div style="width: 50%;float: left; border-bottom: 3px solid #ff0101; font-size:17px"><b>Facturación </b></div>
                          </div>
                          <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                              <div style="width: 30%;float: left;">Fecha y hora de timbrado:</div>
                              <div style="width: 70%;float: left;">'.$query_fac->fechatimbre.'</div>
                          </div>
                          <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                              <div style="width: 30%;float: left;">Razon social:</div>
                              <div style="width: 70%;float: left;">'.$query_fac->Nombre.'</div>
                          </div>
                          <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                              <div style="width: 30%;float: left;">RFC:</div>
                              <div style="width: 70%;float: left;">'.$query_fac->Rfc.'</div>
                          </div>
                          <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                              <div style="width: 30%;float: left;">Régimen fiscal:</div>
                              <div style="width: 70%;float: left;">'.$query_fac->RegimenFiscalReceptor.' '.$result_rf->descripcion.'</div>
                          </div>
                          <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                              <div style="width: 30%;float: left;">Uso de CFDI:</div>
                              <div style="width: 70%;float: left;">'.$result_uc->uso_cfdi_text.'</div>
                          </div>
                          <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                              <div style="width: 30%;float: left;">Codigo Postal:</div>
                              <div style="width: 70%;float: left;">'.$query_fac->Cp.'</div>
                          </div>
                          <div style="width: 100%; float: left;font-size: 14px; color:#2e4064;">
                              <div style="width: 30%;float: left;">Folio:</div>
                              <div style="width: 70%;float: left;">'.$query_fac->serie.''.$query_fac->Folio.'</div>
                          </div>
                          
                          
                          <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px; text-align:justify">
                              
                          </div>
                          <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                              
                          </div>
                          <div style="width: 100%; float: left;font-size: 14px; margin-bottom: 10px;color:#2e4064; margin-bottom: 10px;">
                              <!--<img src="https://altaproductividadapr.com/public/img/cot_footer.jpg" width="100%">-->
                          </div>

                          
                      </div>
                  </div>';

          $this->email->message($message);
        //===============================================

                
        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
        
        //==================
    }


}