<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Losmasvendidos extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModelGeneral');
        $this->load->model('Login_model');
        if ($this->session->userdata('logeado')){
            $this->nombre=$this->session->userdata('nombre');
            $this->usuario=$this->session->userdata('usuario');
            $this->idcliente=$this->session->userdata('id');
        }else{
            $this->nombre='';
            $this->usuario='';
            $this->idcliente=0;
        }
    }

	public function index(){
        $resulcat=$this->ModelGeneral->get_select('categoria',array('activo'=>1));
        $resulp=$this->ModelGeneral->get_productos();
        $data['productos_get']=$resulp;
        $resulp1=$this->ModelGeneral->get_productosone();
        $data['productos1_get']=$resulp1;
        $data['categorias']=$resulcat;
        $data['idcliente']=$this->idcliente;
        $data['nombre']=$this->nombre;t;
        $data['categorianm']=0;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('masvendidos',$data);
        $this->load->view('templates/footer',$data);
        $this->load->view('iniciojs');
    }

}