<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facturacion extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModelGeneral');
        $this->load->model('Login_model');
        $this->load->model('ModeloCatalogos');
        
    }

	public function index(){
        $datos['fregimen'] = $this->ModeloCatalogos->getselectwheren('f_regimenfiscal',array('activo'=>1));
        $datos['fuso'] = $this->ModeloCatalogos->getselectwheren('f_uso_cfdi',array('activo'=>1));
        $this->load->view('facturacion',$datos);
	}
    function obtenerdatos(){
        $params = $this->input->post();
        $rfc_view = $params['rfc'];
        $datos_fi = $this->ModeloCatalogos->getselectwheren('cliente_fiscales',array('rfc'=>$rfc_view,'activo'=>1));
        $razon_social='';
        $rfc='';
        $cp='';
        $RegimenFiscalReceptor='';
        $uso_cfdi='';
        //correo
        //direccion
        foreach ($datos_fi->result() as $item) {
            $razon_social = $item->razon_social;
            $rfc = $item->rfc;
            $cp = $item->cp;
            $RegimenFiscalReceptor = $item->RegimenFiscalReceptor;
            $uso_cfdi = $item->uso_cfdi;
        }




        $json_data =array(
                            'numdatos'=>$datos_fi->num_rows(),
                            'razon_social'=>$razon_social,
                            'rfc'=>$rfc,
                            'cp'=>$cp,
                            'RegimenFiscalReceptor'=>$RegimenFiscalReceptor,
                            'uso_cfdi'=>$uso_cfdi
                        );

        echo json_encode($json_data);

    }


}