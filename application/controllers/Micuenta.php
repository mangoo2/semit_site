<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Micuenta extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModelGeneral');
        $this->load->model('Login_model');
        $this->load->model('ModelMicuenta');
        if ($this->session->userdata('logeado')){
            $this->nombre=$this->session->userdata('nombre');
            $this->usuario=$this->session->userdata('usuario');
            $this->idcliente=$this->session->userdata('id');
        }else{
            $this->nombre='';
            $this->usuario='';
            $this->idcliente=0;
            redirect('Inicio');
        }
    }

	public function index(){
        $resul=$this->ModelGeneral->get_select('bannerssliders_pestana',array('file!='=>'','activo'=>1));
        $resulcat=$this->ModelGeneral->get_select('categoria',array('activo'=>1));
        $data['pestana']=$resul;
        $resulp=$this->ModelGeneral->get_productos();
        $data['productos_get']=$resulp;
        $resulp1=$this->ModelGeneral->get_productosone();
        $data['productos1_get']=$resulp1;
        $data['nombre']=$this->nombre;
        $data['categorias']=$resulcat;
        $data['categorianm']=0;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('micuenta',$data);
        $this->load->view('templates/footer',$data);
        $this->load->view('iniciojs');
    }

    public function misfavoritos(){
        $resul=$this->ModelGeneral->get_select('bannerssliders_pestana',array('file!='=>'','activo'=>1));
        $resulcat=$this->ModelGeneral->get_select('categoria',array('activo'=>1));
        $data['pestana']=$resul;
        $resulp=$this->ModelGeneral->get_productos();
        $data['productos_get']=$resulp;
        $resulp1=$this->ModelGeneral->get_productosone();
        $data['productos1_get']=$resulp1;
        $data['nombre']=$this->nombre;
        $data['categorias']=$resulcat;
        $data['categorianm']=0;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('micuenta/favoritos',$data);
        $this->load->view('templates/footer',$data);
        $this->load->view('micuenta/favoritosjs');
    }

    function get_favoritos()
    {
        $result=$this->ModelMicuenta->get_productos_favoritos($this->idcliente);
        $html='<table class="table">
              <tbody>';
        foreach ($result as $p){
            $img_file='';
            if($p->file!=''){
                $img_file='<img style="width: 55px;" src="https://adminsys.semit.mx/uploads/productos/'.$p->file.'">'; 
            }else{
                $img_file='<img style="width: 55px;" src="https://adminsys.semit.mx/public/img/favICONSEMIT.png">'; 
            }
            
            $resulpro=$this->ModelGeneral->get_select('carrito_temporal',array('idproducto'=>$p->idproducto,'idcliente'=>$this->idcliente));
            $num_cl='';
            foreach ($resulpro as $pr){
                $num_cl='fill="#009bdb"';
            }

            $html.='<tr class="tr_pro_'.$p->idproducto.'"><td>'.$img_file.'</td><td style="font-size: 18px;">'.$p->nombre.'</td><td><div style="display: flex; align-items: center;">
                <a style="cursor:pointer; width: 62px; height: 57px;" href="'.base_url().'Productos/descripcion/'.$p->idproducto.'"><span class="cora_'.$p->idproducto.'"><svg fill="red" width="45" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-380 -305)"><path d="M402.493 309.685C392.661 309.685 384.691 317.656 384.691 327.488 384.691 337.32 392.661 345.29 402.493 345.29 412.326 345.29 420.296 337.32 420.296 327.488 420.301 317.66 412.337 309.69 402.509 309.685 402.504 309.685 402.499 309.685 402.493 309.685ZM402.493 336.406C402.493 336.406 393.836 329.786 393.836 324.439 393.836 320.873 399.285 317.054 402.493 323.42 405.702 317.054 411.151 320.873 411.151 324.439 411.151 329.786 402.493 336.406 402.493 336.406Z"></path></g></svg></span></a>

                <a style="cursor:pointer; width: 62px; height: 57px;" onclick="add_carrito('.$p->idproducto.')"><span class="car_'.$p->idproducto.'"><svg width="46" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-425 -305)"><path '.$num_cl.' d="M38.4375 25.7395 38.4375 9.84375 8.4375 9.84375 8.4375 7.92188C8.43516 5.87766 6.77859 4.22108 4.73438 4.21875L4.6875 4.21875C4.42861 4.21875 4.21875 4.42861 4.21875 4.6875 4.21875 4.94639 4.42861 5.15625 4.6875 5.15625L4.73438 5.15625C6.26114 5.1578 7.49845 6.39511 7.5 7.92188L7.5 31.4062C7.50216 33.1415 8.85502 34.575 10.5872 34.6777 9.15525 35.5615 8.71097 37.4388 9.5948 38.8708 10.4787 40.3027 12.356 40.747 13.7879 39.8632 15.2198 38.9793 15.6641 37.102 14.7803 35.67 14.5345 35.2719 14.2004 34.9357 13.8038 34.6875L31.1944 34.6875C29.7682 35.5807 29.3362 37.4608 30.2293 38.887 31.1224 40.3131 33.0026 40.7452 34.4287 39.852 35.8549 38.9589 36.287 37.0787 35.3939 35.6526 35.1493 35.262 34.8193 34.9321 34.4287 34.6875L37.9688 34.6875C38.2276 34.6875 38.4375 34.4776 38.4375 34.2188 38.4375 33.9599 38.2276 33.75 37.9688 33.75L10.7812 33.75C9.48745 33.7485 8.43905 32.7 8.4375 31.4062L8.4375 28.552ZM32.8125 39.375C31.6475 39.375 30.7031 38.4306 30.7031 37.2656 30.7031 36.1006 31.6475 35.1562 32.8125 35.1562 33.9775 35.1562 34.9219 36.1006 34.9219 37.2656 34.9206 38.43 33.9769 39.3737 32.8125 39.375ZM12.1875 39.375C11.0225 39.375 10.0781 38.4306 10.0781 37.2656 10.0781 36.1006 11.0225 35.1562 12.1875 35.1562 13.3525 35.1562 14.2969 36.1006 14.2969 37.2656 14.2956 38.43 13.3519 39.3737 12.1875 39.375ZM8.4375 16.4062 15 16.4062 15 21.0938 8.4375 21.0938ZM30 21.0938 23.4375 21.0938 23.4375 16.4062 30 16.4062ZM30.9375 16.4062 37.5 16.4062 37.5 21.0938 30.9375 21.0938ZM23.4375 22.0312 30 22.0312 30 25.5886 23.4375 26.2031ZM22.5 26.2917 15.9375 26.9062 15.9375 22.0312 22.5 22.0312ZM23.4375 15.4688 23.4375 10.7812 30 10.7812 30 15.4688ZM22.5 15.4688 15.9375 15.4688 15.9375 10.7812 22.5 10.7812ZM22.5 16.4062 22.5 21.0938 15.9375 21.0938 15.9375 16.4062ZM8.4375 22.0312 15 22.0312 15 26.9948 8.4375 27.6094ZM30.9375 25.5 30.9375 22.0312 37.5 22.0312 37.5 24.8855ZM37.5 15.4688 30.9375 15.4688 30.9375 10.7812 37.5 10.7812ZM15 10.7812 15 15.4688 8.4375 15.4688 8.4375 10.7812Z" transform="matrix(1.02222 0 0 1 425 305)"></path></g></svg></span></a>

                <a style="cursor:pointer" onclick="eliminar_registro('.$p->idproducto.')"><i class="fa fa-trash" style="font-size: 33px;"></i></a>
            </div>
            </td></tr>';
        }
        $html.='</tbody>
            </table>';
        echo $html;
    }

    function delete_producto()
    {   
        $idpro=$this->input->post('id');
        $this->ModelGeneral->delete_registro(array('idproducto'=>$idpro,'idcliente'=>$this->idcliente),'productos_favoritos');
    }

    public function mispedidos(){
        //var_dump($this->idcliente);die;
        $resul=$this->ModelGeneral->get_select('bannerssliders_pestana',array('file!='=>'','activo'=>1));
        $resulcat=$this->ModelGeneral->get_select('categoria',array('activo'=>1));
        $data['pestana']=$resul;
        $resulp=$this->ModelGeneral->get_productos();
        $data['productos_get']=$resulp;
        $resulp1=$this->ModelGeneral->get_productosone();
        $data['productos1_get']=$resulp1;
        $data['nombre']=$this->nombre;
        $data['categorias']=$resulcat;
        $data['categorianm']=0;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('micuenta/mispedidos',$data);
        $this->load->view('templates/footer',$data);
        $this->load->view('micuenta/mispedidosjs');
    }

    function get_pedidos()
    {
        $result=$this->ModelMicuenta->get_productos_pedidos($this->idcliente);
        $html='';
        foreach ($result as $p){
            
            $img_file='';
            if($p->file!=''){
                $img_file='<img style="width: 90px;" src="https://adminsys.semit.mx/uploads/productos/'.$p->file.'">'; 
            }else{
                $img_file='<img style="width: 90px;" src="https://adminsys.semit.mx/public/img/favICONSEMIT.png">'; 
            }
            
            $resulpro=$this->ModelGeneral->get_select('carrito_temporal',array('idproducto'=>$p->id,'idcliente'=>$this->idcliente));
            $num_cl='';
            foreach ($resulpro as $pr){
                $num_cl='fill="#009bdb"';
            }

            $resulprof=$this->ModelGeneral->get_select('productos_favoritos',array('idproducto'=>$p->id,'idcliente'=>$this->idcliente));
            $num_clf='';
            foreach ($resulprof as $pr){
                $num_clf='fill="red"';
            }

            $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            $fechatxt=date('d',strtotime($p->reg))." de ".$meses[date('n',strtotime($p->reg))-1]. " del ".date('Y',strtotime($p->reg));
//Salida: Miercoles 05 de Septiembre del 2016
            //var_dump($fechatxt);die;

            $html.='<div class="row">
            <div class="col-lg-9">
              <div class="row">
                <div class="col-lg-2">
                  <div class="row">
                    <div class="col-lg-12">
                      '.$img_file.'
                    </div>
                  </div>    
                  <div class="row">
                    <div class="col-lg-12">
                      <a style="cursor:pointer; width: 62px; height: 57px;" onclick="add_favoritos('.$p->id.')"><span class="cora_'.$p->id.'"><svg '.$num_clf.' width="45" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-380 -305)"><path d="M402.493 309.685C392.661 309.685 384.691 317.656 384.691 327.488 384.691 337.32 392.661 345.29 402.493 345.29 412.326 345.29 420.296 337.32 420.296 327.488 420.301 317.66 412.337 309.69 402.509 309.685 402.504 309.685 402.499 309.685 402.493 309.685ZM402.493 336.406C402.493 336.406 393.836 329.786 393.836 324.439 393.836 320.873 399.285 317.054 402.493 323.42 405.702 317.054 411.151 320.873 411.151 324.439 411.151 329.786 402.493 336.406 402.493 336.406Z" /></g></svg></span></a>

                     <a style="cursor:pointer; width: 62px; height: 57px;" onclick="add_carrito('.$p->id.')"><span class="car_'.$p->id.'"><svg width="46" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-425 -305)"><path '.$num_cl.' d="M38.4375 25.7395 38.4375 9.84375 8.4375 9.84375 8.4375 7.92188C8.43516 5.87766 6.77859 4.22108 4.73438 4.21875L4.6875 4.21875C4.42861 4.21875 4.21875 4.42861 4.21875 4.6875 4.21875 4.94639 4.42861 5.15625 4.6875 5.15625L4.73438 5.15625C6.26114 5.1578 7.49845 6.39511 7.5 7.92188L7.5 31.4062C7.50216 33.1415 8.85502 34.575 10.5872 34.6777 9.15525 35.5615 8.71097 37.4388 9.5948 38.8708 10.4787 40.3027 12.356 40.747 13.7879 39.8632 15.2198 38.9793 15.6641 37.102 14.7803 35.67 14.5345 35.2719 14.2004 34.9357 13.8038 34.6875L31.1944 34.6875C29.7682 35.5807 29.3362 37.4608 30.2293 38.887 31.1224 40.3131 33.0026 40.7452 34.4287 39.852 35.8549 38.9589 36.287 37.0787 35.3939 35.6526 35.1493 35.262 34.8193 34.9321 34.4287 34.6875L37.9688 34.6875C38.2276 34.6875 38.4375 34.4776 38.4375 34.2188 38.4375 33.9599 38.2276 33.75 37.9688 33.75L10.7812 33.75C9.48745 33.7485 8.43905 32.7 8.4375 31.4062L8.4375 28.552ZM32.8125 39.375C31.6475 39.375 30.7031 38.4306 30.7031 37.2656 30.7031 36.1006 31.6475 35.1562 32.8125 35.1562 33.9775 35.1562 34.9219 36.1006 34.9219 37.2656 34.9206 38.43 33.9769 39.3737 32.8125 39.375ZM12.1875 39.375C11.0225 39.375 10.0781 38.4306 10.0781 37.2656 10.0781 36.1006 11.0225 35.1562 12.1875 35.1562 13.3525 35.1562 14.2969 36.1006 14.2969 37.2656 14.2956 38.43 13.3519 39.3737 12.1875 39.375ZM8.4375 16.4062 15 16.4062 15 21.0938 8.4375 21.0938ZM30 21.0938 23.4375 21.0938 23.4375 16.4062 30 16.4062ZM30.9375 16.4062 37.5 16.4062 37.5 21.0938 30.9375 21.0938ZM23.4375 22.0312 30 22.0312 30 25.5886 23.4375 26.2031ZM22.5 26.2917 15.9375 26.9062 15.9375 22.0312 22.5 22.0312ZM23.4375 15.4688 23.4375 10.7812 30 10.7812 30 15.4688ZM22.5 15.4688 15.9375 15.4688 15.9375 10.7812 22.5 10.7812ZM22.5 16.4062 22.5 21.0938 15.9375 21.0938 15.9375 16.4062ZM8.4375 22.0312 15 22.0312 15 26.9948 8.4375 27.6094ZM30.9375 25.5 30.9375 22.0312 37.5 22.0312 37.5 24.8855ZM37.5 15.4688 30.9375 15.4688 30.9375 10.7812 37.5 10.7812ZM15 10.7812 15 15.4688 8.4375 15.4688 8.4375 10.7812Z" transform="matrix(1.02222 0 0 1 425 305)" /></g></svg></span></a></div></div>
                </div>
                <div class="col-lg-10">
                  <b style="color:black; font-size: 20px;">'.$p->producto.'</b>
                  <br>
                  <b style="color:#152342">Pedido número:</b><b style="color:#93ba1f;font-size: 15px;">'.$p->idtransacción.'</b>
                </div>
              </div>  
              <div class="row">
                <div class="col-lg-12">
                  <b style="color:#152342">Pedido realizado el:</b> <b style="color:#93ba1f;font-size: 15px;">'.$fechatxt.'</b> <b style="color:#152342;font-size: 15px;">Total:</b> <b style="color:#93ba1f;font-size: 15px;">$'.number_format($p->costo,2,'.',',').'</b> <b style="color:#152342">Enviado a:</b> <b style="color:#93ba1f;font-size: 15px;">'.$p->nombre.'</b>
                </div>
              </div>
            </div>
            <div class="col-lg-3">
              <a class="btn_blue">Realizar seguimiento paquete</a>
              <a class="btn_blue" href="'.base_url().'Micuenta/facturacion/'.$p->idventa.'">Facturar producto</a>
              <a class="btn_blue" onclick="modal_escrbir_resena('.$p->idventa.')">Escribir una reseña</a>
              <a class="btn_blue">Volver a comprar</a>
            </div>  
          </div>   
          <hr class="hr_t">';
        }
        echo $html;
    }

    function facturacion($id=0)
    {
        //var_dump($this->idcliente);die;
        $resul=$this->ModelGeneral->get_select('bannerssliders_pestana',array('file!='=>'','activo'=>1));
        $resulcat=$this->ModelGeneral->get_select('categoria',array('activo'=>1));
        $data['pestana']=$resul;
        $resulp=$this->ModelGeneral->get_productos();
        $data['productos_get']=$resulp;
        $resulp1=$this->ModelGeneral->get_productosone();
        $data['productos1_get']=$resulp1;
        $data['nombre']=$this->nombre;
        $data['categorias']=$resulcat;
        $data['categorianm']=0;
        $data['idventa']=$id;
        $data['get_uso_cfdi']=$this->ModelGeneral->get_select('f_uso_cfdi',array('activo'=>1));
        
        $data['rfc'] = '';
        $data['cp']='';
        $data['correo'] = '';
        $data['razon_social'] = '';
        $data['RegimenFiscalReceptor'] = '';
        $data['uso_cfdi'] = '';
        $result=$this->ModelGeneral->getselectwhere('cliente_fiscales','idcliente',$this->idcliente);

        foreach ($result as $x) {
            $data['rfc'] = $x->rfc;
            $data['cp']= $x->cp;
            $data['correo'] = $x->correo;
            $data['razon_social'] = $x->razon_social;
            $data['RegimenFiscalReceptor'] = $x->RegimenFiscalReceptor;
            $data['uso_cfdi'] = $x->uso_cfdi;
        }
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('micuenta/facturacion',$data);
        $this->load->view('templates/footer',$data);
        $this->load->view('micuenta/facturacionjs');
    }

    function get_facturacion()
    {   
        $idventa=$this->input->post('idventa'); 
        $result=$this->ModelMicuenta->get_productos_venta($idventa,$this->idcliente);
        $html='';
        $factura=0;
        foreach ($result as $p){
            
            $img_file='';
            if($p->file!=''){
                $img_file='<img style="width: 90px;" src="https://adminsys.semit.mx/uploads/productos/'.$p->file.'">'; 
            }else{
                $img_file='<img style="width: 90px;" src="https://adminsys.semit.mx/public/img/favICONSEMIT.png">'; 
            }
            
            $resulpro=$this->ModelGeneral->get_select('carrito_temporal',array('idproducto'=>$p->id,'idcliente'=>$this->idcliente));
            $num_cl='';
            foreach ($resulpro as $pr){
                $num_cl='fill="#009bdb"';
            }

            $resulprof=$this->ModelGeneral->get_select('productos_favoritos',array('idproducto'=>$p->id,'idcliente'=>$this->idcliente));
            $num_clf='';
            foreach ($resulprof as $pr){
                $num_clf='fill="red"';
            }

            $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            $fechatxt=date('d',strtotime($p->reg))." de ".$meses[date('n',strtotime($p->reg))-1]. " del ".date('Y',strtotime($p->reg));
//Salida: Miercoles 05 de Septiembre del 2016
            //var_dump($fechatxt);die;
            
            $html.='<div class="row">
        <div class="col-lg-9">
          <div class="row">
            <div class="col-lg-2">
              <div class="row">
                <div class="col-lg-12">
                  '.$img_file.'
                </div>
              </div>    
            </div>  
            <div class="col-lg-12">
              <b style="color:black; font-size: 20px;">'.$p->producto.'</b>
            </div>
          </div>  
          <div class="row">
            <div class="col-lg-12">
              <b style="color:#152342">Pedido realizado el:</b> <b style="color:#93ba1f;font-size: 15px;">'.$fechatxt.'</b> <b style="color:#152342;font-size: 15px;">Total:</b> <b style="color:#93ba1f;font-size: 15px;">$'.number_format($p->costo,2,'.',',').'</b> <b style="color:#152342">Enviado a:</b> <b style="color:#93ba1f;font-size: 15px;">'.$p->nombre.'</b>
            </div>
          </div>
        </div>
        <div class="col-lg-3"><br>';
            if($p->FacturasId>0){
                $html.='<a class="btn_blue" >Facturada</a>';
                $html.=' <a href="'.base_url().'Folios/facturapdf/'.$p->FacturasId.'" target="_blank"><img style="width: 45px;" src="'.base_url().'public/img/16.png"></a>';
                $html.=' <a href="'.base_url().$p->rutaXml.'" download><img style="width: 45px;" src="'.base_url().'public/img/17.png"></a>';
            }else{
                $html.='<a class="btn_green" onclick="modal_factura('.$p->idventad.')">Facturar</a>';
            }
            
        $html.='</div>  
      </div>   
      <hr class="hr_t"> ';
        }
        echo $html;
    }
    
    public function add_facturacion(){
        $datos = $this->input->post();
        $idcliente = $this->idcliente;
        $result=$this->ModelGeneral->getselectwhere('clientes','id',$idcliente);
        $codigo=0;
        foreach ($result as $x) {
            $codigo=$x->codigo;
        }

        $id=$this->ModelGeneral->add_record('clientes',$datos);  
        $this->enviar($id);
        echo $id;
    }

    public function add_factura_cliente()
    {   
        $data=$this->input->post();
        $result=$this->ModelGeneral->getselectwhere('cliente_fiscales','idcliente',$this->idcliente);
        $id=0;
        foreach ($result as $x) {
            $id=$x->id;
        }
        if($id!=0){
            $this->ModelGeneral->edit_record('id',$id,$data,'cliente_fiscales');
        }else{
            $data['idcliente']=$this->idcliente;
            $id=$this->ModelGeneral->add_record('cliente_fiscales',$data);  
        }
        echo $id;
        
    }

    function add_resena()
    {   
        $data=$this->input->post();
        $data['idcliente']=$this->idcliente;
        $id=$this->ModelGeneral->add_record('resena_producto',$data);  
        echo $id;
        
    }

}