<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pago extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModelGeneral');
        $this->load->model('Login_model');
        $this->load->model('ModelPago');
        //$this->load->library('openpay');
        if ($this->session->userdata('logeado')){
            $this->nombre=$this->session->userdata('nombre');
            $this->usuario=$this->session->userdata('usuario');
            $this->idcliente=$this->session->userdata('id');
            //$this->openpay = Openpay::getInstance('mzuolrwtlcn4ttmv4jvz', 'sk_552c80a7da564eee8200ffd8214b0c70', 'MX');
            $this->client_id_paypay='AW9RhmXeNrX2903kBNCmfhKIR0WDV-tPuLPbLZjMTKqmDYD_5H6JiPr7daoXPOu3iTcUo61zyHJjC3vK';
            $this->clavesecreta_paypay='EMO6u8Behtkjd3l2gOqRiw0tcK1gDlTw1a3_99LFQzc1EuJDHKHBagJm9dX_-JLaUVc2H1w_Sr-WFwhI';
        }else{
            $this->nombre='';
            $this->usuario='';
            $this->idcliente=0;
        }
    }

	public function index(){
        $data['client_id_paypay']=$this->client_id_paypay;
        $resul=$this->ModelGeneral->get_select('bannerssliders_pestana',array('file!='=>'','activo'=>1));
        $resulcat=$this->ModelGeneral->get_select('categoria',array('activo'=>1));
        $data['pestana']=$resul;
        $resulp=$this->ModelGeneral->get_productos();
        $data['productos_get']=$resulp;
        $resulp1=$this->ModelGeneral->get_productosone();
        $data['productos1_get']=$resulp1;
        $data['nombre']=$this->nombre;
        $data['categorias']=$resulcat;
        $data['categorianm']=0;

        $resulp=$this->ModelPago->get_productos_total($this->idcliente); 
        $total=0;
        foreach ($resulp as $x){
            $total=$x->total;
        }
        

        $resultcli=$this->ModelGeneral->get_select('clientes',array('id'=>$this->idcliente));
        foreach ($resultcli as $x){
            $data['cliente']=$x->nombre;
            $data['direccion_predeterminada']=$x->direccion_predeterminada;
            $data['calle']=$x->calle;
            $data['cp']=$x->cp;
            $data['colonia']=$x->colonia;
            $data['telefono']=$x->telefono;
            $data['instrucciones']=$x->instrucciones;
            $data['pais']=$x->pais;
        }
        $data['total_productos']=$total;
        ////
        $resulpago=$this->ModelPago->get_detallespago($this->idcliente);


        $data['estatus']='';
        $data['idpago']=0;
        $data['pasos']=0;
        foreach ($resulpago as $x){
            $data['idpago']=$x->id;
            $data['cliente']=$x->cliente;
            $data['calle']=$x->calle;
            $data['cp']=$x->cp;
            $data['colonia']=$x->colonia;
            $data['telefono']=$x->telefono;
            $data['instrucciones']=$x->instrucciones;
            $data['estatus']=$x->estatus;
            $data['pais']=$x->pais;
            $data['pasos']=$x->pasos;
        }

		$this->load->view('templates/header',$data);
        $this->load->view('templates/navbar',$data);
        $this->load->view('pago',$data);
        $this->load->view('templates/footer',$data);
        $this->load->view('pagojs');
	}
   


    public function getDatosCP(){
        $cp =$this->input->post('cp');
        $col =$this->input->post('col');
        $results=$this->ModelPago->getDatosCPEstado_gruop($cp,$col);
        echo json_encode($results);
    }

    public function getDatosCP_colonia(){
        $cp =$this->input->post('cp');
        $col =$this->input->post('col');
        $results=$this->ModelPago->getDatosCPEstado($cp,$col);
        echo json_encode($results);
    }

    public function getDatosCPSelect(){
        $cp =$this->input->get('cp');
        $col =$this->input->get('col');
        $results=$this->ModelPago->getDatosCPEstado($cp,$col);
        echo json_encode($results);
    }
    
    public function add_pago_detalles(){
        $datosc = $this->input->post();
        $datosp = $this->input->post();
        $idpago = $datosp['id']; 
        unset($datosc['id']);
        $datosc['nombre']=$datosc['cliente'];
        $datosp['idcliente']=$this->idcliente;
        unset($datosc['cliente']);
        if(isset($datosc['direccion_predeterminada'])){
            $datosc['direccion_predeterminada']=1;
        }else{
            $datosc['direccion_predeterminada']=0;
        }
        $this->ModelGeneral->edit_record('id',$this->idcliente,$datosc,'clientes');
        unset($datosp['direccion_predeterminada']);

        if($idpago==0){
            $datosp['pasos']=1;
            $idpago=$this->ModelGeneral->add_record('pago_productos',$datosp);
        }else{
            $this->ModelGeneral->edit_record('id',$idpago,$datosp,'pago_productos');  
        }
        echo $idpago;
    }
    function paypalorder(){
        $clientId = $this->client_id_paypay;
        $clientSecret = $this->clavesecreta_paypay;
        
        $apiUrl = 'https://api.sandbox.paypal.com/v1/oauth2/token';
        $paymentUrl = 'https://api.sandbox.paypal.com/v2/checkout/orders';

        // Paso 1: Obtener un token de acceso de PayPal
        $auth = base64_encode($clientId . ':' . $clientSecret);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . $auth,
            'Accept: application/json',
            'Content-Type: application/x-www-form-urlencoded'
        ));

        $response = curl_exec($ch);
        //log_message('error',$response);
        $tokenData = json_decode($response, true);
        $totalcarrito=$_SESSION['totalcarrito'];
        if (isset($tokenData['access_token'])) {
            // Paso 2: Crear una orden de pago
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $paymentUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
                'intent' => 'CAPTURE',
                'purchase_units' => [
                    [
                        
                        'amount' => [
                            'currency_code' => 'MXN',
                            'value' => "$totalcarrito" // Cambia el valor al monto que desees cobrar
                        ]
                    ]
                ]
            ]));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer ' . $tokenData['access_token'],
                'Accept: application/json',
                'Content-Type: application/json'
            ));

            $response = curl_exec($ch);
            //$orderData = json_decode($response, true);
            //echo $orderData;
            //echo json_decode($response, true);
            /*
            log_message('error',$response);
            if (isset($orderData['id'])) {
                // Redirige al usuario a la página de aprobación de PayPal
                header('Location: ' . $orderData['links'][1]['href']);
                log_message('error','links: ' . $orderData['links'][1]['href']);
            } else {
                echo 'Error al crear la orden de pago: ' . print_r($orderData, true);
                log_message('error','Error al crear la orden de pago: ' . print_r($orderData, true));
            }
            */
            echo $response;
            
        } else {
            echo 'Error al obtener el token de acceso: ' . print_r($tokenData, true);
            log_message('error','Error al obtener el token de acceso: ' . print_r($tokenData, true));
        }
    }
    function capture_paypal_order(){

    }
    function paytoken(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => 'https://api-m.sandbox.paypal.com/v1/oauth2/token',
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_SSL_VERIFYHOST=> false,

          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => 'grant_type=client_credentials&ignoreCache=true&return_authn_schemes=true&return_client_metadata=true&return_unconsented_scopes=true',
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization: Basic QVc5UmhtWGVOclgyOTAza0JOQ21maEtJUjBXRFYtdFB1TFBiTFpqTVRLcW1EWURfNUg2SmlQcjdkYW9YUE91M2lUY1VvNjF6eUhKakMzdks6RU1PNnU4QmVodGtqZDNsMmdPcVJpdzB0Y0sxZ0RsVHcxYTNfOTlMRlF6YzFFdUpESEtIQmFnSm05ZFhfLUpMYVVWYzJIMXdfU3ItV0Z3aEk='
          ),
        ));

        $response = curl_exec($curl);
        if(curl_errno($curl)){
                  echo curl_error($curl);
                }else{
                  $decoded = json_decode($response,true);
                  //echo $decoded;
                }
        curl_close($curl);
        echo $response;
        log_message('error',$response);
    }
    function guardarcompra(){
        $params = $this->input->post();
        $idventa=$this->ModelGeneral->add_record('venta',array('idcliente'=>$this->idcliente,'total'=>$_SESSION['totalcarrito'],'idtransacción'=>$params['id'],'detalles'=>json_encode($params)));

        $resulcat=$this->ModelGeneral->get_productos_temporal($this->idcliente);

        foreach ($resulcat as $item) {
            $this->ModelGeneral->add_record('venta_detalles',array('idventa'=>$idventa,'cantidad'=>$item->cantidad,'producto'=>$item->idproducto,'costo'=>$item->precio_con_iva));
        }
        $this->ModelGeneral->delete_registro(array('idcliente'=>$this->idcliente),'carrito_temporal');
    }


    function enviar_pago(){
        $id=$this->input->post('id');
        //================================
            //cargamos la libreria email
            $this->load->library('email');
            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='semit.mx'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'hola@semit.mx';

            //Nuestra contraseña
            $config["smtp_pass"] = '2(dLH]oiolWz';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('hola@semit.mx','Semit site');
            /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
            */
        //======================
        $result=$this->ModelPago->get_productos_venta($id);
        $trans='';
        $nombre='';
        $total='';
        $calle='';
        $colonia='';
        $fechatxt='';
        $correo='';
        foreach ($result as $x){
            $trans=$x->idtransacción;
            $nombre=$x->nombre;
            $total=$x->total;
            $calle=$x->calle;
            $colonia=$x->colonia;
            $fecha=$x->reg;
            $correo=$x->correo;
            $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
            $fechatxt=date('d',strtotime($x->reg))." de ".$meses[date('n',strtotime($x->reg))-1]. " del ".date('Y',strtotime($x->reg));
        }

        $this->email->to($correo,$nombre);
        //$this->email->to('andres@mangoo.mx',$paciente);
        $asunto='Confirmación de compra';
        //Definimos el asunto del mensaje
        $this->email->subject($asunto);        
        //Definimos el mensaje a enviar
        //$this->email->message($body)
    $message="<div style='background: url(http://estudiosunne.sicoi.net/public/img/anahuac-fondo-footer.jpg);width: 97%;height: 94%;padding: 25px;margin: 0px; font-family: Galano, Helvetica, Arial, sans-serif !important;'>
    <img class='img_logo1' style=' alt='Porto' width='135' height='55' data-sticky-width='82' data-sticky-height='40' data-sticky-top='25' src='http://pagedemo.semit.mx/public/img/SEMIT.jpg'>
    <br>
    <div style='text-align-last: end; width: 69%;'>
        <b style='width:17px;'>Confirmación de pedido<br>
            Pedido:".$trans."
        </b> 
    </div>
    <br><br>
    <b style='width:25px; color: #009bdb;'>Hola ".$nombre."</b>
    <br>
    <b>Gracias por tu pedido</b>
    <br>
    <b>Los detalles de tu pedido se indica a continuación</b>
    <br></br>
    <table style='width:70%; background: #eaeaea;border-top: 3px solid #009bdb;'>
        <thead>
            <tr>
                <th>
                   Tu fecha de solicitud es: <br><b>".$fechatxt."</b>
                   <br><br> 
                   <a style='background:#009bdb;color:white;padding: 2px;border-radius: 8px;text-decoration: wavy; font-size: 18px;' href='http://pagedemo.semit.mx/Micuenta/mispedidos'>&nbsp;&nbsp;&nbsp;Detalles del pedido&nbsp;&nbsp;&nbsp;</a>
                </th>
                <th>
                    Tu pedido será  enviado a:<br>".$nombre."<br>".$calle." ".$colonia."
                    <br><br>
                    Total del pedido (impuestos aplicables incluídos):<br> $".$total."
                </th>
            </tr>
        </thead>
    </table>
</div>";

        $this->email->message($message);

        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }

    }

    function correo()
    {
        $this->load->view('micuenta/correo');
    }

}