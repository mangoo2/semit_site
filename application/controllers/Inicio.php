<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModelGeneral');
        $this->load->model('Login_model');
        if ($this->session->userdata('logeado')){
            $this->nombre=$this->session->userdata('nombre');
            $this->usuario=$this->session->userdata('usuario');
            $this->idcliente=$this->session->userdata('id');
        }else{
            $this->nombre='';
            $this->usuario='';
            $this->idcliente=0;
        }
    }

	public function index(){
        $resul=$this->ModelGeneral->get_select('bannerssliders_pestana',array('file!='=>'','activo'=>1));
        $resulcat=$this->ModelGeneral->get_select('categoria',array('activo'=>1));
        $data['pestana']=$resul;
        $resulp=$this->ModelGeneral->get_productos();
        $data['productos_get']=$resulp;
        $resulp1=$this->ModelGeneral->get_productosone();
        $data['productos1_get']=$resulp1;
        $data['nombre']=$this->nombre;
        $data['categorias']=$resulcat;
        $data['categorianm']=0;
        $data['idcliente']=$this->idcliente;
		$this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('inicio',$data);
        $this->load->view('templates/footer',$data);
        $this->load->view('iniciojs');
	}

    public function categoria($idcat){
        $resul=$this->ModelGeneral->get_select('bannerssliders_pestana',array('file!='=>'','activo'=>1));
        $resulcat=$this->ModelGeneral->get_select('categoria',array('activo'=>1));
        $data['pestana']=$resul;
        $resulp=$this->ModelGeneral->get_productos();
        $data['productos_get']=$resulp;
        $resulp1=$this->ModelGeneral->get_productosone();
        $data['productos1_get']=$resulp1;
        $data['nombre']=$this->nombre;
        $data['categorias']=$resulcat;
        $data['categorianm']=$idcat;
        $data['categoria']=$idcat;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('categoria',$data);
        $this->load->view('templates/footer',$data);
        $this->load->view('categoriajs');
    }

    public function login(){
        $this->load->view('login');
    }

    public function login_data(){
        $username = $this->input->post('usuario');
        $password = $this->input->post('password');
        // Inicializamos la variable de respuesta en 0;
        $count = 0;
        // Obtenemos los datos del usuario ingresado mediante su usuario
        log_message('error', 'usuario: '.$username);
        $respuesta = $this->Login_model->login($username);
        $contrasena='';
        foreach ($respuesta as $item) {
            $contrasena =$item->contrasena;
        }
        // Verificamos si las contraseñas son iguales
        log_message('error', 'contra1: '.$password);
        log_message('error', 'contra2: '.$contrasena);
        $verificar = password_verify($password,$contrasena);
        // Devolvemos la respuesta
        $desactivar=$respuesta[0]->desactivar;
        if($desactivar==1){
            $count=2;
        }else{
            if($verificar) {
                $data = array(
                            'logeado'=>true,
                            'id'=>$respuesta[0]->id,
                            'nombre'=>$respuesta[0]->nombre,
                            'usuario'=>$respuesta[0]->usuario,
                        );
                $this->session->set_userdata($data);
                $count=1;
            }
        }
        echo $count;
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url(), 'refresh');
    }
    
    public function get_productos()
    {
        $id=$this->input->post('idcat');
        $nam=$this->input->post('nam');
        $precio=$this->input->post('precio');
        //var_dump($id);die;
        $resulcat=$this->ModelGeneral->get_select('categoria',array('categoriaId'=>$id));
        $categoriax='';
        foreach ($resulcat as $cat){
            $categoriax=$cat->categoria;
        }
        //var_dump($categoriax);die;
        //max-height: 530px !important; background-image: url(https://adminsys.semit.mx/uploads/productos/20230623_161455_140.png);
        //http://localhost/
        $resulp=$this->ModelGeneral->get_productos_cat($id,$nam,$precio);
        $html='<br>
            <div class="row">
              <div class="col-lg-3">
                <div style="margin: 27px;">
                    <h2 style="color:#012d6a">Categorías</h2>
                    <div style="margin-left: 95px;">';
                    $resulcat=$this->ModelGeneral->get_select('categoria',array('activo'=>1));
                    foreach ($resulcat as $x){ 
                        if($x->categoriaId==$id){
                            $html.='<a style="cursor:pointer" onclick="categoria_get('.$x->categoriaId.')"><p style="color:#012d6a;line-height: 24px; font-size: 21px;"><b style="color:#aed146">'.$x->categoria.'</b></p></a>';
                        }else{
                            $html.='<a style="cursor:pointer" onclick="categoria_get('.$x->categoriaId.')"><p style="color:#012d6a;line-height: 24px; font-size: 21px;">'.$x->categoria.'</p></a>';
                        }
                    } 
                    $html.='</div>
                </div>
              </div>
              <div class="col-lg-9">  
                <div style="margin: 27px;">
                    <div class="row">
                        <div class="col-lg-12" style="text-align: -webkit-right;">
                            <select class="form-select form-control h-auto" id="idprecio" style="width: 190px; padding-left: 41px;" onchange="categoria_data()">
                            <option value="0"';
                            if($precio==0){
                                $html.='selected';
                            }
                            $html.='>Mas relevantes</option>
                            <option value="1"';
                            if($precio==1){
                                $html.='selected';
                            }
                            $html.='>Menor precio</option>
                            <option value="2"';
                            if($precio==2){
                                $html.='selected';
                            }
                            $html.='>Mayor precio</option>
                          </select><br>
                        </div>
                    </div>
                    <div class="row">';
                        foreach ($resulp as $p){
                            $img_file=''; 
                            if($p->file!=''){
                                $img_file='https://adminsys.semit.mx/uploads/productos/'.$p->file; 
                            }else{
                                $img_file='https://adminsys.semit.mx/public/img/favICONSEMIT.png'; 
                            }  

                            $resulpro=$this->ModelGeneral->get_select('carrito_temporal',array('idproducto'=>$p->id,'idcliente'=>$this->idcliente));
                            $num_cl='';
                            foreach ($resulpro as $pr){
                                $num_cl='fill="#009bdb"';
                            }

                            $resulprof=$this->ModelGeneral->get_select('productos_favoritos',array('idproducto'=>$p->id,'idcliente'=>$this->idcliente));
                            $num_clf='';
                            foreach ($resulprof as $pr){
                                $num_clf='fill="red"';
                            }

                        $html.='<div class="col-lg-3">
                          <div class="appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200" data-appear-animation-duration="750" style="animation-delay: 200ms; margin-bottom: 26px;">
                            <div class="portfolio-item hover-effect-1 text-center">
                              <span class="thumb-info thumb-info-no-zoom thumb-info-no-overlay thumb-info-no-bg" style="background: #f5f5f7;">
                                <span class="thumb-info-wrapper thumb-info-wrapper-demos thumb-info-wrapper-link m-0 lazyloaded" style="height: 295px; background: url('.$img_file.') white; background-size: contain;
    background-position: center;
    background-repeat:no-repeat;background-size: contain;
    background-position: center;
    background-repeat:no-repeat;" 
                                  ><a href="'.base_url().'Productos/descripcion/'.$p->id.'"></a></span>';
                                if($this->idcliente!=0){  
                                  $html.='<div class="row" style="position: absolute; top: 4px; display: inline-flex;">
                                  <div class="col-lg-12">
                                    <a style="cursor:pointer; width: 62px; height: 57px;" onclick="add_favoritos('.$p->id.')"><span class="cora_'.$p->id.'"><svg '.$num_clf.' width="45" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-380 -305)"><path d="M402.493 309.685C392.661 309.685 384.691 317.656 384.691 327.488 384.691 337.32 392.661 345.29 402.493 345.29 412.326 345.29 420.296 337.32 420.296 327.488 420.301 317.66 412.337 309.69 402.509 309.685 402.504 309.685 402.499 309.685 402.493 309.685ZM402.493 336.406C402.493 336.406 393.836 329.786 393.836 324.439 393.836 320.873 399.285 317.054 402.493 323.42 405.702 317.054 411.151 320.873 411.151 324.439 411.151 329.786 402.493 336.406 402.493 336.406Z" /></g></svg></span></a>

                                   <a style="cursor:pointer; width: 62px; height: 57px;" onclick="add_carrito('.$p->id.')"><span class="car_'.$p->id.'"><svg width="46" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" overflow="hidden"><g transform="translate(-425 -305)"><path '.$num_cl.' d="M38.4375 25.7395 38.4375 9.84375 8.4375 9.84375 8.4375 7.92188C8.43516 5.87766 6.77859 4.22108 4.73438 4.21875L4.6875 4.21875C4.42861 4.21875 4.21875 4.42861 4.21875 4.6875 4.21875 4.94639 4.42861 5.15625 4.6875 5.15625L4.73438 5.15625C6.26114 5.1578 7.49845 6.39511 7.5 7.92188L7.5 31.4062C7.50216 33.1415 8.85502 34.575 10.5872 34.6777 9.15525 35.5615 8.71097 37.4388 9.5948 38.8708 10.4787 40.3027 12.356 40.747 13.7879 39.8632 15.2198 38.9793 15.6641 37.102 14.7803 35.67 14.5345 35.2719 14.2004 34.9357 13.8038 34.6875L31.1944 34.6875C29.7682 35.5807 29.3362 37.4608 30.2293 38.887 31.1224 40.3131 33.0026 40.7452 34.4287 39.852 35.8549 38.9589 36.287 37.0787 35.3939 35.6526 35.1493 35.262 34.8193 34.9321 34.4287 34.6875L37.9688 34.6875C38.2276 34.6875 38.4375 34.4776 38.4375 34.2188 38.4375 33.9599 38.2276 33.75 37.9688 33.75L10.7812 33.75C9.48745 33.7485 8.43905 32.7 8.4375 31.4062L8.4375 28.552ZM32.8125 39.375C31.6475 39.375 30.7031 38.4306 30.7031 37.2656 30.7031 36.1006 31.6475 35.1562 32.8125 35.1562 33.9775 35.1562 34.9219 36.1006 34.9219 37.2656 34.9206 38.43 33.9769 39.3737 32.8125 39.375ZM12.1875 39.375C11.0225 39.375 10.0781 38.4306 10.0781 37.2656 10.0781 36.1006 11.0225 35.1562 12.1875 35.1562 13.3525 35.1562 14.2969 36.1006 14.2969 37.2656 14.2956 38.43 13.3519 39.3737 12.1875 39.375ZM8.4375 16.4062 15 16.4062 15 21.0938 8.4375 21.0938ZM30 21.0938 23.4375 21.0938 23.4375 16.4062 30 16.4062ZM30.9375 16.4062 37.5 16.4062 37.5 21.0938 30.9375 21.0938ZM23.4375 22.0312 30 22.0312 30 25.5886 23.4375 26.2031ZM22.5 26.2917 15.9375 26.9062 15.9375 22.0312 22.5 22.0312ZM23.4375 15.4688 23.4375 10.7812 30 10.7812 30 15.4688ZM22.5 15.4688 15.9375 15.4688 15.9375 10.7812 22.5 10.7812ZM22.5 16.4062 22.5 21.0938 15.9375 21.0938 15.9375 16.4062ZM8.4375 22.0312 15 22.0312 15 26.9948 8.4375 27.6094ZM30.9375 25.5 30.9375 22.0312 37.5 22.0312 37.5 24.8855ZM37.5 15.4688 30.9375 15.4688 30.9375 10.7812 37.5 10.7812ZM15 10.7812 15 15.4688 8.4375 15.4688 8.4375 10.7812Z" transform="matrix(1.02222 0 0 1 425 305)" /></g></svg></span></a></div></div>';
                                }
                    
                                $html.='<div class="row">
                                  <div class="col-lg-2"></div>
                                  <div class="col-lg-8">
                                    <h5 style="background: #469ddc; color: white; border-radius: 7px; font-size: 19px;">$ ';$html.=$p->precio_con_iva;$html.='</h5>  
                                  </div> 
                                </div>  
                                <div class="row" style="height: 84px;">
                                  <div class="col-lg-12">
                                    <h6 style="text-align: initial; margin-left: 6px; color: #012d6a; font-size: 14px;  text-align-last: center;"><b>';$html.=$p->nombre;$html.='</b></h6>
                                    <a class="btn btn-outline btn-primary btn-xs mb-2" onclick="add_carrito('.$p->id.')">Agregar a carrito</a>
                                  </div>
                                </div>  
                              </span>
                            </div>
                          </div>
                        </div>';
                        } 
                    $html.='</div>
                </div>    
              </div>';
        echo $html;
    }

    function validar_correo(){
        $correo = $this->input->post('correo');
        $result=$this->ModelGeneral->getselectwhere('clientes','correo',$correo);
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    function validar(){
        $usuario = $this->input->post('usuario');
        $result=$this->ModelGeneral->getselectwhere('clientes','usuario',$usuario);
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }
    
    function validar_celular(){
        $celular = $this->input->post('celular');
        $result=$this->ModelGeneral->getselectwhere('clientes','celular',$celular);
        $resultado=0;
        foreach ($result as $row) {
            $resultado=1;
        }
        echo $resultado;
    }

    public function add_usuarios(){
        $datos = $this->input->post();
        $pss_verifica = $datos['contrasena'];
        $pass = password_hash($datos['contrasena'], PASSWORD_BCRYPT);
        $datos['contrasena'] = $pass;
        unset($datos['contrasena2']);
        $datos['codigo']=$this->generarCodigo();
        $datos['tipo_registro'] = 2;
        $id=$this->ModelGeneral->add_record('clientes',$datos);  
        $this->enviar($id);
        echo $id;
    }

    function generarCodigo(){
        $caracteres_permitidos = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $longitud = 5;
        return substr(str_shuffle($caracteres_permitidos), 0, $longitud);
    }

    public function validar_usuario()
    {   
        $id=$this->input->post('reg');
        $codigox=$this->input->post('codigo');
        $result=$this->ModelGeneral->getselectwhere('clientes','id',$id);
        $codigo=0;
        foreach ($result as $x) {
            $codigo=$x->codigo;
        }
        $validarc=0;
        if($codigo==$codigox){
            $data = array('estatus'=>1);
            $this->ModelGeneral->edit_record('id',$id,$data,'clientes');
            $validarc=1;
        }else{
            $validarc=0;
        }
        echo $validarc;
        
    }

    function enviar($id){

        //================================
            //cargamos la libreria email
            $this->load->library('email');
            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='semit.mx'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'hola@semit.mx';

            //Nuestra contraseña
            $config["smtp_pass"] = '2(dLH]oiolWz';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('hola@semit.mx','Semit site');
            /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
            */
        //======================
        $result=$this->ModelGeneral->getselectwhere('clientes','id',$id);
        $correo='';
        $usuario='';
        $codigo='';
        foreach ($result as $x){
            $correo=$x->correo;
            $usuario=$x->nombre;
            $codigo=$x->codigo;
        }

        $this->email->to($correo,$usuario);
        //$this->email->to('andres@mangoo.mx',$paciente);
        $asunto='Validar cuenta';
        //Definimos el asunto del mensaje
        $this->email->subject($asunto);        
        //Definimos el mensaje a enviar
        //$this->email->message($body)
        $message  = "<div style='background: url(http://estudiosunne.sicoi.net/public/img/anahuac-fondo-footer.jpg) #003166;width: 97%;height: 94%;padding: 25px;margin: 0px;'>
                <img class='img_logo1' style=' alt='Porto' width='135' height='55' data-sticky-width='82' data-sticky-height='40' data-sticky-top='25' src='http://pagedemo.semit.mx/public/img/SEMIT.jpg'>
                <br>
                Te enviamos el código de seguridad que tendrás que ingresar para crear tu cuenta con nosotros.
                <br>
                <b style='width:17px;'>Código:".$codigo."</b> 
                <br>
                <a style='color: black;' href='http://pagedemo.semit.mx/Inicio/aviso_de_privacidad'>Te invitamos a conocer nuestro aviso de privacidad aquí es la ligar de aviso de privacidad</a>
                  </div>";

        $this->email->message($message);

        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }

    }

    public function enviar_correo()
    {
        $correo = $this->input->post('correo');
        $result=$this->ModelGeneral->getlike('clientes','usuario',$correo,'celular',$correo);
        $resultado=0;
        $id=0;
        foreach ($result as $row) {
            $resultado=1;
            $id=$row->id;
        }
        $datos['codigo']=$this->generarCodigo();
        $this->ModelGeneral->edit_record('id',$id,$datos,'clientes');  
        $this->enviar_validar($id);

        echo $resultado;
    }

    function enviar_validar($id){
        //================================
            //cargamos la libreria email
            $this->load->library('email');
            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='semit.mx'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'hola@semit.mx';

            //Nuestra contraseña
            $config["smtp_pass"] = '2(dLH]oiolWz';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('hola@semit.mx','Semit site');
            /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
            */
        //======================
        $result=$this->ModelGeneral->getselectwhere('clientes','id',$id);
        $correo='';
        $usuario='';
        $codigo='';
        foreach ($result as $x){
            $correo=$x->correo;
            $usuario=$x->nombre;
            $codigo=$x->codigo;
        }
        $this->email->to($correo,$usuario);
        //$this->email->to('andres@mangoo.mx',$paciente);
        $asunto='Editar contraseña';
        //Definimos el asunto del mensaje
        $this->email->subject($asunto);        
        //Definimos el mensaje a enviar
        //$this->email->message($body)
        $message  = "<div style='background: url(http://estudiosunne.sicoi.net/public/img/anahuac-fondo-footer.jpg) #003166;width: 97%;height: 94%;padding: 25px;margin: 0px;'><a <a href='".base_url()."Inicio/editarpassword/".$codigo."'>Da clic en el siguiente enlace para poder editar tu contraseña</a>
                  </div>";

        $this->email->message($message);

        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }
    }

    public function editarpassword($codigo){
        $resulcat=$this->ModelGeneral->get_select('clientes',array('codigo'=>$codigo));
        $idg=0;
        foreach ($resulcat as $x){
            $idg=$x->id;
        }
        $data['idg']=$idg;
        $data['codigo']=$data['codigo'];;
        
        if($idg!=0){
            $this->load->view('update_login',$data);
        }else{
            header('Location: ' . base_url().'Inicio');
        }
    }

    public function editar_contrasena()
    {   
        $idg = $this->input->post('idg');
        $contrasena = $this->input->post('contrasena');
        $pass = password_hash($contrasena, PASSWORD_BCRYPT);
        $contrasena = $pass;
        $this->ModelGeneral->edit_record('id',$idg,array('codigo'=>'','contrasena'=>$contrasena),'clientes');  
    }

    function add_carrito_compra()
    {
        $id=$this->input->post('id');
        if($this->idcliente!=0){
            $resulcat=$this->ModelGeneral->get_select('carrito_temporal',array('idproducto'=>$id,'idcliente'=>$this->idcliente));
            $idg=0;
            $cantidad=0;
            foreach ($resulcat as $x){
                $idg=$x->id;
                $cantidad=$x->cantidad;
            }
            $suma=$cantidad+1;
            if($idg!=0){
                $this->ModelGeneral->edit_record('id',$idg,array('cantidad'=>$suma),'carrito_temporal');
            }else{
                $data = array('idproducto'=>$id,'cantidad'=>$suma,'idcliente'=>$this->idcliente);
                $this->ModelGeneral->add_record('carrito_temporal',$data);
            }
        }
        echo $this->idcliente;
    }

    function add_favoritos()
    {
        $id=$this->input->post('id');
        if($this->idcliente!=0){
            $resulcat=$this->ModelGeneral->get_select('productos_favoritos',array('idproducto'=>$id,'idcliente'=>$this->idcliente));
            $idg=0;
            foreach ($resulcat as $x){
                $idg=$x->id;
            }
            if($idg!=0){
                $this->ModelGeneral->delete_registro(array('idproducto'=>$id,'idcliente'=>$this->idcliente),'productos_favoritos');
            }else{
                $data = array('idproducto'=>$id,'idcliente'=>$this->idcliente);
                $this->ModelGeneral->add_record('productos_favoritos',$data);
            }
        }
        echo $idg;
    }

    function add_favoritos_desc()
    {
        $id=$this->input->post('id');
        if($this->idcliente!=0){
            $resulcat=$this->ModelGeneral->get_select('productos_favoritos',array('idproducto'=>$id,'idcliente'=>$this->idcliente));
            $idg=0;
            foreach ($resulcat as $x){
                $idg=$x->id;
            }
            if($idg!=0){

            }else{
                $data = array('idproducto'=>$id,'idcliente'=>$this->idcliente);
                $this->ModelGeneral->add_record('productos_favoritos',$data);
            }
        }
        echo $idg;
    }


    function get_productos_compras()
    {   

        $html='<div class="modal_carrito" style="max-height: 400px;
    overflow-y: scroll;overflow-x: hidden;"><ol class="mini-products-list">';
            $resulcat=$this->ModelGeneral->get_productos_temporal($this->idcliente);
            $suma=0;
            foreach ($resulcat as $x){
                $multi=$x->cantidad*$x->precio_con_iva;
                if($x->file!=''){
                    $img_file='https://adminsys.semit.mx/uploads/productos/'.$x->file; 
                }else{
                    $img_file='https://adminsys.semit.mx/public/img/favICONSEMIT.png'; 
                } 

                $set1='';$set2='';$set3='';$set4='';$set5='';$set6='';
                $set7='';$set8='';$set9='';$set10='';$set11='';$set12='';
                $set13='';$set14='';$set15='';$set16='';$set17='';$set18='';
                $set19='';$set20='';
                if($x->cantidad==1){ $set1='selected';
                }else if($x->cantidad==2){ $set2='selected';
                }else if($x->cantidad==3){ $set3='selected';
                }else if($x->cantidad==4){ $set4='selected';
                }else if($x->cantidad==5){ $set5='selected';
                }else if($x->cantidad==6){ $set6='selected';
                }else if($x->cantidad==7){ $set7='selected';
                }else if($x->cantidad==8){ $set8='selected';
                }else if($x->cantidad==9){ $set9='selected';
                }else if($x->cantidad==10){ $set10='selected';
                }else if($x->cantidad==11){ $set11='selected';
                }else if($x->cantidad==12){ $set12='selected';
                }else if($x->cantidad==13){ $set13='selected';
                }else if($x->cantidad==14){ $set14='selected';
                }else if($x->cantidad==15){ $set15='selected';
                }else if($x->cantidad==16){ $set16='selected';
                }else if($x->cantidad==17){ $set17='selected';
                }else if($x->cantidad==18){ $set18='selected';
                }else if($x->cantidad==19){ $set19='selected';
                }else if($x->cantidad==20){ $set20='selected';
                } 
                
                $html.='<li class="item reg_'.$x->id.'">
                    <a href="#" title="Camera X1000" class="product-image"><img src="'.$img_file.'"></a>
                    <div class="product-details">
                        <p class="product-name">
                            <a >'.$x->nombre.'</a>
                        </p>
                        <p class="qty-price" style="display: -webkit-box;
                            display: -ms-flexbox;
                            display: flex;
                            -ms-flex-wrap: wrap;
                            flex-wrap: wrap;
                            -webkit-box-align: stretch;
                            -ms-flex-align: stretch;
                            align-items: stretch;
                            width: 100%;">
                            <select class="form-select form-control h-auto" id="cantidady'.$x->id.'" style="width: 72px;" onchange="modificar_cant('.$x->id.','.$x->precio_con_iva.')">
                                <option value="1" '.$set1.'>1</option>
                                <option value="2" '.$set2.'>2</option>
                                <option value="3" '.$set3.'>3</option>
                                <option value="4" '.$set4.'>4</option>
                                <option value="5" '.$set5.'>5</option>
                                <option value="6" '.$set6.'>6</option>
                                <option value="7" '.$set7.'>7</option>
                                <option value="8" '.$set8.'>8</option>
                                <option value="9" '.$set9.'>9</option>
                                <option value="10" '.$set10.'>10</option>
                                <option value="11" '.$set11.'>11</option>
                                <option value="12" '.$set12.'>12</option>
                                <option value="13" '.$set13.'>13</option>
                                <option value="14" '.$set14.'>14</option>
                                <option value="15" '.$set15.'>15</option>
                                <option value="16" '.$set16.'>16</option>
                                <option value="17" '.$set17.'>17</option>
                                <option value="18" '.$set18.'>18</option>
                                <option value="19" '.$set19.'>19</option>
                                <option value="20" '.$set20.'>20</option>
                              </select>&nbsp;&nbsp;&nbsp;<span style="display: flex;
    align-items: center;
    background-color: #93ba1f;
    /* padding: 7px; */
    width: 60%;
    justify-content: space-evenly;
    color: white;
    font-size: 13px;
    border-radius: 30px;"><b class="cantida_pro_'.$x->id.'">$'.$multi.'</b><input type="hidden" class="precio_total_'.$x->id.'" id="precio_total" value="'.$multi.'"></span>
                        </p>
                        <a title="Remove This Item" class="btn-remove" style="cursor:pointer" onclick="quitar_producto('.$x->id.')"><i class="fas fa-times"></i></a>
                    </div>
                </li>'; 
                $suma+=$multi;
            }
          $html.='
        </ol></div>
        <div class="totals" style="padding-right: 20px;">
          <span class="label">Total:</span>
          <span class="price-total"><span class="price total_compras">$'.$suma.'</span></span>
        </div><div style="padding-right: 20px;"><a class="btn w-100 mb-2 btn-rounded" style="background: #9ebf43; color: white;" onclick="proceder_pago()">Proceder al pago</a>
            <span style="color:red; display:none" class="procesar_validar">Tienes que agregar almenos un producto</span>
        </div>';
        echo $html;
    }
    
    /*
    <ol class="mini-products-list">
                                                    <li class="item">
                                                        <a href="#" title="Camera X1000" class="product-image"><img src="img/products/product-1.jpg" alt="Camera X1000"></a>
                                                        <div class="product-details">
                                                            <p class="product-name">
                                                                <a href="#">Camera X1000 </a>
                                                            </p>
                                                            <p class="qty-price">
                                                                 1X <span class="price">$890</span>
                                                            </p>
                                                            <a href="#" title="Remove This Item" class="btn-remove"><i class="fas fa-times"></i></a>
                                                        </div>
                                                    </li>
                                                </ol>
                                                <div class="totals">
                                                    <span class="label">Total:</span>
                                                    <span class="price-total"><span class="price">$890</span></span>
                                                </div>
                                                <div class="actions">
                                                    <a class="btn btn-dark" href="#">View Cart</a>
                                                    <a class="btn btn-primary" href="#">Checkout</a>
                                                </div>
    */
    function quitarproducto()
    {
        $id=$this->input->post('id');
        $this->ModelGeneral->delete_registro(array('id'=>$id),'carrito_temporal');
    }

    function get_total_compras()
    {    
        $total=0;
        if($this->idcliente!=0){
            $resulcat=$this->ModelGeneral->get_productos_compra($this->idcliente);
            foreach ($resulcat as $x){
                $total=$x->total;
            }
        }
        echo $total;
    }

    public function contacto(){
        $resul=$this->ModelGeneral->get_select('bannerssliders_pestana',array('file!='=>'','activo'=>1));
        $resulcat=$this->ModelGeneral->get_select('categoria',array('activo'=>1));
        $data['pestana']=$resul;
        $resulp=$this->ModelGeneral->get_productos();
        $data['productos_get']=$resulp;
        $resulp1=$this->ModelGeneral->get_productosone();
        $data['productos1_get']=$resulp1;
        $data['nombre']=$this->nombre;
        $data['categorias']=$resulcat;
        $data['categorianm']=0;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('contacto',$data);
        $this->load->view('templates/footer',$data);
        $this->load->view('contactojs');
    }
    
    public function aviso_de_privacidad(){
        $data['avisoprivacidad']='';
        $resul=$this->ModelGeneral->getselectwhere('aviso_privacidad','id',1);
        foreach ($resul as $item){
            $data['avisoprivacidad']=$item->concepto;
        }
        $this->load->view('avisoprivacidad',$data);
    }

    function contacto_correo(){
        $nombre=$this->input->post('nombre');
        $correo=$this->input->post('correo');
        $mensaje=$this->input->post('mensaje');
        //================================
            //cargamos la libreria email
            $this->load->library('email');
            /*
            * Configuramos los parámetros para enviar el email,
            * las siguientes configuraciones es recomendable
            * hacerlas en el fichero email.php dentro del directorio config,
            * en este caso para hacer un ejemplo rápido lo hacemos 
            * en el propio controlador
            */
            //Indicamos el protocolo a utilizar
            $config['protocol'] = 'smtp';
             
            //El servidor de correo que utilizaremos
            $config["smtp_host"] ='semit.mx'; 
             
            //Nuestro usuario
            $config["smtp_user"] = 'hola@semit.mx';

            //Nuestra contraseña
            $config["smtp_pass"] = '2(dLH]oiolWz';

            //Puerto
            $config["smtp_port"] = '465';

            $config["smtp_crypto"] = 'ssl';
                    
            //El juego de caracteres a utilizar
            $config['charset'] = 'utf-8'; 
     
            //Permitimos que se puedan cortar palabras
            $config['wordwrap'] = TRUE;
             
            //El email debe ser valido  
            $config['validate'] = true;

            $config['mailtype'] = 'html';

            //Establecemos esta configuración
            $this->email->initialize($config);
     
            //Ponemos la dirección de correo que enviará el email y un nombre
            $this->email->from('hola@semit.mx','Semit site contacto');
            /*
               * Ponemos el o los destinatarios para los que va el email
               * en este caso al ser un formulario de contacto te lo enviarás a ti
               * mismo
            */
        //======================

        $this->email->to('leon1255555@gmail.com','Semit contacto');
        //$this->email->to('andres@mangoo.mx',$paciente);
        $asunto='Cliente';
        //Definimos el asunto del mensaje
        $this->email->subject($asunto);        
        //Definimos el mensaje a enviar
        //$this->email->message($body)
        $message  = "<div style='background: url(http://estudiosunne.sicoi.net/public/img/anahuac-fondo-footer.jpg) #003166;width: 97%;height: 94%;padding: 25px;margin: 0px;'>Nombre ".$nombre."<br>Correo ".$correo."<br>Mensaje:".$mensaje."
                  </div>";

        $this->email->message($message);

        //Enviamos el email y si se produce bien o mal que avise con una flasdata
        if($this->email->send()){
            //$this->session->set_flashdata('envio', 'Email enviado correctamente');
        }else{
            //$this->session->set_flashdata('envio', 'No se a enviado el email');
        }

    }

}