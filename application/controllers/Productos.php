<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModelGeneral');
        $this->load->model('Login_model');
        if ($this->session->userdata('logeado')){
            $this->nombre=$this->session->userdata('nombre');
            $this->usuario=$this->session->userdata('usuario');
            $this->idcliente=$this->session->userdata('id');
        }else{
            $this->nombre='';
            $this->usuario='';
            $this->idcliente=0;
        }
    }

	public function index(){
        $resul=$this->ModelGeneral->get_select('bannerssliders_pestana',array('file!='=>'','activo'=>1));
        $resulcat=$this->ModelGeneral->get_select('categoria',array('activo'=>1));
        $data['pestana']=$resul;
        $resulp=$this->ModelGeneral->get_productos();
        $data['productos_get']=$resulp;
        $resulp1=$this->ModelGeneral->get_productosone();
        $data['productos1_get']=$resulp1;
        $data['nombre']=$this->nombre;
        $data['categorias']=$resulcat;
        $data['categorianm']=0;

		$this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('productos',$data);
        $this->load->view('templates/footer',$data);
        $this->load->view('productosjs');
	}

    public function descripcion($id=0)
    {
        $resul=$this->ModelGeneral->get_select('bannerssliders_pestana',array('file!='=>'','activo'=>1));
        $resulcat=$this->ModelGeneral->get_select('categoria',array('activo'=>1));
        $data['pestana']=$resul;
        $resulp=$this->ModelGeneral->get_productos();
        $data['productos_get']=$resulp;
        $resulp1=$this->ModelGeneral->get_productosone();
        $data['productos1_get']=$resulp1;
        $data['nombre']=$this->nombre;
        $data['categorias']=$resulcat;
        $data['categorianm']=0;

        $data['resulpro']=$this->ModelGeneral->get_select('productos',array('id'=>$id));
        $data['resulprodll']=$this->ModelGeneral->get_select_order('productos_files',array('idproductos'=>$id,'activo'=>1),'id','DESC');
        //var_dump($data['resulprodll']);die;
        $data['resulpro_cart']=$this->ModelGeneral->get_select('productos_caracteristicas',array('idproductos'=>$id,'activo'=>1));

        $data['idcliente']=$this->idcliente;
        $this->load->view('templates/header');
        $this->load->view('templates/navbar',$data);
        $this->load->view('descripcion',$data);
        $this->load->view('templates/footer',$data);
        $this->load->view('iniciojs');
    }

    function imagen_pro()
    {   
        $id=$this->input->post('id');

        $img_file='https://adminsys.semit.mx/public/img/favICONSEMIT.png';

        $resulpro=$this->ModelGeneral->get_select('productos_files',array('id'=>$id));
        foreach ($resulpro as $x){
            $img_file='https://adminsys.semit.mx/uploads/productos/'.$x->file; 
        }
        $html='<div class="appear-animation animated fadeInUpShorter appear-animation-visible" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="200" data-appear-animation-duration="750" style="animation-delay: 200ms; margin-bottom: 26px;">
                <div class="portfolio-item hover-effect-1 text-center" >
                  <span class="thumb-info thumb-info-no-zoom thumb-info-no-overlay thumb-info-no-bg" style="background: #f5f5f7;">
                    <span class="thumb-info-wrapper thumb-info-wrapper-demos thumb-info-wrapper-link m-0 lazyloaded" style="height: 421px;; background-image: url('.$img_file.')" 
                      >
                    </span>
                  </span>
                </div>
            </div>';
        echo $html;    
    }

    function add_carrito_compra_pro()
    {
        $id=$this->input->post('id');
        $cantidadx=$this->input->post('cantidad');
        if($this->idcliente!=0){
            $resulcat=$this->ModelGeneral->get_select('carrito_temporal',array('idproducto'=>$id,'idcliente'=>$this->idcliente));
            $idg=0;
            $cantidad=0;
            foreach ($resulcat as $x){
                $idg=$x->id;
                $cantidad=$x->cantidad;
            }
            $suma=$cantidad+$cantidadx;
            if($idg!=0){
                $this->ModelGeneral->edit_record('id',$idg,array('cantidad'=>$suma),'carrito_temporal');
            }else{
                $data = array('idproducto'=>$id,'cantidad'=>$suma,'idcliente'=>$this->idcliente);
                $this->ModelGeneral->add_record('carrito_temporal',$data);
            }
        }
        echo $this->idcliente;
    }

    function edit_carrito_compra_pro()
    {
        $id=$this->input->post('id');
        $cantidadx=$this->input->post('cantidad');
        $this->ModelGeneral->edit_record('id',$id,array('cantidad'=>$cantidadx),'carrito_temporal');
    }

}