-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 08-08-2023 a las 10:07:49
-- Versión del servidor: 5.7.23-23
-- Versión de PHP: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `semitmx_adminsys`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id` int(11) NOT NULL,
  `idcliente` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `idtransacción` text COLLATE utf8_unicode_ci,
  `detalles` text COLLATE utf8_unicode_ci,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id`, `idcliente`, `total`, `activo`, `idtransacción`, `detalles`, `reg`) VALUES
(1, 1, '767.24', 1, '8HF85059PM2933416', '{\"id\":\"8HF85059PM2933416\",\"intent\":\"CAPTURE\",\"status\":\"COMPLETED\",\"purchase_units\":[{\"reference_id\":\"default\",\"amount\":{\"currency_code\":\"MXN\",\"value\":\"767.24\"},\"payee\":{\"email_address\":\"sb-4moio26456688@business.example.com\",\"merchant_id\":\"WTYGULZTXZKFQ\"},\"soft_descriptor\":\"PAYPAL *TEST STORE\",\"shipping\":{\"name\":{\"full_name\":\"John Doe\"},\"address\":{\"address_line_1\":\"Free Trade Zone\",\"admin_area_2\":\"Buenos Aires\",\"admin_area_1\":\"Buenos Aires\",\"postal_code\":\"B1675\",\"country_code\":\"AR\"}},\"payments\":{\"captures\":[{\"id\":\"5MD125239B313635A\",\"status\":\"PENDING\",\"status_details\":{\"reason\":\"RECEIVING_PREFERENCE_MANDATES_MANUAL_ACTION\"},\"amount\":{\"currency_code\":\"MXN\",\"value\":\"767.24\"},\"final_capture\":\"true\",\"seller_protection\":{\"status\":\"ELIGIBLE\",\"dispute_categories\":[\"ITEM_NOT_RECEIVED\",\"UNAUTHORIZED_TRANSACTION\"]},\"create_time\":\"2023-08-07T22:52:57Z\",\"update_time\":\"2023-08-07T22:52:57Z\"}]}}],\"payer\":{\"name\":{\"given_name\":\"John\",\"surname\":\"Doe\"},\"email_address\":\"sb-njt3y26456687@personal.example.com\",\"payer_id\":\"4D4YL33VDNZCA\",\"address\":{\"country_code\":\"AR\"}},\"create_time\":\"2023-08-07T22:52:33Z\",\"update_time\":\"2023-08-07T22:52:57Z\",\"links\":[{\"href\":\"https:\\/\\/api.sandbox.paypal.com\\/v2\\/checkout\\/orders\\/8HF85059PM2933416\",\"rel\":\"self\",\"method\":\"GET\"}]}', '2023-08-07 22:52:58'),
(2, 1, '767.24', 1, NULL, '[]', '2023-08-07 22:54:06'),
(3, 1, '767.24', 1, '9B7064402U284711S', '{\"id\":\"9B7064402U284711S\",\"intent\":\"CAPTURE\",\"status\":\"COMPLETED\",\"purchase_units\":[{\"reference_id\":\"default\",\"amount\":{\"currency_code\":\"MXN\",\"value\":\"767.24\"},\"payee\":{\"email_address\":\"sb-4moio26456688@business.example.com\",\"merchant_id\":\"WTYGULZTXZKFQ\"},\"soft_descriptor\":\"PAYPAL *TEST STORE\",\"shipping\":{\"name\":{\"full_name\":\"John Doe\"},\"address\":{\"address_line_1\":\"Free Trade Zone\",\"admin_area_2\":\"Buenos Aires\",\"admin_area_1\":\"Buenos Aires\",\"postal_code\":\"B1675\",\"country_code\":\"AR\"}},\"payments\":{\"captures\":[{\"id\":\"8A949766LF108120Y\",\"status\":\"PENDING\",\"status_details\":{\"reason\":\"RECEIVING_PREFERENCE_MANDATES_MANUAL_ACTION\"},\"amount\":{\"currency_code\":\"MXN\",\"value\":\"767.24\"},\"final_capture\":\"true\",\"seller_protection\":{\"status\":\"ELIGIBLE\",\"dispute_categories\":[\"ITEM_NOT_RECEIVED\",\"UNAUTHORIZED_TRANSACTION\"]},\"create_time\":\"2023-08-07T22:55:22Z\",\"update_time\":\"2023-08-07T22:55:22Z\"}]}}],\"payer\":{\"name\":{\"given_name\":\"John\",\"surname\":\"Doe\"},\"email_address\":\"sb-njt3y26456687@personal.example.com\",\"payer_id\":\"4D4YL33VDNZCA\",\"address\":{\"country_code\":\"AR\"}},\"create_time\":\"2023-08-07T22:55:02Z\",\"update_time\":\"2023-08-07T22:55:22Z\",\"links\":[{\"href\":\"https:\\/\\/api.sandbox.paypal.com\\/v2\\/checkout\\/orders\\/9B7064402U284711S\",\"rel\":\"self\",\"method\":\"GET\"}]}', '2023-08-07 22:55:23'),
(4, 1, '767.24', 1, '4AA95104J37127236', '{\"id\":\"4AA95104J37127236\",\"intent\":\"CAPTURE\",\"status\":\"COMPLETED\",\"purchase_units\":[{\"reference_id\":\"default\",\"amount\":{\"currency_code\":\"MXN\",\"value\":\"767.24\"},\"payee\":{\"email_address\":\"sb-4moio26456688@business.example.com\",\"merchant_id\":\"WTYGULZTXZKFQ\"},\"soft_descriptor\":\"PAYPAL *TEST STORE\",\"shipping\":{\"name\":{\"full_name\":\"gerardo gerardo\"},\"address\":{\"address_line_1\":\"siempre viva\",\"address_line_2\":\"centro\",\"admin_area_2\":\"puebla\",\"admin_area_1\":\"PUE\",\"postal_code\":\"94100\",\"country_code\":\"MX\"}},\"payments\":{\"captures\":[{\"id\":\"5T1279386V2033140\",\"status\":\"PENDING\",\"status_details\":{\"reason\":\"RECEIVING_PREFERENCE_MANDATES_MANUAL_ACTION\"},\"amount\":{\"currency_code\":\"MXN\",\"value\":\"767.24\"},\"final_capture\":\"true\",\"seller_protection\":{\"status\":\"ELIGIBLE\",\"dispute_categories\":[\"ITEM_NOT_RECEIVED\",\"UNAUTHORIZED_TRANSACTION\"]},\"create_time\":\"2023-08-07T23:03:56Z\",\"update_time\":\"2023-08-07T23:03:56Z\"}]}}],\"payer\":{\"name\":{\"given_name\":\"gerardo\",\"surname\":\"gerardo\"},\"email_address\":\"gerardo@mangoo.com.mx\",\"payer_id\":\"4YXCBHNK3ULPQ\",\"address\":{\"country_code\":\"MX\"}},\"create_time\":\"2023-08-07T22:59:09Z\",\"update_time\":\"2023-08-07T23:03:56Z\",\"links\":[{\"href\":\"https:\\/\\/api.sandbox.paypal.com\\/v2\\/checkout\\/orders\\/4AA95104J37127236\",\"rel\":\"self\",\"method\":\"GET\"}]}', '2023-08-07 23:03:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalles`
--

CREATE TABLE `venta_detalles` (
  `id` int(11) NOT NULL,
  `idventa` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `producto` int(11) NOT NULL,
  `costo` decimal(10,2) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `venta_detalles`
--

INSERT INTO `venta_detalles` (`id`, `idventa`, `cantidad`, `producto`, `costo`, `activo`) VALUES
(1, 3, 1, 39, '168.10', 1),
(2, 3, 1, 40, '599.14', 1),
(3, 4, 1, 39, '168.10', 1),
(4, 4, 1, 40, '599.14', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `venta_detalles`
--
ALTER TABLE `venta_detalles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `venta_detalles`
--
ALTER TABLE `venta_detalles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
