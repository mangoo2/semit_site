-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 14-08-2023 a las 21:19:58
-- Versión del servidor: 10.4.10-MariaDB
-- Versión de PHP: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `kyocera_pro20`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `f_configuraciones`
--

DROP TABLE IF EXISTS `f_configuraciones`;
CREATE TABLE IF NOT EXISTS `f_configuraciones` (
  `ConfiguracionesId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Rfc` varchar(45) DEFAULT NULL,
  `Nombre` varchar(500) DEFAULT NULL,
  `Curp` varchar(45) DEFAULT NULL,
  `FolioDe` int(10) UNSIGNED DEFAULT NULL,
  `FolioHasta` int(10) UNSIGNED DEFAULT NULL,
  `FolioActual` int(10) UNSIGNED DEFAULT NULL,
  `Aprobacion` varchar(45) DEFAULT NULL,
  `Serie` varchar(45) DEFAULT NULL,
  `Direccion` varchar(500) DEFAULT NULL,
  `Titulo` varchar(500) DEFAULT NULL,
  `LeyendaAsignacion` varchar(1000) DEFAULT NULL,
  `LeyendaPagare` varchar(1000) DEFAULT NULL,
  `RutaCodigo` varchar(300) DEFAULT '/home/inmex/public_html/factura/files/images/codigo.png',
  `Pais` varchar(45) DEFAULT 'Mexico',
  `LugarExpedicion` varchar(45) DEFAULT 'Mexico',
  `Regimen` varchar(100) DEFAULT NULL,
  `PaisExpedicion` varchar(45) DEFAULT NULL,
  `Calle` varchar(45) DEFAULT NULL,
  `Estado` varchar(45) DEFAULT NULL,
  `Municipio` varchar(45) DEFAULT NULL,
  `CodigoPostal` varchar(45) DEFAULT NULL,
  `Noexterior` varchar(10) DEFAULT NULL,
  `Colonia` varchar(45) DEFAULT NULL,
  `usuario_id` int(10) UNSIGNED DEFAULT NULL,
  `carpeta` varchar(200) NOT NULL,
  `Email` varchar(200) NOT NULL,
  `Asunto` varchar(200) NOT NULL,
  `cuerpo` text NOT NULL,
  `localidad` varchar(200) NOT NULL,
  `archivocer` varchar(100) NOT NULL,
  `archivokey` varchar(100) NOT NULL,
  `paswordkey` varchar(100) NOT NULL,
  `logotipo` varchar(100) NOT NULL,
  `template` varchar(200) NOT NULL,
  `Nointerior` varchar(45) NOT NULL,
  `clavepfx` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `retenciones` int(1) NOT NULL DEFAULT 0,
  `timbresvigentes` int(11) NOT NULL,
  `tv_inicio` date NOT NULL,
  `tv_fin` date NOT NULL,
  PRIMARY KEY (`ConfiguracionesId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `f_configuraciones`
--

INSERT INTO `f_configuraciones` (`ConfiguracionesId`, `Rfc`, `Nombre`, `Curp`, `FolioDe`, `FolioHasta`, `FolioActual`, `Aprobacion`, `Serie`, `Direccion`, `Titulo`, `LeyendaAsignacion`, `LeyendaPagare`, `RutaCodigo`, `Pais`, `LugarExpedicion`, `Regimen`, `PaisExpedicion`, `Calle`, `Estado`, `Municipio`, `CodigoPostal`, `Noexterior`, `Colonia`, `usuario_id`, `carpeta`, `Email`, `Asunto`, `cuerpo`, `localidad`, `archivocer`, `archivokey`, `paswordkey`, `logotipo`, `template`, `Nointerior`, `clavepfx`, `retenciones`, `timbresvigentes`, `tv_inicio`, `tv_fin`) VALUES
(1, 'TEST010203001', 'Pablo Neruda Perez', 'U', 1, 500, NULL, NULL, NULL, 'PUEBLA', NULL, NULL, NULL, '/home/inmex/public_html/factura/files/images/codigo.png', 'Mexico', '', '621', 'México', 'BLVD NORTE', 'Puebla', 'PUEBLA', '72090', '1831', 'VILLAS SAN ALEJANDRO u', NULL, '', 'daniel@mangoo.mx', 'FACTURA', '<p>Gracias por tu confianza. Env&iacute;amos la factura electr&oacute;nica adjunta a &eacute;ste correo.</p>\r\n\r\n<p>Datos bancarios para emisi&oacute;n de cheque y/o transferencia bancaria:<br />\r\nNombre: DANIEL JAASIEL HERN&Aacute;NDEZ RODR&Iacute;GUEZ<br />\r\nRFC: HERD890308UAA</p>\r\n\r\n<p><strong>Banco: BBVA</strong><br />\r\nCuenta: 0186866832<br />\r\nClabe: 012180001868668326<br />\r\nPara dep&oacute;sito en OXXO:Pl&aacute;stico 4152 3136 2219 2685</p>\r\n\r\n<p><strong>Banco: CityBanamex</strong><br />\r\nClabe: 002650701630361594</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Cualquier duda y/o comentario favor de comunicarse a nuestras oficinas con tel&eacute;fono: 01(222) 644 25 33 o v&iacute;a correo electr&oacute;nico: <a href=\"mailto:contacto@mangoo.mx\">contacto@mangoo.mx</a>&nbsp; en horario de atenci&oacute;n a clientes de lunes a viernes de 9:00 am a 7:00 pm,</p>\r\n\r\n<p>Atentamente:</p>\r\n\r\n<p><img alt=\"\" src=\"http://mangoo.mx/logos/MangooLogo.png\" style=\"width: 200px; height: 59px;\" /></p>\r\n\r\n<p><strong>Contabilidad y Finanzas&nbsp;</strong></p>\r\n\r\n<p>Av. Ju&aacute;rez No. 2925 (Torre JV Ju&aacute;rez - PH) Col. La Paz, Puebla, Pue.<br />\r\nTel&eacute;fono: (222) 644 25 33 .&nbsp;<br />\r\nCorreo:&nbsp; &nbsp;facturacion@mangoo.mx</p>', 'Puebla', 'archivos.cer', 'archivos.key', '12345678a', '', '', '', '', 0, 10000, '2021-01-13', '2021-12-31'),
(2, 'TEST010203001', 'Pablo Neruda Perez', 'D', 1, 500, NULL, NULL, NULL, 'PUEBLA', NULL, NULL, NULL, '/home/inmex/public_html/factura/files/images/codigo.png', 'Mexico', '', '621', 'México', 'BLVD NORTE', 'Puebla', 'PUEBLA', '72090', '1831', 'VILLAS SAN ALEJANDRO d', NULL, '', 'daniel@mangoo.mx', 'FACTURA', '<p>Gracias por tu confianza. Env&iacute;amos la factura electr&oacute;nica adjunta a &eacute;ste correo.</p>\r\n\r\n<p>Datos bancarios para emisi&oacute;n de cheque y/o transferencia bancaria:<br />\r\nNombre: DANIEL JAASIEL HERN&Aacute;NDEZ RODR&Iacute;GUEZ<br />\r\nRFC: HERD890308UAA</p>\r\n\r\n<p><strong>Banco: BBVA</strong><br />\r\nCuenta: 0186866832<br />\r\nClabe: 012180001868668326<br />\r\nPara dep&oacute;sito en OXXO:Pl&aacute;stico 4152 3136 2219 2685</p>\r\n\r\n<p><strong>Banco: CityBanamex</strong><br />\r\nClabe: 002650701630361594</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Cualquier duda y/o comentario favor de comunicarse a nuestras oficinas con tel&eacute;fono: 01(222) 644 25 33 o v&iacute;a correo electr&oacute;nico: <a href=\"mailto:contacto@mangoo.mx\">contacto@mangoo.mx</a>&nbsp; en horario de atenci&oacute;n a clientes de lunes a viernes de 9:00 am a 7:00 pm,</p>\r\n\r\n<p>Atentamente:</p>\r\n\r\n<p><img alt=\"\" src=\"http://mangoo.mx/logos/MangooLogo.png\" style=\"width: 200px; height: 59px;\" /></p>\r\n\r\n<p><strong>Contabilidad y Finanzas&nbsp;</strong></p>\r\n\r\n<p>Av. Ju&aacute;rez No. 2925 (Torre JV Ju&aacute;rez - PH) Col. La Paz, Puebla, Pue.<br />\r\nTel&eacute;fono: (222) 644 25 33 .&nbsp;<br />\r\nCorreo:&nbsp; &nbsp;facturacion@mangoo.mx</p>', 'Puebla', 'archivos.cer', 'archivos.key', 'mINERVA09', '', '', '', '', 0, 10000, '2021-01-13', '2021-12-31');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
