-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 14-08-2023 a las 12:40:10
-- Versión del servidor: 10.3.39-MariaDB
-- Versión de PHP: 8.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `factura5_sisfac`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `f_complementopago`
--

CREATE TABLE `f_complementopago` (
  `complementoId` bigint(20) NOT NULL,
  `FacturasId` int(11) NOT NULL,
  `ImpPagado` float NOT NULL COMMENT 'monto',
  `Folio` varchar(15) NOT NULL,
  `Fecha` datetime NOT NULL,
  `Sello` text NOT NULL,
  `NoCertificado` varchar(30) NOT NULL,
  `Certificado` text NOT NULL,
  `SubTotal` float NOT NULL DEFAULT 0 COMMENT 'por defecto',
  `Moneda` varchar(5) NOT NULL DEFAULT 'XXX' COMMENT 'por defecto',
  `Total` float NOT NULL DEFAULT 0 COMMENT 'por defecto',
  `TipoDeComprobante` varchar(3) NOT NULL DEFAULT 'P' COMMENT 'por defecto',
  `LugarExpedicion` varchar(7) NOT NULL,
  `E_rfc` varchar(30) NOT NULL,
  `E_nombre` varchar(300) NOT NULL,
  `E_regimenfiscal` varchar(10) NOT NULL,
  `R_rfc` varchar(30) NOT NULL,
  `R_nombre` varchar(300) NOT NULL,
  `R_regimenfiscal` varchar(10) NOT NULL,
  `ClaveUnidad` varchar(10) NOT NULL DEFAULT 'ACT' COMMENT 'por defecto',
  `ClaveProdServ` varchar(20) NOT NULL DEFAULT '84111506' COMMENT 'por defecto',
  `Cantidad` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'por defecto',
  `Descripcion` varchar(30) NOT NULL DEFAULT 'Pago' COMMENT 'por defecto',
  `ValorUnitario` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'por defecto',
  `Importe` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'por defecto',
  `FechaPago` datetime NOT NULL,
  `FormaDePagoP` varchar(10) NOT NULL,
  `MonedaP` varchar(10) NOT NULL,
  `Monto` float(10,2) NOT NULL,
  `NumOperacion` varchar(30) NOT NULL,
  `CtaBeneficiario` varchar(30) NOT NULL,
  `IdDocumento` varchar(50) NOT NULL,
  `Serie` varchar(50) NOT NULL,
  `Foliod` varchar(50) NOT NULL,
  `MonedaDR` varchar(10) NOT NULL,
  `MetodoDePagoDR` varchar(10) DEFAULT NULL,
  `NumParcialidad` int(11) NOT NULL,
  `ImpSaldoAnt` float NOT NULL,
  `ImpSaldoInsoluto` float NOT NULL,
  `UsoCFDI` varchar(100) DEFAULT NULL,
  `Estado` int(11) NOT NULL DEFAULT 0,
  `rutaXml` text DEFAULT NULL,
  `uuid` varchar(300) DEFAULT NULL,
  `sellosat` text DEFAULT NULL,
  `nocertificadosat` varchar(300) DEFAULT NULL,
  `fechatimbre` datetime DEFAULT NULL,
  `cadenaoriginal` text DEFAULT NULL,
  `rutaAcuseCancelacion` text DEFAULT NULL,
  `fechacancelacion` text DEFAULT NULL,
  `f_relacion` tinyint(1) NOT NULL DEFAULT 0,
  `f_r_tipo` varchar(2) DEFAULT NULL,
  `f_r_uuid` varchar(60) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `correoenviado` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `f_complementopago_documento`
--

CREATE TABLE `f_complementopago_documento` (
  `docId` bigint(20) NOT NULL,
  `complementoId` bigint(20) NOT NULL,
  `facturasId` int(11) NOT NULL,
  `IdDocumento` text NOT NULL,
  `serie` varchar(10) DEFAULT NULL,
  `folio` varchar(10) DEFAULT NULL,
  `NumParcialidad` float NOT NULL,
  `ImpSaldoAnt` decimal(10,2) NOT NULL,
  `ImpPagado` decimal(10,2) NOT NULL,
  `ImpSaldoInsoluto` float NOT NULL,
  `MetodoDePagoDR` varchar(10) NOT NULL,
  `Estado` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `f_facturas`
--

CREATE TABLE `f_facturas` (
  `FacturasId` int(11) NOT NULL,
  `Referencia` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Rfc` varchar(45) DEFAULT NULL,
  `Nombre` varchar(100) DEFAULT NULL,
  `Direccion` varchar(500) DEFAULT NULL,
  `Cp` varchar(45) DEFAULT NULL,
  `serie` varchar(2) NOT NULL,
  `Folio` int(11) DEFAULT NULL,
  `Estado` int(11) DEFAULT 2 COMMENT '0 cancelada 1 timbrada 2 error',
  `LeyendaAsignacion` varchar(550) DEFAULT NULL,
  `Lote` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Caducidad` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Paciente` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `LeyendaPagare` varchar(1000) DEFAULT NULL,
  `PaisReceptor` varchar(45) DEFAULT NULL,
  `TipoComprobante` varchar(45) DEFAULT NULL,
  `FormaPago` varchar(45) DEFAULT NULL,
  `MetodoPago` varchar(45) DEFAULT NULL,
  `CondicionesDePago` text DEFAULT NULL,
  `rutaXml` varchar(100) DEFAULT NULL,
  `clienteId` int(11) UNSIGNED DEFAULT NULL,
  `usuario_id` int(10) UNSIGNED DEFAULT NULL,
  `creada_sesionId` int(10) NOT NULL,
  `cadenaoriginal` longtext NOT NULL,
  `sellocadena` longtext NOT NULL,
  `sellosat` longtext NOT NULL,
  `uuid` varchar(200) NOT NULL,
  `fechatimbre` datetime NOT NULL,
  `subtotal` decimal(12,2) NOT NULL,
  `iva` decimal(10,4) NOT NULL,
  `total` decimal(12,4) NOT NULL,
  `certificado` longtext NOT NULL,
  `nocertificado` longtext NOT NULL,
  `nocertificadosat` longtext NOT NULL,
  `tarjeta` varchar(4) NOT NULL,
  `fechacancelacion` datetime NOT NULL,
  `sellocancelacion` longtext NOT NULL,
  `ordenCompra` varchar(50) NOT NULL,
  `moneda` varchar(50) NOT NULL,
  `observaciones` text NOT NULL,
  `comprobado` tinyint(4) NOT NULL DEFAULT 0,
  `rutaAcuseCancelacion` varchar(200) DEFAULT NULL,
  `uso_cfdi` varchar(4) NOT NULL,
  `numproveedor` varchar(100) NOT NULL,
  `numordencompra` varchar(100) NOT NULL,
  `honorario` decimal(10,4) NOT NULL,
  `isr` decimal(10,4) NOT NULL,
  `ivaretenido` decimal(10,4) NOT NULL,
  `cedular` decimal(10,4) NOT NULL,
  `outsourcing` decimal(10,4) DEFAULT 0.0000,
  `5almillar` tinyint(1) NOT NULL DEFAULT 0,
  `cincoalmillarval` float NOT NULL DEFAULT 0,
  `facturaabierta` tinyint(1) NOT NULL DEFAULT 0,
  `f_relacion` tinyint(1) NOT NULL DEFAULT 0,
  `f_r_tipo` varchar(2) DEFAULT NULL,
  `f_r_uuid` varchar(60) DEFAULT NULL,
  `pg_global` tinyint(1) NOT NULL DEFAULT 0,
  `pg_periodicidad` varchar(2) DEFAULT NULL,
  `pg_meses` varchar(2) DEFAULT NULL,
  `pg_anio` int(11) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `favorito` tinyint(1) NOT NULL DEFAULT 0,
  `correoenviado` tinyint(1) NOT NULL DEFAULT 0,
  `estatus_correo` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `f_facturas_servicios`
--

CREATE TABLE `f_facturas_servicios` (
  `sserviciosId` int(11) NOT NULL,
  `FacturasId` int(11) NOT NULL,
  `Cantidad` decimal(10,2) NOT NULL,
  `Unidad` varchar(10) NOT NULL,
  `servicioId` varchar(11) NOT NULL,
  `Descripcion` longtext NOT NULL,
  `Descripcion2` text NOT NULL,
  `Cu` decimal(10,2) NOT NULL,
  `descuento` float DEFAULT 0,
  `Importe` decimal(10,2) NOT NULL,
  `iva` decimal(10,4) NOT NULL DEFAULT 0.0000
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `f_complementopago`
--
ALTER TABLE `f_complementopago`
  ADD PRIMARY KEY (`complementoId`);

--
-- Indices de la tabla `f_complementopago_documento`
--
ALTER TABLE `f_complementopago_documento`
  ADD PRIMARY KEY (`docId`),
  ADD KEY `comp_fk_documento` (`complementoId`),
  ADD KEY `comp_fk_factura` (`facturasId`);

--
-- Indices de la tabla `f_facturas`
--
ALTER TABLE `f_facturas`
  ADD PRIMARY KEY (`FacturasId`);

--
-- Indices de la tabla `f_facturas_servicios`
--
ALTER TABLE `f_facturas_servicios`
  ADD PRIMARY KEY (`sserviciosId`),
  ADD KEY `factura_fk_fa_servicios` (`FacturasId`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `f_complementopago`
--
ALTER TABLE `f_complementopago`
  MODIFY `complementoId` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `f_complementopago_documento`
--
ALTER TABLE `f_complementopago_documento`
  MODIFY `docId` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `f_facturas`
--
ALTER TABLE `f_facturas`
  MODIFY `FacturasId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `f_facturas_servicios`
--
ALTER TABLE `f_facturas_servicios`
  MODIFY `sserviciosId` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `f_complementopago_documento`
--
ALTER TABLE `f_complementopago_documento`
  ADD CONSTRAINT `comp_fk_documento` FOREIGN KEY (`complementoId`) REFERENCES `f_complementopago` (`complementoId`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `comp_fk_factura` FOREIGN KEY (`facturasId`) REFERENCES `f_facturas` (`FacturasId`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `f_facturas_servicios`
--
ALTER TABLE `f_facturas_servicios`
  ADD CONSTRAINT `factura_fk_fa_servicios` FOREIGN KEY (`FacturasId`) REFERENCES `f_facturas` (`FacturasId`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
ALTER TABLE `f_facturas` ADD `RegimenFiscalReceptor` VARCHAR(4) NULL DEFAULT NULL AFTER `relacion_venta_pro`;
