ALTER TABLE `productos` ADD `idProducto` VARCHAR(255) NOT NULL AFTER `id`;
ALTER TABLE `productos` ADD `marca` VARCHAR(255) NOT NULL AFTER `mas_vendidos`;
ALTER TABLE `productos` ADD `codigoBarras` VARCHAR(255) NOT NULL AFTER `marca`;
ALTER TABLE `productos` ADD `unidadMedida` VARCHAR(255) NOT NULL AFTER `codigoBarras`;
ALTER TABLE `productos` CHANGE `activo` `activo` VARCHAR(50) NOT NULL DEFAULT '1';
ALTER TABLE `productos` ADD `activoDesde` DATE NULL DEFAULT NULL AFTER `unidadMedida`;
ALTER TABLE `productos` ADD `activoHasta` DATE NULL DEFAULT NULL AFTER `activoDesde`;
ALTER TABLE `productos` ADD `cpaginas` INT NOT NULL AFTER `activoHasta`;
ALTER TABLE `productos` ADD `listaPrecio` INT NOT NULL AFTER `cpaginas`;
ALTER TABLE `productos` ADD `existenciaAlmacen` DECIMAL(10,2) NOT NULL AFTER `listaPrecio`;
ALTER TABLE `productos` ADD `comprometido` DECIMAL(10,2) NOT NULL AFTER `existenciaAlmacen`;
ALTER TABLE `productos` ADD `pedido` DECIMAL(10,2) NOT NULL AFTER `comprometido`;
ALTER TABLE `productos` ADD `longuitud` DECIMAL(10,2) NOT NULL AFTER `pedido`;
ALTER TABLE `productos` ADD `volumen` DECIMAL(10,2) NOT NULL AFTER `longuitud`;
ALTER TABLE `productos` ADD `almacen` VARCHAR(255) NOT NULL AFTER `volumen`;
ALTER TABLE `productos` ADD `PrecioV` DECIMAL NOT NULL AFTER `almacen`;
ALTER TABLE `productos` ADD `UMLargo` VARCHAR(255) NOT NULL AFTER `PrecioV`, ADD `UMAncho` VARCHAR(255) NOT NULL AFTER `UMLargo`, ADD `UMPeso` VARCHAR(255) NOT NULL AFTER `UMAncho`, ADD `porcentajeIva` DECIMAL NOT NULL AFTER `UMPeso`;
ALTER TABLE `productos` ADD `Categoriat` VARCHAR(255) NOT NULL AFTER `porcentajeIva`;
ALTER TABLE `productos` ADD `UMAlto` VARCHAR(255) NOT NULL AFTER `Categoriat`;
ALTER TABLE `productos` CHANGE `activo` `activo` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `productos` ADD `estatus` TINYINT NOT NULL DEFAULT '1' AFTER `UMAlto`;


========================================================================================
ALTER TABLE `clientes` ADD `celular` VARCHAR(15) NOT NULL AFTER `reg`;

ALTER TABLE `clientes` ADD `codigo` VARCHAR(10) NOT NULL AFTER `celular`;

ALTER TABLE `clientes` ADD `estatus` TINYINT NOT NULL DEFAULT '0' AFTER `codigo`;

13 07 2023


===========================================
17/07/2023

http://localhost/semit_site/css/theme-elements.css



sistema semit 
titulo de bloc 
descripcion text editor 
descripcion text editor 
cateogirias  select tener boton de mas
imagen princial 



INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`, `tipo`, `submenutipo`, `idsubmenu`) VALUES (NULL, '4', 'Blog', 'Blog', 'clipboard', '6', '0', '0', '0');

INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '16');


31/07/2023
ALTER TABLE `clientes` ADD `direccion_predeterminada` TINYINT(1) NOT NULL AFTER `correo`;

ALTER TABLE `clientes` ADD `calle` VARCHAR(255) NOT NULL AFTER `direccion_predeterminada`, ADD `cp` VARCHAR(20) NOT NULL AFTER `calle`, ADD `colonia` VARCHAR(255) NOT NULL AFTER `cp`, ADD `telefono` VARCHAR(20) NOT NULL AFTER `colonia`, ADD `instrucciones` TEXT NOT NULL AFTER `telefono`, ADD `pais` VARCHAR(100) NOT NULL AFTER `instrucciones`;

09/08/2023

DROP TABLE IF EXISTS `cliente_fiscales`;
CREATE TABLE IF NOT EXISTS `cliente_fiscales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) NOT NULL,
  `razon_social` text NOT NULL,
  `rfc` varchar(30) NOT NULL,
  `cp` varchar(10) NOT NULL,
  `RegimenFiscalReceptor` int(5) NOT NULL,
  `uso_cfdi` varchar(4) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `reg` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `resena_producto`;
CREATE TABLE IF NOT EXISTS `resena_producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) NOT NULL,
  `idproducto` int(11) NOT NULL,
  `resena` text NOT NULL,
  `reg` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `f_facturas`;
CREATE TABLE IF NOT EXISTS `f_facturas` (
  `FacturasId` int(11) NOT NULL AUTO_INCREMENT,
  `Referencia` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Rfc` varchar(45) DEFAULT NULL,
  `Nombre` varchar(100) DEFAULT NULL,
  `Direccion` varchar(500) DEFAULT NULL,
  `Cp` varchar(45) DEFAULT NULL,
  `serie` varchar(2) NOT NULL,
  `Folio` int(11) DEFAULT NULL,
  `Estado` int(11) DEFAULT '2' COMMENT '0 cancelada 1 timbrada 2 error',
  `LeyendaAsignacion` varchar(550) DEFAULT NULL,
  `Lote` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Caducidad` varchar(45) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Paciente` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `LeyendaPagare` varchar(1000) DEFAULT NULL,
  `PaisReceptor` varchar(45) DEFAULT NULL,
  `TipoComprobante` varchar(45) DEFAULT NULL,
  `FormaPago` varchar(45) DEFAULT NULL,
  `MetodoPago` varchar(45) DEFAULT NULL,
  `CondicionesDePago` text,
  `rutaXml` varchar(100) DEFAULT NULL,
  `clienteId` int(11) UNSIGNED DEFAULT NULL,
  `usuario_id` int(10) UNSIGNED DEFAULT NULL,
  `creada_sesionId` int(10) NOT NULL,
  `cadenaoriginal` longtext NOT NULL,
  `sellocadena` longtext NOT NULL,
  `sellosat` longtext NOT NULL,
  `uuid` varchar(200) NOT NULL,
  `fechatimbre` datetime NOT NULL,
  `subtotal` decimal(12,2) NOT NULL,
  `iva` decimal(10,4) NOT NULL,
  `total` decimal(12,4) NOT NULL,
  `certificado` longtext NOT NULL,
  `nocertificado` longtext NOT NULL,
  `nocertificadosat` longtext NOT NULL,
  `tarjeta` varchar(4) NOT NULL,
  `fechacancelacion` datetime NOT NULL,
  `sellocancelacion` longtext NOT NULL,
  `ordenCompra` varchar(50) NOT NULL,
  `moneda` varchar(50) NOT NULL,
  `observaciones` text NOT NULL,
  `comprobado` tinyint(4) NOT NULL DEFAULT '0',
  `rutaAcuseCancelacion` varchar(200) DEFAULT NULL,
  `uso_cfdi` varchar(4) NOT NULL,
  `numproveedor` varchar(100) NOT NULL,
  `numordencompra` varchar(100) NOT NULL,
  `honorario` decimal(10,4) NOT NULL,
  `isr` decimal(10,4) NOT NULL,
  `ivaretenido` decimal(10,4) NOT NULL,
  `cedular` decimal(10,4) NOT NULL,
  `outsourcing` decimal(10,4) DEFAULT '0.0000',
  `5almillar` tinyint(1) NOT NULL DEFAULT '0',
  `cincoalmillarval` float NOT NULL DEFAULT '0',
  `facturaabierta` tinyint(1) NOT NULL DEFAULT '0',
  `f_relacion` tinyint(1) NOT NULL DEFAULT '0',
  `f_r_tipo` varchar(2) DEFAULT NULL,
  `f_r_uuid` varchar(60) DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '1',
  `favorito` tinyint(1) NOT NULL DEFAULT '0',
  `correoenviado` tinyint(1) NOT NULL DEFAULT '0',
  `estatus_correo` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`FacturasId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


10/08/2023
INSERT INTO `menu_sub` (`MenusubId`, `MenuId`, `Nombre`, `Pagina`, `Icon`, `orden`) VALUES (NULL, '6', 'Solicitud de reembolso', 'Solicitud_reembolso', 'fas fa-dollar-sign', '6');
INSERT INTO `menu_sub_modulo_detalles` (`id`, `MenusubId`, `idmenusubmodulo`, `orden`) VALUES (NULL, '65', '1', '10');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '1', '65');
INSERT INTO `perfiles_detalles` (`Perfil_detalleId`, `perfilId`, `MenusubId`) VALUES (NULL, '3', '65');